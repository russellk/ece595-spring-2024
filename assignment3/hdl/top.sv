/*
 *   Mentor Graphics Corporation
 *   
 *   Copyright 2003-2007 Mentor Graphics Corporation
 *   All Rights Reserved
 *   
 *   THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY
 *   INFORMATION WHICH IS THE PROPERTY OF MENTOR
 *   GRAPHICS CORPORATION OR ITS LICENSORS AND IS
 *   SUBJECT TO LICENSE TERMS.
 *   
 *   
 */

//---------------------------------------------------------------------------
//   Mentor Graphics, Corp.
//
//   (C) Copyright, Mentor Graphics, Corp. 2003-2004
//   All Rights Reserved
//   Licensed Materials - Property of Mentor Graphics, Corp.
//
//   No part of this file may be reproduced, stored in a retrieval system,
//   or transmitted in any form or by any means --- electronic, mechanical,
//   photocopying, recording, or otherwise --- without prior written permission
//   of Mentor Graphics, Corp.
//
//   WARRANTY:
//   Use all material in this file at your own risk.  Mentor Graphics, Corp.
//   makes no claims about any material contained in this file.
//---------------------------------------------------------------------------

`define BUFFER_SIZE 1000
`define BUFFER_BITS 8000

module top;

//
// Clock/Reset generation logic.
//

    reg clock, reset = 1;
    // tbx clkgen
    initial begin
      clock = 0;
      forever #5
        clock = ~clock;
    end

    // tbx clkgen
    initial begin
      reset = 1;
      #100 reset = 0;
    end
    
// 
// DUT instantiation
// - The DUT is nothing but pipeline of registers that is 1024 deep
//   and 8 bit wide.
//

    reg [31:0] bytes_received;
    reg [31:0] bytes_sent;

    reg [7:0] inbyte = 8'hff;
    wire [7:0] outbyte;
    pipe #(1024) dut (outbyte, inbyte, clock, reset);

    initial bytes_received = 32'h0;
    initial bytes_sent = 32'h0;

// 
// DPI Import 'getbuf' : This is used to get the byte stream from HVL.
//

    import "DPI-C" function void getbuf(output bit [`BUFFER_BITS-1:0] stream, 
                                        output int count, output bit eom);

//
// XRTL FSM that fetches the byte stream from HVL applies to the DUT
// one each clock. It stops once it receives eom.
//

    bit eom = 0;
    int remaining = 0;
    bit [`BUFFER_BITS-1:0] stream;
    always @(posedge clock) begin
      if(reset)
          inbyte <= 8'hff;
      else begin
          if(remaining==0 && eom==0) begin
            // $display("HDL getting some bytes ");
            getbuf(stream, remaining, eom);
          end
          if(remaining > 0) begin
            inbyte <= stream[7:0];
            stream = stream >> 8;
            remaining--;
            bytes_received = bytes_received + 1;
          end
      end
    end

//
// DPI import 'sendbuf' : 
// This is used to send the DUT output stream of bytes to HVL
//

    import "DPI-C" function void sendbuf (
        input bit [`BUFFER_BITS:0] buffer, 
        input int count);

    import "DPI-C" function void close_testbench();

//
// XRTL FSM that captures the output character stream and sends it
// to the HVL testbench in large packets.
// - packet size = `BUFFER_SIZE bytes, `BUFFER_BITS bits
// - bytes with the value 8'hff are ignored
// - byte with value '0' is taken as the end of stream
// - makes use of a utility function 'sendbyte' which packs
//   the bytes into a buffer of 40 bytes and sends it to HVL
//

    int loc = 0; 
    int ch;
    bit [`BUFFER_BITS-1:0] buffer = ~(`BUFFER_BITS'h0);
    function void sendbyte (input bit [7:0] b);
    begin
        buffer = buffer << 8;
        buffer [7:0] = b;
        loc=loc+1;
        bytes_sent=bytes_sent+1;
        if(loc==`BUFFER_SIZE || b==0) begin
            sendbuf(buffer, loc);
            buffer = ~(`BUFFER_BITS'h0);
            loc = 0;
        end
    end
    endfunction
    always @(posedge clock) 
        if(!reset) begin
            if(outbyte!=8'hff) begin
                // $display("HDL: sending character: %c ", outbyte);
                sendbyte(outbyte);
            end
            if(outbyte==0) begin
                $display("HDL: characters received: %d ", bytes_received);
                $display("HDL: characters sent: %d ", bytes_sent);
                close_testbench();
                $finish;
            end
        end
        
    
endmodule
