/*
 *   Mentor Graphics Corporation
 *   
 *   Copyright 2003-2007 Mentor Graphics Corporation
 *   All Rights Reserved
 *   
 *   THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY
 *   INFORMATION WHICH IS THE PROPERTY OF MENTOR
 *   GRAPHICS CORPORATION OR ITS LICENSORS AND IS
 *   SUBJECT TO LICENSE TERMS.
 *   
 *   
 */

//---------------------------------------------------------------------------
//   Mentor Graphics, Corp.
//
//   (C) Copyright, Mentor Graphics, Corp. 2003-2004
//   All Rights Reserved
//   Licensed Materials - Property of Mentor Graphics, Corp.
//
//   No part of this file may be reproduced, stored in a retrieval system,
//   or transmitted in any form or by any means --- electronic, mechanical,
//   photocopying, recording, or otherwise --- without prior written permission
//   of Mentor Graphics, Corp.
//
//   WARRANTY:
//   Use all material in this file at your own risk.  Mentor Graphics, Corp.
//   makes no claims about any material contained in this file.
//---------------------------------------------------------------------------

module pipe(outbyte, inbyte, clock, reset);
parameter DEPTH = 8;
output [7:0] outbyte;
input [7:0] inbyte;
input clock, reset;

//
// Generate segments:
// - Each segment adds one level of registers
// - Add as many segments as DEPTH
// - The pipeline consists of a first segment, a series of mid segments and
//   then a last segment.
//
genvar i;
generate begin : segments
  wire [7:0] tmp [0:DEPTH-2];
  for(i=0;i<DEPTH;i++) begin
      if (i==0)          segment first (tmp[i], inbyte, clock, reset);
      else if(i<DEPTH-1) segment mid   (tmp[i], tmp[i-1], clock, reset);
      else               segment last  (outbyte, tmp[i-1], clock, reset);
  end
end
endgenerate

endmodule

module segment(outbyte, inbyte, clock, reset);
output [7:0] outbyte;
reg [7:0] outbyte;
input [7:0] inbyte;
input clock, reset;

always @(posedge clock) begin
    if(reset) 
        outbyte <= 8'hff;
    else begin
        outbyte <= inbyte;
    end
end

endmodule
