/*
 *   Mentor Graphics Corporation
 *   
 *   Copyright 2003-2007 Mentor Graphics Corporation
 *   All Rights Reserved
 *   
 *   THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY
 *   INFORMATION WHICH IS THE PROPERTY OF MENTOR
 *   GRAPHICS CORPORATION OR ITS LICENSORS AND IS
 *   SUBJECT TO LICENSE TERMS.
 *   
 *   
 */

//---------------------------------------------------------------------------
//   Mentor Graphics, Corp.
//
//   (C) Copyright, Mentor Graphics, Corp. 2003-2004
//   All Rights Reserved
//   Licensed Materials - Property of Mentor Graphics, Corp.
//
//   No part of this file may be reproduced, stored in a retrieval system,
//   or transmitted in any form or by any means --- electronic, mechanical,
//   photocopying, recording, or otherwise --- without prior written permission
//   of Mentor Graphics, Corp.

//   Use all material in this file at your own risk.  Mentor Graphics, Corp.
//   makes no claims about any material contained in this file.
//---------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include "svdpi.h"
#include "tbxbindings.h"

#define SEND_MESSAGE_SIZE 40

static const bool chatty        = true;
static const bool really_chatty = false;

static  int total_bytes_sent      = 0;
static  int total_bytes_received  = 0;

//
// DPI import function 'getbuf':
// - used by HDL to fetch the input byte stream. 
// - The first call opens the message file 'msg'.
// - Subsequent calls read further into the file.
// - When the file ends, the argument 'eom' is 
//   set to indicate end of data.
//
FILE* in_msg = NULL;
void getbuf(svBitVecVal* buf, int* count, svBit* eom) {

  // char filename[]    = "test.txt"; 
  char filename[]    = "msg.txt"; 
  // char filename[]    = "Shakespeare.txt";

  // Open file "msg.txt" and start streaming in the bytes..
  if(!in_msg) {
      printf("HVL: Opening file \"%s\"..\n", filename);
      in_msg = fopen(filename, "r");
      if (in_msg == NULL) {
        fprintf(stderr, "Unable to open file %s for reading \n", filename);
        perror("testbench");
        exit(1);
      }
  }

  char b;
  int i = 0;
  while((b = fgetc(in_msg)) != EOF) {
    total_bytes_sent++;
    svPutPartselBit(buf, b, 8*i, 8);
    if (really_chatty) printf("%c",b);
    if(i==SEND_MESSAGE_SIZE-1) {
        *count = SEND_MESSAGE_SIZE;
        *eom = 0;
        if (chatty) printf("\nHVL: Sending %d bytes..\n", *count);
        return;
    }
    i++;
  }
  if (really_chatty) printf("End-of-file reached \n");
  // Send the remaining bytes with eom.
  svPutPartselBit(buf, 0, 8*i, 8);
  if (chatty) printf("HVL: Sending last %d bytes. \n", i+1);
  total_bytes_sent++;
  *count = i + 1;
  *eom = 1;

  return;
}

//
// DPI import function 'sendbuf':
// - used to send the DUT outout bytes back to HVL 
//   which are then displayed on screen
//
void sendbuf(const svBitVecVal* buffer, int count) {

// printf("\n>>>>> sendbuf got call: count=%3d: ", count);
  svBitVecVal b = 0; 
  for(int i=count;i>0;i--) {
    total_bytes_received++;
    svGetPartselBit(&b, buffer, (i-1)*8, 8);
    if(b==0) break;
    if (chatty) {
       printf("%c", b);
       if(b=='\n') printf("HVL:");
    }
  }
  if (chatty) if(b==0) printf("\nHVL: Complete message received.\n");
  fflush(stdout);
}

//
// DPI import function 'close_testbench':
//   used to cleanup, and report end of simualtion checks and statistics
//
void close_testbench()
{
   fclose(in_msg);

   printf("HVL: total bytes sent: %d \n", total_bytes_sent);
   printf("HVL: total bytes received: %d \n", total_bytes_received);
}  
