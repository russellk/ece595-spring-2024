module traffic_control 
  #( parameter
      lights = 2
  ) (
     input logic clock,
     input logic reset,

     input logic [lights-1:0] waiting,
     input logic [lights-1:0] in_use,
     output logic [lights-1:0] clear
  );

  function logic [lights-1:0] top_bit(input logic [lights-1:0] requests);
  begin
    logic [lights-1:0] ret;
    int i;
    ret = 0;
    for (i=0; i<lights; i++) begin
       if (requests[i]) begin
          ret[i] = 1;
          break;
       end
    end
    top_bit = ret;
  end
  endfunction

  logic [7:0] rotate = 0;

  always @(posedge clock) begin
    if (reset) begin
      rotate = 0;
    end else begin
      if (!in_use) clear <= (waiting) ? top_bit(waiting) : 0;
      else         clear <= 0;
    end
  end

endmodule
