module traffic_generator
  #(  parameter 
      probability =   10,
      of          = 1000
  ) (
      input  logic clock,
      input  logic reset,

      output logic car
  );

  always @(posedge clock) begin
    if (reset) begin
      car <= 0;
    end else begin
      if (probability > $urandom % of) car <= 1;
      else car <= 0;
    end
  end
endmodule
