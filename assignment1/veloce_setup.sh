#==============================
# Veloce environment setup
#==============================

export MENTOR_HOME=/opt/mentor

export VMW_HOME=$MENTOR_HOME/Veloce_v19.0.2
export VMW_DOC=$VMW_HOME/doc/pdfdocs
export MED_EMULATOR=$MENTOR_HOME/EMULATORS
export MED_COMMON_LLSW_FILES=$VMW_HOME/TnD
export TBX_HOME=$VMW_HOME/tbx
export VMW_RSH=/usr/bin/ssh

# VELOCE_HOME is needed for newer examples/libraries

export VELOCE_HOME=$VMW_HOME

# update PATH and LD_LIBRARY_PATHS

export PATH=$VMW_HOME/bin:$TBX_HOME/bin:$PATH
export LD_LIBRARY_PATH=$VMW_HOME/lib/i386.linux.gcc433:$TBX_HOME/lib/linux_el30_gnu445:$LD_LIBRARY_PATH
# note to self: check gcc consistency between questa and line above

#(default is 100, excluding syntax errors)
#export VMW_MAX_ERROR_COUNT=5000

# Mentor (Siemens EDA, now) licensing

export LM_LICENSE_FILE='1717@license.cecs.pdx.edu'

export PATH=$PATH:/usr/sbin


# Setup alias values for Veloce documentation

alias bookcase='(cd $VMW_DOC; evince MASTER_bookcase.pdf)&'
alias veloce_user_guide='evince $VMW_DOC/veloce_ug.pdf &'
alias veloce_ref='evince $VMW_DOC/veloce_ref.pdf &'

#==============================
# Questa environment setup
#==============================

# must use a Questa version compatible with Veloce software

export MTI_VCO_MODE=64
#export QUESTA_VERSION=2021.3_1
export QUESTA_VERSION=10.6b
export GCC_VERSION=7.4.0

export QUESTA_HOME=/pkgs/mentor/questa/$QUESTA_VERSION/questasim
export MGC_HOME=$QUESTA_HOME
export GCC_HOME=$QUESTA_HOME/gcc-$GCC_VERSION-linux_x86_64
export QUESTA_MVC_HOME=/pkgs/mentor/questa_cdc_formal/10.7b
export QUESTA_LIBS_ARCH=_x86_64
export PATH=$QUESTA_MVC_HOME/bin:$QUESTA_MVC_HOME/linux_x86_64/bin:$PATH
export HOME_0IN=$QUESTA_MVC_HOME/linux_x86_64/
export MGLS_LICENSE_FILE=1717@license.cecs.pdx.edu

#export HOME_0IN=/pkgs/mentor/questa_cdc_formal/10.4b/linux_x86_64
# update PATH and LD_LIBRARY_PATH

export PATH=$QUESTA_HOME/bin:$GCC_HOME/bin:$PATH
export LD_LIBRARY_PATH=$QUESTA_MVC_HOME/questa_mvc_core/linux_x86_64_gcc-4.7.4:$LD_LIBRARY_PATH

# if you don't see this message, you didn't execute the full setup, something went wrong

echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "@@@ Environment setup for Emulation @@@"
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"


