
module testbench ();

  logic clk;

  // tbx clkgen inactive_negedge
  initial begin
     clk = 0;
     forever #2 
        clk = ~clk;
  end

  logic rstn;

  // tbx clkgen
  initial begin
     rstn = 0;
     #20 rstn = 1;
  end

  logic [31:0] image;
  logic        image_valid = 1;
  logic        image_ready;

  logic [31:0] prediction;
  logic        prediction_ready;
  logic        prediction_valid;

  bit   [31:0] feature;

  // drive inputs to the mnist instance

  import "DPI-C" function void get_feature(output bit [31:0] feature);

  import "DPI-C" function void get_num_tests(output int num_tests);

  import "DPI-C" function void finish_up();

  int test, row, col;
  int num_tests;
  int tests_run;
  int count;

  assign image = feature;

  initial begin 
     @(posedge clk);
     get_feature(feature);
     get_num_tests(num_tests);

     count <= 0;
     tests_run <= 0;
     image_valid <= 0;

     while (rstn == 0) @(posedge clk);  // wait for reset

     while (tests_run < num_tests) begin
        image_valid <= 1;
        @(posedge clk);
        while (count<28*28-1) begin
           if (image_ready) begin
              get_feature(feature);
              count <= count + 1;
           end
           @(posedge clk);
        end
        image_valid <= 0;
        count <= 0;
        if (tests_run < num_tests - 1) get_feature(feature);
        while (prediction_valid == 0) @(posedge clk); 
        while (prediction_valid == 1) @(posedge clk);  // wait for the prediction to complete
        tests_run <= tests_run + 1;
        @(posedge clk);
     end

     finish_up();
     $finish;

  end

//  always @(posedge clk)
//    if (image_ready & image_valid) $display("image data: ", image);

  // record outputs from mnist instance

  int p_count;
  int tests_done;

  import "DPI-C" function void send_prediction(
     input bit [31:0] prediction,
     input int count
  );

  assign prediction_ready = 1;

  always @(posedge clk)
  if (rstn == 0) begin
      p_count <= 0;
      tests_done <= 0;
  end else begin
     if (prediction_valid) begin
        send_prediction(prediction, p_count);
        p_count <= (p_count >= 9) ? 0 : p_count + 1;
        if (p_count == 9) tests_done <= tests_done + 1;
     end
  end
/*
  import "DPI-C" function void finish_up();

  always @(posedge clk) begin
    if (tests_done == num_tests) begin
      finish_up();
      @(posedge clk);
      $finish;
    end
  end      
*/
  mnist dut(
     .clk              (clk),
     .rstn             (rstn),

     .image            (image),
     .image_valid      (image_valid),
     .image_ready      (image_ready),

     .prediction       (prediction),
     .prediction_valid (prediction_valid),
     .prediction_ready (prediction_ready)
  );

endmodule
