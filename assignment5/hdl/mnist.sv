

module mnist(

    input           clk,
    input           rstn,

    input   [31:0]  image,
    input           image_valid,
    output          image_ready,

    output  [31:0]  prediction,
    output          prediction_valid,
    input           prediction_ready);

  // layer one local signals
  
  logic [13:0]           layer1_weight_address;
  logic                  layer1_weight_output_enable;
  logic [31:0]           layer1_weights;

  logic [ 9:0]           layer1_bias_address;
  logic                  layer1_bias_output_enable;
  logic [31:0]           layer1_bias_value;

  logic [31:0]           layer1_out;
  logic                  layer1_out_valid;
  logic                  layer1_out_ready;                 

  catapult_conv2d_28_28_5_5_1 large_convolution (
    .clk                             (clk),
    .rstn                            (rstn),

    .image_channel_rsc_dat           (image),
    .image_channel_rsc_vld           (image_valid),
    .image_channel_rsc_rdy           (image_ready),
    
    .weight_memory_rsc_radr          (layer1_weight_address),
    .weight_memory_rsc_re            (layer1_weight_output_enable),
    .weight_memory_rsc_q             (layer1_weights),
    .weight_memory_triosy_lz         (),

    .bias_memory_rsc_radr            (layer1_bias_address),
    .bias_memory_rsc_re              (layer1_bias_output_enable),
    .bias_memory_rsc_q               (layer1_bias_value),
    .bias_memory_triosy_lz           (),

    .output_image_channel_rsc_dat    (layer1_out),
    .output_image_channel_rsc_vld    (layer1_out_valid),
    .output_image_channel_rsc_rdy    (layer1_out_ready),

    .bias_rsc_dat                    (1'b1),
    .bias_triosy_lz                  (),

    .relu_rsc_dat                    (1'b1),
    .relu_triosy_lz                  (),

    .max_pool_rsc_dat                (1'b1),
    .max_pool_triosy_lz              (),
  
    .num_input_images_rsc_dat        (28'h0000001),
    .num_input_images_triosy_lz      (),

    .num_output_images_rsc_dat       (28'h0000014),
    .num_output_images_triosy_lz     ()
  );

  rom #(.address_width(14), .data_width(32), .datafile("weights/conv2d_weights.hex")) layer1_weight_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer1_weight_address),
    .output_enable                   (layer1_weight_output_enable),
    .data_out                        (layer1_weights)
  );

  rom #(.address_width(10), .data_width(32), .datafile("weights/conv2d_biases.hex")) layer1_bias_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer1_bias_address),
    .output_enable                   (layer1_bias_output_enable),
    .data_out                        (layer1_bias_value)
  );

  // layer two local signals
  
  logic [13:0]           layer2_weight_address;
  logic                  layer2_weight_output_enable;
  logic [31:0]           layer2_weights;

  logic [ 9:0]           layer2_bias_address;
  logic                  layer2_bias_output_enable;
  logic [31:0]           layer2_bias_value;

  logic [31:0]           layer2_out;
  logic                  layer2_out_valid;
  logic                  layer2_out_ready;                 

  catapult_conv2d_14_14_3_3_1 small_convolution (
    .clk                             (clk),
    .rstn                            (rstn),

    .image_channel_rsc_dat           (layer1_out),
    .image_channel_rsc_vld           (layer1_out_valid),
    .image_channel_rsc_rdy           (layer1_out_ready),
    
    .weight_memory_rsc_radr          (layer2_weight_address),
    .weight_memory_rsc_re            (layer2_weight_output_enable),
    .weight_memory_rsc_q             (layer2_weights),
    .weight_memory_triosy_lz         (),

    .bias_memory_rsc_radr            (layer2_bias_address),
    .bias_memory_rsc_re              (layer2_bias_output_enable),
    .bias_memory_rsc_q               (layer2_bias_value),
    .bias_memory_triosy_lz           (),

    .output_image_channel_rsc_dat    (layer2_out),
    .output_image_channel_rsc_vld    (layer2_out_valid),
    .output_image_channel_rsc_rdy    (layer2_out_ready),

    .bias_rsc_dat                    (1'b1),
    .bias_triosy_lz                  (),

    .relu_rsc_dat                    (1'b1),
    .relu_triosy_lz                  (),

    .max_pool_rsc_dat                (1'b1),
    .max_pool_triosy_lz              (),
  
    .num_input_images_rsc_dat        (28'h0000014),
    .num_input_images_triosy_lz       (),

    .num_output_images_rsc_dat       (28'h0000032),
    .num_output_images_triosy_lz     ()
  );

  rom #(.address_width(14), .data_width(32), .datafile("weights/conv2d_1_weights.hex")) layer2_weight_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer2_weight_address),
    .output_enable                   (layer2_weight_output_enable),
    .data_out                        (layer2_weights)
  );

  rom #(.address_width(10), .data_width(32), .datafile("weights/conv2d_1_biases.hex")) layer2_bias_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer2_bias_address),
    .output_enable                   (layer2_bias_output_enable),
    .data_out                        (layer2_bias_value)
  );

  // layer three local signals
  
  logic [21:0]           layer3_weight_even_address;
  logic                  layer3_weight_even_output_enable;
  logic [31:0]           layer3_even_weights;

  logic [21:0]           layer3_weight_odd_address;
  logic                  layer3_weight_odd_output_enable;
  logic [31:0]           layer3_odd_weights;

  logic [10:0]           layer3_bias_address;
  logic                  layer3_bias_output_enable;
  logic [31:0]           layer3_bias_value;

  logic [31:0]           layer3_out;
  logic                  layer3_out_valid;
  logic                  layer3_out_ready;                 

  logic [31:0]           layer3_out_fifo;
  logic                  layer3_out_valid_fifo;
  logic                  layer3_out_ready_fifo;                 

`ifdef TRACE
  catapult_dense_16000_2500_1 first_dense_layer (
`else
  catapult_dense_16000_2500 first_dense_layer (
`endif
    .clk                             (clk),
    .rstn                            (rstn),
`ifdef DEBUG
    .debug_channel_rsc_dat           (),
    .debug_channel_rsc_vld           (),
    .debug_channel_rsc_rdy           (1'b1),
`endif
    .image_channel_rsc_dat           (layer2_out),
    .image_channel_rsc_vld           (layer2_out_valid),
    .image_channel_rsc_rdy           (layer2_out_ready),
    
    .weight_memory_data_rsc_0_0_radr     (layer3_weight_even_address),
    .weight_memory_data_rsc_0_0_re       (layer3_weight_even_output_enable),
    .weight_memory_data_rsc_0_0_q        (layer3_even_weights),
    .weight_memory_data_tri_0_0_osy_lz   (),

    .weight_memory_data_rsc_0_1_radr     (layer3_weight_odd_address),
    .weight_memory_data_rsc_0_1_re       (layer3_weight_odd_output_enable),
    .weight_memory_data_rsc_0_1_q        (layer3_odd_weights),
    .weight_memory_data_tri_0_1_osy_lz   (),

    .bias_memory_rsc_radr            (layer3_bias_address),
    .bias_memory_rsc_re              (layer3_bias_output_enable),
    .bias_memory_rsc_q               (layer3_bias_value),
    .bias_memory_triosy_lz           (),

    .output_image_channel_rsc_dat    (layer3_out),
    .output_image_channel_rsc_vld    (layer3_out_valid),
    .output_image_channel_rsc_rdy    (layer3_out_ready),

    .bias_rsc_dat                    (1'b1),
    .bias_triosy_lz                  (),

    .relu_rsc_dat                    (1'b1),
    .relu_triosy_lz                  (),

    .input_vector_length_rsc_dat     (28'h0000992),
    .input_vector_length_triosy_lz   (),

    .output_image_elements_rsc_dat   (28'h00001F4),
    .output_image_elements_triosy_lz ()
  );
/*
  fifo #(.width(32), .depth(10)) layer3_fifo(
        CLK                          (clk),
        RESET_N                      (rstn),
        DATA_STROBE                  (layer3_out_valid),
        DATA_IN                      (layer3_out),
        DATA_READY                   (layer3_out_valid_fifo),
        DATA_OUT                     (layer3_out_fifo),
        DATA_ACK                     (layer3_out_ready_fifo),
        NOT_FULL                     (layer3_out_ready)
  );
*/
  rom #(.address_width(22), .data_width(32), .datafile("weights/dense_weights_bank0.hex")) layer3_even_weight_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer3_weight_even_address),
    .output_enable                   (layer3_weight_even_output_enable),
    .data_out                        (layer3_even_weights)
  );

  rom #(.address_width(22), .data_width(32), .datafile("weights/dense_weights_bank1.hex")) layer3_odd_weight_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer3_weight_odd_address),
    .output_enable                   (layer3_weight_odd_output_enable),
    .data_out                        (layer3_odd_weights)
  );

  rom #(.address_width(11), .data_width(32), .datafile("weights/dense_biases.hex")) layer3_bias_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer3_bias_address),
    .output_enable                   (layer3_bias_output_enable),
    .data_out                        (layer3_bias_value)
  );

  // layer four local signals
  
  logic [21:0]           layer4_weight_even_address;
  logic                  layer4_weight_even_output_enable;
  logic [31:0]           layer4_even_weights;

  logic [21:0]           layer4_weight_odd_address;
  logic                  layer4_weight_odd_output_enable;
  logic [31:0]           layer4_odd_weights;

  logic [10:0]           layer4_bias_address;
  logic                  layer4_bias_output_enable;
  logic [31:0]           layer4_bias_value;

  logic [31:0]           layer4_out;
  logic                  layer4_out_valid;
  logic                  layer4_out_ready;                 

`ifdef TRACE
  catapult_dense_16000_2500_1 second_dense_layer (
`else
  catapult_dense_16000_2500 second_dense_layer (
`endif
    .clk                             (clk),
    .rstn                            (rstn),

`ifdef DEBUG
    .debug_channel_rsc_dat           (),
    .debug_channel_rsc_vld           (),
    .debug_channel_rsc_rdy           (1'b1),
`endif
    .image_channel_rsc_dat           (layer3_out),
    .image_channel_rsc_vld           (layer3_out_valid),
    .image_channel_rsc_rdy           (layer3_out_ready),
    
    .weight_memory_data_rsc_0_0_radr     (layer4_weight_even_address),
    .weight_memory_data_rsc_0_0_re       (layer4_weight_even_output_enable),
    .weight_memory_data_rsc_0_0_q        (layer4_even_weights),
    .weight_memory_data_tri_0_0_osy_lz   (),

    .weight_memory_data_rsc_0_1_radr     (layer4_weight_odd_address),
    .weight_memory_data_rsc_0_1_re       (layer4_weight_odd_output_enable),
    .weight_memory_data_rsc_0_1_q        (layer4_odd_weights),
    .weight_memory_data_tri_0_1_osy_lz   (),

    .bias_memory_rsc_radr            (layer4_bias_address),
    .bias_memory_rsc_re              (layer4_bias_output_enable),
    .bias_memory_rsc_q               (layer4_bias_value),
    .bias_memory_triosy_lz           (),

    .output_image_channel_rsc_dat    (layer4_out),
    .output_image_channel_rsc_vld    (layer4_out_valid),
    .output_image_channel_rsc_rdy    (layer4_out_ready),

    .bias_rsc_dat                    (1'b1),
    .bias_triosy_lz                  (),

    .relu_rsc_dat                    (1'b1),
    .relu_triosy_lz                  (),

    .input_vector_length_rsc_dat     (28'h00001F4),
    .input_vector_length_triosy_lz   (),

    .output_image_elements_rsc_dat   (28'h000000A),
    .output_image_elements_triosy_lz ()
  );

  rom #(.address_width(22), .data_width(32), .datafile("weights/dense_1_weights_bank0.hex")) layer4_even_weight_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer4_weight_even_address),
    .output_enable                   (layer4_weight_even_output_enable),
    .data_out                        (layer4_even_weights)
  );

  rom #(.address_width(22), .data_width(32), .datafile("weights/dense_1_weights_bank1.hex")) layer4_odd_weight_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer4_weight_odd_address),
    .output_enable                   (layer4_weight_odd_output_enable),
    .data_out                        (layer4_odd_weights)
  );

  rom #(.address_width(11), .data_width(32), .datafile("weights/dense_1_biases.hex")) layer4_bias_mem (
    .clk                             (clk),
    .rstn                            (rstn),

    .read_address                    (layer4_bias_address),
    .output_enable                   (layer4_bias_output_enable),
    .data_out                        (layer4_bias_value)
  );

  // layer four local signals

  catapult_softmax_10_1 softmax (
    .clk                             (clk),
    .rstn                            (rstn),

    .input_channel_rsc_dat           (layer4_out),
    .input_channel_rsc_vld           (layer4_out_valid),
    .input_channel_rsc_rdy           (layer4_out_ready),
    
    .output_channel_rsc_dat          (prediction),
    .output_channel_rsc_vld          (prediction_valid),
    .output_channel_rsc_rdy          (prediction_ready),

    .count_rsc_dat                   (28'h000000A),
    .count_triosy_lz                 ()
  );


endmodule
