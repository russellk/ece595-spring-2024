
//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_in_wait_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module ccs_in_wait_v1 (idat, rdy, ivld, dat, irdy, vld);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] idat;
  output             rdy;
  output             ivld;
  input  [width-1:0] dat;
  input              irdy;
  input              vld;

  wire   [width-1:0] idat;
  wire               rdy;
  wire               ivld;

  localparam stallOff = 0; 
  wire                  stall_ctrl;
  assign stall_ctrl = stallOff;

  assign idat = dat;
  assign rdy = irdy && !stall_ctrl;
  assign ivld = vld && !stall_ctrl;

endmodule


//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_out_wait_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module ccs_out_wait_v1 (dat, irdy, vld, idat, rdy, ivld);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] dat;
  output             irdy;
  output             vld;
  input  [width-1:0] idat;
  input              rdy;
  input              ivld;

  wire   [width-1:0] dat;
  wire               irdy;
  wire               vld;

  localparam stallOff = 0; 
  wire stall_ctrl;
  assign stall_ctrl = stallOff;

  assign dat = idat;
  assign irdy = rdy && !stall_ctrl;
  assign vld = ivld && !stall_ctrl;

endmodule



//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/mgc_io_sync_v2.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module mgc_io_sync_v2 (ld, lz);
    parameter valid = 0;

    input  ld;
    output lz;

    wire   lz;

    assign lz = ld;

endmodule


//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_in_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module ccs_in_v1 (idat, dat);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] idat;
  input  [width-1:0] dat;

  wire   [width-1:0] idat;

  assign idat = dat;

endmodule


//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_stallbuf_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2015 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------

module ccs_stallbuf_v1 (stalldrv);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output   stalldrv;
  localparam stall_off = 0; 
   
  assign stalldrv = stall_off;

endmodule




//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_ram_sync_1R1W.v 
module ccs_ram_sync_1R1W
#(
parameter data_width = 8,
parameter addr_width = 7,
parameter depth = 128
)(
	radr, wadr, d, we, re, clk, q
);

	input [addr_width-1:0] radr;
	input [addr_width-1:0] wadr;
	input [data_width-1:0] d;
	input we;
	input re;
	input clk;
	output[data_width-1:0] q;

   // synopsys translate_off
	reg [data_width-1:0] q;

	reg [data_width-1:0] mem [depth-1:0];
		
	always @(posedge clk) begin
		if (we) begin
			mem[wadr] <= d; // Write port
		end
		if (re) begin
			q <= mem[radr] ; // read port
		end
	end
   // synopsys translate_on

endmodule

//------> ./rtl.v 
// ----------------------------------------------------------------------
//  HLS HDL:        Verilog Netlister
//  HLS Version:    2024.1/1091966 Production Release
//  HLS Date:       Wed Feb 14 09:07:18 PST 2024
// 
//  Generated by:   russk@orw-russk-vm
//  Generated date: Tue Apr 16 16:37:03 2024
// ----------------------------------------------------------------------

// 
// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_17_32_10_786_786_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_17_32_10_786_786_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [9:0] wadr;
  input [31:0] q;
  output re;
  output [9:0] radr;
  input [9:0] radr_d;
  input [9:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_16_32_10_786_786_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_16_32_10_786_786_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [9:0] wadr;
  input [31:0] q;
  output re;
  output [9:0] radr;
  input [9:0] radr_d;
  input [9:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_14_32_10_786_786_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_14_32_10_786_786_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [9:0] wadr;
  input [31:0] q;
  output re;
  output [9:0] radr;
  input [9:0] radr_d;
  input [9:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_13_32_14_15720_15720_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_13_32_14_15720_15720_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [13:0] wadr;
  input [31:0] q;
  output re;
  output [13:0] radr;
  input [13:0] radr_d;
  input [13:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_3_32_10_1024_1024_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_3_32_10_1024_1024_32_5_gen
    (
  q, re, radr, radr_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d
);
  input [31:0] q;
  output re;
  output [9:0] radr;
  input [9:0] radr_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_2_32_14_16384_16384_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_2_32_14_16384_16384_32_5_gen
    (
  q, re, radr, radr_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d
);
  input [31:0] q;
  output re;
  output [13:0] radr;
  input [13:0] radr_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_run_fsm
//  FSM Module
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_run_fsm (
  clk, rstn, run_wen, fsm_output, main_C_0_tr0, main_loop_C_0_tr0, load_images_loop_C_1_tr0,
      main_C_1_tr0, main_loop_1_C_0_tr0, main_loop_2_C_0_tr0, catapult_conv2d_28_28_5_5_1_convolve_do_C_0_tr0,
      inner_loop_C_1_tr0, main_loop_3_C_0_tr0, main_loop_3_C_0_tr1, catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4_tr0,
      stride_loop_C_2_tr0, copy_loop_C_0_tr0, main_loop_5_C_0_tr0, main_loop_1_C_4_tr0
);
  input clk;
  input rstn;
  input run_wen;
  output [25:0] fsm_output;
  reg [25:0] fsm_output;
  input main_C_0_tr0;
  input main_loop_C_0_tr0;
  input load_images_loop_C_1_tr0;
  input main_C_1_tr0;
  input main_loop_1_C_0_tr0;
  input main_loop_2_C_0_tr0;
  input catapult_conv2d_28_28_5_5_1_convolve_do_C_0_tr0;
  input inner_loop_C_1_tr0;
  input main_loop_3_C_0_tr0;
  input main_loop_3_C_0_tr1;
  input catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4_tr0;
  input stride_loop_C_2_tr0;
  input copy_loop_C_0_tr0;
  input main_loop_5_C_0_tr0;
  input main_loop_1_C_4_tr0;


  // FSM State Type Declaration for catapult_conv2d_28_28_5_5_1_run_run_fsm_1
  parameter
    main_C_0 = 5'd0,
    load_images_loop_C_0 = 5'd1,
    main_loop_C_0 = 5'd2,
    load_images_loop_C_1 = 5'd3,
    main_C_1 = 5'd4,
    main_loop_1_C_0 = 5'd5,
    main_loop_2_C_0 = 5'd6,
    inner_loop_C_0 = 5'd7,
    catapult_conv2d_28_28_5_5_1_convolve_do_C_0 = 5'd8,
    inner_loop_C_1 = 5'd9,
    main_loop_1_C_1 = 5'd10,
    main_loop_1_C_2 = 5'd11,
    main_loop_3_C_0 = 5'd12,
    catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_0 = 5'd13,
    catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_1 = 5'd14,
    catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_2 = 5'd15,
    catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_3 = 5'd16,
    catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4 = 5'd17,
    stride_loop_C_0 = 5'd18,
    stride_loop_C_1 = 5'd19,
    stride_loop_C_2 = 5'd20,
    copy_loop_C_0 = 5'd21,
    main_loop_1_C_3 = 5'd22,
    main_loop_5_C_0 = 5'd23,
    main_loop_1_C_4 = 5'd24,
    main_C_2 = 5'd25;

  reg [4:0] state_var;
  reg [4:0] state_var_NS;


  // Interconnect Declarations for Component Instantiations 
  always @(*)
  begin : catapult_conv2d_28_28_5_5_1_run_run_fsm_1
    case (state_var)
      load_images_loop_C_0 : begin
        fsm_output = 26'b00000000000000000000000010;
        state_var_NS = main_loop_C_0;
      end
      main_loop_C_0 : begin
        fsm_output = 26'b00000000000000000000000100;
        if ( main_loop_C_0_tr0 ) begin
          state_var_NS = load_images_loop_C_1;
        end
        else begin
          state_var_NS = main_loop_C_0;
        end
      end
      load_images_loop_C_1 : begin
        fsm_output = 26'b00000000000000000000001000;
        if ( load_images_loop_C_1_tr0 ) begin
          state_var_NS = main_C_1;
        end
        else begin
          state_var_NS = load_images_loop_C_0;
        end
      end
      main_C_1 : begin
        fsm_output = 26'b00000000000000000000010000;
        if ( main_C_1_tr0 ) begin
          state_var_NS = main_C_2;
        end
        else begin
          state_var_NS = main_loop_1_C_0;
        end
      end
      main_loop_1_C_0 : begin
        fsm_output = 26'b00000000000000000000100000;
        if ( main_loop_1_C_0_tr0 ) begin
          state_var_NS = main_loop_1_C_1;
        end
        else begin
          state_var_NS = main_loop_2_C_0;
        end
      end
      main_loop_2_C_0 : begin
        fsm_output = 26'b00000000000000000001000000;
        if ( main_loop_2_C_0_tr0 ) begin
          state_var_NS = inner_loop_C_0;
        end
        else begin
          state_var_NS = main_loop_2_C_0;
        end
      end
      inner_loop_C_0 : begin
        fsm_output = 26'b00000000000000000010000000;
        state_var_NS = catapult_conv2d_28_28_5_5_1_convolve_do_C_0;
      end
      catapult_conv2d_28_28_5_5_1_convolve_do_C_0 : begin
        fsm_output = 26'b00000000000000000100000000;
        if ( catapult_conv2d_28_28_5_5_1_convolve_do_C_0_tr0 ) begin
          state_var_NS = inner_loop_C_1;
        end
        else begin
          state_var_NS = catapult_conv2d_28_28_5_5_1_convolve_do_C_0;
        end
      end
      inner_loop_C_1 : begin
        fsm_output = 26'b00000000000000001000000000;
        if ( inner_loop_C_1_tr0 ) begin
          state_var_NS = main_loop_1_C_1;
        end
        else begin
          state_var_NS = main_loop_2_C_0;
        end
      end
      main_loop_1_C_1 : begin
        fsm_output = 26'b00000000000000010000000000;
        state_var_NS = main_loop_1_C_2;
      end
      main_loop_1_C_2 : begin
        fsm_output = 26'b00000000000000100000000000;
        state_var_NS = main_loop_3_C_0;
      end
      main_loop_3_C_0 : begin
        fsm_output = 26'b00000000000001000000000000;
        if ( main_loop_3_C_0_tr0 ) begin
          state_var_NS = catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_0;
        end
        else if ( main_loop_3_C_0_tr1 ) begin
          state_var_NS = stride_loop_C_0;
        end
        else begin
          state_var_NS = main_loop_3_C_0;
        end
      end
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_0 : begin
        fsm_output = 26'b00000000000010000000000000;
        state_var_NS = catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_1;
      end
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_1 : begin
        fsm_output = 26'b00000000000100000000000000;
        state_var_NS = catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_2;
      end
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_2 : begin
        fsm_output = 26'b00000000001000000000000000;
        state_var_NS = catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_3;
      end
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_3 : begin
        fsm_output = 26'b00000000010000000000000000;
        state_var_NS = catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4;
      end
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4 : begin
        fsm_output = 26'b00000000100000000000000000;
        if ( catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4_tr0 ) begin
          state_var_NS = main_loop_1_C_3;
        end
        else begin
          state_var_NS = catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_0;
        end
      end
      stride_loop_C_0 : begin
        fsm_output = 26'b00000001000000000000000000;
        state_var_NS = stride_loop_C_1;
      end
      stride_loop_C_1 : begin
        fsm_output = 26'b00000010000000000000000000;
        state_var_NS = stride_loop_C_2;
      end
      stride_loop_C_2 : begin
        fsm_output = 26'b00000100000000000000000000;
        if ( stride_loop_C_2_tr0 ) begin
          state_var_NS = copy_loop_C_0;
        end
        else begin
          state_var_NS = stride_loop_C_0;
        end
      end
      copy_loop_C_0 : begin
        fsm_output = 26'b00001000000000000000000000;
        if ( copy_loop_C_0_tr0 ) begin
          state_var_NS = main_loop_1_C_3;
        end
        else begin
          state_var_NS = stride_loop_C_0;
        end
      end
      main_loop_1_C_3 : begin
        fsm_output = 26'b00010000000000000000000000;
        state_var_NS = main_loop_5_C_0;
      end
      main_loop_5_C_0 : begin
        fsm_output = 26'b00100000000000000000000000;
        if ( main_loop_5_C_0_tr0 ) begin
          state_var_NS = main_loop_1_C_4;
        end
        else begin
          state_var_NS = main_loop_5_C_0;
        end
      end
      main_loop_1_C_4 : begin
        fsm_output = 26'b01000000000000000000000000;
        if ( main_loop_1_C_4_tr0 ) begin
          state_var_NS = main_C_2;
        end
        else begin
          state_var_NS = main_loop_1_C_0;
        end
      end
      main_C_2 : begin
        fsm_output = 26'b10000000000000000000000000;
        state_var_NS = main_C_0;
      end
      // main_C_0
      default : begin
        fsm_output = 26'b00000000000000000000000001;
        if ( main_C_0_tr0 ) begin
          state_var_NS = main_C_1;
        end
        else begin
          state_var_NS = load_images_loop_C_0;
        end
      end
    endcase
  end

  always @(posedge clk) begin
    if ( ~ rstn ) begin
      state_var <= main_C_0;
    end
    else if ( run_wen ) begin
      state_var <= state_var_NS;
    end
  end

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_staller
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_staller (
  clk, rstn, catapult_conv2d_28_28_5_5_1_stall, run_wen, run_wten, image_channel_rsci_wen_comp,
      output_image_channel_rsci_wen_comp
);
  input clk;
  input rstn;
  input catapult_conv2d_28_28_5_5_1_stall;
  output run_wen;
  output run_wten;
  input image_channel_rsci_wen_comp;
  input output_image_channel_rsci_wen_comp;


  // Interconnect Declarations
  reg run_wten_reg;


  // Interconnect Declarations for Component Instantiations 
  assign run_wen = image_channel_rsci_wen_comp & output_image_channel_rsci_wen_comp
      & (~ catapult_conv2d_28_28_5_5_1_stall);
  assign run_wten = run_wten_reg;
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      run_wten_reg <= 1'b0;
    end
    else begin
      run_wten_reg <= ~ run_wen;
    end
  end
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_wait_dp (
  clk, rstn, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z, run_wen, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z_oreg,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z_oreg, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z_oreg
);
  input clk;
  input rstn;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z;
  input run_wen;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z_oreg;
  output [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z_oreg;
  reg [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z_oreg;



  // Interconnect Declarations for Component Instantiations 
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
    end
    else if ( run_wen ) begin
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z_oreg <= outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z;
    end
  end
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_num_output_images_triosy_obj_num_output_images_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_num_output_images_triosy_obj_num_output_images_triosy_wait_ctrl
    (
  run_wten, num_output_images_triosy_obj_iswt0, num_output_images_triosy_obj_biwt
);
  input run_wten;
  input num_output_images_triosy_obj_iswt0;
  output num_output_images_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign num_output_images_triosy_obj_biwt = (~ run_wten) & num_output_images_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_num_input_images_triosy_obj_num_input_images_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_num_input_images_triosy_obj_num_input_images_triosy_wait_ctrl
    (
  run_wten, num_input_images_triosy_obj_iswt0, num_input_images_triosy_obj_biwt
);
  input run_wten;
  input num_input_images_triosy_obj_iswt0;
  output num_input_images_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign num_input_images_triosy_obj_biwt = (~ run_wten) & num_input_images_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_max_pool_triosy_obj_max_pool_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_max_pool_triosy_obj_max_pool_triosy_wait_ctrl
    (
  run_wten, max_pool_triosy_obj_iswt0, max_pool_triosy_obj_biwt
);
  input run_wten;
  input max_pool_triosy_obj_iswt0;
  output max_pool_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign max_pool_triosy_obj_biwt = (~ run_wten) & max_pool_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_relu_triosy_obj_relu_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_relu_triosy_obj_relu_triosy_wait_ctrl (
  run_wten, relu_triosy_obj_iswt0, relu_triosy_obj_biwt
);
  input run_wten;
  input relu_triosy_obj_iswt0;
  output relu_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign relu_triosy_obj_biwt = (~ run_wten) & relu_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_bias_triosy_obj_bias_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_bias_triosy_obj_bias_triosy_wait_ctrl (
  run_wten, bias_triosy_obj_iswt0, bias_triosy_obj_biwt
);
  input run_wten;
  input bias_triosy_obj_iswt0;
  output bias_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign bias_triosy_obj_biwt = (~ run_wten) & bias_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl
    (
  run_wten, bias_memory_triosy_obj_iswt0, bias_memory_triosy_obj_biwt
);
  input run_wten;
  input bias_memory_triosy_obj_iswt0;
  output bias_memory_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign bias_memory_triosy_obj_biwt = (~ run_wten) & bias_memory_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_weight_memory_triosy_obj_weight_memory_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_weight_memory_triosy_obj_weight_memory_triosy_wait_ctrl
    (
  run_wten, weight_memory_triosy_obj_iswt0, weight_memory_triosy_obj_biwt
);
  input run_wten;
  input weight_memory_triosy_obj_iswt0;
  output weight_memory_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_triosy_obj_biwt = (~ run_wten) & weight_memory_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_dp
    (
  clk, rstn, output_buffer_rsci_q_d, output_buffer_rsci_q_d_mxwt, output_buffer_rsci_biwt_1,
      output_buffer_rsci_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] output_buffer_rsci_q_d;
  output [31:0] output_buffer_rsci_q_d_mxwt;
  input output_buffer_rsci_biwt_1;
  input output_buffer_rsci_bdwt_2;


  // Interconnect Declarations
  reg output_buffer_rsci_bcwt_1;
  reg [31:0] output_buffer_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign output_buffer_rsci_q_d_mxwt = MUX_v_32_2_2(output_buffer_rsci_q_d, output_buffer_rsci_q_d_bfwt,
      output_buffer_rsci_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_buffer_rsci_bcwt_1 <= 1'b0;
    end
    else begin
      output_buffer_rsci_bcwt_1 <= ~((~(output_buffer_rsci_bcwt_1 | output_buffer_rsci_biwt_1))
          | output_buffer_rsci_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_buffer_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( output_buffer_rsci_biwt_1 ) begin
      output_buffer_rsci_q_d_bfwt <= output_buffer_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_ctrl
    (
  run_wen, run_wten, output_buffer_rsci_oswt_1, output_buffer_rsci_biwt_1, output_buffer_rsci_bdwt_2,
      output_buffer_rsci_we_d_run_sct_pff, output_buffer_rsci_iswt0_pff, output_buffer_rsci_re_d_run_sct_pff,
      output_buffer_rsci_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input output_buffer_rsci_oswt_1;
  output output_buffer_rsci_biwt_1;
  output output_buffer_rsci_bdwt_2;
  output output_buffer_rsci_we_d_run_sct_pff;
  input output_buffer_rsci_iswt0_pff;
  output output_buffer_rsci_re_d_run_sct_pff;
  input output_buffer_rsci_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign output_buffer_rsci_bdwt_2 = output_buffer_rsci_oswt_1 & run_wen;
  assign output_buffer_rsci_biwt_1 = (~ run_wten) & output_buffer_rsci_oswt_1;
  assign output_buffer_rsci_we_d_run_sct_pff = output_buffer_rsci_iswt0_pff & run_wen;
  assign output_buffer_rsci_re_d_run_sct_pff = output_buffer_rsci_oswt_1_pff & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_relu_output_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_relu_output_rsc_wait_dp
    (
  clk, rstn, relu_output_rsci_q_d, relu_output_rsci_q_d_mxwt, relu_output_rsci_biwt_1,
      relu_output_rsci_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] relu_output_rsci_q_d;
  output [31:0] relu_output_rsci_q_d_mxwt;
  input relu_output_rsci_biwt_1;
  input relu_output_rsci_bdwt_2;


  // Interconnect Declarations
  reg relu_output_rsci_bcwt_1;
  reg [31:0] relu_output_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign relu_output_rsci_q_d_mxwt = MUX_v_32_2_2(relu_output_rsci_q_d, relu_output_rsci_q_d_bfwt,
      relu_output_rsci_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      relu_output_rsci_bcwt_1 <= 1'b0;
    end
    else begin
      relu_output_rsci_bcwt_1 <= ~((~(relu_output_rsci_bcwt_1 | relu_output_rsci_biwt_1))
          | relu_output_rsci_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      relu_output_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( relu_output_rsci_biwt_1 ) begin
      relu_output_rsci_q_d_bfwt <= relu_output_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_relu_output_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_relu_output_rsc_wait_ctrl
    (
  run_wen, run_wten, relu_output_rsci_oswt_1, relu_output_rsci_biwt_1, relu_output_rsci_bdwt_2,
      relu_output_rsci_we_d_run_sct_pff, relu_output_rsci_iswt0_pff, relu_output_rsci_re_d_run_sct_pff,
      relu_output_rsci_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input relu_output_rsci_oswt_1;
  output relu_output_rsci_biwt_1;
  output relu_output_rsci_bdwt_2;
  output relu_output_rsci_we_d_run_sct_pff;
  input relu_output_rsci_iswt0_pff;
  output relu_output_rsci_re_d_run_sct_pff;
  input relu_output_rsci_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign relu_output_rsci_bdwt_2 = relu_output_rsci_oswt_1 & run_wen;
  assign relu_output_rsci_biwt_1 = (~ run_wten) & relu_output_rsci_oswt_1;
  assign relu_output_rsci_we_d_run_sct_pff = relu_output_rsci_iswt0_pff & run_wen;
  assign relu_output_rsci_re_d_run_sct_pff = relu_output_rsci_oswt_1_pff & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_dp
    (
  clk, rstn, convolution_output_rsci_q_d, convolution_output_rsci_q_d_mxwt, convolution_output_rsci_biwt,
      convolution_output_rsci_bdwt
);
  input clk;
  input rstn;
  input [31:0] convolution_output_rsci_q_d;
  output [31:0] convolution_output_rsci_q_d_mxwt;
  input convolution_output_rsci_biwt;
  input convolution_output_rsci_bdwt;


  // Interconnect Declarations
  reg convolution_output_rsci_bcwt;
  reg [31:0] convolution_output_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign convolution_output_rsci_q_d_mxwt = MUX_v_32_2_2(convolution_output_rsci_q_d,
      convolution_output_rsci_q_d_bfwt, convolution_output_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      convolution_output_rsci_bcwt <= 1'b0;
    end
    else begin
      convolution_output_rsci_bcwt <= ~((~(convolution_output_rsci_bcwt | convolution_output_rsci_biwt))
          | convolution_output_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      convolution_output_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( convolution_output_rsci_biwt ) begin
      convolution_output_rsci_q_d_bfwt <= convolution_output_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_ctrl
    (
  run_wen, run_wten, convolution_output_rsci_oswt, convolution_output_rsci_biwt,
      convolution_output_rsci_bdwt, convolution_output_rsci_we_d_run_sct_pff, convolution_output_rsci_iswt0_1_pff,
      convolution_output_rsci_re_d_run_sct_pff, convolution_output_rsci_oswt_pff
);
  input run_wen;
  input run_wten;
  input convolution_output_rsci_oswt;
  output convolution_output_rsci_biwt;
  output convolution_output_rsci_bdwt;
  output convolution_output_rsci_we_d_run_sct_pff;
  input convolution_output_rsci_iswt0_1_pff;
  output convolution_output_rsci_re_d_run_sct_pff;
  input convolution_output_rsci_oswt_pff;



  // Interconnect Declarations for Component Instantiations 
  assign convolution_output_rsci_bdwt = convolution_output_rsci_oswt & run_wen;
  assign convolution_output_rsci_biwt = (~ run_wten) & convolution_output_rsci_oswt;
  assign convolution_output_rsci_we_d_run_sct_pff = convolution_output_rsci_iswt0_1_pff
      & run_wen;
  assign convolution_output_rsci_re_d_run_sct_pff = convolution_output_rsci_oswt_pff
      & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_current_image_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_current_image_rsc_wait_dp
    (
  clk, rstn, current_image_rsci_q_d, current_image_rsci_q_d_mxwt, current_image_rsci_biwt_1,
      current_image_rsci_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] current_image_rsci_q_d;
  output [31:0] current_image_rsci_q_d_mxwt;
  input current_image_rsci_biwt_1;
  input current_image_rsci_bdwt_2;


  // Interconnect Declarations
  reg current_image_rsci_bcwt_1;
  reg [31:0] current_image_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign current_image_rsci_q_d_mxwt = MUX_v_32_2_2(current_image_rsci_q_d, current_image_rsci_q_d_bfwt,
      current_image_rsci_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_image_rsci_bcwt_1 <= 1'b0;
    end
    else begin
      current_image_rsci_bcwt_1 <= ~((~(current_image_rsci_bcwt_1 | current_image_rsci_biwt_1))
          | current_image_rsci_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_image_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( current_image_rsci_biwt_1 ) begin
      current_image_rsci_q_d_bfwt <= current_image_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_current_image_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_current_image_rsc_wait_ctrl
    (
  run_wen, run_wten, current_image_rsci_oswt_1, current_image_rsci_biwt_1, current_image_rsci_bdwt_2,
      current_image_rsci_we_d_run_sct_pff, current_image_rsci_iswt0_pff, current_image_rsci_re_d_run_sct_pff,
      current_image_rsci_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input current_image_rsci_oswt_1;
  output current_image_rsci_biwt_1;
  output current_image_rsci_bdwt_2;
  output current_image_rsci_we_d_run_sct_pff;
  input current_image_rsci_iswt0_pff;
  output current_image_rsci_re_d_run_sct_pff;
  input current_image_rsci_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign current_image_rsci_bdwt_2 = current_image_rsci_oswt_1 & run_wen;
  assign current_image_rsci_biwt_1 = (~ run_wten) & current_image_rsci_oswt_1;
  assign current_image_rsci_we_d_run_sct_pff = current_image_rsci_iswt0_pff & run_wen;
  assign current_image_rsci_re_d_run_sct_pff = current_image_rsci_oswt_1_pff & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci_output_image_channel_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci_output_image_channel_wait_dp
    (
  clk, rstn, output_image_channel_rsci_oswt, output_image_channel_rsci_wen_comp,
      output_image_channel_rsci_biwt, output_image_channel_rsci_bdwt, output_image_channel_rsci_bcwt
);
  input clk;
  input rstn;
  input output_image_channel_rsci_oswt;
  output output_image_channel_rsci_wen_comp;
  input output_image_channel_rsci_biwt;
  input output_image_channel_rsci_bdwt;
  output output_image_channel_rsci_bcwt;
  reg output_image_channel_rsci_bcwt;



  // Interconnect Declarations for Component Instantiations 
  assign output_image_channel_rsci_wen_comp = (~ output_image_channel_rsci_oswt)
      | output_image_channel_rsci_biwt | output_image_channel_rsci_bcwt;
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_image_channel_rsci_bcwt <= 1'b0;
    end
    else begin
      output_image_channel_rsci_bcwt <= ~((~(output_image_channel_rsci_bcwt | output_image_channel_rsci_biwt))
          | output_image_channel_rsci_bdwt);
    end
  end
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci_output_image_channel_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci_output_image_channel_wait_ctrl
    (
  run_wen, output_image_channel_rsci_oswt, output_image_channel_rsci_biwt, output_image_channel_rsci_bdwt,
      output_image_channel_rsci_bcwt, output_image_channel_rsci_irdy, output_image_channel_rsci_ivld_run_sct
);
  input run_wen;
  input output_image_channel_rsci_oswt;
  output output_image_channel_rsci_biwt;
  output output_image_channel_rsci_bdwt;
  input output_image_channel_rsci_bcwt;
  input output_image_channel_rsci_irdy;
  output output_image_channel_rsci_ivld_run_sct;


  // Interconnect Declarations
  wire output_image_channel_rsci_ogwt;


  // Interconnect Declarations for Component Instantiations 
  assign output_image_channel_rsci_bdwt = output_image_channel_rsci_oswt & run_wen;
  assign output_image_channel_rsci_biwt = output_image_channel_rsci_ogwt & output_image_channel_rsci_irdy;
  assign output_image_channel_rsci_ogwt = output_image_channel_rsci_oswt & (~ output_image_channel_rsci_bcwt);
  assign output_image_channel_rsci_ivld_run_sct = output_image_channel_rsci_ogwt;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp
    (
  clk, rstn, bias_memory_rsci_q_d, bias_memory_rsci_q_d_mxwt, bias_memory_rsci_biwt,
      bias_memory_rsci_bdwt
);
  input clk;
  input rstn;
  input [31:0] bias_memory_rsci_q_d;
  output [31:0] bias_memory_rsci_q_d_mxwt;
  input bias_memory_rsci_biwt;
  input bias_memory_rsci_bdwt;


  // Interconnect Declarations
  reg bias_memory_rsci_bcwt;
  reg [31:0] bias_memory_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign bias_memory_rsci_q_d_mxwt = MUX_v_32_2_2(bias_memory_rsci_q_d, bias_memory_rsci_q_d_bfwt,
      bias_memory_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      bias_memory_rsci_bcwt <= 1'b0;
    end
    else begin
      bias_memory_rsci_bcwt <= ~((~(bias_memory_rsci_bcwt | bias_memory_rsci_biwt))
          | bias_memory_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      bias_memory_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( bias_memory_rsci_biwt ) begin
      bias_memory_rsci_q_d_bfwt <= bias_memory_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl
    (
  run_wen, run_wten, bias_memory_rsci_oswt, bias_memory_rsci_biwt, bias_memory_rsci_bdwt,
      bias_memory_rsci_biwt_pff, bias_memory_rsci_oswt_pff
);
  input run_wen;
  input run_wten;
  input bias_memory_rsci_oswt;
  output bias_memory_rsci_biwt;
  output bias_memory_rsci_bdwt;
  output bias_memory_rsci_biwt_pff;
  input bias_memory_rsci_oswt_pff;



  // Interconnect Declarations for Component Instantiations 
  assign bias_memory_rsci_bdwt = bias_memory_rsci_oswt & run_wen;
  assign bias_memory_rsci_biwt = (~ run_wten) & bias_memory_rsci_oswt;
  assign bias_memory_rsci_biwt_pff = run_wen & bias_memory_rsci_oswt_pff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_dp
    (
  clk, rstn, weight_memory_rsci_q_d, weight_memory_rsci_q_d_mxwt, weight_memory_rsci_biwt,
      weight_memory_rsci_bdwt
);
  input clk;
  input rstn;
  input [31:0] weight_memory_rsci_q_d;
  output [31:0] weight_memory_rsci_q_d_mxwt;
  input weight_memory_rsci_biwt;
  input weight_memory_rsci_bdwt;


  // Interconnect Declarations
  reg weight_memory_rsci_bcwt;
  reg [31:0] weight_memory_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_rsci_q_d_mxwt = MUX_v_32_2_2(weight_memory_rsci_q_d, weight_memory_rsci_q_d_bfwt,
      weight_memory_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_memory_rsci_bcwt <= 1'b0;
    end
    else begin
      weight_memory_rsci_bcwt <= ~((~(weight_memory_rsci_bcwt | weight_memory_rsci_biwt))
          | weight_memory_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_memory_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( weight_memory_rsci_biwt ) begin
      weight_memory_rsci_q_d_bfwt <= weight_memory_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_ctrl
    (
  run_wen, run_wten, weight_memory_rsci_oswt, weight_memory_rsci_biwt, weight_memory_rsci_bdwt,
      weight_memory_rsci_biwt_pff, weight_memory_rsci_oswt_pff
);
  input run_wen;
  input run_wten;
  input weight_memory_rsci_oswt;
  output weight_memory_rsci_biwt;
  output weight_memory_rsci_bdwt;
  output weight_memory_rsci_biwt_pff;
  input weight_memory_rsci_oswt_pff;



  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_rsci_bdwt = weight_memory_rsci_oswt & run_wen;
  assign weight_memory_rsci_biwt = (~ run_wten) & weight_memory_rsci_oswt;
  assign weight_memory_rsci_biwt_pff = run_wen & weight_memory_rsci_oswt_pff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_image_channel_rsci_image_channel_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_image_channel_rsci_image_channel_wait_dp (
  clk, rstn, image_channel_rsci_oswt, image_channel_rsci_wen_comp, image_channel_rsci_idat_mxwt,
      image_channel_rsci_biwt, image_channel_rsci_bdwt, image_channel_rsci_bcwt,
      image_channel_rsci_idat
);
  input clk;
  input rstn;
  input image_channel_rsci_oswt;
  output image_channel_rsci_wen_comp;
  output [31:0] image_channel_rsci_idat_mxwt;
  input image_channel_rsci_biwt;
  input image_channel_rsci_bdwt;
  output image_channel_rsci_bcwt;
  reg image_channel_rsci_bcwt;
  input [31:0] image_channel_rsci_idat;


  // Interconnect Declarations
  reg [31:0] image_channel_rsci_idat_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign image_channel_rsci_wen_comp = (~ image_channel_rsci_oswt) | image_channel_rsci_biwt
      | image_channel_rsci_bcwt;
  assign image_channel_rsci_idat_mxwt = MUX_v_32_2_2(image_channel_rsci_idat, image_channel_rsci_idat_bfwt,
      image_channel_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      image_channel_rsci_bcwt <= 1'b0;
    end
    else begin
      image_channel_rsci_bcwt <= ~((~(image_channel_rsci_bcwt | image_channel_rsci_biwt))
          | image_channel_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      image_channel_rsci_idat_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( image_channel_rsci_biwt ) begin
      image_channel_rsci_idat_bfwt <= image_channel_rsci_idat;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_image_channel_rsci_image_channel_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_image_channel_rsci_image_channel_wait_ctrl
    (
  run_wen, image_channel_rsci_oswt, image_channel_rsci_biwt, image_channel_rsci_bdwt,
      image_channel_rsci_bcwt, image_channel_rsci_irdy_run_sct, image_channel_rsci_ivld
);
  input run_wen;
  input image_channel_rsci_oswt;
  output image_channel_rsci_biwt;
  output image_channel_rsci_bdwt;
  input image_channel_rsci_bcwt;
  output image_channel_rsci_irdy_run_sct;
  input image_channel_rsci_ivld;


  // Interconnect Declarations
  wire image_channel_rsci_ogwt;


  // Interconnect Declarations for Component Instantiations 
  assign image_channel_rsci_bdwt = image_channel_rsci_oswt & run_wen;
  assign image_channel_rsci_biwt = image_channel_rsci_ogwt & image_channel_rsci_ivld;
  assign image_channel_rsci_ogwt = image_channel_rsci_oswt & (~ image_channel_rsci_bcwt);
  assign image_channel_rsci_irdy_run_sct = image_channel_rsci_ogwt;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_num_output_images_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_num_output_images_triosy_obj (
  num_output_images_triosy_lz, run_wten, num_output_images_triosy_obj_iswt0
);
  output num_output_images_triosy_lz;
  input run_wten;
  input num_output_images_triosy_obj_iswt0;


  // Interconnect Declarations
  wire num_output_images_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) num_output_images_triosy_obj (
      .ld(num_output_images_triosy_obj_biwt),
      .lz(num_output_images_triosy_lz)
    );
  catapult_conv2d_28_28_5_5_1_run_num_output_images_triosy_obj_num_output_images_triosy_wait_ctrl
      catapult_conv2d_28_28_5_5_1_run_num_output_images_triosy_obj_num_output_images_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .num_output_images_triosy_obj_iswt0(num_output_images_triosy_obj_iswt0),
      .num_output_images_triosy_obj_biwt(num_output_images_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_num_input_images_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_num_input_images_triosy_obj (
  num_input_images_triosy_lz, run_wten, num_input_images_triosy_obj_iswt0
);
  output num_input_images_triosy_lz;
  input run_wten;
  input num_input_images_triosy_obj_iswt0;


  // Interconnect Declarations
  wire num_input_images_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) num_input_images_triosy_obj (
      .ld(num_input_images_triosy_obj_biwt),
      .lz(num_input_images_triosy_lz)
    );
  catapult_conv2d_28_28_5_5_1_run_num_input_images_triosy_obj_num_input_images_triosy_wait_ctrl
      catapult_conv2d_28_28_5_5_1_run_num_input_images_triosy_obj_num_input_images_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .num_input_images_triosy_obj_iswt0(num_input_images_triosy_obj_iswt0),
      .num_input_images_triosy_obj_biwt(num_input_images_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_max_pool_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_max_pool_triosy_obj (
  max_pool_triosy_lz, run_wten, max_pool_triosy_obj_iswt0
);
  output max_pool_triosy_lz;
  input run_wten;
  input max_pool_triosy_obj_iswt0;


  // Interconnect Declarations
  wire max_pool_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) max_pool_triosy_obj (
      .ld(max_pool_triosy_obj_biwt),
      .lz(max_pool_triosy_lz)
    );
  catapult_conv2d_28_28_5_5_1_run_max_pool_triosy_obj_max_pool_triosy_wait_ctrl catapult_conv2d_28_28_5_5_1_run_max_pool_triosy_obj_max_pool_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .max_pool_triosy_obj_iswt0(max_pool_triosy_obj_iswt0),
      .max_pool_triosy_obj_biwt(max_pool_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_relu_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_relu_triosy_obj (
  relu_triosy_lz, run_wten, relu_triosy_obj_iswt0
);
  output relu_triosy_lz;
  input run_wten;
  input relu_triosy_obj_iswt0;


  // Interconnect Declarations
  wire relu_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) relu_triosy_obj (
      .ld(relu_triosy_obj_biwt),
      .lz(relu_triosy_lz)
    );
  catapult_conv2d_28_28_5_5_1_run_relu_triosy_obj_relu_triosy_wait_ctrl catapult_conv2d_28_28_5_5_1_run_relu_triosy_obj_relu_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .relu_triosy_obj_iswt0(relu_triosy_obj_iswt0),
      .relu_triosy_obj_biwt(relu_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_bias_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_bias_triosy_obj (
  bias_triosy_lz, run_wten, bias_triosy_obj_iswt0
);
  output bias_triosy_lz;
  input run_wten;
  input bias_triosy_obj_iswt0;


  // Interconnect Declarations
  wire bias_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) bias_triosy_obj (
      .ld(bias_triosy_obj_biwt),
      .lz(bias_triosy_lz)
    );
  catapult_conv2d_28_28_5_5_1_run_bias_triosy_obj_bias_triosy_wait_ctrl catapult_conv2d_28_28_5_5_1_run_bias_triosy_obj_bias_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .bias_triosy_obj_iswt0(bias_triosy_obj_iswt0),
      .bias_triosy_obj_biwt(bias_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_bias_memory_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_bias_memory_triosy_obj (
  bias_memory_triosy_lz, run_wten, bias_memory_triosy_obj_iswt0
);
  output bias_memory_triosy_lz;
  input run_wten;
  input bias_memory_triosy_obj_iswt0;


  // Interconnect Declarations
  wire bias_memory_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) bias_memory_triosy_obj (
      .ld(bias_memory_triosy_obj_biwt),
      .lz(bias_memory_triosy_lz)
    );
  catapult_conv2d_28_28_5_5_1_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl
      catapult_conv2d_28_28_5_5_1_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .bias_memory_triosy_obj_iswt0(bias_memory_triosy_obj_iswt0),
      .bias_memory_triosy_obj_biwt(bias_memory_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_weight_memory_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_weight_memory_triosy_obj (
  weight_memory_triosy_lz, run_wten, weight_memory_triosy_obj_iswt0
);
  output weight_memory_triosy_lz;
  input run_wten;
  input weight_memory_triosy_obj_iswt0;


  // Interconnect Declarations
  wire weight_memory_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) weight_memory_triosy_obj (
      .ld(weight_memory_triosy_obj_biwt),
      .lz(weight_memory_triosy_lz)
    );
  catapult_conv2d_28_28_5_5_1_run_weight_memory_triosy_obj_weight_memory_triosy_wait_ctrl
      catapult_conv2d_28_28_5_5_1_run_weight_memory_triosy_obj_weight_memory_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .weight_memory_triosy_obj_iswt0(weight_memory_triosy_obj_iswt0),
      .weight_memory_triosy_obj_biwt(weight_memory_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1 (
  clk, rstn, output_buffer_rsci_q_d, run_wen, run_wten, output_buffer_rsci_oswt_1,
      output_buffer_rsci_q_d_mxwt, output_buffer_rsci_we_d_pff, output_buffer_rsci_iswt0_pff,
      output_buffer_rsci_re_d_pff, output_buffer_rsci_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] output_buffer_rsci_q_d;
  input run_wen;
  input run_wten;
  input output_buffer_rsci_oswt_1;
  output [31:0] output_buffer_rsci_q_d_mxwt;
  output output_buffer_rsci_we_d_pff;
  input output_buffer_rsci_iswt0_pff;
  output output_buffer_rsci_re_d_pff;
  input output_buffer_rsci_oswt_1_pff;


  // Interconnect Declarations
  wire output_buffer_rsci_biwt_1;
  wire output_buffer_rsci_bdwt_2;
  wire output_buffer_rsci_we_d_run_sct_iff;
  wire output_buffer_rsci_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_ctrl
      catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .output_buffer_rsci_oswt_1(output_buffer_rsci_oswt_1),
      .output_buffer_rsci_biwt_1(output_buffer_rsci_biwt_1),
      .output_buffer_rsci_bdwt_2(output_buffer_rsci_bdwt_2),
      .output_buffer_rsci_we_d_run_sct_pff(output_buffer_rsci_we_d_run_sct_iff),
      .output_buffer_rsci_iswt0_pff(output_buffer_rsci_iswt0_pff),
      .output_buffer_rsci_re_d_run_sct_pff(output_buffer_rsci_re_d_run_sct_iff),
      .output_buffer_rsci_oswt_1_pff(output_buffer_rsci_oswt_1_pff)
    );
  catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_dp
      catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_buffer_rsci_q_d(output_buffer_rsci_q_d),
      .output_buffer_rsci_q_d_mxwt(output_buffer_rsci_q_d_mxwt),
      .output_buffer_rsci_biwt_1(output_buffer_rsci_biwt_1),
      .output_buffer_rsci_bdwt_2(output_buffer_rsci_bdwt_2)
    );
  assign output_buffer_rsci_we_d_pff = output_buffer_rsci_we_d_run_sct_iff;
  assign output_buffer_rsci_re_d_pff = output_buffer_rsci_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1 (
  clk, rstn, relu_output_rsci_q_d, run_wen, run_wten, relu_output_rsci_oswt_1, relu_output_rsci_q_d_mxwt,
      relu_output_rsci_we_d_pff, relu_output_rsci_iswt0_pff, relu_output_rsci_re_d_pff,
      relu_output_rsci_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] relu_output_rsci_q_d;
  input run_wen;
  input run_wten;
  input relu_output_rsci_oswt_1;
  output [31:0] relu_output_rsci_q_d_mxwt;
  output relu_output_rsci_we_d_pff;
  input relu_output_rsci_iswt0_pff;
  output relu_output_rsci_re_d_pff;
  input relu_output_rsci_oswt_1_pff;


  // Interconnect Declarations
  wire relu_output_rsci_biwt_1;
  wire relu_output_rsci_bdwt_2;
  wire relu_output_rsci_we_d_run_sct_iff;
  wire relu_output_rsci_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_relu_output_rsc_wait_ctrl catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_relu_output_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .relu_output_rsci_oswt_1(relu_output_rsci_oswt_1),
      .relu_output_rsci_biwt_1(relu_output_rsci_biwt_1),
      .relu_output_rsci_bdwt_2(relu_output_rsci_bdwt_2),
      .relu_output_rsci_we_d_run_sct_pff(relu_output_rsci_we_d_run_sct_iff),
      .relu_output_rsci_iswt0_pff(relu_output_rsci_iswt0_pff),
      .relu_output_rsci_re_d_run_sct_pff(relu_output_rsci_re_d_run_sct_iff),
      .relu_output_rsci_oswt_1_pff(relu_output_rsci_oswt_1_pff)
    );
  catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_relu_output_rsc_wait_dp catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_relu_output_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .relu_output_rsci_q_d(relu_output_rsci_q_d),
      .relu_output_rsci_q_d_mxwt(relu_output_rsci_q_d_mxwt),
      .relu_output_rsci_biwt_1(relu_output_rsci_biwt_1),
      .relu_output_rsci_bdwt_2(relu_output_rsci_bdwt_2)
    );
  assign relu_output_rsci_we_d_pff = relu_output_rsci_we_d_run_sct_iff;
  assign relu_output_rsci_re_d_pff = relu_output_rsci_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1 (
  clk, rstn, convolution_output_rsci_q_d, run_wen, run_wten, convolution_output_rsci_oswt,
      convolution_output_rsci_q_d_mxwt, convolution_output_rsci_we_d_pff, convolution_output_rsci_iswt0_1_pff,
      convolution_output_rsci_re_d_pff, convolution_output_rsci_oswt_pff
);
  input clk;
  input rstn;
  input [31:0] convolution_output_rsci_q_d;
  input run_wen;
  input run_wten;
  input convolution_output_rsci_oswt;
  output [31:0] convolution_output_rsci_q_d_mxwt;
  output convolution_output_rsci_we_d_pff;
  input convolution_output_rsci_iswt0_1_pff;
  output convolution_output_rsci_re_d_pff;
  input convolution_output_rsci_oswt_pff;


  // Interconnect Declarations
  wire convolution_output_rsci_biwt;
  wire convolution_output_rsci_bdwt;
  wire convolution_output_rsci_we_d_run_sct_iff;
  wire convolution_output_rsci_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_ctrl
      catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .convolution_output_rsci_oswt(convolution_output_rsci_oswt),
      .convolution_output_rsci_biwt(convolution_output_rsci_biwt),
      .convolution_output_rsci_bdwt(convolution_output_rsci_bdwt),
      .convolution_output_rsci_we_d_run_sct_pff(convolution_output_rsci_we_d_run_sct_iff),
      .convolution_output_rsci_iswt0_1_pff(convolution_output_rsci_iswt0_1_pff),
      .convolution_output_rsci_re_d_run_sct_pff(convolution_output_rsci_re_d_run_sct_iff),
      .convolution_output_rsci_oswt_pff(convolution_output_rsci_oswt_pff)
    );
  catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_dp
      catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .convolution_output_rsci_q_d(convolution_output_rsci_q_d),
      .convolution_output_rsci_q_d_mxwt(convolution_output_rsci_q_d_mxwt),
      .convolution_output_rsci_biwt(convolution_output_rsci_biwt),
      .convolution_output_rsci_bdwt(convolution_output_rsci_bdwt)
    );
  assign convolution_output_rsci_we_d_pff = convolution_output_rsci_we_d_run_sct_iff;
  assign convolution_output_rsci_re_d_pff = convolution_output_rsci_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1 (
  clk, rstn, current_image_rsci_q_d, run_wen, run_wten, current_image_rsci_oswt_1,
      current_image_rsci_q_d_mxwt, current_image_rsci_we_d_pff, current_image_rsci_iswt0_pff,
      current_image_rsci_re_d_pff, current_image_rsci_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] current_image_rsci_q_d;
  input run_wen;
  input run_wten;
  input current_image_rsci_oswt_1;
  output [31:0] current_image_rsci_q_d_mxwt;
  output current_image_rsci_we_d_pff;
  input current_image_rsci_iswt0_pff;
  output current_image_rsci_re_d_pff;
  input current_image_rsci_oswt_1_pff;


  // Interconnect Declarations
  wire current_image_rsci_biwt_1;
  wire current_image_rsci_bdwt_2;
  wire current_image_rsci_we_d_run_sct_iff;
  wire current_image_rsci_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_current_image_rsc_wait_ctrl
      catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_current_image_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .current_image_rsci_oswt_1(current_image_rsci_oswt_1),
      .current_image_rsci_biwt_1(current_image_rsci_biwt_1),
      .current_image_rsci_bdwt_2(current_image_rsci_bdwt_2),
      .current_image_rsci_we_d_run_sct_pff(current_image_rsci_we_d_run_sct_iff),
      .current_image_rsci_iswt0_pff(current_image_rsci_iswt0_pff),
      .current_image_rsci_re_d_run_sct_pff(current_image_rsci_re_d_run_sct_iff),
      .current_image_rsci_oswt_1_pff(current_image_rsci_oswt_1_pff)
    );
  catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_current_image_rsc_wait_dp
      catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_current_image_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .current_image_rsci_q_d(current_image_rsci_q_d),
      .current_image_rsci_q_d_mxwt(current_image_rsci_q_d_mxwt),
      .current_image_rsci_biwt_1(current_image_rsci_biwt_1),
      .current_image_rsci_bdwt_2(current_image_rsci_bdwt_2)
    );
  assign current_image_rsci_we_d_pff = current_image_rsci_we_d_run_sct_iff;
  assign current_image_rsci_re_d_pff = current_image_rsci_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci (
  clk, rstn, output_image_channel_rsc_dat, output_image_channel_rsc_vld, output_image_channel_rsc_rdy,
      run_wen, output_image_channel_rsci_oswt, output_image_channel_rsci_wen_comp,
      output_image_channel_rsci_idat
);
  input clk;
  input rstn;
  output [31:0] output_image_channel_rsc_dat;
  output output_image_channel_rsc_vld;
  input output_image_channel_rsc_rdy;
  input run_wen;
  input output_image_channel_rsci_oswt;
  output output_image_channel_rsci_wen_comp;
  input [31:0] output_image_channel_rsci_idat;


  // Interconnect Declarations
  wire output_image_channel_rsci_biwt;
  wire output_image_channel_rsci_bdwt;
  wire output_image_channel_rsci_bcwt;
  wire output_image_channel_rsci_irdy;
  wire output_image_channel_rsci_ivld_run_sct;


  // Interconnect Declarations for Component Instantiations 
  ccs_out_wait_v1 #(.rscid(32'sd4),
  .width(32'sd32)) output_image_channel_rsci (
      .irdy(output_image_channel_rsci_irdy),
      .ivld(output_image_channel_rsci_ivld_run_sct),
      .idat(output_image_channel_rsci_idat),
      .rdy(output_image_channel_rsc_rdy),
      .vld(output_image_channel_rsc_vld),
      .dat(output_image_channel_rsc_dat)
    );
  catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci_output_image_channel_wait_ctrl
      catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci_output_image_channel_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .output_image_channel_rsci_oswt(output_image_channel_rsci_oswt),
      .output_image_channel_rsci_biwt(output_image_channel_rsci_biwt),
      .output_image_channel_rsci_bdwt(output_image_channel_rsci_bdwt),
      .output_image_channel_rsci_bcwt(output_image_channel_rsci_bcwt),
      .output_image_channel_rsci_irdy(output_image_channel_rsci_irdy),
      .output_image_channel_rsci_ivld_run_sct(output_image_channel_rsci_ivld_run_sct)
    );
  catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci_output_image_channel_wait_dp
      catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci_output_image_channel_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_image_channel_rsci_oswt(output_image_channel_rsci_oswt),
      .output_image_channel_rsci_wen_comp(output_image_channel_rsci_wen_comp),
      .output_image_channel_rsci_biwt(output_image_channel_rsci_biwt),
      .output_image_channel_rsci_bdwt(output_image_channel_rsci_bdwt),
      .output_image_channel_rsci_bcwt(output_image_channel_rsci_bcwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1 (
  clk, rstn, bias_memory_rsci_q_d, run_wen, run_wten, bias_memory_rsci_oswt, bias_memory_rsci_q_d_mxwt,
      bias_memory_rsci_re_d_pff, bias_memory_rsci_oswt_pff
);
  input clk;
  input rstn;
  input [31:0] bias_memory_rsci_q_d;
  input run_wen;
  input run_wten;
  input bias_memory_rsci_oswt;
  output [31:0] bias_memory_rsci_q_d_mxwt;
  output bias_memory_rsci_re_d_pff;
  input bias_memory_rsci_oswt_pff;


  // Interconnect Declarations
  wire bias_memory_rsci_biwt;
  wire bias_memory_rsci_bdwt;
  wire bias_memory_rsci_biwt_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .bias_memory_rsci_oswt(bias_memory_rsci_oswt),
      .bias_memory_rsci_biwt(bias_memory_rsci_biwt),
      .bias_memory_rsci_bdwt(bias_memory_rsci_bdwt),
      .bias_memory_rsci_biwt_pff(bias_memory_rsci_biwt_iff),
      .bias_memory_rsci_oswt_pff(bias_memory_rsci_oswt_pff)
    );
  catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .bias_memory_rsci_q_d(bias_memory_rsci_q_d),
      .bias_memory_rsci_q_d_mxwt(bias_memory_rsci_q_d_mxwt),
      .bias_memory_rsci_biwt(bias_memory_rsci_biwt),
      .bias_memory_rsci_bdwt(bias_memory_rsci_bdwt)
    );
  assign bias_memory_rsci_re_d_pff = bias_memory_rsci_biwt_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1 (
  clk, rstn, weight_memory_rsci_q_d, run_wen, run_wten, weight_memory_rsci_oswt,
      weight_memory_rsci_q_d_mxwt, weight_memory_rsci_re_d_pff, weight_memory_rsci_oswt_pff
);
  input clk;
  input rstn;
  input [31:0] weight_memory_rsci_q_d;
  input run_wen;
  input run_wten;
  input weight_memory_rsci_oswt;
  output [31:0] weight_memory_rsci_q_d_mxwt;
  output weight_memory_rsci_re_d_pff;
  input weight_memory_rsci_oswt_pff;


  // Interconnect Declarations
  wire weight_memory_rsci_biwt;
  wire weight_memory_rsci_bdwt;
  wire weight_memory_rsci_biwt_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_ctrl
      catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .weight_memory_rsci_oswt(weight_memory_rsci_oswt),
      .weight_memory_rsci_biwt(weight_memory_rsci_biwt),
      .weight_memory_rsci_bdwt(weight_memory_rsci_bdwt),
      .weight_memory_rsci_biwt_pff(weight_memory_rsci_biwt_iff),
      .weight_memory_rsci_oswt_pff(weight_memory_rsci_oswt_pff)
    );
  catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_dp
      catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .weight_memory_rsci_q_d(weight_memory_rsci_q_d),
      .weight_memory_rsci_q_d_mxwt(weight_memory_rsci_q_d_mxwt),
      .weight_memory_rsci_biwt(weight_memory_rsci_biwt),
      .weight_memory_rsci_bdwt(weight_memory_rsci_bdwt)
    );
  assign weight_memory_rsci_re_d_pff = weight_memory_rsci_biwt_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run_image_channel_rsci
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run_image_channel_rsci (
  clk, rstn, image_channel_rsc_dat, image_channel_rsc_vld, image_channel_rsc_rdy,
      run_wen, image_channel_rsci_oswt, image_channel_rsci_wen_comp, image_channel_rsci_idat_mxwt
);
  input clk;
  input rstn;
  input [31:0] image_channel_rsc_dat;
  input image_channel_rsc_vld;
  output image_channel_rsc_rdy;
  input run_wen;
  input image_channel_rsci_oswt;
  output image_channel_rsci_wen_comp;
  output [31:0] image_channel_rsci_idat_mxwt;


  // Interconnect Declarations
  wire image_channel_rsci_biwt;
  wire image_channel_rsci_bdwt;
  wire image_channel_rsci_bcwt;
  wire image_channel_rsci_irdy_run_sct;
  wire image_channel_rsci_ivld;
  wire [31:0] image_channel_rsci_idat;


  // Interconnect Declarations for Component Instantiations 
  ccs_in_wait_v1 #(.rscid(32'sd1),
  .width(32'sd32)) image_channel_rsci (
      .rdy(image_channel_rsc_rdy),
      .vld(image_channel_rsc_vld),
      .dat(image_channel_rsc_dat),
      .irdy(image_channel_rsci_irdy_run_sct),
      .ivld(image_channel_rsci_ivld),
      .idat(image_channel_rsci_idat)
    );
  catapult_conv2d_28_28_5_5_1_run_image_channel_rsci_image_channel_wait_ctrl catapult_conv2d_28_28_5_5_1_run_image_channel_rsci_image_channel_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .image_channel_rsci_oswt(image_channel_rsci_oswt),
      .image_channel_rsci_biwt(image_channel_rsci_biwt),
      .image_channel_rsci_bdwt(image_channel_rsci_bdwt),
      .image_channel_rsci_bcwt(image_channel_rsci_bcwt),
      .image_channel_rsci_irdy_run_sct(image_channel_rsci_irdy_run_sct),
      .image_channel_rsci_ivld(image_channel_rsci_ivld)
    );
  catapult_conv2d_28_28_5_5_1_run_image_channel_rsci_image_channel_wait_dp catapult_conv2d_28_28_5_5_1_run_image_channel_rsci_image_channel_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .image_channel_rsci_oswt(image_channel_rsci_oswt),
      .image_channel_rsci_wen_comp(image_channel_rsci_wen_comp),
      .image_channel_rsci_idat_mxwt(image_channel_rsci_idat_mxwt),
      .image_channel_rsci_biwt(image_channel_rsci_biwt),
      .image_channel_rsci_bdwt(image_channel_rsci_bdwt),
      .image_channel_rsci_bcwt(image_channel_rsci_bcwt),
      .image_channel_rsci_idat(image_channel_rsci_idat)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1_run
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1_run (
  clk, rstn, image_channel_rsc_dat, image_channel_rsc_vld, image_channel_rsc_rdy,
      weight_memory_triosy_lz, bias_memory_triosy_lz, output_image_channel_rsc_dat,
      output_image_channel_rsc_vld, output_image_channel_rsc_rdy, bias_rsc_dat, bias_triosy_lz,
      relu_rsc_dat, relu_triosy_lz, max_pool_rsc_dat, max_pool_triosy_lz, num_input_images_rsc_dat,
      num_input_images_triosy_lz, num_output_images_rsc_dat, num_output_images_triosy_lz,
      weight_memory_rsci_radr_d, weight_memory_rsci_q_d, bias_memory_rsci_radr_d,
      bias_memory_rsci_q_d, current_image_rsci_radr_d, current_image_rsci_wadr_d,
      current_image_rsci_d_d, current_image_rsci_q_d, convolution_output_rsci_radr_d,
      convolution_output_rsci_wadr_d, convolution_output_rsci_d_d, convolution_output_rsci_q_d,
      relu_output_rsci_radr_d, relu_output_rsci_wadr_d, relu_output_rsci_d_d, relu_output_rsci_q_d,
      output_buffer_rsci_radr_d, output_buffer_rsci_wadr_d, output_buffer_rsci_d_d,
      output_buffer_rsci_q_d, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_a, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_b,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_a,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_b, outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z,
      catapult_conv2d_28_28_5_5_1_stall, weight_memory_rsci_re_d_pff, bias_memory_rsci_re_d_pff,
      current_image_rsci_we_d_pff, current_image_rsci_re_d_pff, convolution_output_rsci_we_d_pff,
      convolution_output_rsci_re_d_pff, relu_output_rsci_we_d_pff, relu_output_rsci_re_d_pff,
      output_buffer_rsci_we_d_pff, output_buffer_rsci_re_d_pff
);
  input clk;
  input rstn;
  input [31:0] image_channel_rsc_dat;
  input image_channel_rsc_vld;
  output image_channel_rsc_rdy;
  output weight_memory_triosy_lz;
  output bias_memory_triosy_lz;
  output [31:0] output_image_channel_rsc_dat;
  output output_image_channel_rsc_vld;
  input output_image_channel_rsc_rdy;
  input bias_rsc_dat;
  output bias_triosy_lz;
  input relu_rsc_dat;
  output relu_triosy_lz;
  input max_pool_rsc_dat;
  output max_pool_triosy_lz;
  input [27:0] num_input_images_rsc_dat;
  output num_input_images_triosy_lz;
  input [27:0] num_output_images_rsc_dat;
  output num_output_images_triosy_lz;
  output [13:0] weight_memory_rsci_radr_d;
  input [31:0] weight_memory_rsci_q_d;
  output [9:0] bias_memory_rsci_radr_d;
  input [31:0] bias_memory_rsci_q_d;
  output [13:0] current_image_rsci_radr_d;
  output [13:0] current_image_rsci_wadr_d;
  output [31:0] current_image_rsci_d_d;
  input [31:0] current_image_rsci_q_d;
  output [9:0] convolution_output_rsci_radr_d;
  output [9:0] convolution_output_rsci_wadr_d;
  output [31:0] convolution_output_rsci_d_d;
  input [31:0] convolution_output_rsci_q_d;
  output [9:0] relu_output_rsci_radr_d;
  output [9:0] relu_output_rsci_wadr_d;
  output [31:0] relu_output_rsci_d_d;
  input [31:0] relu_output_rsci_q_d;
  output [9:0] output_buffer_rsci_radr_d;
  output [9:0] output_buffer_rsci_wadr_d;
  output [31:0] output_buffer_rsci_d_d;
  input [31:0] output_buffer_rsci_q_d;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_a;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_a;
  output [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_b;
  reg [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_b;
  input [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z;
  input catapult_conv2d_28_28_5_5_1_stall;
  output weight_memory_rsci_re_d_pff;
  output bias_memory_rsci_re_d_pff;
  output current_image_rsci_we_d_pff;
  output current_image_rsci_re_d_pff;
  output convolution_output_rsci_we_d_pff;
  output convolution_output_rsci_re_d_pff;
  output relu_output_rsci_we_d_pff;
  output relu_output_rsci_re_d_pff;
  output output_buffer_rsci_we_d_pff;
  output output_buffer_rsci_re_d_pff;


  // Interconnect Declarations
  wire run_wen;
  wire run_wten;
  wire image_channel_rsci_wen_comp;
  wire [31:0] image_channel_rsci_idat_mxwt;
  wire [31:0] weight_memory_rsci_q_d_mxwt;
  wire [31:0] bias_memory_rsci_q_d_mxwt;
  wire output_image_channel_rsci_wen_comp;
  reg [31:0] output_image_channel_rsci_idat;
  wire bias_rsci_idat;
  wire relu_rsci_idat;
  wire max_pool_rsci_idat;
  wire [27:0] num_input_images_rsci_idat;
  wire [27:0] num_output_images_rsci_idat;
  wire [31:0] current_image_rsci_q_d_mxwt;
  wire [31:0] convolution_output_rsci_q_d_mxwt;
  wire [31:0] relu_output_rsci_q_d_mxwt;
  wire [31:0] output_buffer_rsci_q_d_mxwt;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z_oreg;
  wire [63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z_oreg;
  wire [25:0] fsm_output;
  wire [4:0] outer_conv_loop_3_operator_28_true_acc_1_tmp;
  wire [5:0] nl_outer_conv_loop_3_operator_28_true_acc_1_tmp;
  wire [4:0] outer_conv_loop_3_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_tmp;
  wire [5:0] nl_outer_conv_loop_3_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_tmp;
  wire [5:0] catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp;
  wire [6:0] nl_catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp;
  wire [4:0] catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp;
  wire [5:0] nl_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp;
  wire or_dcpl_12;
  wire and_dcpl_11;
  wire or_dcpl_29;
  wire or_dcpl_32;
  wire and_dcpl_30;
  wire and_dcpl_53;
  wire or_dcpl_60;
  wire or_dcpl_61;
  wire or_dcpl_62;
  wire or_dcpl_63;
  wire or_dcpl_65;
  wire or_dcpl_66;
  wire or_dcpl_68;
  wire or_dcpl_69;
  wire or_dcpl_71;
  wire or_dcpl_74;
  wire or_dcpl_76;
  wire or_dcpl_77;
  wire or_dcpl_78;
  wire or_dcpl_81;
  wire or_dcpl_82;
  wire or_dcpl_85;
  wire or_dcpl_90;
  wire or_dcpl_93;
  wire and_dcpl_57;
  wire or_dcpl_110;
  wire or_dcpl_113;
  wire and_dcpl_78;
  wire and_dcpl_83;
  wire and_dcpl_86;
  wire or_dcpl_138;
  wire and_dcpl_95;
  wire and_dcpl_96;
  wire and_dcpl_97;
  wire and_dcpl_100;
  wire or_dcpl_141;
  wire or_dcpl_147;
  wire and_dcpl_105;
  wire and_dcpl_106;
  wire or_dcpl_157;
  wire or_dcpl_161;
  wire or_dcpl_171;
  wire and_dcpl_125;
  wire and_dcpl_127;
  wire or_tmp_70;
  wire or_tmp_75;
  wire or_tmp_260;
  wire or_tmp_298;
  wire and_265_cse;
  wire and_290_cse;
  wire and_845_cse;
  wire and_870_cse;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm;
  wire exit_main_loop_1_sva_mx0;
  reg inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1;
  reg catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_5_itm;
  reg [4:0] catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva;
  wire main_loop_3_and_unfl_sva_1;
  wire main_loop_3_nor_ovfl_sva_1;
  reg [31:0] catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qr_lpi_2_dfm;
  wire outer_conv_loop_1_operator_28_true_and_2;
  reg outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1;
  reg outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1;
  reg [4:0] catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_sva_1_5_1;
  reg outer_conv_loop_2_operator_28_true_and_itm_1;
  reg [31:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_5;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_and_unfl_sva_1;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_nor_ovfl_sva_1;
  reg [47:0] catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_slc_catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_1_63_15_48_1_itm_1;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_slc_catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_1_63_15_0_itm_1;
  reg [27:0] inner_loop_i_sva;
  wire [4:0] main_loop_2_index_4_0_sva_mx0;
  reg load_images_loop_acc_3_cse_sva_28;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_6;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_7;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_stage_0;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_3;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_5;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_4;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_5;
  reg outer_conv_loop_5_inner_conv_loop_5_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_4;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_3;
  reg max_pool_sva;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_2;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_1;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_1;
  reg operator_28_true_1_slc_operator_28_true_1_acc_5_svs_1;
  reg [4:0] catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_2;
  reg relu_sva;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_6;
  reg operator_28_true_return_sva;
  reg main_loop_2_asn_167_itm_1;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_itm_1;
  reg bias_sva;
  reg [31:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_2;
  reg outer_conv_loop_2_operator_28_true_or_itm_1;
  reg [2:0] outer_conv_loop_1_operator_28_true_acc_1_itm_1;
  reg [2:0] outer_conv_loop_1_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_1;
  wire [3:0] nl_outer_conv_loop_1_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_1;
  reg outer_conv_loop_5_inner_conv_loop_4_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1;
  reg outer_conv_loop_3_operator_28_true_acc_cse_4_sva_1;
  reg outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_1;
  wire [32:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1;
  wire [33:0] nl_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1;
  wire [48:0] catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1;
  wire [49:0] nl_catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1;
  wire catapult_conv2d_28_28_5_5_1_convolve_center_pixel_not_seb;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_9;
  wire num_output_images_and_cse;
  wire shift_register_and_1_cse;
  wire shift_register_and_2_cse;
  wire shift_register_and_3_cse;
  wire shift_register_and_4_cse;
  wire shift_register_and_85_cse;
  reg reg_num_output_images_triosy_obj_iswt0_cse;
  reg reg_output_buffer_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_relu_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_convolution_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_current_image_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_output_image_channel_rsci_iswt0_cse;
  reg reg_bias_memory_rsci_iswt0_cse;
  reg reg_weight_memory_rsci_iswt0_cse;
  reg reg_image_channel_rsci_iswt0_cse;
  wire or_36_cse;
  wire catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_land_1_lpi_3_dfm_mx0w5;
  reg [2:0] outer_conv_loop_4_inner_conv_loop_1_operator_28_true_1_acc_psp_4_2_sva_1;
  wire [3:0] nl_outer_conv_loop_4_inner_conv_loop_1_operator_28_true_1_acc_psp_4_2_sva_1;
  wire or_133_cse;
  wire weight_memory_rsci_re_d_iff;
  wire bias_memory_rsci_re_d_iff;
  wire and_761_rmff;
  wire current_image_rsci_we_d_iff;
  wire current_image_rsci_re_d_iff;
  wire and_757_rmff;
  wire convolution_output_rsci_we_d_iff;
  wire convolution_output_rsci_re_d_iff;
  wire or_432_rmff;
  wire relu_output_rsci_we_d_iff;
  wire relu_output_rsci_re_d_iff;
  wire or_430_rmff;
  wire output_buffer_rsci_we_d_iff;
  wire output_buffer_rsci_re_d_iff;
  wire and_736_rmff;
  reg [13:0] weight_index_13_0_lpi_3;
  reg [9:0] bias_index_9_0_sva;
  reg [31:0] catapult_conv2d_28_28_5_5_1_max_cd_lpi_3_dfm;
  reg [5:0] inner_loop_acc_7_psp;
  wire [6:0] nl_inner_loop_acc_7_psp;
  reg [3:0] inner_loop_acc_5_sdt_3_0;
  reg [6:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_if_1_and_unfl_sva_1;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_ovfl_sva_1;
  wire main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1;
  wire main_loop_4_qif_slc_bias_output_32_31_0_ncse_0_sva_1;
  wire catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_mx0c1;
  wire [4:0] main_loop_2_index_4_0_sva_2;
  wire [5:0] nl_main_loop_2_index_4_0_sva_2;
  wire [4:0] catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_mx1;
  wire or_555_tmp;
  wire and_1000_cse;
  wire [28:0] z_out;
  wire or_tmp_379;
  wire [32:0] z_out_1;
  wire or_tmp_383;
  wire [9:0] z_out_2;
  wire [10:0] nl_z_out_2;
  wire [9:0] z_out_3;
  wire [10:0] nl_z_out_3;
  wire [4:0] z_out_4;
  wire [5:0] nl_z_out_4;
  wire [10:0] z_out_5;
  wire [11:0] nl_z_out_5;
  wire [12:0] z_out_6;
  wire [13:0] nl_z_out_6;
  wire or_tmp_393;
  wire [10:0] z_out_7;
  wire [11:0] nl_z_out_7;
  wire [6:0] z_out_8;
  wire [7:0] nl_z_out_8;
  reg [31:0] current_filter_13_lpi_2;
  reg [31:0] current_filter_12_lpi_2;
  reg [31:0] current_filter_14_lpi_2;
  reg [31:0] current_filter_11_lpi_2;
  reg [31:0] current_filter_15_lpi_2;
  reg [31:0] current_filter_10_lpi_2;
  reg [31:0] current_filter_16_lpi_2;
  reg [31:0] current_filter_9_lpi_2;
  reg [31:0] current_filter_17_lpi_2;
  reg [31:0] current_filter_8_lpi_2;
  reg [31:0] current_filter_18_lpi_2;
  reg [31:0] current_filter_7_lpi_2;
  reg [31:0] current_filter_19_lpi_2;
  reg [31:0] current_filter_6_lpi_2;
  reg [31:0] current_filter_20_lpi_2;
  reg [31:0] current_filter_5_lpi_2;
  reg [31:0] current_filter_21_lpi_2;
  reg [31:0] current_filter_4_lpi_2;
  reg [31:0] current_filter_22_lpi_2;
  reg [31:0] current_filter_3_lpi_2;
  reg [31:0] current_filter_23_lpi_2;
  reg [31:0] current_filter_2_lpi_2;
  reg [31:0] current_filter_24_lpi_2;
  reg [31:0] current_filter_1_lpi_2;
  reg [31:0] shift_register_1_lpi_2;
  reg [31:0] shift_register_2_lpi_2;
  reg [31:0] shift_register_3_lpi_2;
  reg [31:0] shift_register_4_lpi_2;
  reg [31:0] shift_register_5_lpi_2;
  reg [31:0] shift_register_6_lpi_2;
  reg [31:0] shift_register_7_lpi_2;
  reg [31:0] shift_register_8_lpi_2;
  reg [31:0] shift_register_9_lpi_2;
  reg [31:0] shift_register_10_lpi_2;
  reg [31:0] shift_register_11_lpi_2;
  reg [31:0] shift_register_12_lpi_2;
  reg [31:0] shift_register_13_lpi_2;
  reg [31:0] shift_register_14_lpi_2;
  reg [31:0] shift_register_15_lpi_2;
  reg [31:0] shift_register_16_lpi_2;
  reg [31:0] shift_register_17_lpi_2;
  reg [31:0] shift_register_18_lpi_2;
  reg [31:0] shift_register_19_lpi_2;
  reg [31:0] shift_register_20_lpi_2;
  reg [31:0] shift_register_21_lpi_2;
  reg [31:0] shift_register_22_lpi_2;
  reg [31:0] shift_register_23_lpi_2;
  reg [31:0] shift_register_24_lpi_2;
  reg [31:0] shift_register_25_lpi_2;
  reg [31:0] shift_register_26_lpi_2;
  reg [31:0] shift_register_27_lpi_2;
  reg [31:0] shift_register_28_lpi_2;
  reg [31:0] shift_register_29_lpi_2;
  reg [31:0] shift_register_30_lpi_2;
  reg [31:0] shift_register_31_lpi_2;
  reg [31:0] shift_register_32_lpi_2;
  reg [31:0] shift_register_33_lpi_2;
  reg [31:0] shift_register_34_lpi_2;
  reg [31:0] shift_register_35_lpi_2;
  reg [31:0] shift_register_36_lpi_2;
  reg [31:0] shift_register_37_lpi_2;
  reg [31:0] shift_register_38_lpi_2;
  reg [31:0] shift_register_39_lpi_2;
  reg [31:0] shift_register_40_lpi_2;
  reg [31:0] shift_register_41_lpi_2;
  reg [31:0] shift_register_42_lpi_2;
  reg [31:0] shift_register_43_lpi_2;
  reg [31:0] shift_register_44_lpi_2;
  reg [31:0] shift_register_45_lpi_2;
  reg [31:0] shift_register_46_lpi_2;
  reg [31:0] shift_register_47_lpi_2;
  reg [31:0] shift_register_48_lpi_2;
  reg [31:0] shift_register_49_lpi_2;
  reg [31:0] shift_register_50_lpi_2;
  reg [31:0] shift_register_51_lpi_2;
  reg [31:0] shift_register_52_lpi_2;
  reg [31:0] shift_register_53_lpi_2;
  reg [31:0] shift_register_54_lpi_2;
  reg [31:0] shift_register_55_lpi_2;
  reg [31:0] shift_register_56_lpi_2;
  reg [31:0] shift_register_57_lpi_2;
  reg [31:0] shift_register_58_lpi_2;
  reg [31:0] shift_register_59_lpi_2;
  reg [31:0] shift_register_60_lpi_2;
  reg [31:0] shift_register_61_lpi_2;
  reg [31:0] shift_register_62_lpi_2;
  reg [31:0] shift_register_63_lpi_2;
  reg [31:0] shift_register_64_lpi_2;
  reg [31:0] shift_register_65_lpi_2;
  reg [31:0] shift_register_66_lpi_2;
  reg [31:0] shift_register_67_lpi_2;
  reg [31:0] shift_register_68_lpi_2;
  reg [31:0] shift_register_69_lpi_2;
  reg [31:0] shift_register_70_lpi_2;
  reg [31:0] shift_register_71_lpi_2;
  reg [31:0] shift_register_72_lpi_2;
  reg [31:0] shift_register_73_lpi_2;
  reg [31:0] shift_register_74_lpi_2;
  reg [31:0] shift_register_75_lpi_2;
  reg [31:0] shift_register_76_lpi_2;
  reg [31:0] shift_register_77_lpi_2;
  reg [31:0] shift_register_78_lpi_2;
  reg [31:0] shift_register_79_lpi_2;
  reg [31:0] shift_register_80_lpi_2;
  reg [31:0] shift_register_81_lpi_2;
  reg [31:0] shift_register_82_lpi_2;
  reg [31:0] shift_register_83_lpi_2;
  reg [31:0] shift_register_84_lpi_2;
  reg [31:0] shift_register_85_lpi_2;
  reg [31:0] shift_register_86_lpi_2;
  reg [31:0] shift_register_87_lpi_2;
  reg [31:0] shift_register_88_lpi_2;
  reg [31:0] shift_register_89_lpi_2;
  reg [31:0] shift_register_90_lpi_2;
  reg [31:0] shift_register_91_lpi_2;
  reg [31:0] shift_register_92_lpi_2;
  reg [31:0] shift_register_93_lpi_2;
  reg [31:0] shift_register_94_lpi_2;
  reg [31:0] shift_register_95_lpi_2;
  reg [31:0] shift_register_96_lpi_2;
  reg [31:0] shift_register_97_lpi_2;
  reg [31:0] shift_register_98_lpi_2;
  reg [31:0] shift_register_99_lpi_2;
  reg [31:0] shift_register_100_lpi_2;
  reg [31:0] shift_register_101_lpi_2;
  reg [31:0] shift_register_102_lpi_2;
  reg [31:0] shift_register_103_lpi_2;
  reg [31:0] shift_register_104_lpi_2;
  reg [31:0] shift_register_105_lpi_2;
  reg [31:0] shift_register_106_lpi_2;
  reg [31:0] shift_register_107_lpi_2;
  reg [31:0] shift_register_108_lpi_2;
  reg [31:0] shift_register_109_lpi_2;
  reg [31:0] shift_register_110_lpi_2;
  reg [31:0] shift_register_111_lpi_2;
  reg [31:0] shift_register_112_lpi_2;
  reg [31:0] shift_register_113_lpi_2;
  reg [31:0] shift_register_114_lpi_2;
  reg [31:0] shift_register_115_lpi_2;
  reg [31:0] catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_2;
  reg [31:0] max_pool_shift_register_4_lpi_2;
  reg [31:0] max_pool_shift_register_5_lpi_2;
  reg [31:0] max_pool_shift_register_6_lpi_2;
  reg [31:0] max_pool_shift_register_7_lpi_2;
  reg [31:0] max_pool_shift_register_8_lpi_2;
  reg [31:0] max_pool_shift_register_9_lpi_2;
  reg [31:0] max_pool_shift_register_10_lpi_2;
  reg [31:0] max_pool_shift_register_11_lpi_2;
  reg [31:0] max_pool_shift_register_12_lpi_2;
  reg [31:0] max_pool_shift_register_13_lpi_2;
  reg [31:0] max_pool_shift_register_14_lpi_2;
  reg [31:0] max_pool_shift_register_15_lpi_2;
  reg [31:0] max_pool_shift_register_16_lpi_2;
  reg [31:0] max_pool_shift_register_17_lpi_2;
  reg [31:0] max_pool_shift_register_18_lpi_2;
  reg [31:0] max_pool_shift_register_19_lpi_2;
  reg [31:0] max_pool_shift_register_20_lpi_2;
  reg [31:0] max_pool_shift_register_21_lpi_2;
  reg [31:0] max_pool_shift_register_22_lpi_2;
  reg [31:0] max_pool_shift_register_23_lpi_2;
  reg [31:0] max_pool_shift_register_24_lpi_2;
  reg [31:0] max_pool_shift_register_25_lpi_2;
  reg [31:0] max_pool_shift_register_26_lpi_2;
  reg [31:0] catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_asn_1_ncse_lpi_2;
  reg [31:0] max_pool_shift_register_27_lpi_2;
  reg [31:0] max_pool_shift_register_29_lpi_2;
  reg [27:0] num_input_images_sva;
  reg [27:0] num_output_images_sva;
  reg [27:0] main_loop_1_o_sva;
  reg [31:0] current_filter_0_lpi_4;
  reg [6:0] catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva;
  reg [1:0] catapult_conv2d_28_28_5_5_1_convolve_do_pcnxt_sva;
  reg [4:0] catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc2_sva;
  reg [4:0] catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_2_1;
  reg [4:0] catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_1_1;
  reg catapult_conv2d_28_28_5_5_1_convolve_do_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_6_svs_1;
  reg [31:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_3;
  reg [31:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_4;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_9_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_9_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_13_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_13_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_21_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_21_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_5_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_5_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_10_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_10_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_14_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_14_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_22_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_22_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_6_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_6_lpi_4_dfm_2;
  reg outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_2;
  reg outer_conv_loop_3_operator_28_true_acc_cse_4_sva_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_23_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_23_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_7_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_7_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_12_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_12_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_16_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_16_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_20_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_20_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_24_lpi_4_dfm_1_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_24_lpi_4_dfm_1_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_8_lpi_4_dfm_1_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_8_lpi_4_dfm_1_2;
  reg [2:0] operator_28_true_1_acc_1_psp_sva_1;
  wire [3:0] nl_operator_28_true_1_acc_1_psp_sva_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_1_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_1_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_2_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_2_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_3_lpi_4_dfm_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_3_lpi_4_dfm_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_4_lpi_4_dfm_1_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_4_lpi_4_dfm_1_2;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_lpi_4_dfm_1_1;
  reg catapult_conv2d_28_28_5_5_1_in_bounds_return_lpi_4_dfm_1_2;
  reg [63:0] inner_conv_loop_inner_conv_loop_and_9_itm_1;
  reg outer_conv_loop_3_operator_28_true_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_5_1_5_itm_1;
  reg [63:0] inner_conv_loop_inner_conv_loop_and_12_itm_1;
  reg [63:0] inner_conv_loop_acc_18_itm_1;
  wire [65:0] nl_inner_conv_loop_acc_18_itm_1;
  reg [63:0] inner_conv_loop_acc_17_itm_1;
  wire [65:0] nl_inner_conv_loop_acc_17_itm_1;
  reg [63:0] inner_conv_loop_acc_17_itm_2;
  reg [63:0] inner_conv_loop_acc_16_itm_1;
  wire [65:0] nl_inner_conv_loop_acc_16_itm_1;
  reg [63:0] inner_conv_loop_acc_15_itm_1;
  wire [65:0] nl_inner_conv_loop_acc_15_itm_1;
  reg [63:0] inner_conv_loop_acc_24_itm_1;
  wire [65:0] nl_inner_conv_loop_acc_24_itm_1;
  reg [63:0] inner_conv_loop_inner_conv_loop_and_itm_1;
  reg [63:0] inner_conv_loop_acc_20_itm_1;
  wire [65:0] nl_inner_conv_loop_acc_20_itm_1;
  reg operator_28_true_1_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_psp_1_4_2_itm_1;
  reg operator_28_true_1_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_psp_1_4_2_itm_2;
  reg [63:0] inner_conv_loop_acc_13_itm_1;
  wire [64:0] nl_inner_conv_loop_acc_13_itm_1;
  reg [63:0] inner_conv_loop_acc_13_itm_2;
  reg [63:0] inner_conv_loop_inner_conv_loop_and_6_itm_1;
  reg [63:0] inner_conv_loop_acc_19_itm_1;
  wire [65:0] nl_inner_conv_loop_acc_19_itm_1;
  reg [63:0] inner_conv_loop_acc_itm_1;
  wire [65:0] nl_inner_conv_loop_acc_itm_1;
  reg [31:0] catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_5_lpi_4_itm_1;
  reg [31:0] catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_33_lpi_4_itm_1;
  reg [31:0] catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_89_lpi_4_itm_1;
  wire [4:0] catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_dfm_mx0;
  wire [31:0] catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_4_dfm_mx1w0;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_2_mx1c1;
  wire catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_2_1_mx1c1;
  wire inner_loop_i_sva_mx0c1;
  wire [27:0] load_images_loop_i_sva_2;
  wire [28:0] nl_load_images_loop_i_sva_2;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_mx0w0;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_mx0c1;
  wire catapult_conv2d_28_28_5_5_1_convolve_count_9_0_sva_mx0c0;
  wire catapult_conv2d_28_28_5_5_1_convolve_count_9_0_sva_mx0c4;
  wire [27:0] main_loop_1_o_sva_2;
  wire [28:0] nl_main_loop_1_o_sva_2;
  wire [4:0] catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva_mx1;
  wire outer_conv_loop_3_operator_28_true_or_2;
  wire catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c0;
  wire catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c2;
  wire catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c3;
  wire catapult_conv2d_28_28_5_5_1_convolve_row_slc_catapult_conv2d_28_28_5_5_1_convolve_catapult_conv2d_28_28_5_5_1_convolve_row_mux_1_cse_sva_1;
  reg [31:0] reg_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b_cse;
  wire catapult_conv2d_28_28_5_5_1_convolve_first_and_cse;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_9;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_9;
  reg [1:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_8_7;
  wire nand_24_seb;
  reg [1:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9;
  reg [8:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_9;
  reg reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd;
  reg catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_9;
  wire max_pool_shift_register_and_24_cse;
  wire and_750_cse;
  wire catapult_conv2d_28_28_5_5_1_convolve_center_pixel_and_4_cse;
  wire and_1057_cse;
  wire or_595_tmp;
  wire main_loop_4_or_itm;
  wire catapult_conv2d_28_28_5_5_1_max_cd_acc_itm_32;
  wire operator_28_true_acc_itm_5_1;
  wire operator_28_true_1_acc_itm_5_1;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_itm_6_1;
  wire outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_itm_3_1;
  wire outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_itm_3_1;
  wire outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_itm_5_1;
  wire [48:0] outer_conv_loop_5_inner_conv_loop_5_acc_1_itm_63_15_1;
  wire outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1;
  wire outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1;
  wire operator_28_true_3_acc_itm_4_1;
  wire catapult_conv2d_28_28_5_5_1_convolve_count_and_ssc;
  reg reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd;
  reg [8:0] reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1;
  reg [1:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7;
  reg [6:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0;
  reg [1:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_8_7;
  reg [6:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_6_0;
  wire or_546_ssc;
  wire nand_27_seb;
  wire or_589_ssc;
  reg [1:0] reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_1_ftd;
  reg [6:0] reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_1_ftd_1;
  reg [1:0] reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd_1_8_7;
  reg [6:0] reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd_1_6_0;
  reg [1:0] reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_1_ftd;
  reg [6:0] reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_1_ftd_1;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_nor_5_cse;

  wire[4:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_2_acc_nl;
  wire[5:0] nl_catapult_conv2d_28_28_5_5_1_convolve_do_if_2_acc_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_mux_1_nl;
  wire reg_catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_rgt_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_or_4_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_or_5_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_or_6_nl;
  wire[10:0] operator_28_true_4_acc_nl;
  wire[11:0] nl_operator_28_true_4_acc_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_aelse_mux_22_nl;
  wire or_461_nl;
  wire[1:0] mux_nl;
  wire[1:0] catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_and_nl;
  wire[1:0] catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_mux1h_nl;
  wire[6:0] mux_9_nl;
  wire[6:0] catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_and_1_nl;
  wire[6:0] catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_mux1h_2_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_count_not_1_nl;
  wire[6:0] catapult_conv2d_28_28_5_5_1_perform_max_pool_out_pixel_catapult_conv2d_28_28_5_5_1_perform_max_pool_out_pixel_and_nl;
  wire[6:0] catapult_conv2d_28_28_5_5_1_perform_max_pool_out_pixel_mux_nl;
  wire nand_28_nl;
  wire or_471_nl;
  wire[5:0] load_images_loop_acc_10_nl;
  wire[6:0] nl_load_images_loop_acc_10_nl;
  wire[31:0] catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_and_nl;
  wire[4:0] mux1h_1_nl;
  wire and_1005_nl;
  wire not_274_nl;
  wire[4:0] catapult_conv2d_28_28_5_5_1_convolve_row_mux_5_nl;
  wire not_261_nl;
  wire[1:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_or_1_nl;
  wire[1:0] catapult_conv2d_28_28_5_5_1_convolve_center_pixel_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_mux_nl;
  wire[8:0] mux1h_nl;
  wire nor_62_nl;
  wire and_1076_nl;
  wire operator_28_true_1_or_nl;
  wire[5:0] operator_28_true_acc_1_nl;
  wire[6:0] nl_operator_28_true_acc_1_nl;
  wire[4:0] outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl;
  wire[5:0] nl_outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl;
  wire[6:0] catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_and_1_nl;
  wire[6:0] catapult_conv2d_28_28_5_5_1_send_results_end_acc_nl;
  wire[7:0] nl_catapult_conv2d_28_28_5_5_1_send_results_end_acc_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_aelse_mux_nl;
  wire or_514_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_center_pixel_mux1h_17_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_center_pixel_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_or_nl;
  wire catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_and_1_nl;
  wire or_522_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_25_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_26_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_4_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_5_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_7_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_10_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_11_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_17_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_18_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_19_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_20_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_21_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_22_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_23_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_24_nl;
  wire operator_28_true_1_not_6_nl;
  wire inner_conv_loop_not_39_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_1_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_3_nl;
  wire inner_conv_loop_not_32_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_13_nl;
  wire operator_28_true_not_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_14_nl;
  wire operator_28_true_not_3_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_15_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_16_nl;
  wire main_loop_4_main_loop_4_mux_nl;
  wire[1:0] main_loop_4_mux1h_4_nl;
  wire[6:0] main_loop_4_mux1h_5_nl;
  wire[3:0] outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl;
  wire[4:0] nl_outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl;
  wire[3:0] outer_conv_loop_1_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl;
  wire[4:0] nl_outer_conv_loop_1_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl;
  wire[3:0] outer_conv_loop_1_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl;
  wire[4:0] nl_outer_conv_loop_1_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl;
  wire[3:0] outer_conv_loop_1_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl;
  wire[4:0] nl_outer_conv_loop_1_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl;
  wire[32:0] catapult_conv2d_28_28_5_5_1_max_cd_acc_nl;
  wire[33:0] nl_catapult_conv2d_28_28_5_5_1_max_cd_acc_nl;
  wire[31:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_catapult_conv2d_28_28_5_5_1_convolve_do_if_and_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_first_not_nl;
  wire catapult_conv2d_28_28_5_5_1_max_cd_and_1_nl;
  wire catapult_conv2d_28_28_5_5_1_max_cd_and_2_nl;
  wire catapult_conv2d_28_28_5_5_1_max_cd_and_3_nl;
  wire catapult_conv2d_28_28_5_5_1_max_cd_and_4_nl;
  wire catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_nl;
  wire or_85_nl;
  wire or_179_nl;
  wire[5:0] operator_28_true_acc_nl;
  wire[6:0] nl_operator_28_true_acc_nl;
  wire[5:0] operator_28_true_1_acc_nl;
  wire[6:0] nl_operator_28_true_1_acc_nl;
  wire[6:0] catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_nl;
  wire[7:0] nl_catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_nl;
  wire[3:0] outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl;
  wire[4:0] nl_outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl;
  wire[3:0] outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl;
  wire[4:0] nl_outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl;
  wire[5:0] outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_nl;
  wire[6:0] nl_outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_nor_2_nl;
  wire[29:0] catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_nor_nl;
  wire[29:0] catapult_conv2d_28_28_5_5_1_convolve_do_nor_2_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_nor_1_nl;
  wire[3:0] catapult_conv2d_28_28_5_5_1_convolve_row_slc_catapult_conv2d_28_28_5_5_1_convolve_catapult_conv2d_28_28_5_5_1_convolve_row_mux_nl;
  wire[63:0] outer_conv_loop_5_inner_conv_loop_5_acc_1_nl;
  wire[64:0] nl_outer_conv_loop_5_inner_conv_loop_5_acc_1_nl;
  wire[3:0] outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl;
  wire[4:0] nl_outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl;
  wire[3:0] outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl;
  wire[4:0] nl_outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl;
  wire[4:0] operator_28_true_3_acc_nl;
  wire[5:0] nl_operator_28_true_3_acc_nl;
  wire[10:0] operator_28_true_acc_nl_1;
  wire[11:0] nl_operator_28_true_acc_nl_1;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_if_qelse_mux_2_nl;
  wire[8:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_qelse_mux_5_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_if_1_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_2_nl;
  wire[29:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_1_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_nl;
  wire[29:0] catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_1_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_if_1_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_1_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_do_aelse_catapult_conv2d_28_28_5_5_1_convolve_do_aelse_or_nl;
  wire main_loop_4_main_loop_4_and_nl;
  wire[29:0] mux_6_nl;
  wire[29:0] main_loop_3_main_loop_3_nor_nl;
  wire[29:0] main_loop_3_nor_1_nl;
  wire nand_29_nl;
  wire catapult_conv2d_28_28_5_5_1_relu_fn_mux_nl;
  wire catapult_conv2d_28_28_5_5_1_relu_fn_catapult_conv2d_28_28_5_5_1_relu_fn_and_2_nl;
  wire[1:0] catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_if_catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_if_and_nl;
  wire[6:0] catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_if_mux_1_nl;
  wire catapult_conv2d_28_28_5_5_1_convolve_center_pixel_mux_nl;
  wire and_738_nl;
  wire and_740_nl;
  wire[29:0] acc_nl;
  wire[30:0] nl_acc_nl;
  wire[24:0] main_loop_2_or_3_nl;
  wire[24:0] main_loop_2_mux1h_8_nl;
  wire[2:0] main_loop_2_mux1h_9_nl;
  wire[2:0] outer_conv_loop_2_operator_28_true_acc_2_nl;
  wire[3:0] nl_outer_conv_loop_2_operator_28_true_acc_2_nl;
  wire main_loop_2_or_4_nl;
  wire[27:0] main_loop_2_mux1h_10_nl;
  wire main_loop_2_or_5_nl;
  wire[33:0] acc_1_nl;
  wire[34:0] nl_acc_1_nl;
  wire[28:0] catapult_conv2d_28_28_5_5_1_in_bounds_if_3_or_3_nl;
  wire[28:0] catapult_conv2d_28_28_5_5_1_in_bounds_if_3_mux1h_5_nl;
  wire[2:0] catapult_conv2d_28_28_5_5_1_in_bounds_if_3_mux1h_6_nl;
  wire[2:0] outer_conv_loop_2_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_1_nl;
  wire[3:0] nl_outer_conv_loop_2_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_1_nl;
  wire catapult_conv2d_28_28_5_5_1_in_bounds_if_3_or_4_nl;
  wire[31:0] catapult_conv2d_28_28_5_5_1_in_bounds_if_3_mux1h_7_nl;
  wire catapult_conv2d_28_28_5_5_1_in_bounds_if_3_or_5_nl;
  wire catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_4_nl;
  wire[1:0] catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_5_nl;
  wire catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_6_nl;
  wire[5:0] catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_7_nl;
  wire[9:0] main_loop_3_mux_1_nl;
  wire[2:0] catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_and_4_nl;
  wire[1:0] catapult_conv2d_28_28_5_5_1_convolve_do_mux_6_nl;
  wire[1:0] catapult_conv2d_28_28_5_5_1_convolve_do_mux_7_nl;
  wire[3:0] catapult_conv2d_28_28_5_5_1_convolve_do_qif_mux_4_nl;
  wire main_loop_5_main_loop_5_and_2_nl;
  wire main_loop_5_mux_2_nl;
  wire[2:0] main_loop_5_main_loop_5_and_3_nl;
  wire[2:0] main_loop_5_mux_3_nl;
  wire not_295_nl;
  wire[5:0] main_loop_5_mux1h_4_nl;
  wire[9:0] main_loop_5_mux1h_5_nl;
  wire[5:0] operator_28_true_mux_2_nl;

  // Interconnect Declarations for Component Instantiations 
  wire  nl_catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_inst_current_image_rsci_iswt0_pff;
  assign nl_catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_inst_current_image_rsci_iswt0_pff
      = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2 & (fsm_output[2]);
  wire  nl_catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_inst_convolution_output_rsci_iswt0_1_pff;
  assign nl_catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_inst_convolution_output_rsci_iswt0_1_pff
      = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_7 & (~ catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_6)
      & (fsm_output[8]);
  wire  nl_catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_inst_relu_output_rsci_iswt0_pff;
  assign nl_catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_inst_relu_output_rsci_iswt0_pff
      = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2 & (fsm_output[12]);
  wire  nl_catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_inst_output_buffer_rsci_iswt0_pff;
  assign nl_catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_inst_output_buffer_rsci_iswt0_pff
      = (fsm_output[19]) | (catapult_conv2d_28_28_5_5_1_convolve_do_stage_0 & (fsm_output[16]));
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_C_0_tr0;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_C_0_tr0 = ~ (z_out_1[28]);
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_load_images_loop_C_1_tr0;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_load_images_loop_C_1_tr0
      = ~ (z_out[28]);
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_1_C_0_tr0;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_1_C_0_tr0 = ~
      load_images_loop_acc_3_cse_sva_28;
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_2_C_0_tr0;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_2_C_0_tr0 = ~
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0;
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_catapult_conv2d_28_28_5_5_1_convolve_do_C_0_tr0;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_catapult_conv2d_28_28_5_5_1_convolve_do_C_0_tr0
      = (~(catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_4 | catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2))
      & (~(catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_3 | catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_5))
      & (~(catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_6 | catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_7
      | catapult_conv2d_28_28_5_5_1_convolve_do_stage_0));
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_inner_loop_C_1_tr0;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_inner_loop_C_1_tr0 = ~ (z_out[28]);
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_3_C_0_tr0;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_3_C_0_tr0 = and_dcpl_30
      & max_pool_sva;
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_3_C_0_tr1;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_3_C_0_tr1 = and_dcpl_30
      & (~ max_pool_sva);
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4_tr0;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4_tr0
      = ~ inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1;
  wire  nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_copy_loop_C_0_tr0;
  assign nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_copy_loop_C_0_tr0 = (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1==9'b110000111);
  ccs_in_v1 #(.rscid(32'sd5),
  .width(32'sd1)) bias_rsci (
      .dat(bias_rsc_dat),
      .idat(bias_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd6),
  .width(32'sd1)) relu_rsci (
      .dat(relu_rsc_dat),
      .idat(relu_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd7),
  .width(32'sd1)) max_pool_rsci (
      .dat(max_pool_rsc_dat),
      .idat(max_pool_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd8),
  .width(32'sd28)) num_input_images_rsci (
      .dat(num_input_images_rsc_dat),
      .idat(num_input_images_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd9),
  .width(32'sd28)) num_output_images_rsci (
      .dat(num_output_images_rsc_dat),
      .idat(num_output_images_rsci_idat)
    );
  catapult_conv2d_28_28_5_5_1_run_image_channel_rsci catapult_conv2d_28_28_5_5_1_run_image_channel_rsci_inst
      (
      .clk(clk),
      .rstn(rstn),
      .image_channel_rsc_dat(image_channel_rsc_dat),
      .image_channel_rsc_vld(image_channel_rsc_vld),
      .image_channel_rsc_rdy(image_channel_rsc_rdy),
      .run_wen(run_wen),
      .image_channel_rsci_oswt(reg_image_channel_rsci_iswt0_cse),
      .image_channel_rsci_wen_comp(image_channel_rsci_wen_comp),
      .image_channel_rsci_idat_mxwt(image_channel_rsci_idat_mxwt)
    );
  catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1 catapult_conv2d_28_28_5_5_1_run_weight_memory_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .weight_memory_rsci_q_d(weight_memory_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .weight_memory_rsci_oswt(reg_weight_memory_rsci_iswt0_cse),
      .weight_memory_rsci_q_d_mxwt(weight_memory_rsci_q_d_mxwt),
      .weight_memory_rsci_re_d_pff(weight_memory_rsci_re_d_iff),
      .weight_memory_rsci_oswt_pff(or_tmp_260)
    );
  catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1 catapult_conv2d_28_28_5_5_1_run_bias_memory_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .bias_memory_rsci_q_d(bias_memory_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .bias_memory_rsci_oswt(reg_bias_memory_rsci_iswt0_cse),
      .bias_memory_rsci_q_d_mxwt(bias_memory_rsci_q_d_mxwt),
      .bias_memory_rsci_re_d_pff(bias_memory_rsci_re_d_iff),
      .bias_memory_rsci_oswt_pff(and_761_rmff)
    );
  catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci catapult_conv2d_28_28_5_5_1_run_output_image_channel_rsci_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_image_channel_rsc_dat(output_image_channel_rsc_dat),
      .output_image_channel_rsc_vld(output_image_channel_rsc_vld),
      .output_image_channel_rsc_rdy(output_image_channel_rsc_rdy),
      .run_wen(run_wen),
      .output_image_channel_rsci_oswt(reg_output_image_channel_rsci_iswt0_cse),
      .output_image_channel_rsci_wen_comp(output_image_channel_rsci_wen_comp),
      .output_image_channel_rsci_idat(output_image_channel_rsci_idat)
    );
  catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1 catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .current_image_rsci_q_d(current_image_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .current_image_rsci_oswt_1(reg_current_image_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .current_image_rsci_q_d_mxwt(current_image_rsci_q_d_mxwt),
      .current_image_rsci_we_d_pff(current_image_rsci_we_d_iff),
      .current_image_rsci_iswt0_pff(nl_catapult_conv2d_28_28_5_5_1_run_current_image_rsci_1_inst_current_image_rsci_iswt0_pff),
      .current_image_rsci_re_d_pff(current_image_rsci_re_d_iff),
      .current_image_rsci_oswt_1_pff(and_757_rmff)
    );
  catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1 catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .convolution_output_rsci_q_d(convolution_output_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .convolution_output_rsci_oswt(reg_convolution_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .convolution_output_rsci_q_d_mxwt(convolution_output_rsci_q_d_mxwt),
      .convolution_output_rsci_we_d_pff(convolution_output_rsci_we_d_iff),
      .convolution_output_rsci_iswt0_1_pff(nl_catapult_conv2d_28_28_5_5_1_run_convolution_output_rsci_1_inst_convolution_output_rsci_iswt0_1_pff),
      .convolution_output_rsci_re_d_pff(convolution_output_rsci_re_d_iff),
      .convolution_output_rsci_oswt_pff(or_432_rmff)
    );
  catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1 catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .relu_output_rsci_q_d(relu_output_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .relu_output_rsci_oswt_1(reg_relu_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .relu_output_rsci_q_d_mxwt(relu_output_rsci_q_d_mxwt),
      .relu_output_rsci_we_d_pff(relu_output_rsci_we_d_iff),
      .relu_output_rsci_iswt0_pff(nl_catapult_conv2d_28_28_5_5_1_run_relu_output_rsci_1_inst_relu_output_rsci_iswt0_pff),
      .relu_output_rsci_re_d_pff(relu_output_rsci_re_d_iff),
      .relu_output_rsci_oswt_1_pff(or_430_rmff)
    );
  catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1 catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_buffer_rsci_q_d(output_buffer_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .output_buffer_rsci_oswt_1(reg_output_buffer_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .output_buffer_rsci_q_d_mxwt(output_buffer_rsci_q_d_mxwt),
      .output_buffer_rsci_we_d_pff(output_buffer_rsci_we_d_iff),
      .output_buffer_rsci_iswt0_pff(nl_catapult_conv2d_28_28_5_5_1_run_output_buffer_rsci_1_inst_output_buffer_rsci_iswt0_pff),
      .output_buffer_rsci_re_d_pff(output_buffer_rsci_re_d_iff),
      .output_buffer_rsci_oswt_1_pff(and_736_rmff)
    );
  catapult_conv2d_28_28_5_5_1_run_weight_memory_triosy_obj catapult_conv2d_28_28_5_5_1_run_weight_memory_triosy_obj_inst
      (
      .weight_memory_triosy_lz(weight_memory_triosy_lz),
      .run_wten(run_wten),
      .weight_memory_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_28_28_5_5_1_run_bias_memory_triosy_obj catapult_conv2d_28_28_5_5_1_run_bias_memory_triosy_obj_inst
      (
      .bias_memory_triosy_lz(bias_memory_triosy_lz),
      .run_wten(run_wten),
      .bias_memory_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_28_28_5_5_1_run_bias_triosy_obj catapult_conv2d_28_28_5_5_1_run_bias_triosy_obj_inst
      (
      .bias_triosy_lz(bias_triosy_lz),
      .run_wten(run_wten),
      .bias_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_28_28_5_5_1_run_relu_triosy_obj catapult_conv2d_28_28_5_5_1_run_relu_triosy_obj_inst
      (
      .relu_triosy_lz(relu_triosy_lz),
      .run_wten(run_wten),
      .relu_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_28_28_5_5_1_run_max_pool_triosy_obj catapult_conv2d_28_28_5_5_1_run_max_pool_triosy_obj_inst
      (
      .max_pool_triosy_lz(max_pool_triosy_lz),
      .run_wten(run_wten),
      .max_pool_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_28_28_5_5_1_run_num_input_images_triosy_obj catapult_conv2d_28_28_5_5_1_run_num_input_images_triosy_obj_inst
      (
      .num_input_images_triosy_lz(num_input_images_triosy_lz),
      .run_wten(run_wten),
      .num_input_images_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_28_28_5_5_1_run_num_output_images_triosy_obj catapult_conv2d_28_28_5_5_1_run_num_output_images_triosy_obj_inst
      (
      .num_output_images_triosy_lz(num_output_images_triosy_lz),
      .run_wten(run_wten),
      .num_output_images_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_28_28_5_5_1_run_wait_dp catapult_conv2d_28_28_5_5_1_run_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z),
      .run_wen(run_wen),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z_oreg),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z_oreg(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z_oreg)
    );
  catapult_conv2d_28_28_5_5_1_run_staller catapult_conv2d_28_28_5_5_1_run_staller_inst
      (
      .clk(clk),
      .rstn(rstn),
      .catapult_conv2d_28_28_5_5_1_stall(catapult_conv2d_28_28_5_5_1_stall),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .image_channel_rsci_wen_comp(image_channel_rsci_wen_comp),
      .output_image_channel_rsci_wen_comp(output_image_channel_rsci_wen_comp)
    );
  catapult_conv2d_28_28_5_5_1_run_run_fsm catapult_conv2d_28_28_5_5_1_run_run_fsm_inst
      (
      .clk(clk),
      .rstn(rstn),
      .run_wen(run_wen),
      .fsm_output(fsm_output),
      .main_C_0_tr0(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_C_0_tr0),
      .main_loop_C_0_tr0(and_dcpl_30),
      .load_images_loop_C_1_tr0(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_load_images_loop_C_1_tr0),
      .main_C_1_tr0(exit_main_loop_1_sva_mx0),
      .main_loop_1_C_0_tr0(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_1_C_0_tr0),
      .main_loop_2_C_0_tr0(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_2_C_0_tr0),
      .catapult_conv2d_28_28_5_5_1_convolve_do_C_0_tr0(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_catapult_conv2d_28_28_5_5_1_convolve_do_C_0_tr0),
      .inner_loop_C_1_tr0(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_inner_loop_C_1_tr0),
      .main_loop_3_C_0_tr0(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_3_C_0_tr0),
      .main_loop_3_C_0_tr1(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_main_loop_3_C_0_tr1),
      .catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4_tr0(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_C_4_tr0),
      .stride_loop_C_2_tr0(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm),
      .copy_loop_C_0_tr0(nl_catapult_conv2d_28_28_5_5_1_run_run_fsm_inst_copy_loop_C_0_tr0),
      .main_loop_5_C_0_tr0(and_dcpl_30),
      .main_loop_1_C_4_tr0(exit_main_loop_1_sva_mx0)
    );
  assign num_output_images_and_cse = run_wen & (~ and_dcpl_53);
  assign shift_register_and_1_cse = run_wen & (~(and_265_cse | (or_dcpl_29 & (fsm_output[8]))));
  assign shift_register_and_2_cse = run_wen & ((and_dcpl_57 & and_dcpl_11 & (fsm_output[8]))
      | or_tmp_70);
  assign shift_register_and_3_cse = run_wen & ((~(or_36_cse | and_265_cse)) | (fsm_output[9]));
  assign shift_register_and_4_cse = run_wen & (or_tmp_75 | (fsm_output[9]));
  assign shift_register_and_85_cse = run_wen & (~(and_265_cse | and_290_cse));
  assign or_36_cse = (~ catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2) | catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_1;
  assign max_pool_shift_register_and_24_cse = run_wen & (max_pool_sva | inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1)
      & (fsm_output[17]);
  assign or_133_cse = (fsm_output[4]) | (fsm_output[24]);
  assign and_736_rmff = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0 & (fsm_output[23]);
  assign or_430_rmff = (fsm_output[18]) | (fsm_output[14]) | (fsm_output[13]);
  assign and_750_cse = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0 & (fsm_output[12]);
  assign or_432_rmff = and_750_cse | (catapult_conv2d_28_28_5_5_1_convolve_do_stage_0
      & (~ (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9[1]))
      & (~ operator_28_true_return_sva) & (fsm_output[8]));
  assign and_757_rmff = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0 & (z_out_7[6])
      & (fsm_output[8]);
  assign and_761_rmff = bias_sva & (fsm_output[10]);
  assign outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b = reg_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b_cse;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_nor_5_cse = ~((fsm_output[6]) |
      (fsm_output[8]));
  assign and_1057_cse = (~ catapult_conv2d_28_28_5_5_1_convolve_do_stage_0) & (fsm_output[12]);
  assign or_546_ssc = or_dcpl_32 | or_dcpl_141;
  assign nand_27_seb = ~(and_dcpl_100 & (~((fsm_output[20]) | (fsm_output[13]) |
      (fsm_output[17]))));
  assign or_589_ssc = and_1057_cse | catapult_conv2d_28_28_5_5_1_convolve_count_9_0_sva_mx0c4;
  assign catapult_conv2d_28_28_5_5_1_convolve_count_and_ssc = run_wen & (catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2
      | catapult_conv2d_28_28_5_5_1_convolve_count_9_0_sva_mx0c0 | (fsm_output[8])
      | (fsm_output[12]) | catapult_conv2d_28_28_5_5_1_convolve_count_9_0_sva_mx0c4);
  assign catapult_conv2d_28_28_5_5_1_convolve_center_pixel_and_4_cse = run_wen &
      (~ (fsm_output[2]));
  assign and_1000_cse = (~ operator_28_true_3_acc_itm_4_1) & (fsm_output[13]);
  assign or_555_tmp = catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_mx0c1
      | and_1000_cse;
  assign nand_24_seb = ~((~((fsm_output[15]) | (fsm_output[23]) | (fsm_output[14])))
      & (~((fsm_output[7]) | (fsm_output[16]))) & and_dcpl_97);
  assign or_595_tmp = (~((and_dcpl_125 & and_dcpl_127) | and_870_cse)) | (fsm_output[8]);
  assign catapult_conv2d_28_28_5_5_1_convolve_first_and_cse = run_wen & (~ (fsm_output[8]));
  assign catapult_conv2d_28_28_5_5_1_convolve_center_pixel_not_seb = ~(and_dcpl_125
      & and_dcpl_96 & (~ (fsm_output[8])));
  assign main_loop_4_or_itm = (fsm_output[12]) | (~(or_dcpl_161 | (~(or_dcpl_147
      | (fsm_output[16]) | (fsm_output[13])))));
  assign nl_catapult_conv2d_28_28_5_5_1_max_cd_acc_nl = conv_s2u_32_33(relu_output_rsci_q_d_mxwt)
      - conv_s2u_32_33(catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_2);
  assign catapult_conv2d_28_28_5_5_1_max_cd_acc_nl = nl_catapult_conv2d_28_28_5_5_1_max_cd_acc_nl[32:0];
  assign catapult_conv2d_28_28_5_5_1_max_cd_acc_itm_32 = readslicef_33_1_32(catapult_conv2d_28_28_5_5_1_max_cd_acc_nl);
  assign or_85_nl = or_36_cse | (~ operator_28_true_1_slc_operator_28_true_1_acc_5_svs_1);
  assign catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_dfm_mx0 = MUX_v_5_2_2(catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva,
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_2_1, or_85_nl);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_4_dfm_mx1w0 = MUX_v_32_2_2(32'b00000000000000000000000000000000,
      current_image_rsci_q_d_mxwt, catapult_conv2d_28_28_5_5_1_convolve_do_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_6_svs_1);
  assign nl_load_images_loop_i_sva_2 = inner_loop_i_sva + 28'b0000000000000000000000000001;
  assign load_images_loop_i_sva_2 = nl_load_images_loop_i_sva_2[27:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_mx0w0 = or_dcpl_12 & catapult_conv2d_28_28_5_5_1_convolve_do_stage_0;
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_land_1_lpi_3_dfm_mx0w5
      = ~(catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_5_itm | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva[4]));
  assign exit_main_loop_1_sva_mx0 = MUX_s_1_2_2((~ (z_out_1[28])), (~ (z_out[28])),
      fsm_output[24]);
  assign nl_main_loop_1_o_sva_2 = main_loop_1_o_sva + 28'b0000000000000000000000000001;
  assign main_loop_1_o_sva_2 = nl_main_loop_1_o_sva_2[27:0];
  assign or_179_nl = (~ catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2) | main_loop_2_asn_167_itm_1;
  assign main_loop_2_index_4_0_sva_mx0 = MUX_v_5_2_2(catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva,
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva, or_179_nl);
  assign nl_operator_28_true_acc_nl = conv_u2s_5_6(main_loop_2_index_4_0_sva_2) +
      6'b100111;
  assign operator_28_true_acc_nl = nl_operator_28_true_acc_nl[5:0];
  assign operator_28_true_acc_itm_5_1 = readslicef_6_1_5(operator_28_true_acc_nl);
  assign nl_main_loop_2_index_4_0_sva_2 = main_loop_2_index_4_0_sva_mx0 + 5'b00001;
  assign main_loop_2_index_4_0_sva_2 = nl_main_loop_2_index_4_0_sva_2[4:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_mx1 = MUX_v_5_2_2(catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_1_1,
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva, or_36_cse);
  assign catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva_mx1 = MUX_v_5_2_2(catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva,
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva, or_36_cse);
  assign outer_conv_loop_1_operator_28_true_and_2 = (catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva[0])
      & (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[0]);
  assign outer_conv_loop_3_operator_28_true_or_2 = (catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva[0])
      | (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[0]);
  assign nl_operator_28_true_1_acc_nl = conv_s2s_5_6(z_out_2[6:2]) + 6'b111001;
  assign operator_28_true_1_acc_nl = nl_operator_28_true_1_acc_nl[5:0];
  assign operator_28_true_1_acc_itm_5_1 = readslicef_6_1_5(operator_28_true_1_acc_nl);
  assign nl_catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_nl = (z_out_5[10:4]) +
      7'b1001111;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_nl = nl_catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_nl[6:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_itm_6_1 = readslicef_7_1_6(catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_nl);
  assign nl_outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl
      = ({1'b1 , (outer_conv_loop_3_operator_28_true_acc_1_tmp[2:0])}) + 4'b0001;
  assign outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl
      = nl_outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl[3:0];
  assign outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_itm_3_1
      = readslicef_4_1_3(outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl);
  assign nl_outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl
      = ({1'b1 , (outer_conv_loop_3_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_tmp[2:0])})
      + 4'b0001;
  assign outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl
      = nl_outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl[3:0];
  assign outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_itm_3_1
      = readslicef_4_1_3(outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl);
  assign nl_outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_nl = ({catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp
      , catapult_conv2d_28_28_5_5_1_convolve_row_slc_catapult_conv2d_28_28_5_5_1_convolve_catapult_conv2d_28_28_5_5_1_convolve_row_mux_1_cse_sva_1})
      + 6'b000001;
  assign outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_nl = nl_outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_nl[5:0];
  assign outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_itm_5_1 = readslicef_6_1_5(outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_nl);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_nor_2_nl
      = ~((~((catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1[31]) |
      catapult_conv2d_28_28_5_5_1_convolve_do_and_unfl_sva_1)) | catapult_conv2d_28_28_5_5_1_convolve_do_nor_ovfl_sva_1);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_nor_2_nl = ~(MUX_v_30_2_2((catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1[30:1]),
      30'b111111111111111111111111111111, catapult_conv2d_28_28_5_5_1_convolve_do_nor_ovfl_sva_1));
  assign catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_nor_nl
      = ~(MUX_v_30_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_nor_2_nl, 30'b111111111111111111111111111111,
      catapult_conv2d_28_28_5_5_1_convolve_do_and_unfl_sva_1));
  assign catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_nor_1_nl
      = ~((~((catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1[0]) | catapult_conv2d_28_28_5_5_1_convolve_do_nor_ovfl_sva_1))
      | catapult_conv2d_28_28_5_5_1_convolve_do_and_unfl_sva_1);
  assign nl_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1 = conv_s2s_32_33(catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_5)
      + conv_s2s_32_33({catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_nor_2_nl
      , catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_nor_nl
      , catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_nor_1_nl});
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1 = nl_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1[32:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_1_and_unfl_sva_1 = (catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1[32:31]==2'b10);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_ovfl_sva_1 = ~((catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1[32:31]!=2'b01));
  assign nl_catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1 = conv_s2u_48_49(catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_slc_catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_1_63_15_48_1_itm_1)
      + conv_u2u_1_49(catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_slc_catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_1_63_15_0_itm_1);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1 = nl_catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1[48:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_do_and_unfl_sva_1 = (catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1[48])
      & (~((catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1[47:31]==17'b11111111111111111)));
  assign catapult_conv2d_28_28_5_5_1_convolve_do_nor_ovfl_sva_1 = ~((catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1[48])
      | (~((catapult_conv2d_28_28_5_5_1_convolve_do_slc_64_16_sat_sva_1[47:31]!=17'b00000000000000000))));
  assign nl_catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp = (catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva[6:1])
      + 6'b111111;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp = nl_catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[5:0];
  assign nl_outer_conv_loop_3_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_tmp
      = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[5:1]) + conv_u2s_1_5(outer_conv_loop_3_operator_28_true_or_2);
  assign outer_conv_loop_3_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_tmp
      = nl_outer_conv_loop_3_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_tmp[4:0];
  assign nl_outer_conv_loop_3_operator_28_true_acc_1_tmp = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[5:1])
      + 5'b00001;
  assign outer_conv_loop_3_operator_28_true_acc_1_tmp = nl_outer_conv_loop_3_operator_28_true_acc_1_tmp[4:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_row_slc_catapult_conv2d_28_28_5_5_1_convolve_catapult_conv2d_28_28_5_5_1_convolve_row_mux_nl
      = MUX_v_4_4_2((catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_mx1[4:1]),
      (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva_mx1[4:1]), (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_dfm_mx0[4:1]),
      (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc2_sva[4:1]), catapult_conv2d_28_28_5_5_1_convolve_do_pcnxt_sva);
  assign nl_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp = conv_u2s_4_5(catapult_conv2d_28_28_5_5_1_convolve_row_slc_catapult_conv2d_28_28_5_5_1_convolve_catapult_conv2d_28_28_5_5_1_convolve_row_mux_nl)
      + 5'b11111;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp = nl_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp[4:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_row_slc_catapult_conv2d_28_28_5_5_1_convolve_catapult_conv2d_28_28_5_5_1_convolve_row_mux_1_cse_sva_1
      = MUX_s_1_4_2((catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_mx1[0]),
      (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva_mx1[0]), (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_dfm_mx0[0]),
      (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc2_sva[0]), catapult_conv2d_28_28_5_5_1_convolve_do_pcnxt_sva);
  assign nl_outer_conv_loop_5_inner_conv_loop_5_acc_1_nl = inner_conv_loop_acc_24_itm_1
      + inner_conv_loop_acc_itm_1;
  assign outer_conv_loop_5_inner_conv_loop_5_acc_1_nl = nl_outer_conv_loop_5_inner_conv_loop_5_acc_1_nl[63:0];
  assign outer_conv_loop_5_inner_conv_loop_5_acc_1_itm_63_15_1 = readslicef_64_49_15(outer_conv_loop_5_inner_conv_loop_5_acc_1_nl);
  assign nl_outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl
      = ({1'b1 , outer_conv_loop_4_inner_conv_loop_1_operator_28_true_1_acc_psp_4_2_sva_1})
      + 4'b0001;
  assign outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl
      = nl_outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl[3:0];
  assign outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
      = readslicef_4_1_3(outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl);
  assign nl_outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl
      = ({1'b1 , operator_28_true_1_acc_1_psp_sva_1}) + 4'b0001;
  assign outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl
      = nl_outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl[3:0];
  assign outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
      = readslicef_4_1_3(outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_nl);
  assign main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1 = ~((~((z_out_1[31])
      | main_loop_3_and_unfl_sva_1)) | main_loop_3_nor_ovfl_sva_1);
  assign main_loop_4_qif_slc_bias_output_32_31_0_ncse_0_sva_1 = ~((~((z_out_1[0])
      | main_loop_3_nor_ovfl_sva_1)) | main_loop_3_and_unfl_sva_1);
  assign main_loop_3_and_unfl_sva_1 = (z_out_1[32:31]==2'b10);
  assign main_loop_3_nor_ovfl_sva_1 = ~((z_out_1[32:31]!=2'b01));
  assign nl_operator_28_true_3_acc_nl = conv_s2s_4_5(z_out_4[4:1]) + 5'b11001;
  assign operator_28_true_3_acc_nl = nl_operator_28_true_3_acc_nl[4:0];
  assign operator_28_true_3_acc_itm_4_1 = readslicef_5_1_4(operator_28_true_3_acc_nl);
  assign nl_operator_28_true_acc_nl_1 = conv_u2s_10_11({reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd
      , reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1}) + 11'b10011110001;
  assign operator_28_true_acc_nl_1 = nl_operator_28_true_acc_nl_1[10:0];
  assign or_dcpl_12 = (~ catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2) | (readslicef_11_1_10(operator_28_true_acc_nl_1));
  assign and_dcpl_11 = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0 & catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_itm_6_1;
  assign or_dcpl_29 = ~(catapult_conv2d_28_28_5_5_1_convolve_do_stage_0 & catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_itm_6_1);
  assign or_dcpl_32 = (fsm_output[19:18]!=2'b00);
  assign and_dcpl_30 = ~(catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2 | catapult_conv2d_28_28_5_5_1_convolve_do_stage_0);
  assign and_dcpl_53 = ~((fsm_output[25]) | (fsm_output[0]));
  assign or_dcpl_60 = (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[4]) |
      (~ (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[0]));
  assign or_dcpl_61 = or_dcpl_60 | (~ (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[2]));
  assign or_dcpl_62 = (~ catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2) | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[1]);
  assign or_dcpl_63 = or_dcpl_62 | (~ (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[3]));
  assign or_dcpl_65 = (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[4]) |
      (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[0]);
  assign or_dcpl_66 = or_dcpl_65 | (~ (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[2]));
  assign or_dcpl_68 = ~(catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2 & (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[1]));
  assign or_dcpl_69 = or_dcpl_68 | (~ (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[3]));
  assign or_dcpl_71 = or_dcpl_60 | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[2]);
  assign or_dcpl_74 = or_dcpl_65 | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[2]);
  assign or_dcpl_76 = (~ (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[4]))
      | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[0]);
  assign or_dcpl_77 = or_dcpl_76 | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[2]);
  assign or_dcpl_78 = or_dcpl_62 | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[3]);
  assign or_dcpl_81 = ~((catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[4])
      & (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[0]));
  assign or_dcpl_82 = or_dcpl_81 | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[2]);
  assign or_dcpl_85 = or_dcpl_68 | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[3]);
  assign or_dcpl_90 = or_dcpl_76 | (~ (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[2]));
  assign or_dcpl_93 = or_dcpl_81 | (~ (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva[2]));
  assign and_dcpl_57 = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2 & (~ catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_1);
  assign or_dcpl_110 = (fsm_output[5]) | (fsm_output[7]);
  assign or_dcpl_113 = (fsm_output[11]) | (fsm_output[9]) | (fsm_output[22]);
  assign and_dcpl_78 = ~((fsm_output[6]) | (fsm_output[12]));
  assign and_dcpl_83 = ~((fsm_output[23]) | (fsm_output[2]));
  assign and_dcpl_86 = ~((fsm_output[11]) | (fsm_output[9]) | (fsm_output[22]));
  assign or_dcpl_138 = (fsm_output[12]) | (fsm_output[8]);
  assign and_dcpl_95 = ~((fsm_output[17]) | (fsm_output[8]));
  assign and_dcpl_96 = ~((fsm_output[13:12]!=2'b00));
  assign and_dcpl_97 = and_dcpl_96 & and_dcpl_95;
  assign and_dcpl_100 = ~((fsm_output[18]) | (fsm_output[19]) | (fsm_output[21]));
  assign or_dcpl_141 = (fsm_output[20]) | (fsm_output[13]);
  assign or_dcpl_147 = (fsm_output[15:14]!=2'b00);
  assign and_dcpl_105 = ~((fsm_output[16]) | (fsm_output[13]));
  assign and_dcpl_106 = ~((fsm_output[15:14]!=2'b00));
  assign or_dcpl_157 = (fsm_output[17:16]!=2'b00);
  assign or_dcpl_161 = or_dcpl_147 | (fsm_output[16]);
  assign or_dcpl_171 = (fsm_output[15]) | (fsm_output[18]) | (fsm_output[19]);
  assign and_dcpl_125 = and_dcpl_106 & (~ (fsm_output[16]));
  assign and_dcpl_127 = ~((fsm_output[13]) | (fsm_output[17]));
  assign and_265_cse = ~((fsm_output[9:8]!=2'b00));
  assign or_tmp_70 = (fsm_output[9]) | (or_36_cse & and_dcpl_11 & (fsm_output[8]));
  assign or_tmp_75 = and_dcpl_57 & (fsm_output[8]);
  assign and_290_cse = or_36_cse & (fsm_output[8]);
  assign or_tmp_260 = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0 & (fsm_output[6]);
  assign and_845_cse = (fsm_output[8:7]!=2'b00);
  assign or_tmp_298 = (fsm_output[9]) | (fsm_output[5]);
  assign and_870_cse = or_dcpl_147 | or_dcpl_157;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_2_mx1c1 = catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_3
      & (~ catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_2) & catapult_conv2d_28_28_5_5_1_convolve_do_stage_0;
  assign catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_2_1_mx1c1 = and_dcpl_57
      & operator_28_true_1_slc_operator_28_true_1_acc_5_svs_1 & or_dcpl_29;
  assign inner_loop_i_sva_mx0c1 = (fsm_output[3]) | (fsm_output[9]);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_mx0c1 = or_dcpl_113 | or_dcpl_110
      | (fsm_output[1]);
  assign catapult_conv2d_28_28_5_5_1_convolve_count_9_0_sva_mx0c0 = and_dcpl_100
      & (~((fsm_output[20]) | (fsm_output[2]))) & and_dcpl_97;
  assign catapult_conv2d_28_28_5_5_1_convolve_count_9_0_sva_mx0c4 = or_dcpl_32 |
      (fsm_output[21]) | or_dcpl_141 | (fsm_output[17]);
  assign catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_mx0c1 = and_dcpl_106
      & and_dcpl_105 & and_dcpl_78 & and_dcpl_95;
  assign catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c0 = (~((fsm_output[15])
      | (fsm_output[22]))) & (~((fsm_output[23]) | (fsm_output[14]))) & and_dcpl_105
      & (~ (fsm_output[8]));
  assign catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c2 = catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_land_1_lpi_3_dfm_mx0w5
      & (fsm_output[13]);
  assign catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c3 = (catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_5_itm
      | (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva[4])) & (fsm_output[13]);
  assign weight_memory_rsci_radr_d = weight_index_13_0_lpi_3;
  assign weight_memory_rsci_re_d_pff = weight_memory_rsci_re_d_iff;
  assign bias_memory_rsci_radr_d = bias_index_9_0_sva;
  assign bias_memory_rsci_re_d_pff = bias_memory_rsci_re_d_iff;
  assign current_image_rsci_radr_d = {z_out_6 , (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[0])};
  assign current_image_rsci_wadr_d = {z_out_6 , (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[0])};
  assign current_image_rsci_d_d = image_channel_rsci_idat_mxwt;
  assign current_image_rsci_we_d_pff = current_image_rsci_we_d_iff;
  assign current_image_rsci_re_d_pff = current_image_rsci_re_d_iff;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_qelse_mux_2_nl = MUX_s_1_2_2((catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9[0]),
      reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd, fsm_output[12]);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_qelse_mux_5_nl = MUX_v_9_2_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0,
      reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1, fsm_output[12]);
  assign convolution_output_rsci_radr_d = {catapult_conv2d_28_28_5_5_1_convolve_do_if_qelse_mux_2_nl
      , catapult_conv2d_28_28_5_5_1_convolve_do_if_qelse_mux_5_nl};
  assign convolution_output_rsci_wadr_d = {catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_9
      , reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_1_ftd
      , reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_1_ftd_1};
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_1_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_2_nl
      = ~((~((catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1[31]) | catapult_conv2d_28_28_5_5_1_convolve_do_if_1_and_unfl_sva_1))
      | catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_ovfl_sva_1);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_1_nl = ~(MUX_v_30_2_2((catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1[30:1]),
      30'b111111111111111111111111111111, catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_ovfl_sva_1));
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_1_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_nl
      = ~(MUX_v_30_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_1_nl, 30'b111111111111111111111111111111,
      catapult_conv2d_28_28_5_5_1_convolve_do_if_1_and_unfl_sva_1));
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_1_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_1_nl
      = ~((~((catapult_conv2d_28_28_5_5_1_convolve_do_if_1_acc_sat_sva_1[0]) | catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_ovfl_sva_1))
      | catapult_conv2d_28_28_5_5_1_convolve_do_if_1_and_unfl_sva_1);
  assign convolution_output_rsci_d_d = {catapult_conv2d_28_28_5_5_1_convolve_do_if_1_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_2_nl
      , catapult_conv2d_28_28_5_5_1_convolve_do_if_1_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_nl
      , catapult_conv2d_28_28_5_5_1_convolve_do_if_1_catapult_conv2d_28_28_5_5_1_convolve_do_if_1_nor_1_nl};
  assign convolution_output_rsci_we_d_pff = convolution_output_rsci_we_d_iff;
  assign convolution_output_rsci_re_d_pff = convolution_output_rsci_re_d_iff;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_aelse_catapult_conv2d_28_28_5_5_1_convolve_do_aelse_or_nl
      = (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm
      & (fsm_output[18])) | (fsm_output[14]);
  assign relu_output_rsci_radr_d = {reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1
      , catapult_conv2d_28_28_5_5_1_convolve_do_aelse_catapult_conv2d_28_28_5_5_1_convolve_do_aelse_or_nl};
  assign relu_output_rsci_wadr_d = {catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_9
      , catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_8_7
      , catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0};
  assign main_loop_4_main_loop_4_and_nl = main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1
      & (~ relu_sva);
  assign main_loop_3_nor_1_nl = ~(MUX_v_30_2_2((z_out_1[30:1]), 30'b111111111111111111111111111111,
      main_loop_3_nor_ovfl_sva_1));
  assign main_loop_3_main_loop_3_nor_nl = ~(MUX_v_30_2_2(main_loop_3_nor_1_nl, 30'b111111111111111111111111111111,
      main_loop_3_and_unfl_sva_1));
  assign nand_29_nl = ~(main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1 & relu_sva);
  assign mux_6_nl = MUX_v_30_2_2((signext_30_1(~ main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1)),
      main_loop_3_main_loop_3_nor_nl, nand_29_nl);
  assign catapult_conv2d_28_28_5_5_1_relu_fn_catapult_conv2d_28_28_5_5_1_relu_fn_and_2_nl
      = main_loop_4_qif_slc_bias_output_32_31_0_ncse_0_sva_1 & (~ main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1);
  assign catapult_conv2d_28_28_5_5_1_relu_fn_mux_nl = MUX_s_1_2_2(main_loop_4_qif_slc_bias_output_32_31_0_ncse_0_sva_1,
      catapult_conv2d_28_28_5_5_1_relu_fn_catapult_conv2d_28_28_5_5_1_relu_fn_and_2_nl,
      relu_sva);
  assign relu_output_rsci_d_d = {main_loop_4_main_loop_4_and_nl , mux_6_nl , catapult_conv2d_28_28_5_5_1_relu_fn_mux_nl};
  assign relu_output_rsci_we_d_pff = relu_output_rsci_we_d_iff;
  assign relu_output_rsci_re_d_pff = relu_output_rsci_re_d_iff;
  assign output_buffer_rsci_radr_d = {(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9[0])
      , catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0};
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_if_catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_if_and_nl
      = MUX_v_2_2_2(2'b00, (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[8:7]),
      (fsm_output[19]));
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_if_mux_1_nl = MUX_v_7_2_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0,
      (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[6:0]), fsm_output[19]);
  assign catapult_conv2d_28_28_5_5_1_convolve_center_pixel_mux_nl = MUX_s_1_2_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_1,
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm,
      fsm_output[19]);
  assign output_buffer_rsci_wadr_d = {catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_if_catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_if_and_nl
      , catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_if_mux_1_nl , catapult_conv2d_28_28_5_5_1_convolve_center_pixel_mux_nl};
  assign and_738_nl = (z_out_1[32]) & (fsm_output[16]);
  assign and_740_nl = (~ (z_out_1[32])) & (fsm_output[16]);
  assign output_buffer_rsci_d_d = MUX1HOT_v_32_3_2(catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qr_lpi_2_dfm,
      catapult_conv2d_28_28_5_5_1_max_cd_lpi_3_dfm, relu_output_rsci_q_d_mxwt, {and_738_nl
      , and_740_nl , (fsm_output[19])});
  assign output_buffer_rsci_we_d_pff = output_buffer_rsci_we_d_iff;
  assign output_buffer_rsci_re_d_pff = output_buffer_rsci_re_d_iff;
  assign or_tmp_379 = (fsm_output[16]) | (fsm_output[13]);
  assign or_tmp_383 = (fsm_output[13]) | (fsm_output[21]);
  assign or_tmp_393 = (fsm_output[7]) | (fsm_output[1]);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_image_channel_rsci_idat <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (fsm_output[23]) & catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2
        ) begin
      output_image_channel_rsci_idat <= output_buffer_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      bias_index_9_0_sva <= 10'b0000000000;
    end
    else if ( run_wen & ((~ and_dcpl_53) | (fsm_output[10])) ) begin
      bias_index_9_0_sva <= MUX_v_10_2_2(10'b0000000000, z_out_2, (fsm_output[10]));
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      num_output_images_sva <= 28'b0000000000000000000000000000;
      max_pool_sva <= 1'b0;
      relu_sva <= 1'b0;
      bias_sva <= 1'b0;
      num_input_images_sva <= 28'b0000000000000000000000000000;
      load_images_loop_acc_3_cse_sva_28 <= 1'b0;
    end
    else if ( num_output_images_and_cse ) begin
      num_output_images_sva <= num_output_images_rsci_idat;
      max_pool_sva <= max_pool_rsci_idat;
      relu_sva <= relu_rsci_idat;
      bias_sva <= bias_rsci_idat;
      num_input_images_sva <= num_input_images_rsci_idat;
      load_images_loop_acc_3_cse_sva_28 <= z_out_1[28];
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_13_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_63 | or_dcpl_61)) ) begin
      current_filter_13_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_12_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_63 | or_dcpl_66)) ) begin
      current_filter_12_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_14_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_69 | or_dcpl_66)) ) begin
      current_filter_14_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_11_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_69 | or_dcpl_71)) ) begin
      current_filter_11_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_15_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_69 | or_dcpl_61)) ) begin
      current_filter_15_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_10_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_69 | or_dcpl_74)) ) begin
      current_filter_10_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_16_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_78 | or_dcpl_77)) ) begin
      current_filter_16_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_9_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_63 | or_dcpl_71)) ) begin
      current_filter_9_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_17_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_78 | or_dcpl_82)) ) begin
      current_filter_17_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_8_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_63 | or_dcpl_74)) ) begin
      current_filter_8_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_18_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_85 | or_dcpl_77)) ) begin
      current_filter_18_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_7_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_85 | or_dcpl_61)) ) begin
      current_filter_7_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_19_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_85 | or_dcpl_82)) ) begin
      current_filter_19_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_6_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_85 | or_dcpl_66)) ) begin
      current_filter_6_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_20_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_78 | or_dcpl_90)) ) begin
      current_filter_20_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_5_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_78 | or_dcpl_61)) ) begin
      current_filter_5_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_21_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_78 | or_dcpl_93)) ) begin
      current_filter_21_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_4_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_78 | or_dcpl_66)) ) begin
      current_filter_4_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_22_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_85 | or_dcpl_90)) ) begin
      current_filter_22_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_3_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_85 | or_dcpl_71)) ) begin
      current_filter_3_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_23_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_85 | or_dcpl_93)) ) begin
      current_filter_23_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_2_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_85 | or_dcpl_74)) ) begin
      current_filter_2_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_24_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_63 | or_dcpl_77)) ) begin
      current_filter_24_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_1_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((~ (fsm_output[6])) | or_dcpl_78 | or_dcpl_71)) ) begin
      current_filter_1_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      shift_register_1_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~ and_265_cse) ) begin
      shift_register_1_lpi_2 <= shift_register_2_lpi_2;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      shift_register_2_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_6_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_7_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_8_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_9_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_10_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_11_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_12_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_13_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_14_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_15_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_16_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_17_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_18_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_19_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_20_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_21_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_22_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_23_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_24_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_25_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_26_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_27_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_28_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_34_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_35_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_36_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_37_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_38_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_39_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_40_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_41_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_42_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_43_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_44_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_45_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_46_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_47_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_48_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_49_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_50_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_51_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_52_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_53_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_54_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_55_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_56_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_57_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_58_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_59_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_60_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_61_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_62_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_63_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_64_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_65_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_66_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_67_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_68_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_69_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_70_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_71_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_72_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_73_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_74_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_75_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_76_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_77_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_78_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_79_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_80_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_81_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_82_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_83_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_90_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_91_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_92_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_93_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_94_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_95_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_96_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_97_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_98_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_99_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_100_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_101_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_102_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_103_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_104_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_105_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_106_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_107_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_108_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_109_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_110_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_111_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( shift_register_and_1_cse ) begin
      shift_register_2_lpi_2 <= shift_register_3_lpi_2;
      shift_register_6_lpi_2 <= shift_register_7_lpi_2;
      shift_register_7_lpi_2 <= shift_register_8_lpi_2;
      shift_register_8_lpi_2 <= shift_register_9_lpi_2;
      shift_register_9_lpi_2 <= shift_register_10_lpi_2;
      shift_register_10_lpi_2 <= shift_register_11_lpi_2;
      shift_register_11_lpi_2 <= shift_register_12_lpi_2;
      shift_register_12_lpi_2 <= shift_register_13_lpi_2;
      shift_register_13_lpi_2 <= shift_register_14_lpi_2;
      shift_register_14_lpi_2 <= shift_register_15_lpi_2;
      shift_register_15_lpi_2 <= shift_register_16_lpi_2;
      shift_register_16_lpi_2 <= shift_register_17_lpi_2;
      shift_register_17_lpi_2 <= shift_register_18_lpi_2;
      shift_register_18_lpi_2 <= shift_register_19_lpi_2;
      shift_register_19_lpi_2 <= shift_register_20_lpi_2;
      shift_register_20_lpi_2 <= shift_register_21_lpi_2;
      shift_register_21_lpi_2 <= shift_register_22_lpi_2;
      shift_register_22_lpi_2 <= shift_register_23_lpi_2;
      shift_register_23_lpi_2 <= shift_register_24_lpi_2;
      shift_register_24_lpi_2 <= shift_register_25_lpi_2;
      shift_register_25_lpi_2 <= shift_register_26_lpi_2;
      shift_register_26_lpi_2 <= shift_register_27_lpi_2;
      shift_register_27_lpi_2 <= shift_register_28_lpi_2;
      shift_register_28_lpi_2 <= shift_register_29_lpi_2;
      shift_register_34_lpi_2 <= shift_register_35_lpi_2;
      shift_register_35_lpi_2 <= shift_register_36_lpi_2;
      shift_register_36_lpi_2 <= shift_register_37_lpi_2;
      shift_register_37_lpi_2 <= shift_register_38_lpi_2;
      shift_register_38_lpi_2 <= shift_register_39_lpi_2;
      shift_register_39_lpi_2 <= shift_register_40_lpi_2;
      shift_register_40_lpi_2 <= shift_register_41_lpi_2;
      shift_register_41_lpi_2 <= shift_register_42_lpi_2;
      shift_register_42_lpi_2 <= shift_register_43_lpi_2;
      shift_register_43_lpi_2 <= shift_register_44_lpi_2;
      shift_register_44_lpi_2 <= shift_register_45_lpi_2;
      shift_register_45_lpi_2 <= shift_register_46_lpi_2;
      shift_register_46_lpi_2 <= shift_register_47_lpi_2;
      shift_register_47_lpi_2 <= shift_register_48_lpi_2;
      shift_register_48_lpi_2 <= shift_register_49_lpi_2;
      shift_register_49_lpi_2 <= shift_register_50_lpi_2;
      shift_register_50_lpi_2 <= shift_register_51_lpi_2;
      shift_register_51_lpi_2 <= shift_register_52_lpi_2;
      shift_register_52_lpi_2 <= shift_register_53_lpi_2;
      shift_register_53_lpi_2 <= shift_register_54_lpi_2;
      shift_register_54_lpi_2 <= shift_register_55_lpi_2;
      shift_register_55_lpi_2 <= shift_register_56_lpi_2;
      shift_register_56_lpi_2 <= shift_register_57_lpi_2;
      shift_register_57_lpi_2 <= shift_register_58_lpi_2;
      shift_register_58_lpi_2 <= shift_register_59_lpi_2;
      shift_register_59_lpi_2 <= shift_register_60_lpi_2;
      shift_register_60_lpi_2 <= shift_register_61_lpi_2;
      shift_register_61_lpi_2 <= shift_register_62_lpi_2;
      shift_register_62_lpi_2 <= shift_register_63_lpi_2;
      shift_register_63_lpi_2 <= shift_register_64_lpi_2;
      shift_register_64_lpi_2 <= shift_register_65_lpi_2;
      shift_register_65_lpi_2 <= shift_register_66_lpi_2;
      shift_register_66_lpi_2 <= shift_register_67_lpi_2;
      shift_register_67_lpi_2 <= shift_register_68_lpi_2;
      shift_register_68_lpi_2 <= shift_register_69_lpi_2;
      shift_register_69_lpi_2 <= shift_register_70_lpi_2;
      shift_register_70_lpi_2 <= shift_register_71_lpi_2;
      shift_register_71_lpi_2 <= shift_register_72_lpi_2;
      shift_register_72_lpi_2 <= shift_register_73_lpi_2;
      shift_register_73_lpi_2 <= shift_register_74_lpi_2;
      shift_register_74_lpi_2 <= shift_register_75_lpi_2;
      shift_register_75_lpi_2 <= shift_register_76_lpi_2;
      shift_register_76_lpi_2 <= shift_register_77_lpi_2;
      shift_register_77_lpi_2 <= shift_register_78_lpi_2;
      shift_register_78_lpi_2 <= shift_register_79_lpi_2;
      shift_register_79_lpi_2 <= shift_register_80_lpi_2;
      shift_register_80_lpi_2 <= shift_register_81_lpi_2;
      shift_register_81_lpi_2 <= shift_register_82_lpi_2;
      shift_register_82_lpi_2 <= shift_register_83_lpi_2;
      shift_register_83_lpi_2 <= shift_register_84_lpi_2;
      shift_register_90_lpi_2 <= shift_register_91_lpi_2;
      shift_register_91_lpi_2 <= shift_register_92_lpi_2;
      shift_register_92_lpi_2 <= shift_register_93_lpi_2;
      shift_register_93_lpi_2 <= shift_register_94_lpi_2;
      shift_register_94_lpi_2 <= shift_register_95_lpi_2;
      shift_register_95_lpi_2 <= shift_register_96_lpi_2;
      shift_register_96_lpi_2 <= shift_register_97_lpi_2;
      shift_register_97_lpi_2 <= shift_register_98_lpi_2;
      shift_register_98_lpi_2 <= shift_register_99_lpi_2;
      shift_register_99_lpi_2 <= shift_register_100_lpi_2;
      shift_register_100_lpi_2 <= shift_register_101_lpi_2;
      shift_register_101_lpi_2 <= shift_register_102_lpi_2;
      shift_register_102_lpi_2 <= shift_register_103_lpi_2;
      shift_register_103_lpi_2 <= shift_register_104_lpi_2;
      shift_register_104_lpi_2 <= shift_register_105_lpi_2;
      shift_register_105_lpi_2 <= shift_register_106_lpi_2;
      shift_register_106_lpi_2 <= shift_register_107_lpi_2;
      shift_register_107_lpi_2 <= shift_register_108_lpi_2;
      shift_register_108_lpi_2 <= shift_register_109_lpi_2;
      shift_register_109_lpi_2 <= shift_register_110_lpi_2;
      shift_register_110_lpi_2 <= shift_register_111_lpi_2;
      shift_register_111_lpi_2 <= shift_register_112_lpi_2;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      shift_register_3_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_29_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_31_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_84_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_112_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( shift_register_and_2_cse ) begin
      shift_register_3_lpi_2 <= MUX_v_32_2_2(shift_register_5_lpi_2, shift_register_4_lpi_2,
          or_tmp_70);
      shift_register_29_lpi_2 <= MUX_v_32_2_2(reg_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b_cse,
          shift_register_30_lpi_2, or_tmp_70);
      shift_register_31_lpi_2 <= MUX_v_32_2_2(shift_register_33_lpi_2, shift_register_32_lpi_2,
          or_tmp_70);
      shift_register_84_lpi_2 <= MUX_v_32_2_2(shift_register_86_lpi_2, shift_register_85_lpi_2,
          or_tmp_70);
      shift_register_112_lpi_2 <= MUX_v_32_2_2(shift_register_114_lpi_2, shift_register_113_lpi_2,
          or_tmp_70);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      shift_register_4_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_30_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_32_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_85_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_113_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( shift_register_and_3_cse ) begin
      shift_register_4_lpi_2 <= shift_register_5_lpi_2;
      shift_register_30_lpi_2 <= MUX_v_32_2_2(reg_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b_cse,
          shift_register_31_lpi_2, fsm_output[9]);
      shift_register_32_lpi_2 <= shift_register_33_lpi_2;
      shift_register_85_lpi_2 <= shift_register_86_lpi_2;
      shift_register_113_lpi_2 <= shift_register_114_lpi_2;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      shift_register_5_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_33_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_89_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( shift_register_and_4_cse ) begin
      shift_register_5_lpi_2 <= MUX_v_32_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_5_lpi_4_itm_1,
          shift_register_6_lpi_2, fsm_output[9]);
      shift_register_33_lpi_2 <= MUX_v_32_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_33_lpi_4_itm_1,
          shift_register_34_lpi_2, fsm_output[9]);
      shift_register_89_lpi_2 <= MUX_v_32_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_89_lpi_4_itm_1,
          shift_register_90_lpi_2, fsm_output[9]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      shift_register_86_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_87_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_88_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_114_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_115_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( shift_register_and_85_cse ) begin
      shift_register_86_lpi_2 <= shift_register_87_lpi_2;
      shift_register_87_lpi_2 <= shift_register_88_lpi_2;
      shift_register_88_lpi_2 <= shift_register_89_lpi_2;
      shift_register_114_lpi_2 <= shift_register_115_lpi_2;
      shift_register_115_lpi_2 <= catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qr_lpi_2_dfm;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2 & (~
        catapult_conv2d_28_28_5_5_1_convolve_do_stage_0)) | catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_2_mx1c1)
        & (fsm_output[8]) ) begin
      catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_2 <= MUX_v_32_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_4_dfm_mx1w0,
          catapult_conv2d_28_28_5_5_1_max_cd_lpi_3_dfm, catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_2_mx1c1);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      max_pool_shift_register_4_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_5_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_6_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_7_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_8_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_9_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_10_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_11_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_12_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_13_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_14_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_15_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_16_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_17_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_18_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_19_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_20_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_21_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_22_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_23_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_24_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_25_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_26_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_27_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_29_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( max_pool_shift_register_and_24_cse ) begin
      max_pool_shift_register_4_lpi_2 <= max_pool_shift_register_6_lpi_2;
      max_pool_shift_register_5_lpi_2 <= max_pool_shift_register_7_lpi_2;
      max_pool_shift_register_6_lpi_2 <= max_pool_shift_register_8_lpi_2;
      max_pool_shift_register_7_lpi_2 <= max_pool_shift_register_9_lpi_2;
      max_pool_shift_register_8_lpi_2 <= max_pool_shift_register_10_lpi_2;
      max_pool_shift_register_9_lpi_2 <= max_pool_shift_register_11_lpi_2;
      max_pool_shift_register_10_lpi_2 <= max_pool_shift_register_12_lpi_2;
      max_pool_shift_register_11_lpi_2 <= max_pool_shift_register_13_lpi_2;
      max_pool_shift_register_12_lpi_2 <= max_pool_shift_register_14_lpi_2;
      max_pool_shift_register_13_lpi_2 <= max_pool_shift_register_15_lpi_2;
      max_pool_shift_register_14_lpi_2 <= max_pool_shift_register_16_lpi_2;
      max_pool_shift_register_15_lpi_2 <= max_pool_shift_register_17_lpi_2;
      max_pool_shift_register_16_lpi_2 <= max_pool_shift_register_18_lpi_2;
      max_pool_shift_register_17_lpi_2 <= max_pool_shift_register_19_lpi_2;
      max_pool_shift_register_18_lpi_2 <= max_pool_shift_register_20_lpi_2;
      max_pool_shift_register_19_lpi_2 <= max_pool_shift_register_21_lpi_2;
      max_pool_shift_register_20_lpi_2 <= max_pool_shift_register_22_lpi_2;
      max_pool_shift_register_21_lpi_2 <= max_pool_shift_register_23_lpi_2;
      max_pool_shift_register_22_lpi_2 <= max_pool_shift_register_24_lpi_2;
      max_pool_shift_register_23_lpi_2 <= max_pool_shift_register_25_lpi_2;
      max_pool_shift_register_24_lpi_2 <= max_pool_shift_register_26_lpi_2;
      max_pool_shift_register_25_lpi_2 <= max_pool_shift_register_27_lpi_2;
      max_pool_shift_register_26_lpi_2 <= catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_asn_1_ncse_lpi_2;
      max_pool_shift_register_27_lpi_2 <= max_pool_shift_register_29_lpi_2;
      max_pool_shift_register_29_lpi_2 <= catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_3;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_asn_1_ncse_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1
        & (~((~((fsm_output[24]) | (fsm_output[17]))) | ((~ max_pool_sva) & (fsm_output[24])))))
        | (max_pool_sva & (fsm_output[24]))) ) begin
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_asn_1_ncse_lpi_2 <= catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_2;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      reg_num_output_images_triosy_obj_iswt0_cse <= 1'b0;
      reg_output_buffer_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= 1'b0;
      reg_relu_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= 1'b0;
      reg_convolution_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse
          <= 1'b0;
      reg_current_image_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= 1'b0;
      reg_output_image_channel_rsci_iswt0_cse <= 1'b0;
      reg_bias_memory_rsci_iswt0_cse <= 1'b0;
      reg_weight_memory_rsci_iswt0_cse <= 1'b0;
      reg_image_channel_rsci_iswt0_cse <= 1'b0;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_a <= 32'b00000000000000000000000000000000;
      reg_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b_cse <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_a <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_b <= 32'b00000000000000000000000000000000;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_a <= 32'b00000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2 <= 1'b0;
      main_loop_2_asn_167_itm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva <= 5'b00000;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9 <= 2'b00;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0 <= 9'b000000000;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_6
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_5
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_4
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_3
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_2
          <= 1'b0;
      outer_conv_loop_1_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_1
          <= 3'b000;
      outer_conv_loop_1_operator_28_true_acc_1_itm_1 <= 3'b000;
      catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_sva_1_5_1 <= 5'b00000;
      outer_conv_loop_2_operator_28_true_and_itm_1 <= 1'b0;
      outer_conv_loop_2_operator_28_true_or_itm_1 <= 1'b0;
      outer_conv_loop_4_inner_conv_loop_1_operator_28_true_1_acc_psp_4_2_sva_1 <=
          3'b000;
      operator_28_true_1_acc_1_psp_sva_1 <= 3'b000;
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_1_1 <= 5'b00000;
      operator_28_true_1_slc_operator_28_true_1_acc_5_svs_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_1 <= 1'b0;
      outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1
          <= 1'b0;
      outer_conv_loop_5_inner_conv_loop_5_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1
          <= 1'b0;
      outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1
          <= 1'b0;
      outer_conv_loop_5_inner_conv_loop_4_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1
          <= 1'b0;
      outer_conv_loop_3_operator_28_true_acc_cse_4_sva_1 <= 1'b0;
      outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_1
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc2_sva <= 5'b00000;
      catapult_conv2d_28_28_5_5_1_convolve_do_pcnxt_sva <= 2'b00;
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_3 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_4 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_5 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_6 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_7 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_9
          <= 1'b0;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_1_ftd
          <= 2'b00;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_1_ftd_1
          <= 7'b0000000;
      catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_5 <= 32'b00000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_slc_catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_1_63_15_48_1_itm_1
          <= 48'b000000000000000000000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_slc_catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_1_63_15_0_itm_1
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_do_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_6_svs_1
          <= 1'b0;
      inner_conv_loop_acc_24_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_89_lpi_4_itm_1
          <= 32'b00000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_33_lpi_4_itm_1
          <= 32'b00000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_5_lpi_4_itm_1
          <= 32'b00000000000000000000000000000000;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd
          <= 1'b0;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd_1_8_7
          <= 2'b00;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd_1_6_0
          <= 7'b0000000;
      catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_4 <= 32'b00000000000000000000000000000000;
      inner_conv_loop_acc_20_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_13_itm_2 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_19_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_18_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_17_itm_2 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_16_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_15_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_9
          <= 1'b0;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_1_ftd
          <= 2'b00;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_1_ftd_1
          <= 7'b0000000;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_21_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_5_lpi_4_dfm_2 <= 1'b0;
      inner_conv_loop_inner_conv_loop_and_6_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_14_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_4_lpi_4_dfm_1_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_lpi_4_dfm_1_2 <= 1'b0;
      inner_conv_loop_inner_conv_loop_and_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_8_lpi_4_dfm_1_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_1_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_2_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_3_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_12_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_16_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_20_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_24_lpi_4_dfm_1_2 <= 1'b0;
      inner_conv_loop_inner_conv_loop_and_9_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_22_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_6_lpi_4_dfm_2 <= 1'b0;
      inner_conv_loop_inner_conv_loop_and_12_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_9
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_8_7
          <= 2'b00;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_6_0
          <= 7'b0000000;
      inner_conv_loop_acc_13_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_17_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_10_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_13_lpi_4_dfm_2 <= 1'b0;
      operator_28_true_1_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_psp_1_4_2_itm_2
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_9_lpi_4_dfm_2 <= 1'b0;
      outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_2
          <= 1'b0;
      outer_conv_loop_3_operator_28_true_acc_cse_4_sva_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_23_lpi_4_dfm_2 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_7_lpi_4_dfm_2 <= 1'b0;
      outer_conv_loop_3_operator_28_true_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_5_1_5_itm_1
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_9
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7
          <= 2'b00;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0
          <= 7'b0000000;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_lpi_4_dfm_1_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_4_lpi_4_dfm_1_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_3_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_2_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_1_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_8_lpi_4_dfm_1_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_24_lpi_4_dfm_1_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_20_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_16_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_12_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_6_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_22_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_14_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_5_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_21_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_itm_1
          <= 1'b0;
      operator_28_true_1_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_psp_1_4_2_itm_1
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_7_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_23_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_10_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_13_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_9_lpi_4_dfm_1 <= 1'b0;
    end
    else if ( run_wen ) begin
      reg_num_output_images_triosy_obj_iswt0_cse <= exit_main_loop_1_sva_mx0 & or_133_cse;
      reg_output_buffer_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= and_736_rmff;
      reg_relu_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= or_430_rmff;
      reg_convolution_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse
          <= or_432_rmff;
      reg_current_image_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= and_757_rmff;
      reg_output_image_channel_rsci_iswt0_cse <= catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2
          & (fsm_output[23]);
      reg_bias_memory_rsci_iswt0_cse <= and_761_rmff;
      reg_weight_memory_rsci_iswt0_cse <= or_tmp_260;
      reg_image_channel_rsci_iswt0_cse <= (fsm_output[1]) | (or_dcpl_12 & catapult_conv2d_28_28_5_5_1_convolve_do_stage_0
          & (fsm_output[2]));
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_b <= shift_register_2_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_a <= current_filter_1_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_b <= shift_register_3_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_a <= current_filter_2_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_b <= shift_register_4_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_a <= current_filter_3_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_b <= shift_register_5_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_a <= current_filter_4_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_b <= shift_register_29_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_a <= current_filter_5_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_b <= shift_register_30_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_a <= current_filter_6_lpi_2;
      reg_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b_cse <= shift_register_31_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_a <= current_filter_7_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_b <= shift_register_32_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_a <= current_filter_8_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_b <= shift_register_33_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_a <= current_filter_9_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_b <= shift_register_57_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_a <= current_filter_10_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_b <= shift_register_58_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_a <= current_filter_11_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_b <= shift_register_59_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_a <= current_filter_12_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_b <= shift_register_60_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_a <= current_filter_13_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_b <= shift_register_61_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_a <= current_filter_14_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_b <= shift_register_85_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_a <= current_filter_15_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_b <= shift_register_86_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_a <= current_filter_16_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_b <= shift_register_87_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_a <= current_filter_17_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_b <= shift_register_88_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_a <= current_filter_18_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_b <= shift_register_89_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_a <= current_filter_19_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_b <= shift_register_113_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_a <= current_filter_20_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_b <= shift_register_114_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_a <= current_filter_21_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_b <= shift_register_115_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_a <= current_filter_22_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_b <= catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qr_lpi_2_dfm;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_a <= current_filter_23_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_b <= catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_4_dfm_mx1w0;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_a <= current_filter_24_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_b <= shift_register_1_lpi_2;
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_a <= current_filter_0_lpi_4;
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2 <= (catapult_conv2d_28_28_5_5_1_convolve_do_aelse_mux_22_nl
          & (~(or_dcpl_113 | or_dcpl_110))) | (and_dcpl_86 & and_dcpl_83 & (~((fsm_output[7:5]!=3'b000)))
          & (~((fsm_output[12]) | (fsm_output[8]))));
      main_loop_2_asn_167_itm_1 <= MUX_s_1_2_2((~ operator_28_true_acc_itm_5_1),
          (catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp[4]), fsm_output[8]);
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva <= MUX_v_5_2_2(5'b00000,
          catapult_conv2d_28_28_5_5_1_convolve_row_mux_5_nl, not_261_nl);
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9 <= MUX_v_2_2_2(2'b00,
          catapult_conv2d_28_28_5_5_1_convolve_center_pixel_or_1_nl, nand_24_seb);
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0 <= MUX_v_9_2_2(9'b000000000,
          mux1h_nl, and_1076_nl);
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_6
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_5;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_5
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_4;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_4
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_3;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_3
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_2;
      catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_2 <= catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_1;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_2
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_1;
      outer_conv_loop_1_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_1
          <= nl_outer_conv_loop_1_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_1[2:0];
      outer_conv_loop_1_operator_28_true_acc_1_itm_1 <= z_out_8[2:0];
      catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_sva_1_5_1 <= catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[5:1];
      outer_conv_loop_2_operator_28_true_and_itm_1 <= outer_conv_loop_1_operator_28_true_and_2;
      outer_conv_loop_2_operator_28_true_or_itm_1 <= outer_conv_loop_3_operator_28_true_or_2;
      outer_conv_loop_4_inner_conv_loop_1_operator_28_true_1_acc_psp_4_2_sva_1 <=
          nl_outer_conv_loop_4_inner_conv_loop_1_operator_28_true_1_acc_psp_4_2_sva_1[2:0];
      operator_28_true_1_acc_1_psp_sva_1 <= nl_operator_28_true_1_acc_1_psp_sva_1[2:0];
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_1_1 <= catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc2_sva;
      operator_28_true_1_slc_operator_28_true_1_acc_5_svs_1 <= operator_28_true_1_acc_itm_5_1;
      catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_1 <= ~ catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_itm_6_1;
      outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1
          <= outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_itm_3_1;
      outer_conv_loop_5_inner_conv_loop_5_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1
          <= outer_conv_loop_3_operator_28_true_acc_1_tmp[4];
      outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1
          <= outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_itm_3_1;
      outer_conv_loop_5_inner_conv_loop_4_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1
          <= outer_conv_loop_3_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_tmp[4];
      outer_conv_loop_3_operator_28_true_acc_cse_4_sva_1 <= readslicef_6_1_5(operator_28_true_acc_1_nl);
      outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_1
          <= readslicef_5_1_4(outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl);
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc2_sva <= catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_dfm_mx0;
      catapult_conv2d_28_28_5_5_1_convolve_do_pcnxt_sva <= MUX_v_2_2_2(2'b00, (z_out_4[1:0]),
          (fsm_output[8]));
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_3 <= catapult_conv2d_28_28_5_5_1_convolve_do_aelse_mux_nl
          & ((fsm_output[11]) | (fsm_output[12]) | (fsm_output[8]));
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_4 <= catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_3
          & or_dcpl_138;
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_5 <= catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_4
          & (fsm_output[8]);
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_6 <= catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_5
          & (fsm_output[8]);
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_7 <= catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_6
          & (fsm_output[8]);
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm
          <= (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_mux1h_17_nl & (~((fsm_output[21])
          | (fsm_output[12])))) | (fsm_output[20]);
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_9
          <= reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_1_ftd
          <= reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd_1_8_7;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_6_1_ftd_1
          <= reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd_1_6_0;
      catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_5 <= catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_4;
      catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_slc_catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_1_63_15_48_1_itm_1
          <= outer_conv_loop_5_inner_conv_loop_5_acc_1_itm_63_15_1[48:1];
      catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_slc_catapult_conv2d_28_28_5_5_1_convolve_do_accumulator_1_63_15_0_itm_1
          <= outer_conv_loop_5_inner_conv_loop_5_acc_1_itm_63_15_1[0];
      catapult_conv2d_28_28_5_5_1_convolve_do_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_6_svs_1
          <= z_out_7[6];
      inner_conv_loop_acc_24_itm_1 <= nl_inner_conv_loop_acc_24_itm_1[63:0];
      inner_conv_loop_acc_itm_1 <= nl_inner_conv_loop_acc_itm_1[63:0];
      catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_89_lpi_4_itm_1
          <= shift_register_90_lpi_2;
      catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_33_lpi_4_itm_1
          <= shift_register_34_lpi_2;
      catapult_conv2d_28_28_5_5_1_convolve_do_last_value_asn_shift_register_5_lpi_4_itm_1
          <= shift_register_6_lpi_2;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_9;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd_1_8_7
          <= reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_1_ftd;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_5_ftd_1_6_0
          <= reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_1_ftd_1;
      catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_4 <= catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_3;
      inner_conv_loop_acc_20_itm_1 <= nl_inner_conv_loop_acc_20_itm_1[63:0];
      inner_conv_loop_acc_13_itm_2 <= inner_conv_loop_acc_13_itm_1;
      inner_conv_loop_acc_19_itm_1 <= nl_inner_conv_loop_acc_19_itm_1[63:0];
      inner_conv_loop_acc_18_itm_1 <= nl_inner_conv_loop_acc_18_itm_1[63:0];
      inner_conv_loop_acc_17_itm_2 <= inner_conv_loop_acc_17_itm_1;
      inner_conv_loop_acc_16_itm_1 <= nl_inner_conv_loop_acc_16_itm_1[63:0];
      inner_conv_loop_acc_15_itm_1 <= nl_inner_conv_loop_acc_15_itm_1[63:0];
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_9
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_9;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_1_ftd
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_8_7;
      reg_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_4_1_ftd_1
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_6_0;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_21_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_21_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_5_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_5_lpi_4_dfm_1;
      inner_conv_loop_inner_conv_loop_and_6_itm_1 <= MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
          outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_10_lpi_4_dfm_2);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_14_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_14_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_4_lpi_4_dfm_1_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_4_lpi_4_dfm_1_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_lpi_4_dfm_1_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_lpi_4_dfm_1_1;
      inner_conv_loop_inner_conv_loop_and_itm_1 <= MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
          outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_9_lpi_4_dfm_2);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_8_lpi_4_dfm_1_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_8_lpi_4_dfm_1_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_1_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_1_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_2_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_2_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_3_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_3_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_12_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_12_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_16_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_16_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_20_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_20_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_24_lpi_4_dfm_1_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_24_lpi_4_dfm_1_1;
      inner_conv_loop_inner_conv_loop_and_9_itm_1 <= MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
          outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z_oreg, operator_28_true_1_not_6_nl);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_22_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_22_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_6_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_6_lpi_4_dfm_1;
      inner_conv_loop_inner_conv_loop_and_12_itm_1 <= MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
          outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z_oreg, inner_conv_loop_not_39_nl);
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_9
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_9;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_8_7
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_3_6_0
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0;
      inner_conv_loop_acc_13_itm_1 <= nl_inner_conv_loop_acc_13_itm_1[63:0];
      inner_conv_loop_acc_17_itm_1 <= nl_inner_conv_loop_acc_17_itm_1[63:0];
      catapult_conv2d_28_28_5_5_1_in_bounds_return_10_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_10_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_13_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_13_lpi_4_dfm_1;
      operator_28_true_1_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_psp_1_4_2_itm_2
          <= operator_28_true_1_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_psp_1_4_2_itm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_9_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_9_lpi_4_dfm_1;
      outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_2
          <= outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_1;
      outer_conv_loop_3_operator_28_true_acc_cse_4_sva_2 <= outer_conv_loop_3_operator_28_true_acc_cse_4_sva_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_23_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_23_lpi_4_dfm_1;
      catapult_conv2d_28_28_5_5_1_in_bounds_return_7_lpi_4_dfm_2 <= catapult_conv2d_28_28_5_5_1_in_bounds_return_7_lpi_4_dfm_1;
      outer_conv_loop_3_operator_28_true_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_5_1_5_itm_1
          <= catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_sva_1_5_1[4];
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_9
          <= main_loop_4_main_loop_4_mux_nl & catapult_conv2d_28_28_5_5_1_convolve_center_pixel_not_seb;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7
          <= MUX_v_2_2_2(2'b00, main_loop_4_mux1h_4_nl, catapult_conv2d_28_28_5_5_1_convolve_center_pixel_not_seb);
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0
          <= MUX_v_7_2_2(7'b0000000, main_loop_4_mux1h_5_nl, catapult_conv2d_28_28_5_5_1_convolve_center_pixel_not_seb);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_lpi_4_dfm_1_1 <= outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1
          & outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ outer_conv_loop_5_inner_conv_loop_5_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_4_lpi_4_dfm_1_1 <= outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1
          & outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ outer_conv_loop_5_inner_conv_loop_4_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_3_lpi_4_dfm_1 <= outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ outer_conv_loop_3_operator_28_true_acc_cse_4_sva_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_2_lpi_4_dfm_1 <= outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_1_lpi_4_dfm_1 <= outer_conv_loop_5_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_sva_1_5_1[4]));
      catapult_conv2d_28_28_5_5_1_in_bounds_return_8_lpi_4_dfm_1_1 <= outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1
          & outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ outer_conv_loop_5_inner_conv_loop_5_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_24_lpi_4_dfm_1_1 <= outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_ncse_3_sva_1_1
          & outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ outer_conv_loop_5_inner_conv_loop_4_slc_operator_28_true_slc_operator_28_true_acc_31_2_psp_4_1_itm_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_20_lpi_4_dfm_1 <= outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ outer_conv_loop_3_operator_28_true_acc_cse_4_sva_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_16_lpi_4_dfm_1 <= outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_12_lpi_4_dfm_1 <= outer_conv_loop_4_inner_conv_loop_2_catapult_conv2d_28_28_5_5_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_sva_1_5_1[4]));
      catapult_conv2d_28_28_5_5_1_in_bounds_return_6_lpi_4_dfm_1 <= (z_out[3]) &
          (~ inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_22_lpi_4_dfm_1 <= (z_out_1[3])
          & (~ inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_14_lpi_4_dfm_1 <= ~((readslicef_4_1_3(outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl))
          | inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_5_lpi_4_dfm_1 <= (readslicef_4_1_3(outer_conv_loop_1_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl))
          & (~ main_loop_2_asn_167_itm_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_21_lpi_4_dfm_1 <= (readslicef_4_1_3(outer_conv_loop_1_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl))
          & (~ main_loop_2_asn_167_itm_1);
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_itm_1
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9[1];
      operator_28_true_1_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_psp_1_4_2_itm_1
          <= catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp[4];
      catapult_conv2d_28_28_5_5_1_in_bounds_return_7_lpi_4_dfm_1 <= outer_conv_loop_3_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_itm_3_1
          & (~ (outer_conv_loop_3_operator_28_true_acc_1_tmp[4]));
      catapult_conv2d_28_28_5_5_1_in_bounds_return_23_lpi_4_dfm_1 <= outer_conv_loop_3_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_itm_3_1
          & (~ (outer_conv_loop_3_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_tmp[4]));
      catapult_conv2d_28_28_5_5_1_in_bounds_return_10_lpi_4_dfm_1 <= ~((catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[5])
          | outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_itm_5_1);
      catapult_conv2d_28_28_5_5_1_in_bounds_return_13_lpi_4_dfm_1 <= ~((readslicef_4_1_3(outer_conv_loop_1_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl))
          | (catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp[4]));
      catapult_conv2d_28_28_5_5_1_in_bounds_return_9_lpi_4_dfm_1 <= ~((catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[5])
          | (catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp[4]));
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_2_1 <= 5'b00000;
    end
    else if ( run_wen & (and_dcpl_11 | catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_2_1_mx1c1)
        & (fsm_output[8]) ) begin
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_2_1 <= MUX_v_5_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_if_2_acc_nl,
          catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva, catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc1_sva_2_1_mx1c1);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      inner_loop_i_sva <= 28'b0000000000000000000000000000;
    end
    else if ( run_wen & ((fsm_output[0]) | (fsm_output[5]) | inner_loop_i_sva_mx0c1)
        ) begin
      inner_loop_i_sva <= MUX_v_28_2_2(28'b0000000000000000000000000000, load_images_loop_i_sva_2,
          inner_loop_i_sva_mx0c1);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_index_13_0_lpi_3 <= 14'b00000000000000;
    end
    else if ( run_wen & (~((and_dcpl_53 & (~ (fsm_output[6]))) | ((~ catapult_conv2d_28_28_5_5_1_convolve_do_stage_0)
        & (fsm_output[6])))) ) begin
      weight_index_13_0_lpi_3 <= MUX_v_14_2_2(14'b00000000000000, (z_out[13:0]),
          or_tmp_260);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0 <= 1'b0;
    end
    else if ( run_wen & ((~(reg_catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_rgt_nl
        | (and_dcpl_86 & and_dcpl_83 & (~ (fsm_output[5])) & (~((fsm_output[7]) |
        (fsm_output[1]))) & and_dcpl_78 & (~ (fsm_output[8]))))) | (fsm_output[2])
        | catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_mx0c1 | (fsm_output[13]))
        ) begin
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0 <= (catapult_conv2d_28_28_5_5_1_convolve_do_mux_1_nl
          & (~((fsm_output[12]) | (fsm_output[23]))) & catapult_conv2d_28_28_5_5_1_convolve_do_nor_5_cse)
          | catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_mx0c1;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd <= 1'b0;
      reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1 <= 9'b000000000;
    end
    else if ( catapult_conv2d_28_28_5_5_1_convolve_count_and_ssc ) begin
      reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd <= (z_out_3[9]) & (~
          catapult_conv2d_28_28_5_5_1_convolve_count_9_0_sva_mx0c0);
      reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1 <= MUX_v_9_2_2(9'b000000000,
          ({mux_nl , mux_9_nl}), catapult_conv2d_28_28_5_5_1_convolve_count_not_1_nl);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_9
          <= 1'b0;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_8_7
          <= 2'b00;
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0
          <= 7'b0000000;
    end
    else if ( catapult_conv2d_28_28_5_5_1_convolve_center_pixel_and_4_cse ) begin
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_9
          <= MUX1HOT_s_1_3_2((z_out_7[9]), (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9[0]),
          catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_9,
          {(fsm_output[1]) , (fsm_output[8]) , (fsm_output[12])});
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_8_7
          <= MUX1HOT_v_2_3_2((z_out_7[8:7]), (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0[8:7]),
          catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7,
          {(fsm_output[1]) , (fsm_output[8]) , (fsm_output[12])});
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0
          <= MUX1HOT_v_7_4_2((z_out_7[6:0]), (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0[6:0]),
          catapult_conv2d_28_28_5_5_1_perform_max_pool_out_pixel_catapult_conv2d_28_28_5_5_1_perform_max_pool_out_pixel_and_nl,
          catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0,
          {(fsm_output[1]) , (fsm_output[8]) , or_471_nl , and_750_cse});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      inner_loop_acc_7_psp <= 6'b000000;
    end
    else if ( run_wen & (~((fsm_output[2]) | (fsm_output[8]))) ) begin
      inner_loop_acc_7_psp <= nl_inner_loop_acc_7_psp[5:0];
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_loop_1_o_sva <= 28'b0000000000000000000000000000;
    end
    else if ( run_wen & or_133_cse ) begin
      main_loop_1_o_sva <= MUX_v_28_2_2(28'b0000000000000000000000000000, main_loop_1_o_sva_2,
          (fsm_output[24]));
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_0_lpi_4 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(and_845_cse | ((or_dcpl_78 | or_dcpl_74) & (fsm_output[6]))))
        ) begin
      current_filter_0_lpi_4 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qr_lpi_2_dfm <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((~ (z_out_1[32])) | or_tmp_298 | or_tmp_75 | (fsm_output[11])
        | (fsm_output[17])) & (~(or_dcpl_147 | (fsm_output[7]) | (fsm_output[6])
        | (fsm_output[12]) | and_290_cse)) ) begin
      catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qr_lpi_2_dfm <= MUX1HOT_v_32_5_2(catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_2,
          catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_4_dfm_mx1w0, catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_and_nl,
          catapult_conv2d_28_28_5_5_1_max_cd_lpi_3_dfm, max_pool_shift_register_4_lpi_2,
          {or_tmp_298 , or_tmp_75 , (fsm_output[11]) , (fsm_output[13]) , (fsm_output[17])});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva <= 5'b00000;
    end
    else if ( run_wen & ((fsm_output[6]) | catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_mx0c1
        | (fsm_output[8]) | (fsm_output[12]) | (fsm_output[13])) ) begin
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva <= MUX_v_5_2_2(5'b00000,
          mux1h_1_nl, not_274_nl);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_1
          <= 1'b0;
    end
    else if ( run_wen & (~(or_dcpl_147 | (fsm_output[13]))) ) begin
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_1
          <= catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1 <= 1'b0;
    end
    else if ( run_wen & ((fsm_output[13]) | (fsm_output[8])) ) begin
      inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1 <= MUX_s_1_2_2(outer_conv_loop_2_inner_conv_loop_1_operator_28_true_1_acc_itm_5_1,
          (z_out_8[6]), fsm_output[13]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      operator_28_true_return_sva <= 1'b0;
      inner_loop_acc_5_sdt_3_0 <= 4'b0000;
    end
    else if ( catapult_conv2d_28_28_5_5_1_convolve_first_and_cse ) begin
      operator_28_true_return_sva <= ~((inner_loop_i_sva!=28'b0000000000000000000000000000));
      inner_loop_acc_5_sdt_3_0 <= z_out_7[3:0];
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva <= 7'b0000000;
    end
    else if ( run_wen & (catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c0 |
        (fsm_output[8]) | catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c2
        | catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c3 | (fsm_output[22]))
        ) begin
      catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva <= MUX1HOT_v_7_5_2(7'b1000110,
          catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_and_1_nl,
          (z_out_3[7:1]), catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0,
          catapult_conv2d_28_28_5_5_1_send_results_end_acc_nl, {catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c0
          , (fsm_output[8]) , catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c2
          , catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva_mx0c3 , (fsm_output[22])});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_3 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((fsm_output[15]) | (fsm_output[8])) ) begin
      catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_3 <= MUX_v_32_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_2,
          relu_output_rsci_q_d_mxwt, fsm_output[15]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(or_dcpl_171 | (fsm_output[23:20]!=4'b0000) | or_dcpl_157))
        ) begin
      catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_2 <= MUX_v_32_2_2(catapult_conv2d_28_28_5_5_1_max_cd_lpi_3_dfm,
          relu_output_rsci_q_d_mxwt, fsm_output[14]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_max_cd_lpi_3_dfm <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (catapult_conv2d_28_28_5_5_1_convolve_do_asn_6_itm_2 | (~
        catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_3) | (~ catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_itm_1)
        | (fsm_output[15]) | (fsm_output[17])) ) begin
      catapult_conv2d_28_28_5_5_1_max_cd_lpi_3_dfm <= MUX1HOT_v_32_5_2(catapult_conv2d_28_28_5_5_1_convolve_do_if_catapult_conv2d_28_28_5_5_1_convolve_do_if_and_nl,
          catapult_conv2d_28_28_5_5_1_convolve_do_qr_lpi_2, relu_output_rsci_q_d_mxwt,
          catapult_conv2d_28_28_5_5_1_convolve_do_if_qr_lpi_4_dfm_1_2, max_pool_shift_register_5_lpi_2,
          {catapult_conv2d_28_28_5_5_1_max_cd_and_1_nl , catapult_conv2d_28_28_5_5_1_max_cd_and_2_nl
          , catapult_conv2d_28_28_5_5_1_max_cd_and_3_nl , catapult_conv2d_28_28_5_5_1_max_cd_and_4_nl
          , (fsm_output[17])});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_5_itm <= 1'b0;
    end
    else if ( run_wen & (~(and_870_cse | (operator_28_true_3_acc_itm_4_1 & (fsm_output[13]))))
        ) begin
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_5_itm <= catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_nl
          & (~ (fsm_output[12]));
    end
  end
  assign or_461_nl = (fsm_output[23]) | (fsm_output[6]) | or_dcpl_138;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_aelse_mux_22_nl = MUX_s_1_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_mx0w0,
      catapult_conv2d_28_28_5_5_1_convolve_do_stage_0, or_461_nl);
  assign catapult_conv2d_28_28_5_5_1_convolve_row_mux_5_nl = MUX_v_5_2_2(main_loop_2_index_4_0_sva_mx0,
      catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva_mx1, fsm_output[8]);
  assign not_261_nl = ~ or_tmp_298;
  assign catapult_conv2d_28_28_5_5_1_convolve_center_pixel_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_mux_nl
      = MUX_v_2_2_2((z_out_5[10:9]), (z_out_7[10:9]), fsm_output[23]);
  assign catapult_conv2d_28_28_5_5_1_convolve_center_pixel_or_1_nl = MUX_v_2_2_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_mux_nl,
      2'b11, (fsm_output[7]));
  assign nor_62_nl = ~((~(or_dcpl_161 | (fsm_output[13]) | (fsm_output[12]) | (fsm_output[17])))
      | or_595_tmp);
  assign mux1h_nl = MUX1HOT_v_9_4_2(9'b111000110, catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0,
      (z_out_7[8:0]), (z_out_5[8:0]), {(fsm_output[7]) , nor_62_nl , (fsm_output[23])
      , or_595_tmp});
  assign and_1076_nl = nand_24_seb & (~(and_dcpl_127 & ((fsm_output[12]) | (fsm_output[13])
      | (fsm_output[17]))));
  assign nl_outer_conv_loop_1_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_1
      = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[3:1]) + conv_u2u_1_3(outer_conv_loop_3_operator_28_true_or_2);
  assign operator_28_true_1_or_nl = catapult_conv2d_28_28_5_5_1_convolve_row_slc_catapult_conv2d_28_28_5_5_1_convolve_catapult_conv2d_28_28_5_5_1_convolve_row_mux_1_cse_sva_1
      | (catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp[0]);
  assign nl_outer_conv_loop_4_inner_conv_loop_1_operator_28_true_1_acc_psp_4_2_sva_1
      = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp[3:1]) + conv_u2u_1_3(operator_28_true_1_or_nl);
  assign nl_operator_28_true_1_acc_1_psp_sva_1  = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_tmp[3:1])
      + 3'b001;
  assign nl_operator_28_true_acc_1_nl = catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp
      + 6'b000001;
  assign operator_28_true_acc_1_nl = nl_operator_28_true_acc_1_nl[5:0];
  assign nl_outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl
      = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[5:1]) + conv_u2s_1_5(outer_conv_loop_1_operator_28_true_and_2);
  assign outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl
      = nl_outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl[4:0];
  assign or_514_nl = (fsm_output[12:11]!=2'b00);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_aelse_mux_nl = MUX_s_1_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_2,
      relu_sva, or_514_nl);
  assign catapult_conv2d_28_28_5_5_1_convolve_center_pixel_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_or_nl
      = (z_out_5[10]) | (~ (fsm_output[8]));
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_and_1_nl
      = (z_out_3[0]) & catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_land_1_lpi_3_dfm_mx0w5;
  assign or_522_nl = or_dcpl_171 | (fsm_output[14]) | (fsm_output[16]);
  assign catapult_conv2d_28_28_5_5_1_convolve_center_pixel_mux1h_17_nl = MUX1HOT_s_1_3_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_or_nl,
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_and_1_nl,
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm,
      {and_845_cse , (fsm_output[13]) , or_522_nl});
  assign nl_inner_conv_loop_acc_24_itm_1  = inner_conv_loop_acc_18_itm_1 + inner_conv_loop_acc_17_itm_2
      + inner_conv_loop_acc_16_itm_1 + inner_conv_loop_acc_15_itm_1;
  assign nl_inner_conv_loop_acc_itm_1  = inner_conv_loop_acc_19_itm_1 + inner_conv_loop_acc_20_itm_1
      + inner_conv_loop_acc_13_itm_2;
  assign inner_conv_loop_inner_conv_loop_and_25_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_4_lpi_4_dfm_1_2);
  assign inner_conv_loop_inner_conv_loop_and_26_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_lpi_4_dfm_1_2);
  assign nl_inner_conv_loop_acc_20_itm_1  = inner_conv_loop_inner_conv_loop_and_itm_1
      + inner_conv_loop_inner_conv_loop_and_25_nl + inner_conv_loop_inner_conv_loop_and_26_nl;
  assign inner_conv_loop_inner_conv_loop_and_4_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_21_lpi_4_dfm_2);
  assign inner_conv_loop_inner_conv_loop_and_5_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_5_lpi_4_dfm_2);
  assign inner_conv_loop_inner_conv_loop_and_7_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_14_lpi_4_dfm_2);
  assign nl_inner_conv_loop_acc_19_itm_1  = inner_conv_loop_inner_conv_loop_and_4_nl
      + inner_conv_loop_inner_conv_loop_and_5_nl + inner_conv_loop_inner_conv_loop_and_6_itm_1
      + inner_conv_loop_inner_conv_loop_and_7_nl;
  assign inner_conv_loop_inner_conv_loop_and_10_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_22_lpi_4_dfm_2);
  assign inner_conv_loop_inner_conv_loop_and_11_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_6_lpi_4_dfm_2);
  assign nl_inner_conv_loop_acc_18_itm_1  = inner_conv_loop_inner_conv_loop_and_9_itm_1
      + inner_conv_loop_inner_conv_loop_and_10_nl + inner_conv_loop_inner_conv_loop_and_11_nl
      + inner_conv_loop_inner_conv_loop_and_12_itm_1;
  assign inner_conv_loop_inner_conv_loop_and_17_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_12_lpi_4_dfm_2);
  assign inner_conv_loop_inner_conv_loop_and_18_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_16_lpi_4_dfm_2);
  assign inner_conv_loop_inner_conv_loop_and_19_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_20_lpi_4_dfm_2);
  assign inner_conv_loop_inner_conv_loop_and_20_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_24_lpi_4_dfm_1_2);
  assign nl_inner_conv_loop_acc_16_itm_1  = inner_conv_loop_inner_conv_loop_and_17_nl
      + inner_conv_loop_inner_conv_loop_and_18_nl + inner_conv_loop_inner_conv_loop_and_19_nl
      + inner_conv_loop_inner_conv_loop_and_20_nl;
  assign inner_conv_loop_inner_conv_loop_and_21_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_8_lpi_4_dfm_1_2);
  assign inner_conv_loop_inner_conv_loop_and_22_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_1_lpi_4_dfm_2);
  assign inner_conv_loop_inner_conv_loop_and_23_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_2_lpi_4_dfm_2);
  assign inner_conv_loop_inner_conv_loop_and_24_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_3_lpi_4_dfm_2);
  assign nl_inner_conv_loop_acc_15_itm_1  = inner_conv_loop_inner_conv_loop_and_21_nl
      + inner_conv_loop_inner_conv_loop_and_22_nl + inner_conv_loop_inner_conv_loop_and_23_nl
      + inner_conv_loop_inner_conv_loop_and_24_nl;
  assign operator_28_true_1_not_6_nl = ~ catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_5_itm;
  assign inner_conv_loop_not_39_nl = ~ outer_conv_loop_3_operator_28_true_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_5_1_5_itm_1;
  assign inner_conv_loop_inner_conv_loop_and_1_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_13_lpi_4_dfm_2);
  assign inner_conv_loop_not_32_nl = ~ operator_28_true_1_slc_catapult_conv2d_28_28_5_5_1_convolve_do_acc_8_psp_1_4_2_itm_2;
  assign inner_conv_loop_inner_conv_loop_and_3_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z_oreg, inner_conv_loop_not_32_nl);
  assign nl_inner_conv_loop_acc_13_itm_1  = inner_conv_loop_inner_conv_loop_and_1_nl
      + inner_conv_loop_inner_conv_loop_and_3_nl;
  assign operator_28_true_not_nl = ~ outer_conv_loop_3_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_cse_4_sva_2;
  assign inner_conv_loop_inner_conv_loop_and_13_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z_oreg, operator_28_true_not_nl);
  assign operator_28_true_not_3_nl = ~ outer_conv_loop_3_operator_28_true_acc_cse_4_sva_2;
  assign inner_conv_loop_inner_conv_loop_and_14_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z_oreg, operator_28_true_not_3_nl);
  assign inner_conv_loop_inner_conv_loop_and_15_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_23_lpi_4_dfm_2);
  assign inner_conv_loop_inner_conv_loop_and_16_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z_oreg, catapult_conv2d_28_28_5_5_1_in_bounds_return_7_lpi_4_dfm_2);
  assign nl_inner_conv_loop_acc_17_itm_1  = inner_conv_loop_inner_conv_loop_and_13_nl
      + inner_conv_loop_inner_conv_loop_and_14_nl + inner_conv_loop_inner_conv_loop_and_15_nl
      + inner_conv_loop_inner_conv_loop_and_16_nl;
  assign main_loop_4_main_loop_4_mux_nl = MUX_s_1_2_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_9,
      (z_out_2[9]), fsm_output[12]);
  assign main_loop_4_mux1h_4_nl = MUX1HOT_v_2_3_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_8_7,
      (z_out_2[8:7]), catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7,
      {(fsm_output[8]) , main_loop_4_or_itm , or_dcpl_161});
  assign main_loop_4_mux1h_5_nl = MUX1HOT_v_7_3_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0,
      (z_out_2[6:0]), catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0,
      {(fsm_output[8]) , main_loop_4_or_itm , or_dcpl_161});
  assign nl_outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl
      = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_sva_1_5_1[3:0]) + conv_u2s_1_4(outer_conv_loop_2_operator_28_true_and_itm_1);
  assign outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl
      = nl_outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl[3:0];
  assign nl_outer_conv_loop_1_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl
      = ({1'b1 , outer_conv_loop_1_operator_28_true_acc_1_itm_1}) + 4'b0001;
  assign outer_conv_loop_1_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl
      = nl_outer_conv_loop_1_inner_conv_loop_5_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl[3:0];
  assign nl_outer_conv_loop_1_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl
      = ({1'b1 , outer_conv_loop_1_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_1})
      + 4'b0001;
  assign outer_conv_loop_1_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl
      = nl_outer_conv_loop_1_inner_conv_loop_4_catapult_conv2d_28_28_5_5_1_in_bounds_if_3_acc_nl[3:0];
  assign nl_outer_conv_loop_1_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl
      = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[4:1]) + conv_u2s_1_4(outer_conv_loop_1_operator_28_true_and_2);
  assign outer_conv_loop_1_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl
      = nl_outer_conv_loop_1_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl[3:0];
  assign nl_catapult_conv2d_28_28_5_5_1_convolve_do_if_2_acc_nl = catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc0_sva_mx1
      + 5'b00001;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_2_acc_nl = nl_catapult_conv2d_28_28_5_5_1_convolve_do_if_2_acc_nl[4:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_do_mux_1_nl = MUX_s_1_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_mx0w0,
      catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_land_1_lpi_3_dfm_mx0w5,
      fsm_output[13]);
  assign catapult_conv2d_28_28_5_5_1_convolve_do_or_4_nl = (~ catapult_conv2d_28_28_5_5_1_convolve_do_stage_0)
      | catapult_conv2d_28_28_5_5_1_convolve_do_acc_10_itm_6_1;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_or_5_nl = (~(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_9
      & (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7[1])
      & (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0[3])))
      | (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0[2:0]!=3'b111)
      | (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7[0])
      | (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0[6:4]!=3'b000)
      | (~ reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd) | (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1!=9'b100001111)
      | (~ catapult_conv2d_28_28_5_5_1_convolve_do_stage_0);
  assign nl_operator_28_true_4_acc_nl = ({catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9
      , catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0}) + ({1'b1
      , (signext_8_7(~ catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva)) , 2'b00})
      + 11'b00000000001;
  assign operator_28_true_4_acc_nl = nl_operator_28_true_4_acc_nl[10:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_do_or_6_nl = (~ catapult_conv2d_28_28_5_5_1_convolve_do_stage_0)
      | (readslicef_11_1_10(operator_28_true_4_acc_nl));
  assign reg_catapult_conv2d_28_28_5_5_1_convolve_do_stage_0_rgt_nl = MUX1HOT_s_1_4_2(operator_28_true_acc_itm_5_1,
      catapult_conv2d_28_28_5_5_1_convolve_do_or_4_nl, catapult_conv2d_28_28_5_5_1_convolve_do_or_5_nl,
      catapult_conv2d_28_28_5_5_1_convolve_do_or_6_nl, {(fsm_output[6]) , (fsm_output[8])
      , (fsm_output[12]) , (fsm_output[23])});
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_mux1h_nl = MUX1HOT_v_2_3_2((reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[8:7]),
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7,
      (z_out_2[8:7]), {or_546_ssc , (fsm_output[17]) , (fsm_output[21])});
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_and_nl = MUX_v_2_2_2(2'b00,
      catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_mux1h_nl, nand_27_seb);
  assign mux_nl = MUX_v_2_2_2((z_out_3[8:7]), catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_and_nl,
      or_589_ssc);
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_mux1h_2_nl = MUX1HOT_v_7_3_2((reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[6:0]),
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0,
      (z_out_2[6:0]), {or_546_ssc , (fsm_output[17]) , (fsm_output[21])});
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_and_1_nl = MUX_v_7_2_2(7'b0000000,
      catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_mux1h_2_nl, nand_27_seb);
  assign mux_9_nl = MUX_v_7_2_2((z_out_3[6:0]), catapult_conv2d_28_28_5_5_1_perform_max_pool_tail_pixel_and_1_nl,
      or_589_ssc);
  assign catapult_conv2d_28_28_5_5_1_convolve_count_not_1_nl = ~ catapult_conv2d_28_28_5_5_1_convolve_count_9_0_sva_mx0c0;
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_out_pixel_mux_nl = MUX_v_7_2_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0,
      catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva, fsm_output[17]);
  assign nand_28_nl = ~(and_dcpl_106 & and_dcpl_127);
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_out_pixel_catapult_conv2d_28_28_5_5_1_perform_max_pool_out_pixel_and_nl
      = MUX_v_7_2_2(7'b0000000, catapult_conv2d_28_28_5_5_1_perform_max_pool_out_pixel_mux_nl,
      nand_28_nl);
  assign or_471_nl = and_1057_cse | or_dcpl_147 | (fsm_output[13]) | (fsm_output[17]);
  assign nl_load_images_loop_acc_10_nl = (~ (inner_loop_i_sva[5:0])) + ({(inner_loop_i_sva[3:0])
      , 2'b01});
  assign load_images_loop_acc_10_nl = nl_load_images_loop_acc_10_nl[5:0];
  assign nl_inner_loop_acc_7_psp  = (z_out_7[9:4]) + load_images_loop_acc_10_nl;
  assign catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_and_nl
      = MUX_v_32_2_2(32'b00000000000000000000000000000000, bias_memory_rsci_q_d_mxwt,
      bias_sva);
  assign and_1005_nl = (fsm_output[13]) & (~ or_555_tmp);
  assign mux1h_1_nl = MUX1HOT_v_5_4_2(main_loop_2_index_4_0_sva_2, catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva_mx1,
      5'b10010, z_out_4, {(fsm_output[6]) , (fsm_output[8]) , (fsm_output[12]) ,
      and_1005_nl});
  assign not_274_nl = ~ or_555_tmp;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_and_1_nl
      = MUX_v_7_2_2(7'b0000000, (z_out_2[6:0]), operator_28_true_1_acc_itm_5_1);
  assign nl_catapult_conv2d_28_28_5_5_1_send_results_end_acc_nl = ({(~ max_pool_sva)
      , max_pool_sva , max_pool_sva , 1'b0 , (~ max_pool_sva) , 1'b0 , max_pool_sva})
      + 7'b1111111;
  assign catapult_conv2d_28_28_5_5_1_send_results_end_acc_nl = nl_catapult_conv2d_28_28_5_5_1_send_results_end_acc_nl[6:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_first_not_nl = ~ operator_28_true_return_sva;
  assign catapult_conv2d_28_28_5_5_1_convolve_do_if_catapult_conv2d_28_28_5_5_1_convolve_do_if_and_nl
      = MUX_v_32_2_2(32'b00000000000000000000000000000000, convolution_output_rsci_q_d_mxwt,
      catapult_conv2d_28_28_5_5_1_convolve_first_not_nl);
  assign catapult_conv2d_28_28_5_5_1_max_cd_and_1_nl = (~ catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_itm_1)
      & (fsm_output[8]);
  assign catapult_conv2d_28_28_5_5_1_max_cd_and_2_nl = catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_itm_1
      & (fsm_output[8]);
  assign catapult_conv2d_28_28_5_5_1_max_cd_and_3_nl = (~ catapult_conv2d_28_28_5_5_1_max_cd_acc_itm_32)
      & (fsm_output[15]);
  assign catapult_conv2d_28_28_5_5_1_max_cd_and_4_nl = catapult_conv2d_28_28_5_5_1_max_cd_acc_itm_32
      & (fsm_output[15]);
  assign catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_nl = MUX_s_1_2_2(inner_conv_loop_1_operator_28_true_1_acc_cse_2_5_sva_1,
      (~ catapult_conv2d_28_28_5_5_1_perform_max_pool_if_do_mux_5_itm), and_1000_cse);
  assign main_loop_2_mux1h_8_nl = MUX1HOT_v_25_3_2((signext_25_11(weight_index_13_0_lpi_3[13:3])),
      (main_loop_1_o_sva_2[27:3]), (load_images_loop_i_sva_2[27:3]), {(fsm_output[6])
      , (fsm_output[24]) , inner_loop_i_sva_mx0c1});
  assign main_loop_2_or_3_nl = MUX_v_25_2_2(main_loop_2_mux1h_8_nl, 25'b1111111111111111111111111,
      (fsm_output[8]));
  assign nl_outer_conv_loop_2_operator_28_true_acc_2_nl = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_sva_1_5_1[2:0])
      + 3'b001;
  assign outer_conv_loop_2_operator_28_true_acc_2_nl = nl_outer_conv_loop_2_operator_28_true_acc_2_nl[2:0];
  assign main_loop_2_mux1h_9_nl = MUX1HOT_v_3_4_2((weight_index_13_0_lpi_3[2:0]),
      outer_conv_loop_2_operator_28_true_acc_2_nl, (main_loop_1_o_sva_2[2:0]), (load_images_loop_i_sva_2[2:0]),
      {(fsm_output[6]) , (fsm_output[8]) , (fsm_output[24]) , inner_loop_i_sva_mx0c1});
  assign main_loop_2_or_4_nl = catapult_conv2d_28_28_5_5_1_convolve_do_nor_5_cse
      | (fsm_output[24]) | inner_loop_i_sva_mx0c1;
  assign main_loop_2_or_5_nl = (fsm_output[6]) | (fsm_output[8]);
  assign main_loop_2_mux1h_10_nl = MUX1HOT_v_28_3_2(28'b0000000000000000000000000001,
      (~ num_output_images_sva), (~ num_input_images_sva), {main_loop_2_or_5_nl ,
      (fsm_output[24]) , inner_loop_i_sva_mx0c1});
  assign nl_acc_nl = conv_s2u_29_30({main_loop_2_or_3_nl , main_loop_2_mux1h_9_nl
      , main_loop_2_or_4_nl}) + conv_s2u_29_30({main_loop_2_mux1h_10_nl , 1'b1});
  assign acc_nl = nl_acc_nl[29:0];
  assign z_out = readslicef_30_29_1(acc_nl);
  assign catapult_conv2d_28_28_5_5_1_in_bounds_if_3_mux1h_5_nl = MUX1HOT_v_29_4_2((signext_29_25(~
      (num_input_images_rsci_idat[27:3]))), (signext_29_25(~ (num_output_images_sva[27:3]))),
      (convolution_output_rsci_q_d_mxwt[31:3]), (catapult_conv2d_28_28_5_5_1_max_cd_lpi_3_dfm[31:3]),
      {(fsm_output[0]) , (fsm_output[4]) , (fsm_output[12]) , or_tmp_379});
  assign catapult_conv2d_28_28_5_5_1_in_bounds_if_3_or_3_nl = MUX_v_29_2_2(catapult_conv2d_28_28_5_5_1_in_bounds_if_3_mux1h_5_nl,
      29'b11111111111111111111111111111, (fsm_output[8]));
  assign nl_outer_conv_loop_2_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_1_nl
      = (catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_psp_1_sva_1_5_1[2:0]) + conv_u2s_1_3(outer_conv_loop_2_operator_28_true_or_itm_1);
  assign outer_conv_loop_2_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_1_nl
      = nl_outer_conv_loop_2_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_1_nl[2:0];
  assign catapult_conv2d_28_28_5_5_1_in_bounds_if_3_mux1h_6_nl = MUX1HOT_v_3_5_2(outer_conv_loop_2_inner_conv_loop_4_operator_28_true_slc_operator_28_true_operator_28_true_acc_1_nl,
      (~ (num_input_images_rsci_idat[2:0])), (~ (num_output_images_sva[2:0])), (convolution_output_rsci_q_d_mxwt[2:0]),
      (catapult_conv2d_28_28_5_5_1_max_cd_lpi_3_dfm[2:0]), {(fsm_output[8]) , (fsm_output[0])
      , (fsm_output[4]) , (fsm_output[12]) , or_tmp_379});
  assign catapult_conv2d_28_28_5_5_1_in_bounds_if_3_or_4_nl = (~((fsm_output[8])
      | (fsm_output[0]) | (fsm_output[4]) | (fsm_output[12]))) | or_tmp_379;
  assign catapult_conv2d_28_28_5_5_1_in_bounds_if_3_or_5_nl = (fsm_output[8]) | (fsm_output[0])
      | (fsm_output[4]);
  assign catapult_conv2d_28_28_5_5_1_in_bounds_if_3_mux1h_7_nl = MUX1HOT_v_32_3_2(32'b00000000000000000000000000000001,
      catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qr_lpi_2_dfm, (~ catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qr_lpi_2_dfm),
      {catapult_conv2d_28_28_5_5_1_in_bounds_if_3_or_5_nl , (fsm_output[12]) , or_tmp_379});
  assign nl_acc_1_nl = conv_s2u_33_34({catapult_conv2d_28_28_5_5_1_in_bounds_if_3_or_3_nl
      , catapult_conv2d_28_28_5_5_1_in_bounds_if_3_mux1h_6_nl , catapult_conv2d_28_28_5_5_1_in_bounds_if_3_or_4_nl})
      + conv_s2u_33_34({catapult_conv2d_28_28_5_5_1_in_bounds_if_3_mux1h_7_nl , 1'b1});
  assign acc_1_nl = nl_acc_1_nl[33:0];
  assign z_out_1 = readslicef_34_33_1(acc_1_nl);
  assign catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_4_nl = MUX1HOT_s_1_4_2((bias_index_9_0_sva[9]),
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_9,
      (catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva[6]), (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[8]),
      {(fsm_output[10]) , (fsm_output[12]) , (fsm_output[8]) , or_tmp_383});
  assign catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_5_nl = MUX1HOT_v_2_4_2((bias_index_9_0_sva[8:7]),
      catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_8_7,
      (signext_2_1(catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva[6])), (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[8:7]),
      {(fsm_output[10]) , (fsm_output[12]) , (fsm_output[8]) , or_tmp_383});
  assign catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_6_nl = MUX1HOT_s_1_4_2((bias_index_9_0_sva[6]),
      (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0[6]),
      (catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva[6]), (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[6]),
      {(fsm_output[10]) , (fsm_output[12]) , (fsm_output[8]) , or_tmp_383});
  assign catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_7_nl = MUX1HOT_v_6_4_2((bias_index_9_0_sva[5:0]),
      (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_2_6_0[5:0]),
      (catapult_conv2d_28_28_5_5_1_convolve_col_6_0_sva[5:0]), (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[5:0]),
      {(fsm_output[10]) , (fsm_output[12]) , (fsm_output[8]) , or_tmp_383});
  assign nl_z_out_2 = ({catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_4_nl
      , catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_5_nl , catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_6_nl
      , catapult_conv2d_28_28_5_5_1_apply_bias_bias_value_qif_mux1h_7_nl}) + 10'b0000000001;
  assign z_out_2 = nl_z_out_2[9:0];
  assign main_loop_3_mux_1_nl = MUX_v_10_2_2(({reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd
      , reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1}), (signext_10_8({catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0
      , catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_10_1_itm_1})),
      fsm_output[13]);
  assign nl_z_out_3 = main_loop_3_mux_1_nl + 10'b0000000001;
  assign z_out_3 = nl_z_out_3[9:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_and_4_nl
      = MUX_v_3_2_2(3'b000, (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva[4:2]),
      (fsm_output[13]));
  assign catapult_conv2d_28_28_5_5_1_convolve_do_mux_6_nl = MUX_v_2_2_2(catapult_conv2d_28_28_5_5_1_convolve_do_pcnxt_sva,
      (catapult_conv2d_28_28_5_5_1_convolve_row_4_0_pc3_sva[1:0]), fsm_output[13]);
  assign nl_z_out_4 = ({catapult_conv2d_28_28_5_5_1_convolve_do_catapult_conv2d_28_28_5_5_1_convolve_do_and_4_nl
      , catapult_conv2d_28_28_5_5_1_convolve_do_mux_6_nl}) + 5'b00001;
  assign z_out_4 = nl_z_out_4[4:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_do_mux_7_nl = MUX_v_2_2_2(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9,
      (signext_2_1(catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0[8])),
      fsm_output[13]);
  assign nl_z_out_5 = ({catapult_conv2d_28_28_5_5_1_convolve_do_mux_7_nl , catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0})
      + 11'b00000000001;
  assign z_out_5 = nl_z_out_5[10:0];
  assign catapult_conv2d_28_28_5_5_1_convolve_do_qif_mux_4_nl = MUX_v_4_2_2(inner_loop_acc_5_sdt_3_0,
      (catapult_conv2d_28_28_5_5_1_convolve_center_pixel_slc_catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_9_0_1_itm_1_6_0[3:0]),
      fsm_output[2]);
  assign nl_z_out_6 = ({inner_loop_acc_7_psp , catapult_conv2d_28_28_5_5_1_convolve_do_qif_mux_4_nl
      , (inner_loop_i_sva[2:0])}) + conv_u2u_9_13({reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd
      , (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[8:1])});
  assign z_out_6 = nl_z_out_6[12:0];
  assign main_loop_5_mux_2_nl = MUX_s_1_2_2((catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_10_9[0]),
      (inner_loop_i_sva[12]), or_tmp_393);
  assign main_loop_5_main_loop_5_and_2_nl = main_loop_5_mux_2_nl & (~ (fsm_output[8]));
  assign main_loop_5_mux_3_nl = MUX_v_3_2_2((catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0[8:6]),
      (inner_loop_i_sva[11:9]), or_tmp_393);
  assign not_295_nl = ~ (fsm_output[8]);
  assign main_loop_5_main_loop_5_and_3_nl = MUX_v_3_2_2(3'b000, main_loop_5_mux_3_nl,
      not_295_nl);
  assign main_loop_5_mux1h_4_nl = MUX1HOT_v_6_3_2((catapult_conv2d_28_28_5_5_1_convolve_center_pixel_10_0_sva_8_0[5:0]),
      (inner_loop_i_sva[8:3]), ({reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd
      , (reg_catapult_conv2d_28_28_5_5_1_convolve_count_9_0_ftd_1[8:4])}), {(fsm_output[23])
      , or_tmp_393 , (fsm_output[8])});
  assign main_loop_5_mux1h_5_nl = MUX1HOT_v_10_3_2(10'b0000000001, (inner_loop_i_sva[9:0]),
      10'b1111001111, {(fsm_output[23]) , or_tmp_393 , (fsm_output[8])});
  assign nl_z_out_7 = conv_u2u_10_11({main_loop_5_main_loop_5_and_2_nl , main_loop_5_main_loop_5_and_3_nl
      , main_loop_5_mux1h_4_nl}) + conv_u2u_10_11(main_loop_5_mux1h_5_nl);
  assign z_out_7 = nl_z_out_7[10:0];
  assign operator_28_true_mux_2_nl = MUX_v_6_2_2((signext_6_3(catapult_conv2d_28_28_5_5_1_convolve_do_acc_9_tmp[3:1])),
      (z_out_5[8:3]), fsm_output[13]);
  assign nl_z_out_8 = ({(fsm_output[13]) , 2'b00 , (signext_3_1(fsm_output[13]))
      , 1'b1}) + conv_u2u_6_7(operator_28_true_mux_2_nl);
  assign z_out_8 = nl_z_out_8[6:0];

  function automatic  MUX1HOT_s_1_3_2;
    input  input_2;
    input  input_1;
    input  input_0;
    input [2:0] sel;
    reg  result;
  begin
    result = input_0 & sel[0];
    result = result | (input_1 & sel[1]);
    result = result | (input_2 & sel[2]);
    MUX1HOT_s_1_3_2 = result;
  end
  endfunction


  function automatic  MUX1HOT_s_1_4_2;
    input  input_3;
    input  input_2;
    input  input_1;
    input  input_0;
    input [3:0] sel;
    reg  result;
  begin
    result = input_0 & sel[0];
    result = result | (input_1 & sel[1]);
    result = result | (input_2 & sel[2]);
    result = result | (input_3 & sel[3]);
    MUX1HOT_s_1_4_2 = result;
  end
  endfunction


  function automatic [9:0] MUX1HOT_v_10_3_2;
    input [9:0] input_2;
    input [9:0] input_1;
    input [9:0] input_0;
    input [2:0] sel;
    reg [9:0] result;
  begin
    result = input_0 & {10{sel[0]}};
    result = result | (input_1 & {10{sel[1]}});
    result = result | (input_2 & {10{sel[2]}});
    MUX1HOT_v_10_3_2 = result;
  end
  endfunction


  function automatic [24:0] MUX1HOT_v_25_3_2;
    input [24:0] input_2;
    input [24:0] input_1;
    input [24:0] input_0;
    input [2:0] sel;
    reg [24:0] result;
  begin
    result = input_0 & {25{sel[0]}};
    result = result | (input_1 & {25{sel[1]}});
    result = result | (input_2 & {25{sel[2]}});
    MUX1HOT_v_25_3_2 = result;
  end
  endfunction


  function automatic [27:0] MUX1HOT_v_28_3_2;
    input [27:0] input_2;
    input [27:0] input_1;
    input [27:0] input_0;
    input [2:0] sel;
    reg [27:0] result;
  begin
    result = input_0 & {28{sel[0]}};
    result = result | (input_1 & {28{sel[1]}});
    result = result | (input_2 & {28{sel[2]}});
    MUX1HOT_v_28_3_2 = result;
  end
  endfunction


  function automatic [28:0] MUX1HOT_v_29_4_2;
    input [28:0] input_3;
    input [28:0] input_2;
    input [28:0] input_1;
    input [28:0] input_0;
    input [3:0] sel;
    reg [28:0] result;
  begin
    result = input_0 & {29{sel[0]}};
    result = result | (input_1 & {29{sel[1]}});
    result = result | (input_2 & {29{sel[2]}});
    result = result | (input_3 & {29{sel[3]}});
    MUX1HOT_v_29_4_2 = result;
  end
  endfunction


  function automatic [1:0] MUX1HOT_v_2_3_2;
    input [1:0] input_2;
    input [1:0] input_1;
    input [1:0] input_0;
    input [2:0] sel;
    reg [1:0] result;
  begin
    result = input_0 & {2{sel[0]}};
    result = result | (input_1 & {2{sel[1]}});
    result = result | (input_2 & {2{sel[2]}});
    MUX1HOT_v_2_3_2 = result;
  end
  endfunction


  function automatic [1:0] MUX1HOT_v_2_4_2;
    input [1:0] input_3;
    input [1:0] input_2;
    input [1:0] input_1;
    input [1:0] input_0;
    input [3:0] sel;
    reg [1:0] result;
  begin
    result = input_0 & {2{sel[0]}};
    result = result | (input_1 & {2{sel[1]}});
    result = result | (input_2 & {2{sel[2]}});
    result = result | (input_3 & {2{sel[3]}});
    MUX1HOT_v_2_4_2 = result;
  end
  endfunction


  function automatic [31:0] MUX1HOT_v_32_3_2;
    input [31:0] input_2;
    input [31:0] input_1;
    input [31:0] input_0;
    input [2:0] sel;
    reg [31:0] result;
  begin
    result = input_0 & {32{sel[0]}};
    result = result | (input_1 & {32{sel[1]}});
    result = result | (input_2 & {32{sel[2]}});
    MUX1HOT_v_32_3_2 = result;
  end
  endfunction


  function automatic [31:0] MUX1HOT_v_32_5_2;
    input [31:0] input_4;
    input [31:0] input_3;
    input [31:0] input_2;
    input [31:0] input_1;
    input [31:0] input_0;
    input [4:0] sel;
    reg [31:0] result;
  begin
    result = input_0 & {32{sel[0]}};
    result = result | (input_1 & {32{sel[1]}});
    result = result | (input_2 & {32{sel[2]}});
    result = result | (input_3 & {32{sel[3]}});
    result = result | (input_4 & {32{sel[4]}});
    MUX1HOT_v_32_5_2 = result;
  end
  endfunction


  function automatic [2:0] MUX1HOT_v_3_4_2;
    input [2:0] input_3;
    input [2:0] input_2;
    input [2:0] input_1;
    input [2:0] input_0;
    input [3:0] sel;
    reg [2:0] result;
  begin
    result = input_0 & {3{sel[0]}};
    result = result | (input_1 & {3{sel[1]}});
    result = result | (input_2 & {3{sel[2]}});
    result = result | (input_3 & {3{sel[3]}});
    MUX1HOT_v_3_4_2 = result;
  end
  endfunction


  function automatic [2:0] MUX1HOT_v_3_5_2;
    input [2:0] input_4;
    input [2:0] input_3;
    input [2:0] input_2;
    input [2:0] input_1;
    input [2:0] input_0;
    input [4:0] sel;
    reg [2:0] result;
  begin
    result = input_0 & {3{sel[0]}};
    result = result | (input_1 & {3{sel[1]}});
    result = result | (input_2 & {3{sel[2]}});
    result = result | (input_3 & {3{sel[3]}});
    result = result | (input_4 & {3{sel[4]}});
    MUX1HOT_v_3_5_2 = result;
  end
  endfunction


  function automatic [4:0] MUX1HOT_v_5_4_2;
    input [4:0] input_3;
    input [4:0] input_2;
    input [4:0] input_1;
    input [4:0] input_0;
    input [3:0] sel;
    reg [4:0] result;
  begin
    result = input_0 & {5{sel[0]}};
    result = result | (input_1 & {5{sel[1]}});
    result = result | (input_2 & {5{sel[2]}});
    result = result | (input_3 & {5{sel[3]}});
    MUX1HOT_v_5_4_2 = result;
  end
  endfunction


  function automatic [5:0] MUX1HOT_v_6_3_2;
    input [5:0] input_2;
    input [5:0] input_1;
    input [5:0] input_0;
    input [2:0] sel;
    reg [5:0] result;
  begin
    result = input_0 & {6{sel[0]}};
    result = result | (input_1 & {6{sel[1]}});
    result = result | (input_2 & {6{sel[2]}});
    MUX1HOT_v_6_3_2 = result;
  end
  endfunction


  function automatic [5:0] MUX1HOT_v_6_4_2;
    input [5:0] input_3;
    input [5:0] input_2;
    input [5:0] input_1;
    input [5:0] input_0;
    input [3:0] sel;
    reg [5:0] result;
  begin
    result = input_0 & {6{sel[0]}};
    result = result | (input_1 & {6{sel[1]}});
    result = result | (input_2 & {6{sel[2]}});
    result = result | (input_3 & {6{sel[3]}});
    MUX1HOT_v_6_4_2 = result;
  end
  endfunction


  function automatic [6:0] MUX1HOT_v_7_3_2;
    input [6:0] input_2;
    input [6:0] input_1;
    input [6:0] input_0;
    input [2:0] sel;
    reg [6:0] result;
  begin
    result = input_0 & {7{sel[0]}};
    result = result | (input_1 & {7{sel[1]}});
    result = result | (input_2 & {7{sel[2]}});
    MUX1HOT_v_7_3_2 = result;
  end
  endfunction


  function automatic [6:0] MUX1HOT_v_7_4_2;
    input [6:0] input_3;
    input [6:0] input_2;
    input [6:0] input_1;
    input [6:0] input_0;
    input [3:0] sel;
    reg [6:0] result;
  begin
    result = input_0 & {7{sel[0]}};
    result = result | (input_1 & {7{sel[1]}});
    result = result | (input_2 & {7{sel[2]}});
    result = result | (input_3 & {7{sel[3]}});
    MUX1HOT_v_7_4_2 = result;
  end
  endfunction


  function automatic [6:0] MUX1HOT_v_7_5_2;
    input [6:0] input_4;
    input [6:0] input_3;
    input [6:0] input_2;
    input [6:0] input_1;
    input [6:0] input_0;
    input [4:0] sel;
    reg [6:0] result;
  begin
    result = input_0 & {7{sel[0]}};
    result = result | (input_1 & {7{sel[1]}});
    result = result | (input_2 & {7{sel[2]}});
    result = result | (input_3 & {7{sel[3]}});
    result = result | (input_4 & {7{sel[4]}});
    MUX1HOT_v_7_5_2 = result;
  end
  endfunction


  function automatic [8:0] MUX1HOT_v_9_4_2;
    input [8:0] input_3;
    input [8:0] input_2;
    input [8:0] input_1;
    input [8:0] input_0;
    input [3:0] sel;
    reg [8:0] result;
  begin
    result = input_0 & {9{sel[0]}};
    result = result | (input_1 & {9{sel[1]}});
    result = result | (input_2 & {9{sel[2]}});
    result = result | (input_3 & {9{sel[3]}});
    MUX1HOT_v_9_4_2 = result;
  end
  endfunction


  function automatic  MUX_s_1_2_2;
    input  input_0;
    input  input_1;
    input  sel;
    reg  result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_s_1_2_2 = result;
  end
  endfunction


  function automatic  MUX_s_1_4_2;
    input  input_0;
    input  input_1;
    input  input_2;
    input  input_3;
    input [1:0] sel;
    reg  result;
  begin
    case (sel)
      2'b00 : begin
        result = input_0;
      end
      2'b01 : begin
        result = input_1;
      end
      2'b10 : begin
        result = input_2;
      end
      default : begin
        result = input_3;
      end
    endcase
    MUX_s_1_4_2 = result;
  end
  endfunction


  function automatic [9:0] MUX_v_10_2_2;
    input [9:0] input_0;
    input [9:0] input_1;
    input  sel;
    reg [9:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_10_2_2 = result;
  end
  endfunction


  function automatic [13:0] MUX_v_14_2_2;
    input [13:0] input_0;
    input [13:0] input_1;
    input  sel;
    reg [13:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_14_2_2 = result;
  end
  endfunction


  function automatic [24:0] MUX_v_25_2_2;
    input [24:0] input_0;
    input [24:0] input_1;
    input  sel;
    reg [24:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_25_2_2 = result;
  end
  endfunction


  function automatic [27:0] MUX_v_28_2_2;
    input [27:0] input_0;
    input [27:0] input_1;
    input  sel;
    reg [27:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_28_2_2 = result;
  end
  endfunction


  function automatic [28:0] MUX_v_29_2_2;
    input [28:0] input_0;
    input [28:0] input_1;
    input  sel;
    reg [28:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_29_2_2 = result;
  end
  endfunction


  function automatic [1:0] MUX_v_2_2_2;
    input [1:0] input_0;
    input [1:0] input_1;
    input  sel;
    reg [1:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_2_2_2 = result;
  end
  endfunction


  function automatic [29:0] MUX_v_30_2_2;
    input [29:0] input_0;
    input [29:0] input_1;
    input  sel;
    reg [29:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_30_2_2 = result;
  end
  endfunction


  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction


  function automatic [2:0] MUX_v_3_2_2;
    input [2:0] input_0;
    input [2:0] input_1;
    input  sel;
    reg [2:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_3_2_2 = result;
  end
  endfunction


  function automatic [3:0] MUX_v_4_2_2;
    input [3:0] input_0;
    input [3:0] input_1;
    input  sel;
    reg [3:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_4_2_2 = result;
  end
  endfunction


  function automatic [3:0] MUX_v_4_4_2;
    input [3:0] input_0;
    input [3:0] input_1;
    input [3:0] input_2;
    input [3:0] input_3;
    input [1:0] sel;
    reg [3:0] result;
  begin
    case (sel)
      2'b00 : begin
        result = input_0;
      end
      2'b01 : begin
        result = input_1;
      end
      2'b10 : begin
        result = input_2;
      end
      default : begin
        result = input_3;
      end
    endcase
    MUX_v_4_4_2 = result;
  end
  endfunction


  function automatic [4:0] MUX_v_5_2_2;
    input [4:0] input_0;
    input [4:0] input_1;
    input  sel;
    reg [4:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_5_2_2 = result;
  end
  endfunction


  function automatic [63:0] MUX_v_64_2_2;
    input [63:0] input_0;
    input [63:0] input_1;
    input  sel;
    reg [63:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_64_2_2 = result;
  end
  endfunction


  function automatic [5:0] MUX_v_6_2_2;
    input [5:0] input_0;
    input [5:0] input_1;
    input  sel;
    reg [5:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_6_2_2 = result;
  end
  endfunction


  function automatic [6:0] MUX_v_7_2_2;
    input [6:0] input_0;
    input [6:0] input_1;
    input  sel;
    reg [6:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_7_2_2 = result;
  end
  endfunction


  function automatic [8:0] MUX_v_9_2_2;
    input [8:0] input_0;
    input [8:0] input_1;
    input  sel;
    reg [8:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_9_2_2 = result;
  end
  endfunction


  function automatic [0:0] readslicef_11_1_10;
    input [10:0] vector;
    reg [10:0] tmp;
  begin
    tmp = vector >> 10;
    readslicef_11_1_10 = tmp[0:0];
  end
  endfunction


  function automatic [28:0] readslicef_30_29_1;
    input [29:0] vector;
    reg [29:0] tmp;
  begin
    tmp = vector >> 1;
    readslicef_30_29_1 = tmp[28:0];
  end
  endfunction


  function automatic [0:0] readslicef_33_1_32;
    input [32:0] vector;
    reg [32:0] tmp;
  begin
    tmp = vector >> 32;
    readslicef_33_1_32 = tmp[0:0];
  end
  endfunction


  function automatic [32:0] readslicef_34_33_1;
    input [33:0] vector;
    reg [33:0] tmp;
  begin
    tmp = vector >> 1;
    readslicef_34_33_1 = tmp[32:0];
  end
  endfunction


  function automatic [0:0] readslicef_4_1_3;
    input [3:0] vector;
    reg [3:0] tmp;
  begin
    tmp = vector >> 3;
    readslicef_4_1_3 = tmp[0:0];
  end
  endfunction


  function automatic [0:0] readslicef_5_1_4;
    input [4:0] vector;
    reg [4:0] tmp;
  begin
    tmp = vector >> 4;
    readslicef_5_1_4 = tmp[0:0];
  end
  endfunction


  function automatic [48:0] readslicef_64_49_15;
    input [63:0] vector;
    reg [63:0] tmp;
  begin
    tmp = vector >> 15;
    readslicef_64_49_15 = tmp[48:0];
  end
  endfunction


  function automatic [0:0] readslicef_6_1_5;
    input [5:0] vector;
    reg [5:0] tmp;
  begin
    tmp = vector >> 5;
    readslicef_6_1_5 = tmp[0:0];
  end
  endfunction


  function automatic [0:0] readslicef_7_1_6;
    input [6:0] vector;
    reg [6:0] tmp;
  begin
    tmp = vector >> 6;
    readslicef_7_1_6 = tmp[0:0];
  end
  endfunction


  function automatic [9:0] signext_10_8;
    input [7:0] vector;
  begin
    signext_10_8= {{2{vector[7]}}, vector};
  end
  endfunction


  function automatic [24:0] signext_25_11;
    input [10:0] vector;
  begin
    signext_25_11= {{14{vector[10]}}, vector};
  end
  endfunction


  function automatic [28:0] signext_29_25;
    input [24:0] vector;
  begin
    signext_29_25= {{4{vector[24]}}, vector};
  end
  endfunction


  function automatic [1:0] signext_2_1;
    input  vector;
  begin
    signext_2_1= {{1{vector}}, vector};
  end
  endfunction


  function automatic [29:0] signext_30_1;
    input  vector;
  begin
    signext_30_1= {{29{vector}}, vector};
  end
  endfunction


  function automatic [2:0] signext_3_1;
    input  vector;
  begin
    signext_3_1= {{2{vector}}, vector};
  end
  endfunction


  function automatic [5:0] signext_6_3;
    input [2:0] vector;
  begin
    signext_6_3= {{3{vector[2]}}, vector};
  end
  endfunction


  function automatic [7:0] signext_8_7;
    input [6:0] vector;
  begin
    signext_8_7= {{1{vector[6]}}, vector};
  end
  endfunction


  function automatic [4:0] conv_s2s_4_5 ;
    input [3:0]  vector ;
  begin
    conv_s2s_4_5 = {vector[3], vector};
  end
  endfunction


  function automatic [5:0] conv_s2s_5_6 ;
    input [4:0]  vector ;
  begin
    conv_s2s_5_6 = {vector[4], vector};
  end
  endfunction


  function automatic [32:0] conv_s2s_32_33 ;
    input [31:0]  vector ;
  begin
    conv_s2s_32_33 = {vector[31], vector};
  end
  endfunction


  function automatic [29:0] conv_s2u_29_30 ;
    input [28:0]  vector ;
  begin
    conv_s2u_29_30 = {vector[28], vector};
  end
  endfunction


  function automatic [32:0] conv_s2u_32_33 ;
    input [31:0]  vector ;
  begin
    conv_s2u_32_33 = {vector[31], vector};
  end
  endfunction


  function automatic [33:0] conv_s2u_33_34 ;
    input [32:0]  vector ;
  begin
    conv_s2u_33_34 = {vector[32], vector};
  end
  endfunction


  function automatic [48:0] conv_s2u_48_49 ;
    input [47:0]  vector ;
  begin
    conv_s2u_48_49 = {vector[47], vector};
  end
  endfunction


  function automatic [2:0] conv_u2s_1_3 ;
    input [0:0]  vector ;
  begin
    conv_u2s_1_3 = {{2{1'b0}}, vector};
  end
  endfunction


  function automatic [3:0] conv_u2s_1_4 ;
    input [0:0]  vector ;
  begin
    conv_u2s_1_4 = {{3{1'b0}}, vector};
  end
  endfunction


  function automatic [4:0] conv_u2s_1_5 ;
    input [0:0]  vector ;
  begin
    conv_u2s_1_5 = {{4{1'b0}}, vector};
  end
  endfunction


  function automatic [4:0] conv_u2s_4_5 ;
    input [3:0]  vector ;
  begin
    conv_u2s_4_5 =  {1'b0, vector};
  end
  endfunction


  function automatic [5:0] conv_u2s_5_6 ;
    input [4:0]  vector ;
  begin
    conv_u2s_5_6 =  {1'b0, vector};
  end
  endfunction


  function automatic [10:0] conv_u2s_10_11 ;
    input [9:0]  vector ;
  begin
    conv_u2s_10_11 =  {1'b0, vector};
  end
  endfunction


  function automatic [2:0] conv_u2u_1_3 ;
    input [0:0]  vector ;
  begin
    conv_u2u_1_3 = {{2{1'b0}}, vector};
  end
  endfunction


  function automatic [48:0] conv_u2u_1_49 ;
    input [0:0]  vector ;
  begin
    conv_u2u_1_49 = {{48{1'b0}}, vector};
  end
  endfunction


  function automatic [6:0] conv_u2u_6_7 ;
    input [5:0]  vector ;
  begin
    conv_u2u_6_7 = {1'b0, vector};
  end
  endfunction


  function automatic [12:0] conv_u2u_9_13 ;
    input [8:0]  vector ;
  begin
    conv_u2u_9_13 = {{4{1'b0}}, vector};
  end
  endfunction


  function automatic [10:0] conv_u2u_10_11 ;
    input [9:0]  vector ;
  begin
    conv_u2u_10_11 = {1'b0, vector};
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_28_28_5_5_1
// ------------------------------------------------------------------


module catapult_conv2d_28_28_5_5_1 (
  clk, rstn, image_channel_rsc_dat, image_channel_rsc_vld, image_channel_rsc_rdy,
      weight_memory_rsc_radr, weight_memory_rsc_re, weight_memory_rsc_q, weight_memory_triosy_lz,
      bias_memory_rsc_radr, bias_memory_rsc_re, bias_memory_rsc_q, bias_memory_triosy_lz,
      output_image_channel_rsc_dat, output_image_channel_rsc_vld, output_image_channel_rsc_rdy,
      bias_rsc_dat, bias_triosy_lz, relu_rsc_dat, relu_triosy_lz, max_pool_rsc_dat,
      max_pool_triosy_lz, num_input_images_rsc_dat, num_input_images_triosy_lz, num_output_images_rsc_dat,
      num_output_images_triosy_lz
);
  input clk;
  input rstn;
  input [31:0] image_channel_rsc_dat;
  input image_channel_rsc_vld;
  output image_channel_rsc_rdy;
  output [13:0] weight_memory_rsc_radr;
  output weight_memory_rsc_re;
  input [31:0] weight_memory_rsc_q;
  output weight_memory_triosy_lz;
  output [9:0] bias_memory_rsc_radr;
  output bias_memory_rsc_re;
  input [31:0] bias_memory_rsc_q;
  output bias_memory_triosy_lz;
  output [31:0] output_image_channel_rsc_dat;
  output output_image_channel_rsc_vld;
  input output_image_channel_rsc_rdy;
  input bias_rsc_dat;
  output bias_triosy_lz;
  input relu_rsc_dat;
  output relu_triosy_lz;
  input max_pool_rsc_dat;
  output max_pool_triosy_lz;
  input [27:0] num_input_images_rsc_dat;
  output num_input_images_triosy_lz;
  input [27:0] num_output_images_rsc_dat;
  output num_output_images_triosy_lz;


  // Interconnect Declarations
  wire [13:0] weight_memory_rsci_radr_d;
  wire [31:0] weight_memory_rsci_q_d;
  wire [9:0] bias_memory_rsci_radr_d;
  wire [31:0] bias_memory_rsci_q_d;
  wire [13:0] current_image_rsci_radr_d;
  wire [13:0] current_image_rsci_wadr_d;
  wire [31:0] current_image_rsci_d_d;
  wire [31:0] current_image_rsci_q_d;
  wire [9:0] convolution_output_rsci_radr_d;
  wire [9:0] convolution_output_rsci_wadr_d;
  wire [31:0] convolution_output_rsci_d_d;
  wire [31:0] convolution_output_rsci_q_d;
  wire [9:0] relu_output_rsci_radr_d;
  wire [9:0] relu_output_rsci_wadr_d;
  wire [31:0] relu_output_rsci_d_d;
  wire [31:0] relu_output_rsci_q_d;
  wire [9:0] output_buffer_rsci_radr_d;
  wire [9:0] output_buffer_rsci_wadr_d;
  wire [31:0] output_buffer_rsci_d_d;
  wire [31:0] output_buffer_rsci_q_d;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_b;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_a;
  wire [31:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_b;
  wire catapult_conv2d_28_28_5_5_1_stall;
  wire current_image_rsc_we;
  wire [31:0] current_image_rsc_d;
  wire [13:0] current_image_rsc_wadr;
  wire [31:0] current_image_rsc_q;
  wire current_image_rsc_re;
  wire [13:0] current_image_rsc_radr;
  wire convolution_output_rsc_we;
  wire [31:0] convolution_output_rsc_d;
  wire [9:0] convolution_output_rsc_wadr;
  wire [31:0] convolution_output_rsc_q;
  wire convolution_output_rsc_re;
  wire [9:0] convolution_output_rsc_radr;
  wire relu_output_rsc_we;
  wire [31:0] relu_output_rsc_d;
  wire [9:0] relu_output_rsc_wadr;
  wire [31:0] relu_output_rsc_q;
  wire relu_output_rsc_re;
  wire [9:0] relu_output_rsc_radr;
  wire output_buffer_rsc_we;
  wire [31:0] output_buffer_rsc_d;
  wire [9:0] output_buffer_rsc_wadr;
  wire [31:0] output_buffer_rsc_q;
  wire output_buffer_rsc_re;
  wire [9:0] output_buffer_rsc_radr;
  wire weight_memory_rsci_re_d_iff;
  wire bias_memory_rsci_re_d_iff;
  wire current_image_rsci_we_d_iff;
  wire current_image_rsci_re_d_iff;
  wire convolution_output_rsci_we_d_iff;
  wire convolution_output_rsci_re_d_iff;
  wire relu_output_rsci_we_d_iff;
  wire relu_output_rsci_re_d_iff;
  wire output_buffer_rsci_we_d_iff;
  wire output_buffer_rsci_re_d_iff;


  // Interconnect Declarations for Component Instantiations 
  wire signed [63:0] nl_mul_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z;
  assign nl_mul_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z
      = $unsigned(nl_mul_sgnd);
  wire signed [63:0] nl_mul_1_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z;
  assign nl_mul_1_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z
      = $unsigned(nl_mul_1_sgnd);
  wire signed [63:0] nl_mul_2_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z;
  assign nl_mul_2_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z
      = $unsigned(nl_mul_2_sgnd);
  wire signed [63:0] nl_mul_3_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z;
  assign nl_mul_3_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z
      = $unsigned(nl_mul_3_sgnd);
  wire signed [63:0] nl_mul_4_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z;
  assign nl_mul_4_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z
      = $unsigned(nl_mul_4_sgnd);
  wire signed [63:0] nl_mul_5_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z;
  assign nl_mul_5_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z
      = $unsigned(nl_mul_5_sgnd);
  wire signed [63:0] nl_mul_6_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z;
  assign nl_mul_6_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z
      = $unsigned(nl_mul_6_sgnd);
  wire signed [63:0] nl_mul_7_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z;
  assign nl_mul_7_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z
      = $unsigned(nl_mul_7_sgnd);
  wire signed [63:0] nl_mul_8_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z;
  assign nl_mul_8_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z
      = $unsigned(nl_mul_8_sgnd);
  wire signed [63:0] nl_mul_9_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z;
  assign nl_mul_9_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z
      = $unsigned(nl_mul_9_sgnd);
  wire signed [63:0] nl_mul_10_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z;
  assign nl_mul_10_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z
      = $unsigned(nl_mul_10_sgnd);
  wire signed [63:0] nl_mul_11_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z;
  assign nl_mul_11_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z
      = $unsigned(nl_mul_11_sgnd);
  wire signed [63:0] nl_mul_12_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z;
  assign nl_mul_12_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z
      = $unsigned(nl_mul_12_sgnd);
  wire signed [63:0] nl_mul_13_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z;
  assign nl_mul_13_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z
      = $unsigned(nl_mul_13_sgnd);
  wire signed [63:0] nl_mul_14_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z;
  assign nl_mul_14_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z
      = $unsigned(nl_mul_14_sgnd);
  wire signed [63:0] nl_mul_15_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z;
  assign nl_mul_15_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z
      = $unsigned(nl_mul_15_sgnd);
  wire signed [63:0] nl_mul_16_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z;
  assign nl_mul_16_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z
      = $unsigned(nl_mul_16_sgnd);
  wire signed [63:0] nl_mul_17_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z;
  assign nl_mul_17_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z
      = $unsigned(nl_mul_17_sgnd);
  wire signed [63:0] nl_mul_18_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z;
  assign nl_mul_18_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z
      = $unsigned(nl_mul_18_sgnd);
  wire signed [63:0] nl_mul_19_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z;
  assign nl_mul_19_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z
      = $unsigned(nl_mul_19_sgnd);
  wire signed [63:0] nl_mul_20_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z;
  assign nl_mul_20_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z
      = $unsigned(nl_mul_20_sgnd);
  wire signed [63:0] nl_mul_21_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z;
  assign nl_mul_21_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z
      = $unsigned(nl_mul_21_sgnd);
  wire signed [63:0] nl_mul_22_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z;
  assign nl_mul_22_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z
      = $unsigned(nl_mul_22_sgnd);
  wire signed [63:0] nl_mul_23_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z;
  assign nl_mul_23_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z
      = $unsigned(nl_mul_23_sgnd);
  wire signed [63:0] nl_mul_24_sgnd;
  wire [63:0] nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z;
  assign nl_mul_24_sgnd = $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_a)
      * $signed(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_b);
  assign nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z
      = $unsigned(nl_mul_24_sgnd);
  ccs_stallbuf_v1 #(.rscid(32'sd0),
  .width(32'sd1)) catapult_conv2d_28_28_5_5_1_stalldrv (
      .stalldrv(catapult_conv2d_28_28_5_5_1_stall)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd14),
  .depth(32'sd15720)) current_image_rsc_comp (
      .radr(current_image_rsc_radr),
      .wadr(current_image_rsc_wadr),
      .d(current_image_rsc_d),
      .we(current_image_rsc_we),
      .re(current_image_rsc_re),
      .clk(clk),
      .q(current_image_rsc_q)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd10),
  .depth(32'sd786)) convolution_output_rsc_comp (
      .radr(convolution_output_rsc_radr),
      .wadr(convolution_output_rsc_wadr),
      .d(convolution_output_rsc_d),
      .we(convolution_output_rsc_we),
      .re(convolution_output_rsc_re),
      .clk(clk),
      .q(convolution_output_rsc_q)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd10),
  .depth(32'sd786)) relu_output_rsc_comp (
      .radr(relu_output_rsc_radr),
      .wadr(relu_output_rsc_wadr),
      .d(relu_output_rsc_d),
      .we(relu_output_rsc_we),
      .re(relu_output_rsc_re),
      .clk(clk),
      .q(relu_output_rsc_q)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd10),
  .depth(32'sd786)) output_buffer_rsc_comp (
      .radr(output_buffer_rsc_radr),
      .wadr(output_buffer_rsc_wadr),
      .d(output_buffer_rsc_d),
      .we(output_buffer_rsc_we),
      .re(output_buffer_rsc_re),
      .clk(clk),
      .q(output_buffer_rsc_q)
    );
  catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_2_32_14_16384_16384_32_5_gen
      weight_memory_rsci (
      .q(weight_memory_rsc_q),
      .re(weight_memory_rsc_re),
      .radr(weight_memory_rsc_radr),
      .radr_d(weight_memory_rsci_radr_d),
      .re_d(weight_memory_rsci_re_d_iff),
      .q_d(weight_memory_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(weight_memory_rsci_re_d_iff)
    );
  catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_3_32_10_1024_1024_32_5_gen
      bias_memory_rsci (
      .q(bias_memory_rsc_q),
      .re(bias_memory_rsc_re),
      .radr(bias_memory_rsc_radr),
      .radr_d(bias_memory_rsci_radr_d),
      .re_d(bias_memory_rsci_re_d_iff),
      .q_d(bias_memory_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(bias_memory_rsci_re_d_iff)
    );
  catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_13_32_14_15720_15720_32_5_gen
      current_image_rsci (
      .we(current_image_rsc_we),
      .d(current_image_rsc_d),
      .wadr(current_image_rsc_wadr),
      .q(current_image_rsc_q),
      .re(current_image_rsc_re),
      .radr(current_image_rsc_radr),
      .radr_d(current_image_rsci_radr_d),
      .wadr_d(current_image_rsci_wadr_d),
      .d_d(current_image_rsci_d_d),
      .we_d(current_image_rsci_we_d_iff),
      .re_d(current_image_rsci_re_d_iff),
      .q_d(current_image_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(current_image_rsci_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(current_image_rsci_we_d_iff)
    );
  catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_14_32_10_786_786_32_5_gen
      convolution_output_rsci (
      .we(convolution_output_rsc_we),
      .d(convolution_output_rsc_d),
      .wadr(convolution_output_rsc_wadr),
      .q(convolution_output_rsc_q),
      .re(convolution_output_rsc_re),
      .radr(convolution_output_rsc_radr),
      .radr_d(convolution_output_rsci_radr_d),
      .wadr_d(convolution_output_rsci_wadr_d),
      .d_d(convolution_output_rsci_d_d),
      .we_d(convolution_output_rsci_we_d_iff),
      .re_d(convolution_output_rsci_re_d_iff),
      .q_d(convolution_output_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(convolution_output_rsci_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(convolution_output_rsci_we_d_iff)
    );
  catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_16_32_10_786_786_32_5_gen
      relu_output_rsci (
      .we(relu_output_rsc_we),
      .d(relu_output_rsc_d),
      .wadr(relu_output_rsc_wadr),
      .q(relu_output_rsc_q),
      .re(relu_output_rsc_re),
      .radr(relu_output_rsc_radr),
      .radr_d(relu_output_rsci_radr_d),
      .wadr_d(relu_output_rsci_wadr_d),
      .d_d(relu_output_rsci_d_d),
      .we_d(relu_output_rsci_we_d_iff),
      .re_d(relu_output_rsci_re_d_iff),
      .q_d(relu_output_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(relu_output_rsci_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(relu_output_rsci_we_d_iff)
    );
  catapult_conv2d_28_28_5_5_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_17_32_10_786_786_32_5_gen
      output_buffer_rsci (
      .we(output_buffer_rsc_we),
      .d(output_buffer_rsc_d),
      .wadr(output_buffer_rsc_wadr),
      .q(output_buffer_rsc_q),
      .re(output_buffer_rsc_re),
      .radr(output_buffer_rsc_radr),
      .radr_d(output_buffer_rsci_radr_d),
      .wadr_d(output_buffer_rsci_wadr_d),
      .d_d(output_buffer_rsci_d_d),
      .we_d(output_buffer_rsci_we_d_iff),
      .re_d(output_buffer_rsci_re_d_iff),
      .q_d(output_buffer_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(output_buffer_rsci_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(output_buffer_rsci_we_d_iff)
    );
  catapult_conv2d_28_28_5_5_1_run catapult_conv2d_28_28_5_5_1_run_inst (
      .clk(clk),
      .rstn(rstn),
      .image_channel_rsc_dat(image_channel_rsc_dat),
      .image_channel_rsc_vld(image_channel_rsc_vld),
      .image_channel_rsc_rdy(image_channel_rsc_rdy),
      .weight_memory_triosy_lz(weight_memory_triosy_lz),
      .bias_memory_triosy_lz(bias_memory_triosy_lz),
      .output_image_channel_rsc_dat(output_image_channel_rsc_dat),
      .output_image_channel_rsc_vld(output_image_channel_rsc_vld),
      .output_image_channel_rsc_rdy(output_image_channel_rsc_rdy),
      .bias_rsc_dat(bias_rsc_dat),
      .bias_triosy_lz(bias_triosy_lz),
      .relu_rsc_dat(relu_rsc_dat),
      .relu_triosy_lz(relu_triosy_lz),
      .max_pool_rsc_dat(max_pool_rsc_dat),
      .max_pool_triosy_lz(max_pool_triosy_lz),
      .num_input_images_rsc_dat(num_input_images_rsc_dat),
      .num_input_images_triosy_lz(num_input_images_triosy_lz),
      .num_output_images_rsc_dat(num_output_images_rsc_dat),
      .num_output_images_triosy_lz(num_output_images_triosy_lz),
      .weight_memory_rsci_radr_d(weight_memory_rsci_radr_d),
      .weight_memory_rsci_q_d(weight_memory_rsci_q_d),
      .bias_memory_rsci_radr_d(bias_memory_rsci_radr_d),
      .bias_memory_rsci_q_d(bias_memory_rsci_q_d),
      .current_image_rsci_radr_d(current_image_rsci_radr_d),
      .current_image_rsci_wadr_d(current_image_rsci_wadr_d),
      .current_image_rsci_d_d(current_image_rsci_d_d),
      .current_image_rsci_q_d(current_image_rsci_q_d),
      .convolution_output_rsci_radr_d(convolution_output_rsci_radr_d),
      .convolution_output_rsci_wadr_d(convolution_output_rsci_wadr_d),
      .convolution_output_rsci_d_d(convolution_output_rsci_d_d),
      .convolution_output_rsci_q_d(convolution_output_rsci_q_d),
      .relu_output_rsci_radr_d(relu_output_rsci_radr_d),
      .relu_output_rsci_wadr_d(relu_output_rsci_wadr_d),
      .relu_output_rsci_d_d(relu_output_rsci_d_d),
      .relu_output_rsci_q_d(relu_output_rsci_q_d),
      .output_buffer_rsci_radr_d(output_buffer_rsci_radr_d),
      .output_buffer_rsci_wadr_d(output_buffer_rsci_wadr_d),
      .output_buffer_rsci_d_d(output_buffer_rsci_d_d),
      .output_buffer_rsci_q_d(output_buffer_rsci_q_d),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_1_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_2_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_3_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_4_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_5_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_6_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_7_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_8_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_9_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_10_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_11_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_12_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_13_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_14_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_15_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_16_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_17_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_18_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_19_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_20_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_21_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_22_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_23_z[63:0]),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_a(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_a),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_b(outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_b),
      .outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z(nl_catapult_conv2d_28_28_5_5_1_run_inst_outer_conv_loop_1_inner_conv_loop_1_qif_mul_cmp_24_z[63:0]),
      .catapult_conv2d_28_28_5_5_1_stall(catapult_conv2d_28_28_5_5_1_stall),
      .weight_memory_rsci_re_d_pff(weight_memory_rsci_re_d_iff),
      .bias_memory_rsci_re_d_pff(bias_memory_rsci_re_d_iff),
      .current_image_rsci_we_d_pff(current_image_rsci_we_d_iff),
      .current_image_rsci_re_d_pff(current_image_rsci_re_d_iff),
      .convolution_output_rsci_we_d_pff(convolution_output_rsci_we_d_iff),
      .convolution_output_rsci_re_d_pff(convolution_output_rsci_re_d_iff),
      .relu_output_rsci_we_d_pff(relu_output_rsci_we_d_iff),
      .relu_output_rsci_re_d_pff(relu_output_rsci_re_d_iff),
      .output_buffer_rsci_we_d_pff(output_buffer_rsci_we_d_iff),
      .output_buffer_rsci_re_d_pff(output_buffer_rsci_re_d_iff)
    );
endmodule



