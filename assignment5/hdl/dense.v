
//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_in_wait_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module ccs_in_wait_v1 (idat, rdy, ivld, dat, irdy, vld);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] idat;
  output             rdy;
  output             ivld;
  input  [width-1:0] dat;
  input              irdy;
  input              vld;

  wire   [width-1:0] idat;
  wire               rdy;
  wire               ivld;

  localparam stallOff = 0; 
  wire                  stall_ctrl;
  assign stall_ctrl = stallOff;

  assign idat = dat;
  assign rdy = irdy && !stall_ctrl;
  assign ivld = vld && !stall_ctrl;

endmodule


//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_out_wait_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module ccs_out_wait_v1 (dat, irdy, vld, idat, rdy, ivld);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] dat;
  output             irdy;
  output             vld;
  input  [width-1:0] idat;
  input              rdy;
  input              ivld;

  wire   [width-1:0] dat;
  wire               irdy;
  wire               vld;

  localparam stallOff = 0; 
  wire stall_ctrl;
  assign stall_ctrl = stallOff;

  assign dat = idat;
  assign irdy = rdy && !stall_ctrl;
  assign vld = ivld && !stall_ctrl;

endmodule



//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/mgc_io_sync_v2.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module mgc_io_sync_v2 (ld, lz);
    parameter valid = 0;

    input  ld;
    output lz;

    wire   lz;

    assign lz = ld;

endmodule


//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_in_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module ccs_in_v1 (idat, dat);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] idat;
  input  [width-1:0] dat;

  wire   [width-1:0] idat;

  assign idat = dat;

endmodule


//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_stallbuf_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2015 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------

module ccs_stallbuf_v1 (stalldrv);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output   stalldrv;
  localparam stall_off = 0; 
   
  assign stalldrv = stall_off;

endmodule




//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_ram_sync_1R1W.v 
module ccs_ram_sync_1R1W
#(
parameter data_width = 8,
parameter addr_width = 7,
parameter depth = 128
)(
	radr, wadr, d, we, re, clk, q
);

	input [addr_width-1:0] radr;
	input [addr_width-1:0] wadr;
	input [data_width-1:0] d;
	input we;
	input re;
	input clk;
	output[data_width-1:0] q;

	reg [data_width-1:0] q;

	reg [data_width-1:0] mem [depth-1:0];
		
	always @(posedge clk) begin
		if (we) begin
			mem[wadr] <= d; // Write port
		end
		if (re) begin
			q <= mem[radr] ; // read port
		end
	end

endmodule

//------> ./rtl.v 
// ----------------------------------------------------------------------
//  HLS HDL:        Verilog Netlister
//  HLS Version:    2024.1/1091966 Production Release
//  HLS Date:       Wed Feb 14 09:07:18 PST 2024
// 
//  Generated by:   russk@orw-russk-vm
//  Generated date: Mon Mar 18 19:01:37 2024
// ----------------------------------------------------------------------

// 
// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rport_18_32_22_2097155_2097155_32_5_gen
// ------------------------------------------------------------------


module catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rport_18_32_22_2097155_2097155_32_5_gen
    (
  q, re, radr, radr_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d
);
  input [31:0] q;
  output re;
  output [21:0] radr;
  input [21:0] radr_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rport_17_32_22_2097155_2097155_32_5_gen
// ------------------------------------------------------------------


module catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rport_17_32_22_2097155_2097155_32_5_gen
    (
  q, re, radr, radr_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d
);
  input [31:0] q;
  output re;
  output [21:0] radr;
  input [21:0] radr_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_16_32_11_1252_1252_32_5_gen
// ------------------------------------------------------------------


module catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_16_32_11_1252_1252_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [10:0] wadr;
  input [31:0] q;
  output re;
  output [10:0] radr;
  input [10:0] radr_d;
  input [10:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_15_32_11_1252_1252_32_5_gen
// ------------------------------------------------------------------


module catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_15_32_11_1252_1252_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [10:0] wadr;
  input [31:0] q;
  output re;
  output [10:0] radr;
  input [10:0] radr_d;
  input [10:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_14_32_13_8002_8002_32_5_gen
// ------------------------------------------------------------------


module catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_14_32_13_8002_8002_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [12:0] wadr;
  input [31:0] q;
  output re;
  output [12:0] radr;
  input [12:0] radr_d;
  input [12:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_13_32_13_8002_8002_32_5_gen
// ------------------------------------------------------------------


module catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_13_32_13_8002_8002_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [12:0] wadr;
  input [31:0] q;
  output re;
  output [12:0] radr;
  input [12:0] radr_d;
  input [12:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rport_5_32_11_1027_1027_32_5_gen
// ------------------------------------------------------------------


module catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rport_5_32_11_1027_1027_32_5_gen
    (
  q, re, radr, radr_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d
);
  input [31:0] q;
  output re;
  output [10:0] radr;
  input [10:0] radr_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_run_fsm
//  FSM Module
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_run_fsm (
  clk, rstn, run_wen, fsm_output, image_vector_load_loop_C_0_tr0, main_run_loop_C_0_tr0,
      result_vector_send_loop_C_0_tr0
);
  input clk;
  input rstn;
  input run_wen;
  output [5:0] fsm_output;
  reg [5:0] fsm_output;
  input image_vector_load_loop_C_0_tr0;
  input main_run_loop_C_0_tr0;
  input result_vector_send_loop_C_0_tr0;


  // FSM State Type Declaration for catapult_dense_16000_2500_run_run_fsm_1
  parameter
    main_C_0 = 3'd0,
    image_vector_load_loop_C_0 = 3'd1,
    main_run_loop_C_0 = 3'd2,
    main_C_1 = 3'd3,
    result_vector_send_loop_C_0 = 3'd4,
    main_C_2 = 3'd5;

  reg [2:0] state_var;
  reg [2:0] state_var_NS;


  // Interconnect Declarations for Component Instantiations 
  always @(*)
  begin : catapult_dense_16000_2500_run_run_fsm_1
    case (state_var)
      image_vector_load_loop_C_0 : begin
        fsm_output = 6'b000010;
        if ( image_vector_load_loop_C_0_tr0 ) begin
          state_var_NS = main_run_loop_C_0;
        end
        else begin
          state_var_NS = image_vector_load_loop_C_0;
        end
      end
      main_run_loop_C_0 : begin
        fsm_output = 6'b000100;
        if ( main_run_loop_C_0_tr0 ) begin
          state_var_NS = main_C_1;
        end
        else begin
          state_var_NS = main_run_loop_C_0;
        end
      end
      main_C_1 : begin
        fsm_output = 6'b001000;
        state_var_NS = result_vector_send_loop_C_0;
      end
      result_vector_send_loop_C_0 : begin
        fsm_output = 6'b010000;
        if ( result_vector_send_loop_C_0_tr0 ) begin
          state_var_NS = main_C_2;
        end
        else begin
          state_var_NS = result_vector_send_loop_C_0;
        end
      end
      main_C_2 : begin
        fsm_output = 6'b100000;
        state_var_NS = main_C_0;
      end
      // main_C_0
      default : begin
        fsm_output = 6'b000001;
        state_var_NS = image_vector_load_loop_C_0;
      end
    endcase
  end

  always @(posedge clk) begin
    if ( ~ rstn ) begin
      state_var <= main_C_0;
    end
    else if ( run_wen ) begin
      state_var <= state_var_NS;
    end
  end

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_staller
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_staller (
  clk, rstn, catapult_dense_16000_2500_stall, run_wen, run_wten, image_channel_rsci_wen_comp,
      output_image_channel_rsci_wen_comp
);
  input clk;
  input rstn;
  input catapult_dense_16000_2500_stall;
  output run_wen;
  output run_wten;
  input image_channel_rsci_wen_comp;
  input output_image_channel_rsci_wen_comp;


  // Interconnect Declarations
  reg run_wten_reg;


  // Interconnect Declarations for Component Instantiations 
  assign run_wen = image_channel_rsci_wen_comp & output_image_channel_rsci_wen_comp
      & (~ catapult_dense_16000_2500_stall);
  assign run_wten = run_wten_reg;
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      run_wten_reg <= 1'b0;
    end
    else begin
      run_wten_reg <= ~ run_wen;
    end
  end
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_wait_dp (
  clk, rstn, inner_multiply_loop_1_qif_mul_cmp_z, inner_multiply_loop_1_qif_mul_cmp_1_z,
      run_wen, inner_multiply_loop_1_qif_mul_cmp_z_oreg, inner_multiply_loop_1_qif_mul_cmp_1_z_oreg
);
  input clk;
  input rstn;
  input [63:0] inner_multiply_loop_1_qif_mul_cmp_z;
  input [63:0] inner_multiply_loop_1_qif_mul_cmp_1_z;
  input run_wen;
  output [63:0] inner_multiply_loop_1_qif_mul_cmp_z_oreg;
  reg [63:0] inner_multiply_loop_1_qif_mul_cmp_z_oreg;
  output [63:0] inner_multiply_loop_1_qif_mul_cmp_1_z_oreg;
  reg [63:0] inner_multiply_loop_1_qif_mul_cmp_1_z_oreg;



  // Interconnect Declarations for Component Instantiations 
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      inner_multiply_loop_1_qif_mul_cmp_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_multiply_loop_1_qif_mul_cmp_1_z_oreg <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
    end
    else if ( run_wen ) begin
      inner_multiply_loop_1_qif_mul_cmp_z_oreg <= inner_multiply_loop_1_qif_mul_cmp_z;
      inner_multiply_loop_1_qif_mul_cmp_1_z_oreg <= inner_multiply_loop_1_qif_mul_cmp_1_z;
    end
  end
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_image_elements_triosy_obj_output_image_elements_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_image_elements_triosy_obj_output_image_elements_triosy_wait_ctrl
    (
  run_wten, output_image_elements_triosy_obj_iswt0, output_image_elements_triosy_obj_biwt
);
  input run_wten;
  input output_image_elements_triosy_obj_iswt0;
  output output_image_elements_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign output_image_elements_triosy_obj_biwt = (~ run_wten) & output_image_elements_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_input_vector_length_triosy_obj_input_vector_length_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_input_vector_length_triosy_obj_input_vector_length_triosy_wait_ctrl
    (
  run_wten, input_vector_length_triosy_obj_iswt0, input_vector_length_triosy_obj_biwt
);
  input run_wten;
  input input_vector_length_triosy_obj_iswt0;
  output input_vector_length_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign input_vector_length_triosy_obj_biwt = (~ run_wten) & input_vector_length_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl
    (
  run_wten, bias_memory_triosy_obj_iswt0, bias_memory_triosy_obj_biwt
);
  input run_wten;
  input bias_memory_triosy_obj_iswt0;
  output bias_memory_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign bias_memory_triosy_obj_biwt = (~ run_wten) & bias_memory_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_tri_0_0_osy_obj_weight_memory_data_tri_0_0_osy_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_tri_0_0_osy_obj_weight_memory_data_tri_0_0_osy_wait_ctrl
    (
  run_wten, weight_memory_data_tri_0_0_osy_obj_iswt0, weight_memory_data_tri_0_0_osy_obj_biwt
);
  input run_wten;
  input weight_memory_data_tri_0_0_osy_obj_iswt0;
  output weight_memory_data_tri_0_0_osy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_data_tri_0_0_osy_obj_biwt = (~ run_wten) & weight_memory_data_tri_0_0_osy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_tri_0_1_osy_obj_weight_memory_data_tri_0_1_osy_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_tri_0_1_osy_obj_weight_memory_data_tri_0_1_osy_wait_ctrl
    (
  run_wten, weight_memory_data_tri_0_1_osy_obj_iswt0, weight_memory_data_tri_0_1_osy_obj_biwt
);
  input run_wten;
  input weight_memory_data_tri_0_1_osy_obj_iswt0;
  output weight_memory_data_tri_0_1_osy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_data_tri_0_1_osy_obj_biwt = (~ run_wten) & weight_memory_data_tri_0_1_osy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_relu_triosy_obj_relu_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_relu_triosy_obj_relu_triosy_wait_ctrl (
  run_wten, relu_triosy_obj_iswt0, relu_triosy_obj_biwt
);
  input run_wten;
  input relu_triosy_obj_iswt0;
  output relu_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign relu_triosy_obj_biwt = (~ run_wten) & relu_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_bias_triosy_obj_bias_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_bias_triosy_obj_bias_triosy_wait_ctrl (
  run_wten, bias_triosy_obj_iswt0, bias_triosy_obj_biwt
);
  input run_wten;
  input bias_triosy_obj_iswt0;
  output bias_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign bias_triosy_obj_biwt = (~ run_wten) & bias_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1_weight_memory_data_rsc_0_1_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1_weight_memory_data_rsc_0_1_wait_dp
    (
  clk, rstn, weight_memory_data_rsc_0_1_i_q_d, weight_memory_data_rsc_0_1_i_q_d_mxwt,
      weight_memory_data_rsc_0_1_i_biwt, weight_memory_data_rsc_0_1_i_bdwt
);
  input clk;
  input rstn;
  input [31:0] weight_memory_data_rsc_0_1_i_q_d;
  output [31:0] weight_memory_data_rsc_0_1_i_q_d_mxwt;
  input weight_memory_data_rsc_0_1_i_biwt;
  input weight_memory_data_rsc_0_1_i_bdwt;


  // Interconnect Declarations
  reg weight_memory_data_rsc_0_1_i_bcwt;
  reg [31:0] weight_memory_data_rsc_0_1_i_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_data_rsc_0_1_i_q_d_mxwt = MUX_v_32_2_2(weight_memory_data_rsc_0_1_i_q_d,
      weight_memory_data_rsc_0_1_i_q_d_bfwt, weight_memory_data_rsc_0_1_i_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_memory_data_rsc_0_1_i_bcwt <= 1'b0;
    end
    else begin
      weight_memory_data_rsc_0_1_i_bcwt <= ~((~(weight_memory_data_rsc_0_1_i_bcwt
          | weight_memory_data_rsc_0_1_i_biwt)) | weight_memory_data_rsc_0_1_i_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_memory_data_rsc_0_1_i_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( weight_memory_data_rsc_0_1_i_biwt ) begin
      weight_memory_data_rsc_0_1_i_q_d_bfwt <= weight_memory_data_rsc_0_1_i_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1_weight_memory_data_rsc_0_1_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1_weight_memory_data_rsc_0_1_wait_ctrl
    (
  run_wen, run_wten, weight_memory_data_rsc_0_1_i_oswt, weight_memory_data_rsc_0_1_i_biwt,
      weight_memory_data_rsc_0_1_i_bdwt, weight_memory_data_rsc_0_1_i_biwt_pff, weight_memory_data_rsc_0_1_i_oswt_pff
);
  input run_wen;
  input run_wten;
  input weight_memory_data_rsc_0_1_i_oswt;
  output weight_memory_data_rsc_0_1_i_biwt;
  output weight_memory_data_rsc_0_1_i_bdwt;
  output weight_memory_data_rsc_0_1_i_biwt_pff;
  input weight_memory_data_rsc_0_1_i_oswt_pff;



  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_data_rsc_0_1_i_bdwt = weight_memory_data_rsc_0_1_i_oswt &
      run_wen;
  assign weight_memory_data_rsc_0_1_i_biwt = (~ run_wten) & weight_memory_data_rsc_0_1_i_oswt;
  assign weight_memory_data_rsc_0_1_i_biwt_pff = run_wen & weight_memory_data_rsc_0_1_i_oswt_pff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1_weight_memory_data_rsc_0_0_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1_weight_memory_data_rsc_0_0_wait_dp
    (
  clk, rstn, weight_memory_data_rsc_0_0_i_q_d, weight_memory_data_rsc_0_0_i_q_d_mxwt,
      weight_memory_data_rsc_0_0_i_biwt, weight_memory_data_rsc_0_0_i_bdwt
);
  input clk;
  input rstn;
  input [31:0] weight_memory_data_rsc_0_0_i_q_d;
  output [31:0] weight_memory_data_rsc_0_0_i_q_d_mxwt;
  input weight_memory_data_rsc_0_0_i_biwt;
  input weight_memory_data_rsc_0_0_i_bdwt;


  // Interconnect Declarations
  reg weight_memory_data_rsc_0_0_i_bcwt;
  reg [31:0] weight_memory_data_rsc_0_0_i_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_data_rsc_0_0_i_q_d_mxwt = MUX_v_32_2_2(weight_memory_data_rsc_0_0_i_q_d,
      weight_memory_data_rsc_0_0_i_q_d_bfwt, weight_memory_data_rsc_0_0_i_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_memory_data_rsc_0_0_i_bcwt <= 1'b0;
    end
    else begin
      weight_memory_data_rsc_0_0_i_bcwt <= ~((~(weight_memory_data_rsc_0_0_i_bcwt
          | weight_memory_data_rsc_0_0_i_biwt)) | weight_memory_data_rsc_0_0_i_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_memory_data_rsc_0_0_i_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( weight_memory_data_rsc_0_0_i_biwt ) begin
      weight_memory_data_rsc_0_0_i_q_d_bfwt <= weight_memory_data_rsc_0_0_i_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1_weight_memory_data_rsc_0_0_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1_weight_memory_data_rsc_0_0_wait_ctrl
    (
  run_wen, run_wten, weight_memory_data_rsc_0_0_i_oswt, weight_memory_data_rsc_0_0_i_biwt,
      weight_memory_data_rsc_0_0_i_bdwt, weight_memory_data_rsc_0_0_i_biwt_pff, weight_memory_data_rsc_0_0_i_oswt_pff
);
  input run_wen;
  input run_wten;
  input weight_memory_data_rsc_0_0_i_oswt;
  output weight_memory_data_rsc_0_0_i_biwt;
  output weight_memory_data_rsc_0_0_i_bdwt;
  output weight_memory_data_rsc_0_0_i_biwt_pff;
  input weight_memory_data_rsc_0_0_i_oswt_pff;



  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_data_rsc_0_0_i_bdwt = weight_memory_data_rsc_0_0_i_oswt &
      run_wen;
  assign weight_memory_data_rsc_0_0_i_biwt = (~ run_wten) & weight_memory_data_rsc_0_0_i_oswt;
  assign weight_memory_data_rsc_0_0_i_biwt_pff = run_wen & weight_memory_data_rsc_0_0_i_oswt_pff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_output_buffer_rsc_0_1_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_output_buffer_rsc_0_1_wait_dp
    (
  clk, rstn, output_buffer_rsc_0_1_i_q_d, output_buffer_rsc_0_1_i_q_d_mxwt, output_buffer_rsc_0_1_i_biwt_1,
      output_buffer_rsc_0_1_i_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] output_buffer_rsc_0_1_i_q_d;
  output [31:0] output_buffer_rsc_0_1_i_q_d_mxwt;
  input output_buffer_rsc_0_1_i_biwt_1;
  input output_buffer_rsc_0_1_i_bdwt_2;


  // Interconnect Declarations
  reg output_buffer_rsc_0_1_i_bcwt_1;
  reg [31:0] output_buffer_rsc_0_1_i_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign output_buffer_rsc_0_1_i_q_d_mxwt = MUX_v_32_2_2(output_buffer_rsc_0_1_i_q_d,
      output_buffer_rsc_0_1_i_q_d_bfwt, output_buffer_rsc_0_1_i_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_buffer_rsc_0_1_i_bcwt_1 <= 1'b0;
    end
    else begin
      output_buffer_rsc_0_1_i_bcwt_1 <= ~((~(output_buffer_rsc_0_1_i_bcwt_1 | output_buffer_rsc_0_1_i_biwt_1))
          | output_buffer_rsc_0_1_i_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_buffer_rsc_0_1_i_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( output_buffer_rsc_0_1_i_biwt_1 ) begin
      output_buffer_rsc_0_1_i_q_d_bfwt <= output_buffer_rsc_0_1_i_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_output_buffer_rsc_0_1_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_output_buffer_rsc_0_1_wait_ctrl
    (
  run_wen, run_wten, output_buffer_rsc_0_1_i_oswt_1, output_buffer_rsc_0_1_i_biwt_1,
      output_buffer_rsc_0_1_i_bdwt_2, output_buffer_rsc_0_1_i_we_d_run_sct_pff, output_buffer_rsc_0_1_i_iswt0_pff,
      output_buffer_rsc_0_1_i_re_d_run_sct_pff, output_buffer_rsc_0_1_i_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input output_buffer_rsc_0_1_i_oswt_1;
  output output_buffer_rsc_0_1_i_biwt_1;
  output output_buffer_rsc_0_1_i_bdwt_2;
  output output_buffer_rsc_0_1_i_we_d_run_sct_pff;
  input output_buffer_rsc_0_1_i_iswt0_pff;
  output output_buffer_rsc_0_1_i_re_d_run_sct_pff;
  input output_buffer_rsc_0_1_i_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign output_buffer_rsc_0_1_i_bdwt_2 = output_buffer_rsc_0_1_i_oswt_1 & run_wen;
  assign output_buffer_rsc_0_1_i_biwt_1 = (~ run_wten) & output_buffer_rsc_0_1_i_oswt_1;
  assign output_buffer_rsc_0_1_i_we_d_run_sct_pff = output_buffer_rsc_0_1_i_iswt0_pff
      & run_wen;
  assign output_buffer_rsc_0_1_i_re_d_run_sct_pff = output_buffer_rsc_0_1_i_oswt_1_pff
      & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_output_buffer_rsc_0_0_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_output_buffer_rsc_0_0_wait_dp
    (
  clk, rstn, output_buffer_rsc_0_0_i_q_d, output_buffer_rsc_0_0_i_q_d_mxwt, output_buffer_rsc_0_0_i_biwt_1,
      output_buffer_rsc_0_0_i_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] output_buffer_rsc_0_0_i_q_d;
  output [31:0] output_buffer_rsc_0_0_i_q_d_mxwt;
  input output_buffer_rsc_0_0_i_biwt_1;
  input output_buffer_rsc_0_0_i_bdwt_2;


  // Interconnect Declarations
  reg output_buffer_rsc_0_0_i_bcwt_1;
  reg [31:0] output_buffer_rsc_0_0_i_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign output_buffer_rsc_0_0_i_q_d_mxwt = MUX_v_32_2_2(output_buffer_rsc_0_0_i_q_d,
      output_buffer_rsc_0_0_i_q_d_bfwt, output_buffer_rsc_0_0_i_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_buffer_rsc_0_0_i_bcwt_1 <= 1'b0;
    end
    else begin
      output_buffer_rsc_0_0_i_bcwt_1 <= ~((~(output_buffer_rsc_0_0_i_bcwt_1 | output_buffer_rsc_0_0_i_biwt_1))
          | output_buffer_rsc_0_0_i_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_buffer_rsc_0_0_i_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( output_buffer_rsc_0_0_i_biwt_1 ) begin
      output_buffer_rsc_0_0_i_q_d_bfwt <= output_buffer_rsc_0_0_i_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_output_buffer_rsc_0_0_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_output_buffer_rsc_0_0_wait_ctrl
    (
  run_wen, run_wten, output_buffer_rsc_0_0_i_oswt_1, output_buffer_rsc_0_0_i_biwt_1,
      output_buffer_rsc_0_0_i_bdwt_2, output_buffer_rsc_0_0_i_we_d_run_sct_pff, output_buffer_rsc_0_0_i_iswt0_pff,
      output_buffer_rsc_0_0_i_re_d_run_sct_pff, output_buffer_rsc_0_0_i_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input output_buffer_rsc_0_0_i_oswt_1;
  output output_buffer_rsc_0_0_i_biwt_1;
  output output_buffer_rsc_0_0_i_bdwt_2;
  output output_buffer_rsc_0_0_i_we_d_run_sct_pff;
  input output_buffer_rsc_0_0_i_iswt0_pff;
  output output_buffer_rsc_0_0_i_re_d_run_sct_pff;
  input output_buffer_rsc_0_0_i_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign output_buffer_rsc_0_0_i_bdwt_2 = output_buffer_rsc_0_0_i_oswt_1 & run_wen;
  assign output_buffer_rsc_0_0_i_biwt_1 = (~ run_wten) & output_buffer_rsc_0_0_i_oswt_1;
  assign output_buffer_rsc_0_0_i_we_d_run_sct_pff = output_buffer_rsc_0_0_i_iswt0_pff
      & run_wen;
  assign output_buffer_rsc_0_0_i_re_d_run_sct_pff = output_buffer_rsc_0_0_i_oswt_1_pff
      & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_input_image_rsc_0_1_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_input_image_rsc_0_1_wait_dp
    (
  clk, rstn, input_image_rsc_0_1_i_q_d, input_image_rsc_0_1_i_q_d_mxwt, input_image_rsc_0_1_i_biwt_1,
      input_image_rsc_0_1_i_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] input_image_rsc_0_1_i_q_d;
  output [31:0] input_image_rsc_0_1_i_q_d_mxwt;
  input input_image_rsc_0_1_i_biwt_1;
  input input_image_rsc_0_1_i_bdwt_2;


  // Interconnect Declarations
  reg input_image_rsc_0_1_i_bcwt_1;
  reg [31:0] input_image_rsc_0_1_i_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign input_image_rsc_0_1_i_q_d_mxwt = MUX_v_32_2_2(input_image_rsc_0_1_i_q_d,
      input_image_rsc_0_1_i_q_d_bfwt, input_image_rsc_0_1_i_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      input_image_rsc_0_1_i_bcwt_1 <= 1'b0;
    end
    else begin
      input_image_rsc_0_1_i_bcwt_1 <= ~((~(input_image_rsc_0_1_i_bcwt_1 | input_image_rsc_0_1_i_biwt_1))
          | input_image_rsc_0_1_i_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      input_image_rsc_0_1_i_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( input_image_rsc_0_1_i_biwt_1 ) begin
      input_image_rsc_0_1_i_q_d_bfwt <= input_image_rsc_0_1_i_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_input_image_rsc_0_1_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_input_image_rsc_0_1_wait_ctrl
    (
  run_wen, run_wten, input_image_rsc_0_1_i_oswt_1, input_image_rsc_0_1_i_biwt_1,
      input_image_rsc_0_1_i_bdwt_2, input_image_rsc_0_1_i_we_d_run_sct_pff, input_image_rsc_0_1_i_iswt0_pff,
      input_image_rsc_0_1_i_re_d_run_sct_pff, input_image_rsc_0_1_i_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input input_image_rsc_0_1_i_oswt_1;
  output input_image_rsc_0_1_i_biwt_1;
  output input_image_rsc_0_1_i_bdwt_2;
  output input_image_rsc_0_1_i_we_d_run_sct_pff;
  input input_image_rsc_0_1_i_iswt0_pff;
  output input_image_rsc_0_1_i_re_d_run_sct_pff;
  input input_image_rsc_0_1_i_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign input_image_rsc_0_1_i_bdwt_2 = input_image_rsc_0_1_i_oswt_1 & run_wen;
  assign input_image_rsc_0_1_i_biwt_1 = (~ run_wten) & input_image_rsc_0_1_i_oswt_1;
  assign input_image_rsc_0_1_i_we_d_run_sct_pff = input_image_rsc_0_1_i_iswt0_pff
      & run_wen;
  assign input_image_rsc_0_1_i_re_d_run_sct_pff = input_image_rsc_0_1_i_oswt_1_pff
      & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_input_image_rsc_0_0_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_input_image_rsc_0_0_wait_dp
    (
  clk, rstn, input_image_rsc_0_0_i_q_d, input_image_rsc_0_0_i_q_d_mxwt, input_image_rsc_0_0_i_biwt_1,
      input_image_rsc_0_0_i_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] input_image_rsc_0_0_i_q_d;
  output [31:0] input_image_rsc_0_0_i_q_d_mxwt;
  input input_image_rsc_0_0_i_biwt_1;
  input input_image_rsc_0_0_i_bdwt_2;


  // Interconnect Declarations
  reg input_image_rsc_0_0_i_bcwt_1;
  reg [31:0] input_image_rsc_0_0_i_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign input_image_rsc_0_0_i_q_d_mxwt = MUX_v_32_2_2(input_image_rsc_0_0_i_q_d,
      input_image_rsc_0_0_i_q_d_bfwt, input_image_rsc_0_0_i_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      input_image_rsc_0_0_i_bcwt_1 <= 1'b0;
    end
    else begin
      input_image_rsc_0_0_i_bcwt_1 <= ~((~(input_image_rsc_0_0_i_bcwt_1 | input_image_rsc_0_0_i_biwt_1))
          | input_image_rsc_0_0_i_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      input_image_rsc_0_0_i_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( input_image_rsc_0_0_i_biwt_1 ) begin
      input_image_rsc_0_0_i_q_d_bfwt <= input_image_rsc_0_0_i_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_input_image_rsc_0_0_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_input_image_rsc_0_0_wait_ctrl
    (
  run_wen, run_wten, input_image_rsc_0_0_i_oswt_1, input_image_rsc_0_0_i_biwt_1,
      input_image_rsc_0_0_i_bdwt_2, input_image_rsc_0_0_i_we_d_run_sct_pff, input_image_rsc_0_0_i_iswt0_pff,
      input_image_rsc_0_0_i_re_d_run_sct_pff, input_image_rsc_0_0_i_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input input_image_rsc_0_0_i_oswt_1;
  output input_image_rsc_0_0_i_biwt_1;
  output input_image_rsc_0_0_i_bdwt_2;
  output input_image_rsc_0_0_i_we_d_run_sct_pff;
  input input_image_rsc_0_0_i_iswt0_pff;
  output input_image_rsc_0_0_i_re_d_run_sct_pff;
  input input_image_rsc_0_0_i_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign input_image_rsc_0_0_i_bdwt_2 = input_image_rsc_0_0_i_oswt_1 & run_wen;
  assign input_image_rsc_0_0_i_biwt_1 = (~ run_wten) & input_image_rsc_0_0_i_oswt_1;
  assign input_image_rsc_0_0_i_we_d_run_sct_pff = input_image_rsc_0_0_i_iswt0_pff
      & run_wen;
  assign input_image_rsc_0_0_i_re_d_run_sct_pff = input_image_rsc_0_0_i_oswt_1_pff
      & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_image_channel_rsci_output_image_channel_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_image_channel_rsci_output_image_channel_wait_dp
    (
  clk, rstn, output_image_channel_rsci_oswt, output_image_channel_rsci_wen_comp,
      output_image_channel_rsci_biwt, output_image_channel_rsci_bdwt, output_image_channel_rsci_bcwt
);
  input clk;
  input rstn;
  input output_image_channel_rsci_oswt;
  output output_image_channel_rsci_wen_comp;
  input output_image_channel_rsci_biwt;
  input output_image_channel_rsci_bdwt;
  output output_image_channel_rsci_bcwt;
  reg output_image_channel_rsci_bcwt;



  // Interconnect Declarations for Component Instantiations 
  assign output_image_channel_rsci_wen_comp = (~ output_image_channel_rsci_oswt)
      | output_image_channel_rsci_biwt | output_image_channel_rsci_bcwt;
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_image_channel_rsci_bcwt <= 1'b0;
    end
    else begin
      output_image_channel_rsci_bcwt <= ~((~(output_image_channel_rsci_bcwt | output_image_channel_rsci_biwt))
          | output_image_channel_rsci_bdwt);
    end
  end
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_image_channel_rsci_output_image_channel_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_image_channel_rsci_output_image_channel_wait_ctrl
    (
  run_wen, output_image_channel_rsci_oswt, output_image_channel_rsci_biwt, output_image_channel_rsci_bdwt,
      output_image_channel_rsci_bcwt, output_image_channel_rsci_irdy, output_image_channel_rsci_ivld_run_sct
);
  input run_wen;
  input output_image_channel_rsci_oswt;
  output output_image_channel_rsci_biwt;
  output output_image_channel_rsci_bdwt;
  input output_image_channel_rsci_bcwt;
  input output_image_channel_rsci_irdy;
  output output_image_channel_rsci_ivld_run_sct;


  // Interconnect Declarations
  wire output_image_channel_rsci_ogwt;


  // Interconnect Declarations for Component Instantiations 
  assign output_image_channel_rsci_bdwt = output_image_channel_rsci_oswt & run_wen;
  assign output_image_channel_rsci_biwt = output_image_channel_rsci_ogwt & output_image_channel_rsci_irdy;
  assign output_image_channel_rsci_ogwt = output_image_channel_rsci_oswt & (~ output_image_channel_rsci_bcwt);
  assign output_image_channel_rsci_ivld_run_sct = output_image_channel_rsci_ogwt;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp (
  clk, rstn, bias_memory_rsci_q_d, bias_memory_rsci_q_d_mxwt, bias_memory_rsci_biwt,
      bias_memory_rsci_bdwt
);
  input clk;
  input rstn;
  input [31:0] bias_memory_rsci_q_d;
  output [31:0] bias_memory_rsci_q_d_mxwt;
  input bias_memory_rsci_biwt;
  input bias_memory_rsci_bdwt;


  // Interconnect Declarations
  reg bias_memory_rsci_bcwt;
  reg [31:0] bias_memory_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign bias_memory_rsci_q_d_mxwt = MUX_v_32_2_2(bias_memory_rsci_q_d, bias_memory_rsci_q_d_bfwt,
      bias_memory_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      bias_memory_rsci_bcwt <= 1'b0;
    end
    else begin
      bias_memory_rsci_bcwt <= ~((~(bias_memory_rsci_bcwt | bias_memory_rsci_biwt))
          | bias_memory_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      bias_memory_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( bias_memory_rsci_biwt ) begin
      bias_memory_rsci_q_d_bfwt <= bias_memory_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl
    (
  run_wen, run_wten, bias_memory_rsci_oswt, bias_memory_rsci_biwt, bias_memory_rsci_bdwt,
      bias_memory_rsci_biwt_pff, bias_memory_rsci_oswt_pff
);
  input run_wen;
  input run_wten;
  input bias_memory_rsci_oswt;
  output bias_memory_rsci_biwt;
  output bias_memory_rsci_bdwt;
  output bias_memory_rsci_biwt_pff;
  input bias_memory_rsci_oswt_pff;



  // Interconnect Declarations for Component Instantiations 
  assign bias_memory_rsci_bdwt = bias_memory_rsci_oswt & run_wen;
  assign bias_memory_rsci_biwt = (~ run_wten) & bias_memory_rsci_oswt;
  assign bias_memory_rsci_biwt_pff = run_wen & bias_memory_rsci_oswt_pff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_image_channel_rsci_image_channel_wait_dp
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_image_channel_rsci_image_channel_wait_dp (
  clk, rstn, image_channel_rsci_oswt, image_channel_rsci_wen_comp, image_channel_rsci_idat_mxwt,
      image_channel_rsci_biwt, image_channel_rsci_bdwt, image_channel_rsci_bcwt,
      image_channel_rsci_idat
);
  input clk;
  input rstn;
  input image_channel_rsci_oswt;
  output image_channel_rsci_wen_comp;
  output [31:0] image_channel_rsci_idat_mxwt;
  input image_channel_rsci_biwt;
  input image_channel_rsci_bdwt;
  output image_channel_rsci_bcwt;
  reg image_channel_rsci_bcwt;
  input [31:0] image_channel_rsci_idat;


  // Interconnect Declarations
  reg [31:0] image_channel_rsci_idat_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign image_channel_rsci_wen_comp = (~ image_channel_rsci_oswt) | image_channel_rsci_biwt
      | image_channel_rsci_bcwt;
  assign image_channel_rsci_idat_mxwt = MUX_v_32_2_2(image_channel_rsci_idat, image_channel_rsci_idat_bfwt,
      image_channel_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      image_channel_rsci_bcwt <= 1'b0;
    end
    else begin
      image_channel_rsci_bcwt <= ~((~(image_channel_rsci_bcwt | image_channel_rsci_biwt))
          | image_channel_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      image_channel_rsci_idat_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( image_channel_rsci_biwt ) begin
      image_channel_rsci_idat_bfwt <= image_channel_rsci_idat;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_image_channel_rsci_image_channel_wait_ctrl
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_image_channel_rsci_image_channel_wait_ctrl (
  run_wen, image_channel_rsci_oswt, image_channel_rsci_biwt, image_channel_rsci_bdwt,
      image_channel_rsci_bcwt, image_channel_rsci_irdy_run_sct, image_channel_rsci_ivld
);
  input run_wen;
  input image_channel_rsci_oswt;
  output image_channel_rsci_biwt;
  output image_channel_rsci_bdwt;
  input image_channel_rsci_bcwt;
  output image_channel_rsci_irdy_run_sct;
  input image_channel_rsci_ivld;


  // Interconnect Declarations
  wire image_channel_rsci_ogwt;


  // Interconnect Declarations for Component Instantiations 
  assign image_channel_rsci_bdwt = image_channel_rsci_oswt & run_wen;
  assign image_channel_rsci_biwt = image_channel_rsci_ogwt & image_channel_rsci_ivld;
  assign image_channel_rsci_ogwt = image_channel_rsci_oswt & (~ image_channel_rsci_bcwt);
  assign image_channel_rsci_irdy_run_sct = image_channel_rsci_ogwt;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_image_elements_triosy_obj
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_image_elements_triosy_obj (
  output_image_elements_triosy_lz, run_wten, output_image_elements_triosy_obj_iswt0
);
  output output_image_elements_triosy_lz;
  input run_wten;
  input output_image_elements_triosy_obj_iswt0;


  // Interconnect Declarations
  wire output_image_elements_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) output_image_elements_triosy_obj (
      .ld(output_image_elements_triosy_obj_biwt),
      .lz(output_image_elements_triosy_lz)
    );
  catapult_dense_16000_2500_run_output_image_elements_triosy_obj_output_image_elements_triosy_wait_ctrl
      catapult_dense_16000_2500_run_output_image_elements_triosy_obj_output_image_elements_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .output_image_elements_triosy_obj_iswt0(output_image_elements_triosy_obj_iswt0),
      .output_image_elements_triosy_obj_biwt(output_image_elements_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_input_vector_length_triosy_obj
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_input_vector_length_triosy_obj (
  input_vector_length_triosy_lz, run_wten, input_vector_length_triosy_obj_iswt0
);
  output input_vector_length_triosy_lz;
  input run_wten;
  input input_vector_length_triosy_obj_iswt0;


  // Interconnect Declarations
  wire input_vector_length_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) input_vector_length_triosy_obj (
      .ld(input_vector_length_triosy_obj_biwt),
      .lz(input_vector_length_triosy_lz)
    );
  catapult_dense_16000_2500_run_input_vector_length_triosy_obj_input_vector_length_triosy_wait_ctrl
      catapult_dense_16000_2500_run_input_vector_length_triosy_obj_input_vector_length_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .input_vector_length_triosy_obj_iswt0(input_vector_length_triosy_obj_iswt0),
      .input_vector_length_triosy_obj_biwt(input_vector_length_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_bias_memory_triosy_obj
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_bias_memory_triosy_obj (
  bias_memory_triosy_lz, run_wten, bias_memory_triosy_obj_iswt0
);
  output bias_memory_triosy_lz;
  input run_wten;
  input bias_memory_triosy_obj_iswt0;


  // Interconnect Declarations
  wire bias_memory_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) bias_memory_triosy_obj (
      .ld(bias_memory_triosy_obj_biwt),
      .lz(bias_memory_triosy_lz)
    );
  catapult_dense_16000_2500_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl
      catapult_dense_16000_2500_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .bias_memory_triosy_obj_iswt0(bias_memory_triosy_obj_iswt0),
      .bias_memory_triosy_obj_biwt(bias_memory_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_tri_0_0_osy_obj
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_tri_0_0_osy_obj (
  weight_memory_data_tri_0_0_osy_lz, run_wten, weight_memory_data_tri_0_0_osy_obj_iswt0
);
  output weight_memory_data_tri_0_0_osy_lz;
  input run_wten;
  input weight_memory_data_tri_0_0_osy_obj_iswt0;


  // Interconnect Declarations
  wire weight_memory_data_tri_0_0_osy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) weight_memory_data_tri_0_0_osy_obj (
      .ld(weight_memory_data_tri_0_0_osy_obj_biwt),
      .lz(weight_memory_data_tri_0_0_osy_lz)
    );
  catapult_dense_16000_2500_run_weight_memory_data_tri_0_0_osy_obj_weight_memory_data_tri_0_0_osy_wait_ctrl
      catapult_dense_16000_2500_run_weight_memory_data_tri_0_0_osy_obj_weight_memory_data_tri_0_0_osy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .weight_memory_data_tri_0_0_osy_obj_iswt0(weight_memory_data_tri_0_0_osy_obj_iswt0),
      .weight_memory_data_tri_0_0_osy_obj_biwt(weight_memory_data_tri_0_0_osy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_tri_0_1_osy_obj
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_tri_0_1_osy_obj (
  weight_memory_data_tri_0_1_osy_lz, run_wten, weight_memory_data_tri_0_1_osy_obj_iswt0
);
  output weight_memory_data_tri_0_1_osy_lz;
  input run_wten;
  input weight_memory_data_tri_0_1_osy_obj_iswt0;


  // Interconnect Declarations
  wire weight_memory_data_tri_0_1_osy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) weight_memory_data_tri_0_1_osy_obj (
      .ld(weight_memory_data_tri_0_1_osy_obj_biwt),
      .lz(weight_memory_data_tri_0_1_osy_lz)
    );
  catapult_dense_16000_2500_run_weight_memory_data_tri_0_1_osy_obj_weight_memory_data_tri_0_1_osy_wait_ctrl
      catapult_dense_16000_2500_run_weight_memory_data_tri_0_1_osy_obj_weight_memory_data_tri_0_1_osy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .weight_memory_data_tri_0_1_osy_obj_iswt0(weight_memory_data_tri_0_1_osy_obj_iswt0),
      .weight_memory_data_tri_0_1_osy_obj_biwt(weight_memory_data_tri_0_1_osy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_relu_triosy_obj
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_relu_triosy_obj (
  relu_triosy_lz, run_wten, relu_triosy_obj_iswt0
);
  output relu_triosy_lz;
  input run_wten;
  input relu_triosy_obj_iswt0;


  // Interconnect Declarations
  wire relu_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) relu_triosy_obj (
      .ld(relu_triosy_obj_biwt),
      .lz(relu_triosy_lz)
    );
  catapult_dense_16000_2500_run_relu_triosy_obj_relu_triosy_wait_ctrl catapult_dense_16000_2500_run_relu_triosy_obj_relu_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .relu_triosy_obj_iswt0(relu_triosy_obj_iswt0),
      .relu_triosy_obj_biwt(relu_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_bias_triosy_obj
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_bias_triosy_obj (
  bias_triosy_lz, run_wten, bias_triosy_obj_iswt0
);
  output bias_triosy_lz;
  input run_wten;
  input bias_triosy_obj_iswt0;


  // Interconnect Declarations
  wire bias_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) bias_triosy_obj (
      .ld(bias_triosy_obj_biwt),
      .lz(bias_triosy_lz)
    );
  catapult_dense_16000_2500_run_bias_triosy_obj_bias_triosy_wait_ctrl catapult_dense_16000_2500_run_bias_triosy_obj_bias_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .bias_triosy_obj_iswt0(bias_triosy_obj_iswt0),
      .bias_triosy_obj_biwt(bias_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1 (
  clk, rstn, weight_memory_data_rsc_0_1_i_q_d, run_wen, run_wten, weight_memory_data_rsc_0_1_i_oswt,
      weight_memory_data_rsc_0_1_i_q_d_mxwt, weight_memory_data_rsc_0_1_i_re_d_pff,
      weight_memory_data_rsc_0_1_i_oswt_pff
);
  input clk;
  input rstn;
  input [31:0] weight_memory_data_rsc_0_1_i_q_d;
  input run_wen;
  input run_wten;
  input weight_memory_data_rsc_0_1_i_oswt;
  output [31:0] weight_memory_data_rsc_0_1_i_q_d_mxwt;
  output weight_memory_data_rsc_0_1_i_re_d_pff;
  input weight_memory_data_rsc_0_1_i_oswt_pff;


  // Interconnect Declarations
  wire weight_memory_data_rsc_0_1_i_biwt;
  wire weight_memory_data_rsc_0_1_i_bdwt;
  wire weight_memory_data_rsc_0_1_i_biwt_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1_weight_memory_data_rsc_0_1_wait_ctrl
      catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1_weight_memory_data_rsc_0_1_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .weight_memory_data_rsc_0_1_i_oswt(weight_memory_data_rsc_0_1_i_oswt),
      .weight_memory_data_rsc_0_1_i_biwt(weight_memory_data_rsc_0_1_i_biwt),
      .weight_memory_data_rsc_0_1_i_bdwt(weight_memory_data_rsc_0_1_i_bdwt),
      .weight_memory_data_rsc_0_1_i_biwt_pff(weight_memory_data_rsc_0_1_i_biwt_iff),
      .weight_memory_data_rsc_0_1_i_oswt_pff(weight_memory_data_rsc_0_1_i_oswt_pff)
    );
  catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1_weight_memory_data_rsc_0_1_wait_dp
      catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1_weight_memory_data_rsc_0_1_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .weight_memory_data_rsc_0_1_i_q_d(weight_memory_data_rsc_0_1_i_q_d),
      .weight_memory_data_rsc_0_1_i_q_d_mxwt(weight_memory_data_rsc_0_1_i_q_d_mxwt),
      .weight_memory_data_rsc_0_1_i_biwt(weight_memory_data_rsc_0_1_i_biwt),
      .weight_memory_data_rsc_0_1_i_bdwt(weight_memory_data_rsc_0_1_i_bdwt)
    );
  assign weight_memory_data_rsc_0_1_i_re_d_pff = weight_memory_data_rsc_0_1_i_biwt_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1 (
  clk, rstn, weight_memory_data_rsc_0_0_i_q_d, run_wen, run_wten, weight_memory_data_rsc_0_0_i_oswt,
      weight_memory_data_rsc_0_0_i_q_d_mxwt, weight_memory_data_rsc_0_0_i_re_d_pff,
      weight_memory_data_rsc_0_0_i_oswt_pff
);
  input clk;
  input rstn;
  input [31:0] weight_memory_data_rsc_0_0_i_q_d;
  input run_wen;
  input run_wten;
  input weight_memory_data_rsc_0_0_i_oswt;
  output [31:0] weight_memory_data_rsc_0_0_i_q_d_mxwt;
  output weight_memory_data_rsc_0_0_i_re_d_pff;
  input weight_memory_data_rsc_0_0_i_oswt_pff;


  // Interconnect Declarations
  wire weight_memory_data_rsc_0_0_i_biwt;
  wire weight_memory_data_rsc_0_0_i_bdwt;
  wire weight_memory_data_rsc_0_0_i_biwt_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1_weight_memory_data_rsc_0_0_wait_ctrl
      catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1_weight_memory_data_rsc_0_0_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .weight_memory_data_rsc_0_0_i_oswt(weight_memory_data_rsc_0_0_i_oswt),
      .weight_memory_data_rsc_0_0_i_biwt(weight_memory_data_rsc_0_0_i_biwt),
      .weight_memory_data_rsc_0_0_i_bdwt(weight_memory_data_rsc_0_0_i_bdwt),
      .weight_memory_data_rsc_0_0_i_biwt_pff(weight_memory_data_rsc_0_0_i_biwt_iff),
      .weight_memory_data_rsc_0_0_i_oswt_pff(weight_memory_data_rsc_0_0_i_oswt_pff)
    );
  catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1_weight_memory_data_rsc_0_0_wait_dp
      catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1_weight_memory_data_rsc_0_0_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .weight_memory_data_rsc_0_0_i_q_d(weight_memory_data_rsc_0_0_i_q_d),
      .weight_memory_data_rsc_0_0_i_q_d_mxwt(weight_memory_data_rsc_0_0_i_q_d_mxwt),
      .weight_memory_data_rsc_0_0_i_biwt(weight_memory_data_rsc_0_0_i_biwt),
      .weight_memory_data_rsc_0_0_i_bdwt(weight_memory_data_rsc_0_0_i_bdwt)
    );
  assign weight_memory_data_rsc_0_0_i_re_d_pff = weight_memory_data_rsc_0_0_i_biwt_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1 (
  clk, rstn, output_buffer_rsc_0_1_i_q_d, run_wen, run_wten, output_buffer_rsc_0_1_i_oswt_1,
      output_buffer_rsc_0_1_i_q_d_mxwt, output_buffer_rsc_0_1_i_we_d_pff, output_buffer_rsc_0_1_i_iswt0_pff,
      output_buffer_rsc_0_1_i_re_d_pff, output_buffer_rsc_0_1_i_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] output_buffer_rsc_0_1_i_q_d;
  input run_wen;
  input run_wten;
  input output_buffer_rsc_0_1_i_oswt_1;
  output [31:0] output_buffer_rsc_0_1_i_q_d_mxwt;
  output output_buffer_rsc_0_1_i_we_d_pff;
  input output_buffer_rsc_0_1_i_iswt0_pff;
  output output_buffer_rsc_0_1_i_re_d_pff;
  input output_buffer_rsc_0_1_i_oswt_1_pff;


  // Interconnect Declarations
  wire output_buffer_rsc_0_1_i_biwt_1;
  wire output_buffer_rsc_0_1_i_bdwt_2;
  wire output_buffer_rsc_0_1_i_we_d_run_sct_iff;
  wire output_buffer_rsc_0_1_i_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_output_buffer_rsc_0_1_wait_ctrl
      catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_output_buffer_rsc_0_1_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .output_buffer_rsc_0_1_i_oswt_1(output_buffer_rsc_0_1_i_oswt_1),
      .output_buffer_rsc_0_1_i_biwt_1(output_buffer_rsc_0_1_i_biwt_1),
      .output_buffer_rsc_0_1_i_bdwt_2(output_buffer_rsc_0_1_i_bdwt_2),
      .output_buffer_rsc_0_1_i_we_d_run_sct_pff(output_buffer_rsc_0_1_i_we_d_run_sct_iff),
      .output_buffer_rsc_0_1_i_iswt0_pff(output_buffer_rsc_0_1_i_iswt0_pff),
      .output_buffer_rsc_0_1_i_re_d_run_sct_pff(output_buffer_rsc_0_1_i_re_d_run_sct_iff),
      .output_buffer_rsc_0_1_i_oswt_1_pff(output_buffer_rsc_0_1_i_oswt_1_pff)
    );
  catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_output_buffer_rsc_0_1_wait_dp
      catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_output_buffer_rsc_0_1_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_buffer_rsc_0_1_i_q_d(output_buffer_rsc_0_1_i_q_d),
      .output_buffer_rsc_0_1_i_q_d_mxwt(output_buffer_rsc_0_1_i_q_d_mxwt),
      .output_buffer_rsc_0_1_i_biwt_1(output_buffer_rsc_0_1_i_biwt_1),
      .output_buffer_rsc_0_1_i_bdwt_2(output_buffer_rsc_0_1_i_bdwt_2)
    );
  assign output_buffer_rsc_0_1_i_we_d_pff = output_buffer_rsc_0_1_i_we_d_run_sct_iff;
  assign output_buffer_rsc_0_1_i_re_d_pff = output_buffer_rsc_0_1_i_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1 (
  clk, rstn, output_buffer_rsc_0_0_i_q_d, run_wen, run_wten, output_buffer_rsc_0_0_i_oswt_1,
      output_buffer_rsc_0_0_i_q_d_mxwt, output_buffer_rsc_0_0_i_we_d_pff, output_buffer_rsc_0_0_i_iswt0_pff,
      output_buffer_rsc_0_0_i_re_d_pff, output_buffer_rsc_0_0_i_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] output_buffer_rsc_0_0_i_q_d;
  input run_wen;
  input run_wten;
  input output_buffer_rsc_0_0_i_oswt_1;
  output [31:0] output_buffer_rsc_0_0_i_q_d_mxwt;
  output output_buffer_rsc_0_0_i_we_d_pff;
  input output_buffer_rsc_0_0_i_iswt0_pff;
  output output_buffer_rsc_0_0_i_re_d_pff;
  input output_buffer_rsc_0_0_i_oswt_1_pff;


  // Interconnect Declarations
  wire output_buffer_rsc_0_0_i_biwt_1;
  wire output_buffer_rsc_0_0_i_bdwt_2;
  wire output_buffer_rsc_0_0_i_we_d_run_sct_iff;
  wire output_buffer_rsc_0_0_i_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_output_buffer_rsc_0_0_wait_ctrl
      catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_output_buffer_rsc_0_0_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .output_buffer_rsc_0_0_i_oswt_1(output_buffer_rsc_0_0_i_oswt_1),
      .output_buffer_rsc_0_0_i_biwt_1(output_buffer_rsc_0_0_i_biwt_1),
      .output_buffer_rsc_0_0_i_bdwt_2(output_buffer_rsc_0_0_i_bdwt_2),
      .output_buffer_rsc_0_0_i_we_d_run_sct_pff(output_buffer_rsc_0_0_i_we_d_run_sct_iff),
      .output_buffer_rsc_0_0_i_iswt0_pff(output_buffer_rsc_0_0_i_iswt0_pff),
      .output_buffer_rsc_0_0_i_re_d_run_sct_pff(output_buffer_rsc_0_0_i_re_d_run_sct_iff),
      .output_buffer_rsc_0_0_i_oswt_1_pff(output_buffer_rsc_0_0_i_oswt_1_pff)
    );
  catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_output_buffer_rsc_0_0_wait_dp
      catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_output_buffer_rsc_0_0_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_buffer_rsc_0_0_i_q_d(output_buffer_rsc_0_0_i_q_d),
      .output_buffer_rsc_0_0_i_q_d_mxwt(output_buffer_rsc_0_0_i_q_d_mxwt),
      .output_buffer_rsc_0_0_i_biwt_1(output_buffer_rsc_0_0_i_biwt_1),
      .output_buffer_rsc_0_0_i_bdwt_2(output_buffer_rsc_0_0_i_bdwt_2)
    );
  assign output_buffer_rsc_0_0_i_we_d_pff = output_buffer_rsc_0_0_i_we_d_run_sct_iff;
  assign output_buffer_rsc_0_0_i_re_d_pff = output_buffer_rsc_0_0_i_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1 (
  clk, rstn, input_image_rsc_0_1_i_q_d, run_wen, run_wten, input_image_rsc_0_1_i_oswt_1,
      input_image_rsc_0_1_i_q_d_mxwt, input_image_rsc_0_1_i_we_d_pff, input_image_rsc_0_1_i_iswt0_pff,
      input_image_rsc_0_1_i_re_d_pff, input_image_rsc_0_1_i_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] input_image_rsc_0_1_i_q_d;
  input run_wen;
  input run_wten;
  input input_image_rsc_0_1_i_oswt_1;
  output [31:0] input_image_rsc_0_1_i_q_d_mxwt;
  output input_image_rsc_0_1_i_we_d_pff;
  input input_image_rsc_0_1_i_iswt0_pff;
  output input_image_rsc_0_1_i_re_d_pff;
  input input_image_rsc_0_1_i_oswt_1_pff;


  // Interconnect Declarations
  wire input_image_rsc_0_1_i_biwt_1;
  wire input_image_rsc_0_1_i_bdwt_2;
  wire input_image_rsc_0_1_i_we_d_run_sct_iff;
  wire input_image_rsc_0_1_i_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_input_image_rsc_0_1_wait_ctrl
      catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_input_image_rsc_0_1_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .input_image_rsc_0_1_i_oswt_1(input_image_rsc_0_1_i_oswt_1),
      .input_image_rsc_0_1_i_biwt_1(input_image_rsc_0_1_i_biwt_1),
      .input_image_rsc_0_1_i_bdwt_2(input_image_rsc_0_1_i_bdwt_2),
      .input_image_rsc_0_1_i_we_d_run_sct_pff(input_image_rsc_0_1_i_we_d_run_sct_iff),
      .input_image_rsc_0_1_i_iswt0_pff(input_image_rsc_0_1_i_iswt0_pff),
      .input_image_rsc_0_1_i_re_d_run_sct_pff(input_image_rsc_0_1_i_re_d_run_sct_iff),
      .input_image_rsc_0_1_i_oswt_1_pff(input_image_rsc_0_1_i_oswt_1_pff)
    );
  catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_input_image_rsc_0_1_wait_dp
      catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_input_image_rsc_0_1_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .input_image_rsc_0_1_i_q_d(input_image_rsc_0_1_i_q_d),
      .input_image_rsc_0_1_i_q_d_mxwt(input_image_rsc_0_1_i_q_d_mxwt),
      .input_image_rsc_0_1_i_biwt_1(input_image_rsc_0_1_i_biwt_1),
      .input_image_rsc_0_1_i_bdwt_2(input_image_rsc_0_1_i_bdwt_2)
    );
  assign input_image_rsc_0_1_i_we_d_pff = input_image_rsc_0_1_i_we_d_run_sct_iff;
  assign input_image_rsc_0_1_i_re_d_pff = input_image_rsc_0_1_i_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1 (
  clk, rstn, input_image_rsc_0_0_i_q_d, run_wen, run_wten, input_image_rsc_0_0_i_oswt_1,
      input_image_rsc_0_0_i_q_d_mxwt, input_image_rsc_0_0_i_we_d_pff, input_image_rsc_0_0_i_iswt0_pff,
      input_image_rsc_0_0_i_re_d_pff, input_image_rsc_0_0_i_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] input_image_rsc_0_0_i_q_d;
  input run_wen;
  input run_wten;
  input input_image_rsc_0_0_i_oswt_1;
  output [31:0] input_image_rsc_0_0_i_q_d_mxwt;
  output input_image_rsc_0_0_i_we_d_pff;
  input input_image_rsc_0_0_i_iswt0_pff;
  output input_image_rsc_0_0_i_re_d_pff;
  input input_image_rsc_0_0_i_oswt_1_pff;


  // Interconnect Declarations
  wire input_image_rsc_0_0_i_biwt_1;
  wire input_image_rsc_0_0_i_bdwt_2;
  wire input_image_rsc_0_0_i_we_d_run_sct_iff;
  wire input_image_rsc_0_0_i_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_input_image_rsc_0_0_wait_ctrl
      catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_input_image_rsc_0_0_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .input_image_rsc_0_0_i_oswt_1(input_image_rsc_0_0_i_oswt_1),
      .input_image_rsc_0_0_i_biwt_1(input_image_rsc_0_0_i_biwt_1),
      .input_image_rsc_0_0_i_bdwt_2(input_image_rsc_0_0_i_bdwt_2),
      .input_image_rsc_0_0_i_we_d_run_sct_pff(input_image_rsc_0_0_i_we_d_run_sct_iff),
      .input_image_rsc_0_0_i_iswt0_pff(input_image_rsc_0_0_i_iswt0_pff),
      .input_image_rsc_0_0_i_re_d_run_sct_pff(input_image_rsc_0_0_i_re_d_run_sct_iff),
      .input_image_rsc_0_0_i_oswt_1_pff(input_image_rsc_0_0_i_oswt_1_pff)
    );
  catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_input_image_rsc_0_0_wait_dp
      catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_input_image_rsc_0_0_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .input_image_rsc_0_0_i_q_d(input_image_rsc_0_0_i_q_d),
      .input_image_rsc_0_0_i_q_d_mxwt(input_image_rsc_0_0_i_q_d_mxwt),
      .input_image_rsc_0_0_i_biwt_1(input_image_rsc_0_0_i_biwt_1),
      .input_image_rsc_0_0_i_bdwt_2(input_image_rsc_0_0_i_bdwt_2)
    );
  assign input_image_rsc_0_0_i_we_d_pff = input_image_rsc_0_0_i_we_d_run_sct_iff;
  assign input_image_rsc_0_0_i_re_d_pff = input_image_rsc_0_0_i_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_output_image_channel_rsci
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_output_image_channel_rsci (
  clk, rstn, output_image_channel_rsc_dat, output_image_channel_rsc_vld, output_image_channel_rsc_rdy,
      run_wen, output_image_channel_rsci_oswt, output_image_channel_rsci_wen_comp,
      output_image_channel_rsci_idat
);
  input clk;
  input rstn;
  output [31:0] output_image_channel_rsc_dat;
  output output_image_channel_rsc_vld;
  input output_image_channel_rsc_rdy;
  input run_wen;
  input output_image_channel_rsci_oswt;
  output output_image_channel_rsci_wen_comp;
  input [31:0] output_image_channel_rsci_idat;


  // Interconnect Declarations
  wire output_image_channel_rsci_biwt;
  wire output_image_channel_rsci_bdwt;
  wire output_image_channel_rsci_bcwt;
  wire output_image_channel_rsci_irdy;
  wire output_image_channel_rsci_ivld_run_sct;


  // Interconnect Declarations for Component Instantiations 
  ccs_out_wait_v1 #(.rscid(32'sd6),
  .width(32'sd32)) output_image_channel_rsci (
      .irdy(output_image_channel_rsci_irdy),
      .ivld(output_image_channel_rsci_ivld_run_sct),
      .idat(output_image_channel_rsci_idat),
      .rdy(output_image_channel_rsc_rdy),
      .vld(output_image_channel_rsc_vld),
      .dat(output_image_channel_rsc_dat)
    );
  catapult_dense_16000_2500_run_output_image_channel_rsci_output_image_channel_wait_ctrl
      catapult_dense_16000_2500_run_output_image_channel_rsci_output_image_channel_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .output_image_channel_rsci_oswt(output_image_channel_rsci_oswt),
      .output_image_channel_rsci_biwt(output_image_channel_rsci_biwt),
      .output_image_channel_rsci_bdwt(output_image_channel_rsci_bdwt),
      .output_image_channel_rsci_bcwt(output_image_channel_rsci_bcwt),
      .output_image_channel_rsci_irdy(output_image_channel_rsci_irdy),
      .output_image_channel_rsci_ivld_run_sct(output_image_channel_rsci_ivld_run_sct)
    );
  catapult_dense_16000_2500_run_output_image_channel_rsci_output_image_channel_wait_dp
      catapult_dense_16000_2500_run_output_image_channel_rsci_output_image_channel_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_image_channel_rsci_oswt(output_image_channel_rsci_oswt),
      .output_image_channel_rsci_wen_comp(output_image_channel_rsci_wen_comp),
      .output_image_channel_rsci_biwt(output_image_channel_rsci_biwt),
      .output_image_channel_rsci_bdwt(output_image_channel_rsci_bdwt),
      .output_image_channel_rsci_bcwt(output_image_channel_rsci_bcwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_bias_memory_rsci_1
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_bias_memory_rsci_1 (
  clk, rstn, bias_memory_rsci_q_d, run_wen, run_wten, bias_memory_rsci_oswt, bias_memory_rsci_q_d_mxwt,
      bias_memory_rsci_re_d_pff, bias_memory_rsci_oswt_pff
);
  input clk;
  input rstn;
  input [31:0] bias_memory_rsci_q_d;
  input run_wen;
  input run_wten;
  input bias_memory_rsci_oswt;
  output [31:0] bias_memory_rsci_q_d_mxwt;
  output bias_memory_rsci_re_d_pff;
  input bias_memory_rsci_oswt_pff;


  // Interconnect Declarations
  wire bias_memory_rsci_biwt;
  wire bias_memory_rsci_bdwt;
  wire bias_memory_rsci_biwt_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_dense_16000_2500_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl catapult_dense_16000_2500_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .bias_memory_rsci_oswt(bias_memory_rsci_oswt),
      .bias_memory_rsci_biwt(bias_memory_rsci_biwt),
      .bias_memory_rsci_bdwt(bias_memory_rsci_bdwt),
      .bias_memory_rsci_biwt_pff(bias_memory_rsci_biwt_iff),
      .bias_memory_rsci_oswt_pff(bias_memory_rsci_oswt_pff)
    );
  catapult_dense_16000_2500_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp catapult_dense_16000_2500_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .bias_memory_rsci_q_d(bias_memory_rsci_q_d),
      .bias_memory_rsci_q_d_mxwt(bias_memory_rsci_q_d_mxwt),
      .bias_memory_rsci_biwt(bias_memory_rsci_biwt),
      .bias_memory_rsci_bdwt(bias_memory_rsci_bdwt)
    );
  assign bias_memory_rsci_re_d_pff = bias_memory_rsci_biwt_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run_image_channel_rsci
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run_image_channel_rsci (
  clk, rstn, image_channel_rsc_dat, image_channel_rsc_vld, image_channel_rsc_rdy,
      run_wen, image_channel_rsci_oswt, image_channel_rsci_wen_comp, image_channel_rsci_idat_mxwt
);
  input clk;
  input rstn;
  input [31:0] image_channel_rsc_dat;
  input image_channel_rsc_vld;
  output image_channel_rsc_rdy;
  input run_wen;
  input image_channel_rsci_oswt;
  output image_channel_rsci_wen_comp;
  output [31:0] image_channel_rsci_idat_mxwt;


  // Interconnect Declarations
  wire image_channel_rsci_biwt;
  wire image_channel_rsci_bdwt;
  wire image_channel_rsci_bcwt;
  wire image_channel_rsci_irdy_run_sct;
  wire image_channel_rsci_ivld;
  wire [31:0] image_channel_rsci_idat;


  // Interconnect Declarations for Component Instantiations 
  ccs_in_wait_v1 #(.rscid(32'sd3),
  .width(32'sd32)) image_channel_rsci (
      .rdy(image_channel_rsc_rdy),
      .vld(image_channel_rsc_vld),
      .dat(image_channel_rsc_dat),
      .irdy(image_channel_rsci_irdy_run_sct),
      .ivld(image_channel_rsci_ivld),
      .idat(image_channel_rsci_idat)
    );
  catapult_dense_16000_2500_run_image_channel_rsci_image_channel_wait_ctrl catapult_dense_16000_2500_run_image_channel_rsci_image_channel_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .image_channel_rsci_oswt(image_channel_rsci_oswt),
      .image_channel_rsci_biwt(image_channel_rsci_biwt),
      .image_channel_rsci_bdwt(image_channel_rsci_bdwt),
      .image_channel_rsci_bcwt(image_channel_rsci_bcwt),
      .image_channel_rsci_irdy_run_sct(image_channel_rsci_irdy_run_sct),
      .image_channel_rsci_ivld(image_channel_rsci_ivld)
    );
  catapult_dense_16000_2500_run_image_channel_rsci_image_channel_wait_dp catapult_dense_16000_2500_run_image_channel_rsci_image_channel_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .image_channel_rsci_oswt(image_channel_rsci_oswt),
      .image_channel_rsci_wen_comp(image_channel_rsci_wen_comp),
      .image_channel_rsci_idat_mxwt(image_channel_rsci_idat_mxwt),
      .image_channel_rsci_biwt(image_channel_rsci_biwt),
      .image_channel_rsci_bdwt(image_channel_rsci_bdwt),
      .image_channel_rsci_bcwt(image_channel_rsci_bcwt),
      .image_channel_rsci_idat(image_channel_rsci_idat)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500_run
// ------------------------------------------------------------------


module catapult_dense_16000_2500_run (
  clk, rstn, bias_rsc_dat, bias_triosy_lz, relu_rsc_dat, relu_triosy_lz, image_channel_rsc_dat,
      image_channel_rsc_vld, image_channel_rsc_rdy, weight_memory_data_tri_0_0_osy_lz,
      weight_memory_data_tri_0_1_osy_lz, bias_memory_triosy_lz, output_image_channel_rsc_dat,
      output_image_channel_rsc_vld, output_image_channel_rsc_rdy, input_vector_length_rsc_dat,
      input_vector_length_triosy_lz, output_image_elements_rsc_dat, output_image_elements_triosy_lz,
      bias_memory_rsci_radr_d, bias_memory_rsci_q_d, input_image_rsc_0_0_i_q_d, input_image_rsc_0_1_i_q_d,
      output_buffer_rsc_0_0_i_wadr_d, output_buffer_rsc_0_0_i_d_d, output_buffer_rsc_0_0_i_q_d,
      output_buffer_rsc_0_1_i_d_d, output_buffer_rsc_0_1_i_q_d, weight_memory_data_rsc_0_0_i_q_d,
      weight_memory_data_rsc_0_1_i_q_d, inner_multiply_loop_1_qif_mul_cmp_a, inner_multiply_loop_1_qif_mul_cmp_b,
      inner_multiply_loop_1_qif_mul_cmp_z, inner_multiply_loop_1_qif_mul_cmp_1_a,
      inner_multiply_loop_1_qif_mul_cmp_1_b, inner_multiply_loop_1_qif_mul_cmp_1_z,
      catapult_dense_16000_2500_stall, bias_memory_rsci_re_d_pff, input_image_rsc_0_0_i_radr_d_pff,
      input_image_rsc_0_0_i_wadr_d_pff, input_image_rsc_0_0_i_d_d_pff, input_image_rsc_0_0_i_we_d_pff,
      input_image_rsc_0_0_i_re_d_pff, input_image_rsc_0_1_i_we_d_pff, input_image_rsc_0_1_i_re_d_pff,
      output_buffer_rsc_0_0_i_radr_d_pff, output_buffer_rsc_0_0_i_we_d_pff, output_buffer_rsc_0_0_i_re_d_pff,
      output_buffer_rsc_0_1_i_we_d_pff, output_buffer_rsc_0_1_i_re_d_pff, weight_memory_data_rsc_0_0_i_radr_d_pff,
      weight_memory_data_rsc_0_0_i_re_d_pff, weight_memory_data_rsc_0_1_i_re_d_pff
);
  input clk;
  input rstn;
  input bias_rsc_dat;
  output bias_triosy_lz;
  input relu_rsc_dat;
  output relu_triosy_lz;
  input [31:0] image_channel_rsc_dat;
  input image_channel_rsc_vld;
  output image_channel_rsc_rdy;
  output weight_memory_data_tri_0_0_osy_lz;
  output weight_memory_data_tri_0_1_osy_lz;
  output bias_memory_triosy_lz;
  output [31:0] output_image_channel_rsc_dat;
  output output_image_channel_rsc_vld;
  input output_image_channel_rsc_rdy;
  input [27:0] input_vector_length_rsc_dat;
  output input_vector_length_triosy_lz;
  input [27:0] output_image_elements_rsc_dat;
  output output_image_elements_triosy_lz;
  output [10:0] bias_memory_rsci_radr_d;
  input [31:0] bias_memory_rsci_q_d;
  input [31:0] input_image_rsc_0_0_i_q_d;
  input [31:0] input_image_rsc_0_1_i_q_d;
  output [10:0] output_buffer_rsc_0_0_i_wadr_d;
  output [31:0] output_buffer_rsc_0_0_i_d_d;
  input [31:0] output_buffer_rsc_0_0_i_q_d;
  output [31:0] output_buffer_rsc_0_1_i_d_d;
  input [31:0] output_buffer_rsc_0_1_i_q_d;
  input [31:0] weight_memory_data_rsc_0_0_i_q_d;
  input [31:0] weight_memory_data_rsc_0_1_i_q_d;
  output [31:0] inner_multiply_loop_1_qif_mul_cmp_a;
  reg [31:0] inner_multiply_loop_1_qif_mul_cmp_a;
  output [31:0] inner_multiply_loop_1_qif_mul_cmp_b;
  reg [31:0] inner_multiply_loop_1_qif_mul_cmp_b;
  input [63:0] inner_multiply_loop_1_qif_mul_cmp_z;
  output [31:0] inner_multiply_loop_1_qif_mul_cmp_1_a;
  reg [31:0] inner_multiply_loop_1_qif_mul_cmp_1_a;
  output [31:0] inner_multiply_loop_1_qif_mul_cmp_1_b;
  reg [31:0] inner_multiply_loop_1_qif_mul_cmp_1_b;
  input [63:0] inner_multiply_loop_1_qif_mul_cmp_1_z;
  input catapult_dense_16000_2500_stall;
  output bias_memory_rsci_re_d_pff;
  output [12:0] input_image_rsc_0_0_i_radr_d_pff;
  output [12:0] input_image_rsc_0_0_i_wadr_d_pff;
  output [31:0] input_image_rsc_0_0_i_d_d_pff;
  output input_image_rsc_0_0_i_we_d_pff;
  output input_image_rsc_0_0_i_re_d_pff;
  output input_image_rsc_0_1_i_we_d_pff;
  output input_image_rsc_0_1_i_re_d_pff;
  output [10:0] output_buffer_rsc_0_0_i_radr_d_pff;
  output output_buffer_rsc_0_0_i_we_d_pff;
  output output_buffer_rsc_0_0_i_re_d_pff;
  output output_buffer_rsc_0_1_i_we_d_pff;
  output output_buffer_rsc_0_1_i_re_d_pff;
  output [21:0] weight_memory_data_rsc_0_0_i_radr_d_pff;
  output weight_memory_data_rsc_0_0_i_re_d_pff;
  output weight_memory_data_rsc_0_1_i_re_d_pff;


  // Interconnect Declarations
  wire run_wen;
  wire run_wten;
  wire bias_rsci_idat;
  wire relu_rsci_idat;
  wire image_channel_rsci_wen_comp;
  wire [31:0] image_channel_rsci_idat_mxwt;
  wire [31:0] bias_memory_rsci_q_d_mxwt;
  wire output_image_channel_rsci_wen_comp;
  reg [31:0] output_image_channel_rsci_idat;
  wire [27:0] input_vector_length_rsci_idat;
  wire [27:0] output_image_elements_rsci_idat;
  wire [31:0] input_image_rsc_0_0_i_q_d_mxwt;
  wire [31:0] input_image_rsc_0_1_i_q_d_mxwt;
  wire [31:0] output_buffer_rsc_0_0_i_q_d_mxwt;
  wire [31:0] output_buffer_rsc_0_1_i_q_d_mxwt;
  wire [31:0] weight_memory_data_rsc_0_0_i_q_d_mxwt;
  wire [31:0] weight_memory_data_rsc_0_1_i_q_d_mxwt;
  wire [63:0] inner_multiply_loop_1_qif_mul_cmp_z_oreg;
  wire [63:0] inner_multiply_loop_1_qif_mul_cmp_1_z_oreg;
  wire [5:0] fsm_output;
  wire operator_28_true_unequal_tmp;
  wire and_dcpl;
  wire and_dcpl_48;
  wire or_dcpl_27;
  wire and_dcpl_138;
  wire or_dcpl_51;
  wire or_dcpl_53;
  wire or_dcpl_54;
  wire or_dcpl_56;
  wire and_dcpl_163;
  wire and_dcpl_165;
  wire and_dcpl_167;
  wire and_dcpl_168;
  wire and_dcpl_169;
  wire and_dcpl_170;
  wire and_dcpl_171;
  wire and_dcpl_174;
  wire and_dcpl_185;
  wire and_dcpl_189;
  wire not_tmp_97;
  wire or_tmp_57;
  wire or_tmp_67;
  wire and_423_cse;
  reg [27:0] input_vector_length_sva;
  reg main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_5;
  reg main_run_loop_asn_dfmergedata_31_lpi_2_dfm_5;
  reg main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_5;
  reg main_run_loop_asn_dfmergedata_0_lpi_2_dfm_5;
  reg [31:0] main_run_loop_if_1_asn_10_itm_4;
  reg main_run_loop_slc_main_run_loop_i_11_0_1_itm_3;
  reg main_run_loop_and_3_tmp_3;
  reg [63:0] catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1;
  wire [65:0] nl_catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1;
  reg main_run_loop_slc_main_run_loop_i_11_0_1_itm_2;
  reg main_run_loop_and_1_tmp_2;
  wire main_run_loop_and_3_tmp_1;
  reg bias_sva;
  wire main_run_loop_and_1_tmp_1;
  reg relu_sva;
  reg bias_sva_st_1;
  reg bias_sva_st_2;
  reg main_run_loop_stage_0_7;
  reg main_run_loop_stage_0;
  reg main_run_loop_stage_0_6;
  reg main_run_loop_stage_0_2;
  reg main_run_loop_stage_0_4;
  reg main_run_loop_asn_40_itm_5;
  reg operator_28_true_operator_28_true_nor_mdf_sva_st_4;
  reg main_run_loop_slc_main_run_loop_i_11_0_1_itm_4;
  reg exit_main_run_loop_lpi_2_dfm_1;
  reg main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5;
  reg main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_4;
  reg main_run_loop_and_10_itm_4;
  reg main_run_loop_and_2_tmp_4;
  reg exit_main_run_loop_lpi_2_dfm_st_4;
  reg exit_main_run_loop_lpi_2_dfm_st_3;
  reg main_run_loop_asn_40_itm_2;
  reg exitL_exit_outer_multiply_loop_sva;
  reg main_run_loop_and_tmp_4;
  reg main_run_loop_and_7_itm_4;
  reg main_run_loop_and_1_tmp_4;
  reg operator_28_true_operator_28_true_nor_mdf_sva_st_2;
  reg operator_28_true_operator_28_true_nor_mdf_sva_st_3;
  reg main_run_loop_stage_0_3;
  reg main_run_loop_stage_0_5;
  reg main_run_loop_slc_main_run_loop_i_11_0_1_itm_1;
  reg operator_28_true_operator_28_true_nor_mdf_sva_1;
  reg main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5;
  reg main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_3;
  reg outer_multiply_loop_asn_itm_3;
  reg main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5;
  reg main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_2;
  reg main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5;
  reg outer_multiply_loop_asn_itm_2;
  reg main_run_loop_asn_dfmergedata_1_0_lpi_2;
  reg outer_multiply_loop_asn_3_itm_3;
  wire main_run_loop_conc_cse_31_sva_1;
  wire [29:0] main_run_loop_conc_cse_30_1_sva_mx1w2;
  wire main_run_loop_conc_cse_0_sva_1;
  wire [32:0] main_run_loop_if_1_acc_sat_sva_1;
  wire [33:0] nl_main_run_loop_if_1_acc_sat_sva_1;
  wire operator_28_true_operator_28_true_nor_mdf_sva_1_1;
  wire [48:0] catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1;
  wire [49:0] nl_catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1;
  wire [31:0] main_run_loop_acc_sdt;
  wire [32:0] nl_main_run_loop_acc_sdt;
  reg reg_image_channel_rsci_oswt_cse;
  wire output_image_elements_and_cse;
  wire main_run_loop_and_cse;
  wire main_run_loop_and_18_cse;
  reg reg_output_image_elements_triosy_obj_iswt0_cse;
  reg reg_weight_memory_data_rsc_0_1_i_iswt0_cse;
  reg reg_output_buffer_rsc_0_1_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_output_buffer_rsc_0_0_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_input_image_rsc_0_1_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_input_image_rsc_0_0_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_output_image_channel_rsci_iswt0_cse;
  reg reg_bias_memory_rsci_iswt0_cse;
  wire main_run_loop_and_23_cse;
  wire main_run_loop_and_22_cse;
  wire main_run_loop_i_and_ssc;
  wire main_run_loop_or_21_cse;
  wire main_run_loop_and_45_cse;
  wire main_run_loop_and_46_cse;
  wire main_run_loop_or_22_cse;
  wire main_run_loop_or_26_cse;
  wire main_run_loop_and_42_cse;
  wire main_run_loop_and_68_cse;
  wire main_run_loop_and_69_cse;
  wire main_run_loop_and_47_cse;
  wire main_run_loop_and_72_cse;
  wire main_run_loop_and_73_cse;
  wire bias_memory_rsci_re_d_iff;
  wire and_402_rmff;
  wire input_image_rsc_0_0_i_we_d_iff;
  wire input_image_rsc_0_0_i_re_d_iff;
  wire and_394_rmff;
  wire input_image_rsc_0_1_i_we_d_iff;
  wire input_image_rsc_0_1_i_re_d_iff;
  wire and_388_rmff;
  wire output_buffer_rsc_0_0_i_we_d_iff;
  wire output_buffer_rsc_0_0_i_re_d_iff;
  wire and_382_rmff;
  wire output_buffer_rsc_0_1_i_we_d_iff;
  wire output_buffer_rsc_0_1_i_re_d_iff;
  wire and_376_rmff;
  wire weight_memory_data_rsc_0_0_i_re_d_iff;
  wire and_370_rmff;
  wire weight_memory_data_rsc_0_1_i_re_d_iff;
  wire output_buffer_rsc_0_0_i_d_d_run_31_mx1;
  wire output_buffer_rsc_0_0_i_d_d_run_0_mx1;
  wire output_buffer_rsc_0_1_i_d_d_run_31_mx1;
  wire output_buffer_rsc_0_1_i_d_d_run_0_mx1;
  reg [30:0] outer_multiply_loop_n_31_1_lpi_2_dfm_1_1;
  reg [27:0] catapult_dense_16000_2500_load_image_end_sva;
  wire [31:0] z_out_1;
  wire [32:0] nl_z_out_1;
  wire [32:0] z_out_2;
  reg main_run_loop_asn_dfmergedata_1_31_lpi_2;
  reg main_run_loop_asn_dfmergedata_0_lpi_2;
  reg main_run_loop_asn_dfmergedata_31_lpi_2;
  reg main_run_loop_conc_3_dfmergedata_0_lpi_2;
  reg main_run_loop_conc_3_dfmergedata_31_lpi_2;
  reg main_run_loop_conc_1_dfmergedata_0_lpi_2;
  reg main_run_loop_conc_1_dfmergedata_31_lpi_2;
  reg [63:0] catapult_dense_16000_2500_compute_dense_accumulator_lpi_2;
  reg [27:0] output_image_elements_sva;
  reg [10:0] bias_index_10_0_sva;
  reg operator_28_true_operator_28_true_nor_mdf_sva;
  reg operator_28_true_1_slc_operator_28_true_1_acc_20_svs_1;
  reg exit_main_run_loop_sva_1_1;
  reg exit_main_run_loop_sva_1_2;
  reg exit_main_run_loop_sva_1_3;
  reg exit_main_run_loop_sva_1_4;
  reg [63:0] catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1;
  reg inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_2;
  reg inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_3;
  reg operator_28_true_2_slc_operator_28_true_2_acc_27_svs_2;
  reg operator_28_true_2_slc_operator_28_true_2_acc_27_svs_3;
  reg main_run_loop_and_tmp_1;
  reg main_run_loop_and_tmp_2;
  reg main_run_loop_and_tmp_3;
  reg main_run_loop_and_tmp_5;
  reg main_run_loop_and_1_tmp_1_1;
  reg main_run_loop_and_1_tmp_3;
  reg main_run_loop_and_1_tmp_5;
  reg main_run_loop_and_2_tmp_1;
  reg main_run_loop_and_2_tmp_2;
  reg main_run_loop_and_2_tmp_3;
  reg main_run_loop_and_2_tmp_5;
  reg main_run_loop_and_3_tmp_1_1;
  reg main_run_loop_and_3_tmp_2;
  reg main_run_loop_and_3_tmp_4;
  reg main_run_loop_and_3_tmp_5;
  reg main_run_loop_main_run_loop_nor_1_itm_1;
  reg main_run_loop_main_run_loop_nor_1_itm_2;
  reg main_run_loop_main_run_loop_nor_1_itm_3;
  reg main_run_loop_and_7_itm_1;
  reg main_run_loop_and_7_itm_2;
  reg main_run_loop_and_7_itm_3;
  reg main_run_loop_and_7_itm_5;
  reg [31:0] main_run_loop_if_1_asn_10_itm_1;
  reg [31:0] main_run_loop_if_1_asn_10_itm_2;
  reg [31:0] main_run_loop_if_1_asn_10_itm_3;
  reg main_run_loop_if_1_slc_main_run_loop_i_11_0_13_itm_5;
  reg main_run_loop_main_run_loop_nor_3_itm_1;
  reg main_run_loop_main_run_loop_nor_3_itm_2;
  reg main_run_loop_and_10_itm_1;
  reg main_run_loop_and_10_itm_2;
  reg main_run_loop_and_10_itm_3;
  reg main_run_loop_and_10_itm_5;
  reg [10:0] main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_1;
  reg [10:0] main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_2;
  reg [10:0] main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_3;
  reg [10:0] main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_4;
  reg inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_st_1;
  reg operator_28_true_2_slc_operator_28_true_2_acc_27_svs_st_1;
  wire output_image_channel_rsci_idat_mx0c1;
  wire main_run_loop_asn_dfmergedata_1_31_lpi_2_mx0c2;
  wire main_run_loop_conc_3_dfmergedata_0_lpi_2_mx1;
  wire main_run_loop_conc_3_dfmergedata_31_lpi_2_mx1;
  wire main_run_loop_conc_1_dfmergedata_0_lpi_2_mx1;
  wire main_run_loop_conc_1_dfmergedata_31_lpi_2_mx1;
  wire [63:0] catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_mx1;
  wire image_vector_load_loop_index_sva_mx0c0;
  wire catapult_dense_16000_2500_run_catapult_dense_architected_h_ln176_assert_output_image_elements_le_OP_MAX_OUTPUT_LENGTH_CP_ctrl_prb_2;
  wire catapult_dense_16000_2500_run_catapult_dense_architected_h_ln178_assert_input_vector_length_le_OP_MAX_VECTOR_LENGTH_CP_sig_1;
  wire main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5_mx2c0;
  wire main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5_mx2c1;
  wire main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5_mx2c0;
  wire main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5_mx2c1;
  wire main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_mx1c0;
  wire main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_mx1c1;
  wire main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5_mx2c0;
  wire main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5_mx2c1;
  wire main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5_mx2c0;
  wire main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5_mx2c1;
  wire bias_sva_st_1_mx0w1;
  wire catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c0;
  wire catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c1;
  wire catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c2;
  wire main_run_loop_if_1_conc_cmx_31_sva_1;
  wire main_run_loop_if_1_conc_cmx_0_sva_1;
  wire [29:0] main_run_loop_if_1_conc_cmx_30_1_sva_1;
  wire main_run_loop_if_1_and_unfl_sva_1;
  wire main_run_loop_if_1_nor_ovfl_sva_1;
  wire main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_1_mx0;
  wire main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_1_mx0;
  wire main_run_loop_asn_dfmergedata_0_lpi_2_dfm_1_mx0;
  wire main_run_loop_asn_dfmergedata_31_lpi_2_dfm_1_mx0;
  wire [27:0] outer_multiply_loop_x_sva_1;
  wire [28:0] nl_outer_multiply_loop_x_sva_1;
  wire main_run_loop_if_qr_0_lpi_2_dfm_1;
  wire main_run_loop_if_qr_31_lpi_2_dfm_1;
  wire catapult_dense_16000_2500_compute_dense_and_unfl_sva_1;
  wire catapult_dense_16000_2500_compute_dense_nor_ovfl_sva_1;
  wire [63:0] catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_3;
  reg [1:0] main_run_loop_i_sva_1_1_31_30;
  wire [1:0] image_vector_load_loop_index_sva_mx2_31_30;
  wire [10:0] bias_index_10_0_sva_4;
  wire image_vector_load_loop_index_and_ssc;
  reg [1:0] image_vector_load_loop_index_sva_31_30;
  wire main_run_loop_and_30_ssc;
  reg [2:0] main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_29_27;
  reg [26:0] main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_26_0;
  reg [2:0] main_run_loop_conc_1_dfmergedata_30_1_lpi_2_29_27;
  reg [26:0] main_run_loop_conc_1_dfmergedata_30_1_lpi_2_26_0;
  wire main_run_loop_main_run_loop_nor_ssc;
  wire [2:0] main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_29_27;
  wire [26:0] main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_26_0;
  wire main_run_loop_main_run_loop_nor_2_ssc;
  wire [2:0] main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_29_27;
  wire [26:0] main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_26_0;
  reg reg_main_run_loop_if_1_slc_main_run_loop_i_11_0_7_itm_5_cse;
  reg reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse;
  wire and_429_rgt;
  wire main_run_loop_i_and_1_rgt;
  reg [2:0] main_run_loop_asn_dfmergedata_30_1_lpi_2_29_27;
  reg [26:0] main_run_loop_asn_dfmergedata_30_1_lpi_2_26_0;
  reg [2:0] main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_29_27;
  reg [26:0] main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_26_0;
  reg [10:0] reg_main_run_loop_i_slc_main_run_loop_i_11_1_2_itm_5_cse;
  reg reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1;
  reg reg_main_run_loop_asn_40_itm_6_cse;
  wire and_6_cse;
  wire or_265_cse;
  wire or_266_cse;
  wire operator_28_true_2_acc_itm_27_1;
  wire inner_multiply_loop_1_operator_28_true_2_acc_itm_28_1;
  wire operator_28_true_1_acc_itm_20_1;
  wire operator_28_true_acc_itm_32_1;
  wire main_run_loop_if_acc_itm_32_1;
  wire or_269_tmp;
  wire [2:0] main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_29_27;
  wire [26:0] main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_26_0;
  wire [2:0] main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_29_27;
  wire [26:0] main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_26_0;
  reg [2:0] main_run_loop_conc_3_dfmergedata_30_1_lpi_2_29_27;
  reg [26:0] main_run_loop_conc_3_dfmergedata_30_1_lpi_2_26_0;
  wire main_run_loop_i_main_run_loop_i_nor_1_ssc;
  wire main_run_loop_i_and_4_ssc;
  wire main_run_loop_i_and_2_ssc;
  reg [2:0] main_run_loop_i_sva_1_1_29_27;
  reg [26:0] main_run_loop_i_sva_1_1_26_0;
  wire or_260_cse;
  reg [2:0] main_run_loop_asn_dfmergedata_1_30_1_lpi_2_29_27;
  reg [26:0] main_run_loop_asn_dfmergedata_1_30_1_lpi_2_26_0;
  reg [2:0] main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_29_27;
  reg [26:0] main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_26_0;
  wire [2:0] image_vector_load_loop_index_sva_mx2_29_27;
  wire [26:0] image_vector_load_loop_index_sva_mx2_26_0;
  reg [2:0] image_vector_load_loop_index_sva_29_27;
  reg [26:0] image_vector_load_loop_index_sva_26_0;

  wire[28:0] operator_28_true_acc_nl;
  wire[29:0] nl_operator_28_true_acc_nl;
  wire poutput_image_elements_MAX_OUTPUT_LENGTH_prb;
  wire slc_fsm_output_0_16;
  wire poutput_image_elements_MAX_OUTPUT_LENGTH_ctrl_prb;
  wire pinput_vector_length_MAX_VECTOR_LENGTH_prb;
  wire pinput_vector_length_MAX_VECTOR_LENGTH_ctrl_prb;
  wire pvector_length_MAX_VECTOR_LENGTH_prb;
  wire pvector_length_MAX_VECTOR_LENGTH_ctrl_prb;
  wire bias_mux_3_nl;
  wire image_vector_load_loop_index_image_vector_load_loop_index_and_nl;
  wire[1:0] mux_nl;
  wire[1:0] image_vector_load_loop_index_image_vector_load_loop_index_or_nl;
  wire not_285_nl;
  wire[2:0] mux_62_nl;
  wire[2:0] image_vector_load_loop_index_image_vector_load_loop_index_or_1_nl;
  wire not_289_nl;
  wire[26:0] mux_63_nl;
  wire[26:0] image_vector_load_loop_index_image_vector_load_loop_index_or_2_nl;
  wire not_286_nl;
  wire[27:0] catapult_dense_16000_2500_load_image_end_catapult_dense_16000_2500_load_image_end_mux_nl;
  wire[27:0] catapult_dense_16000_2500_load_image_end_acc_nl;
  wire[28:0] nl_catapult_dense_16000_2500_load_image_end_acc_nl;
  wire[27:0] catapult_dense_16000_2500_load_image_end_mux_2_nl;
  wire not_262_nl;
  wire catapult_dense_16000_2500_load_image_end_mux_nl;
  wire catapult_dense_16000_2500_load_image_end_or_1_nl;
  wire[2:0] main_run_loop_if_main_run_loop_if_and_1_nl;
  wire[26:0] main_run_loop_if_main_run_loop_if_and_3_nl;
  wire main_run_loop_or_29_nl;
  wire main_run_loop_and_61_nl;
  wire main_run_loop_or_30_nl;
  wire main_run_loop_and_63_nl;
  wire outer_multiply_loop_and_7_nl;
  wire main_run_loop_or_33_nl;
  wire main_run_loop_and_65_nl;
  wire main_run_loop_or_35_nl;
  wire main_run_loop_and_67_nl;
  wire bias_mux_nl;
  wire or_237_nl;
  wire bias_mux_2_nl;
  wire[10:0] main_run_loop_if_1_acc_nl;
  wire[11:0] nl_main_run_loop_if_1_acc_nl;
  wire or_72_nl;
  wire outer_multiply_loop_mux_6_nl;
  wire[30:0] outer_multiply_loop_acc_2_nl;
  wire[31:0] nl_outer_multiply_loop_acc_2_nl;
  wire[63:0] outer_multiply_loop_mux_9_nl;
  wire[63:0] inner_multiply_loop_inner_multiply_loop_and_nl;
  wire[63:0] inner_multiply_loop_inner_multiply_loop_and_1_nl;
  wire outer_multiply_loop_or_nl;
  wire outer_multiply_loop_and_10_nl;
  wire main_run_loop_mux_19_nl;
  wire operator_28_true_mux_55_nl;
  wire main_run_loop_mux_21_nl;
  wire operator_28_true_mux_56_nl;
  wire main_run_loop_mux_24_nl;
  wire operator_28_true_mux_57_nl;
  wire main_run_loop_mux_26_nl;
  wire operator_28_true_mux_58_nl;
  wire main_run_loop_if_1_mux_10_nl;
  wire main_run_loop_if_1_mux_11_nl;
  wire main_run_loop_if_1_mux_13_nl;
  wire main_run_loop_if_1_mux_15_nl;
  wire[28:0] operator_28_true_1_acc_nl;
  wire[29:0] nl_operator_28_true_1_acc_nl;
  wire[29:0] catapult_dense_16000_2500_compute_dense_nor_2_nl;
  wire[27:0] operator_28_true_2_acc_nl;
  wire[28:0] nl_operator_28_true_2_acc_nl;
  wire[28:0] inner_multiply_loop_1_operator_28_true_2_acc_nl;
  wire[29:0] nl_inner_multiply_loop_1_operator_28_true_2_acc_nl;
  wire[20:0] operator_28_true_1_acc_nl_1;
  wire[21:0] nl_operator_28_true_1_acc_nl_1;
  wire[32:0] operator_28_true_acc_nl_1;
  wire[33:0] nl_operator_28_true_acc_nl_1;
  wire[29:0] main_run_loop_if_1_nor_1_nl;
  wire main_run_loop_if_1_mux_18_nl;
  wire[2:0] main_run_loop_if_1_mux_1_nl;
  wire[26:0] main_run_loop_if_1_mux_19_nl;
  wire main_run_loop_if_1_mux_2_nl;
  wire[32:0] main_run_loop_if_acc_nl;
  wire[33:0] nl_main_run_loop_if_acc_nl;
  wire nor_27_nl;
  wire nand_34_nl;
  wire or_125_nl;
  wire[1:0] outer_multiply_loop_if_mux_3_nl;
  wire[29:0] outer_multiply_loop_if_mux_4_nl;
  wire[33:0] acc_2_nl;
  wire[34:0] nl_acc_2_nl;
  wire[1:0] catapult_dense_16000_2500_compute_dense_end_mux_4_nl;
  wire[29:0] catapult_dense_16000_2500_compute_dense_end_mux_5_nl;
  wire catapult_dense_16000_2500_compute_dense_end_or_1_nl;
  wire[27:0] catapult_dense_16000_2500_compute_dense_end_catapult_dense_16000_2500_compute_dense_end_nand_1_nl;

  // Interconnect Declarations for Component Instantiations 
  wire  nl_catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_inst_input_image_rsc_0_0_i_iswt0_pff;
  assign nl_catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_inst_input_image_rsc_0_0_i_iswt0_pff
      = bias_sva_st_2 & (~ (image_vector_load_loop_index_sva_26_0[0])) & (fsm_output[1]);
  wire  nl_catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_inst_input_image_rsc_0_1_i_iswt0_pff;
  assign nl_catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_inst_input_image_rsc_0_1_i_iswt0_pff
      = bias_sva_st_2 & (image_vector_load_loop_index_sva_26_0[0]) & (fsm_output[1]);
  wire  nl_catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_inst_output_buffer_rsc_0_0_i_iswt0_pff;
  assign nl_catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_inst_output_buffer_rsc_0_0_i_iswt0_pff
      = and_6_cse & reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse &
      (~ reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1) & (fsm_output[2]);
  wire  nl_catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_inst_output_buffer_rsc_0_1_i_iswt0_pff;
  assign nl_catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_inst_output_buffer_rsc_0_1_i_iswt0_pff
      = and_6_cse & reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse &
      reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1 & (fsm_output[2]);
  wire  nl_catapult_dense_16000_2500_run_run_fsm_inst_main_run_loop_C_0_tr0;
  assign nl_catapult_dense_16000_2500_run_run_fsm_inst_main_run_loop_C_0_tr0 = (~(main_run_loop_stage_0_3
      | main_run_loop_stage_0_2)) & (~(main_run_loop_stage_0_4 | main_run_loop_stage_0_7))
      & (~(main_run_loop_stage_0_5 | main_run_loop_stage_0 | main_run_loop_stage_0_6));
  ccs_in_v1 #(.rscid(32'sd1),
  .width(32'sd1)) bias_rsci (
      .dat(bias_rsc_dat),
      .idat(bias_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd2),
  .width(32'sd1)) relu_rsci (
      .dat(relu_rsc_dat),
      .idat(relu_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd7),
  .width(32'sd28)) input_vector_length_rsci (
      .dat(input_vector_length_rsc_dat),
      .idat(input_vector_length_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd8),
  .width(32'sd28)) output_image_elements_rsci (
      .dat(output_image_elements_rsc_dat),
      .idat(output_image_elements_rsci_idat)
    );
  catapult_dense_16000_2500_run_image_channel_rsci catapult_dense_16000_2500_run_image_channel_rsci_inst
      (
      .clk(clk),
      .rstn(rstn),
      .image_channel_rsc_dat(image_channel_rsc_dat),
      .image_channel_rsc_vld(image_channel_rsc_vld),
      .image_channel_rsc_rdy(image_channel_rsc_rdy),
      .run_wen(run_wen),
      .image_channel_rsci_oswt(reg_image_channel_rsci_oswt_cse),
      .image_channel_rsci_wen_comp(image_channel_rsci_wen_comp),
      .image_channel_rsci_idat_mxwt(image_channel_rsci_idat_mxwt)
    );
  catapult_dense_16000_2500_run_bias_memory_rsci_1 catapult_dense_16000_2500_run_bias_memory_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .bias_memory_rsci_q_d(bias_memory_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .bias_memory_rsci_oswt(reg_bias_memory_rsci_iswt0_cse),
      .bias_memory_rsci_q_d_mxwt(bias_memory_rsci_q_d_mxwt),
      .bias_memory_rsci_re_d_pff(bias_memory_rsci_re_d_iff),
      .bias_memory_rsci_oswt_pff(and_402_rmff)
    );
  catapult_dense_16000_2500_run_output_image_channel_rsci catapult_dense_16000_2500_run_output_image_channel_rsci_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_image_channel_rsc_dat(output_image_channel_rsc_dat),
      .output_image_channel_rsc_vld(output_image_channel_rsc_vld),
      .output_image_channel_rsc_rdy(output_image_channel_rsc_rdy),
      .run_wen(run_wen),
      .output_image_channel_rsci_oswt(reg_output_image_channel_rsci_iswt0_cse),
      .output_image_channel_rsci_wen_comp(output_image_channel_rsci_wen_comp),
      .output_image_channel_rsci_idat(output_image_channel_rsci_idat)
    );
  catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1 catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .input_image_rsc_0_0_i_q_d(input_image_rsc_0_0_i_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .input_image_rsc_0_0_i_oswt_1(reg_input_image_rsc_0_0_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .input_image_rsc_0_0_i_q_d_mxwt(input_image_rsc_0_0_i_q_d_mxwt),
      .input_image_rsc_0_0_i_we_d_pff(input_image_rsc_0_0_i_we_d_iff),
      .input_image_rsc_0_0_i_iswt0_pff(nl_catapult_dense_16000_2500_run_input_image_rsc_0_0_i_1_inst_input_image_rsc_0_0_i_iswt0_pff),
      .input_image_rsc_0_0_i_re_d_pff(input_image_rsc_0_0_i_re_d_iff),
      .input_image_rsc_0_0_i_oswt_1_pff(and_394_rmff)
    );
  catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1 catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .input_image_rsc_0_1_i_q_d(input_image_rsc_0_1_i_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .input_image_rsc_0_1_i_oswt_1(reg_input_image_rsc_0_1_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .input_image_rsc_0_1_i_q_d_mxwt(input_image_rsc_0_1_i_q_d_mxwt),
      .input_image_rsc_0_1_i_we_d_pff(input_image_rsc_0_1_i_we_d_iff),
      .input_image_rsc_0_1_i_iswt0_pff(nl_catapult_dense_16000_2500_run_input_image_rsc_0_1_i_1_inst_input_image_rsc_0_1_i_iswt0_pff),
      .input_image_rsc_0_1_i_re_d_pff(input_image_rsc_0_1_i_re_d_iff),
      .input_image_rsc_0_1_i_oswt_1_pff(and_388_rmff)
    );
  catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1 catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_buffer_rsc_0_0_i_q_d(output_buffer_rsc_0_0_i_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .output_buffer_rsc_0_0_i_oswt_1(reg_output_buffer_rsc_0_0_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .output_buffer_rsc_0_0_i_q_d_mxwt(output_buffer_rsc_0_0_i_q_d_mxwt),
      .output_buffer_rsc_0_0_i_we_d_pff(output_buffer_rsc_0_0_i_we_d_iff),
      .output_buffer_rsc_0_0_i_iswt0_pff(nl_catapult_dense_16000_2500_run_output_buffer_rsc_0_0_i_1_inst_output_buffer_rsc_0_0_i_iswt0_pff),
      .output_buffer_rsc_0_0_i_re_d_pff(output_buffer_rsc_0_0_i_re_d_iff),
      .output_buffer_rsc_0_0_i_oswt_1_pff(and_382_rmff)
    );
  catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1 catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_buffer_rsc_0_1_i_q_d(output_buffer_rsc_0_1_i_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .output_buffer_rsc_0_1_i_oswt_1(reg_output_buffer_rsc_0_1_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .output_buffer_rsc_0_1_i_q_d_mxwt(output_buffer_rsc_0_1_i_q_d_mxwt),
      .output_buffer_rsc_0_1_i_we_d_pff(output_buffer_rsc_0_1_i_we_d_iff),
      .output_buffer_rsc_0_1_i_iswt0_pff(nl_catapult_dense_16000_2500_run_output_buffer_rsc_0_1_i_1_inst_output_buffer_rsc_0_1_i_iswt0_pff),
      .output_buffer_rsc_0_1_i_re_d_pff(output_buffer_rsc_0_1_i_re_d_iff),
      .output_buffer_rsc_0_1_i_oswt_1_pff(and_376_rmff)
    );
  catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1 catapult_dense_16000_2500_run_weight_memory_data_rsc_0_0_i_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .weight_memory_data_rsc_0_0_i_q_d(weight_memory_data_rsc_0_0_i_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .weight_memory_data_rsc_0_0_i_oswt(reg_weight_memory_data_rsc_0_1_i_iswt0_cse),
      .weight_memory_data_rsc_0_0_i_q_d_mxwt(weight_memory_data_rsc_0_0_i_q_d_mxwt),
      .weight_memory_data_rsc_0_0_i_re_d_pff(weight_memory_data_rsc_0_0_i_re_d_iff),
      .weight_memory_data_rsc_0_0_i_oswt_pff(and_370_rmff)
    );
  catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1 catapult_dense_16000_2500_run_weight_memory_data_rsc_0_1_i_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .weight_memory_data_rsc_0_1_i_q_d(weight_memory_data_rsc_0_1_i_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .weight_memory_data_rsc_0_1_i_oswt(reg_weight_memory_data_rsc_0_1_i_iswt0_cse),
      .weight_memory_data_rsc_0_1_i_q_d_mxwt(weight_memory_data_rsc_0_1_i_q_d_mxwt),
      .weight_memory_data_rsc_0_1_i_re_d_pff(weight_memory_data_rsc_0_1_i_re_d_iff),
      .weight_memory_data_rsc_0_1_i_oswt_pff(and_370_rmff)
    );
  catapult_dense_16000_2500_run_bias_triosy_obj catapult_dense_16000_2500_run_bias_triosy_obj_inst
      (
      .bias_triosy_lz(bias_triosy_lz),
      .run_wten(run_wten),
      .bias_triosy_obj_iswt0(reg_output_image_elements_triosy_obj_iswt0_cse)
    );
  catapult_dense_16000_2500_run_relu_triosy_obj catapult_dense_16000_2500_run_relu_triosy_obj_inst
      (
      .relu_triosy_lz(relu_triosy_lz),
      .run_wten(run_wten),
      .relu_triosy_obj_iswt0(reg_output_image_elements_triosy_obj_iswt0_cse)
    );
  catapult_dense_16000_2500_run_weight_memory_data_tri_0_1_osy_obj catapult_dense_16000_2500_run_weight_memory_data_tri_0_1_osy_obj_inst
      (
      .weight_memory_data_tri_0_1_osy_lz(weight_memory_data_tri_0_1_osy_lz),
      .run_wten(run_wten),
      .weight_memory_data_tri_0_1_osy_obj_iswt0(reg_output_image_elements_triosy_obj_iswt0_cse)
    );
  catapult_dense_16000_2500_run_weight_memory_data_tri_0_0_osy_obj catapult_dense_16000_2500_run_weight_memory_data_tri_0_0_osy_obj_inst
      (
      .weight_memory_data_tri_0_0_osy_lz(weight_memory_data_tri_0_0_osy_lz),
      .run_wten(run_wten),
      .weight_memory_data_tri_0_0_osy_obj_iswt0(reg_output_image_elements_triosy_obj_iswt0_cse)
    );
  catapult_dense_16000_2500_run_bias_memory_triosy_obj catapult_dense_16000_2500_run_bias_memory_triosy_obj_inst
      (
      .bias_memory_triosy_lz(bias_memory_triosy_lz),
      .run_wten(run_wten),
      .bias_memory_triosy_obj_iswt0(reg_output_image_elements_triosy_obj_iswt0_cse)
    );
  catapult_dense_16000_2500_run_input_vector_length_triosy_obj catapult_dense_16000_2500_run_input_vector_length_triosy_obj_inst
      (
      .input_vector_length_triosy_lz(input_vector_length_triosy_lz),
      .run_wten(run_wten),
      .input_vector_length_triosy_obj_iswt0(reg_output_image_elements_triosy_obj_iswt0_cse)
    );
  catapult_dense_16000_2500_run_output_image_elements_triosy_obj catapult_dense_16000_2500_run_output_image_elements_triosy_obj_inst
      (
      .output_image_elements_triosy_lz(output_image_elements_triosy_lz),
      .run_wten(run_wten),
      .output_image_elements_triosy_obj_iswt0(reg_output_image_elements_triosy_obj_iswt0_cse)
    );
  catapult_dense_16000_2500_run_wait_dp catapult_dense_16000_2500_run_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .inner_multiply_loop_1_qif_mul_cmp_z(inner_multiply_loop_1_qif_mul_cmp_z),
      .inner_multiply_loop_1_qif_mul_cmp_1_z(inner_multiply_loop_1_qif_mul_cmp_1_z),
      .run_wen(run_wen),
      .inner_multiply_loop_1_qif_mul_cmp_z_oreg(inner_multiply_loop_1_qif_mul_cmp_z_oreg),
      .inner_multiply_loop_1_qif_mul_cmp_1_z_oreg(inner_multiply_loop_1_qif_mul_cmp_1_z_oreg)
    );
  catapult_dense_16000_2500_run_staller catapult_dense_16000_2500_run_staller_inst
      (
      .clk(clk),
      .rstn(rstn),
      .catapult_dense_16000_2500_stall(catapult_dense_16000_2500_stall),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .image_channel_rsci_wen_comp(image_channel_rsci_wen_comp),
      .output_image_channel_rsci_wen_comp(output_image_channel_rsci_wen_comp)
    );
  catapult_dense_16000_2500_run_run_fsm catapult_dense_16000_2500_run_run_fsm_inst
      (
      .clk(clk),
      .rstn(rstn),
      .run_wen(run_wen),
      .fsm_output(fsm_output),
      .image_vector_load_loop_C_0_tr0(and_dcpl),
      .main_run_loop_C_0_tr0(nl_catapult_dense_16000_2500_run_run_fsm_inst_main_run_loop_C_0_tr0),
      .result_vector_send_loop_C_0_tr0(and_dcpl_138)
    );
  assign nl_operator_28_true_acc_nl = conv_s2s_28_29(~ output_image_elements_rsci_idat)
      + 29'b00000000000000000100111000101;
  assign operator_28_true_acc_nl = nl_operator_28_true_acc_nl[28:0];
  assign slc_fsm_output_0_16 = fsm_output[0];
  assign poutput_image_elements_MAX_OUTPUT_LENGTH_prb = MUX1HOT_s_1_1_2(~ (readslicef_29_1_28(operator_28_true_acc_nl)),
      slc_fsm_output_0_16);
  // assert(output_image_elements <= (MAX_OUTPUT_LENGTH)) - ../../../common/cpp/catapult_dense_architected.h: line 176
  // psl default clock = (posedge clk);
  // psl catapult_dense_16000_2500_run_catapult_dense_architected_h_ln176_assert_output_image_elements_le_OP_MAX_OUTPUT_LENGTH_CP : assert always ( rstn &&  poutput_image_elements_MAX_OUTPUT_LENGTH_ctrl_prb  -> poutput_image_elements_MAX_OUTPUT_LENGTH_prb );
  assign poutput_image_elements_MAX_OUTPUT_LENGTH_ctrl_prb = catapult_dense_16000_2500_run_catapult_dense_architected_h_ln176_assert_output_image_elements_le_OP_MAX_OUTPUT_LENGTH_CP_ctrl_prb_2;
  assign pinput_vector_length_MAX_VECTOR_LENGTH_prb = catapult_dense_16000_2500_run_catapult_dense_architected_h_ln178_assert_input_vector_length_le_OP_MAX_VECTOR_LENGTH_CP_sig_1;
  // assert(input_vector_length <= (MAX_VECTOR_LENGTH)) - ../../../common/cpp/catapult_dense_architected.h: line 178
  // psl catapult_dense_16000_2500_run_catapult_dense_architected_h_ln178_assert_input_vector_length_le_OP_MAX_VECTOR_LENGTH_CP : assert always ( rstn &&  pinput_vector_length_MAX_VECTOR_LENGTH_ctrl_prb  -> pinput_vector_length_MAX_VECTOR_LENGTH_prb );
  assign pinput_vector_length_MAX_VECTOR_LENGTH_ctrl_prb = catapult_dense_16000_2500_run_catapult_dense_architected_h_ln176_assert_output_image_elements_le_OP_MAX_OUTPUT_LENGTH_CP_ctrl_prb_2;
  assign pvector_length_MAX_VECTOR_LENGTH_prb = catapult_dense_16000_2500_run_catapult_dense_architected_h_ln178_assert_input_vector_length_le_OP_MAX_VECTOR_LENGTH_CP_sig_1;
  // assert(vector_length<=MAX_VECTOR_LENGTH) - ../../../common/cpp/catapult_dense_architected.h: line 63
  // psl catapult_dense_16000_2500_run_catapult_dense_architected_h_ln63_assert_vector_length_le_MAX_VECTOR_LENGTH : assert always ( rstn &&  pvector_length_MAX_VECTOR_LENGTH_ctrl_prb  -> pvector_length_MAX_VECTOR_LENGTH_prb );
  assign pvector_length_MAX_VECTOR_LENGTH_ctrl_prb = catapult_dense_16000_2500_run_catapult_dense_architected_h_ln176_assert_output_image_elements_le_OP_MAX_OUTPUT_LENGTH_CP_ctrl_prb_2;
  assign output_image_elements_and_cse = run_wen & (~ or_tmp_57);
  assign main_run_loop_and_cse = run_wen & ((~(or_dcpl_54 | or_dcpl_53 | or_tmp_67))
      | (fsm_output[0]));
  assign main_run_loop_and_42_cse = run_wen & ((~(or_dcpl_54 | or_dcpl_56 | (fsm_output[3])))
      | (fsm_output[0]) | (fsm_output[4])) & (reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse
      | (fsm_output[0]) | (fsm_output[4]));
  assign main_run_loop_and_18_cse = run_wen & ((fsm_output[0]) | (fsm_output[2]));
  assign image_vector_load_loop_index_and_ssc = run_wen & (bias_sva_st_2 | and_dcpl
      | image_vector_load_loop_index_sva_mx0c0 | (fsm_output[2]) | (fsm_output[4]));
  assign and_370_rmff = and_dcpl_48 & operator_28_true_1_acc_itm_20_1 & (fsm_output[2]);
  assign and_376_rmff = bias_sva & (image_vector_load_loop_index_sva_26_0[0]) & (fsm_output[4]);
  assign and_382_rmff = bias_sva & (~ (image_vector_load_loop_index_sva_26_0[0]))
      & (fsm_output[4]);
  assign and_388_rmff = and_dcpl_48 & operator_28_true_2_acc_itm_27_1 & (fsm_output[2]);
  assign and_394_rmff = and_dcpl_48 & inner_multiply_loop_1_operator_28_true_2_acc_itm_28_1
      & (fsm_output[2]);
  assign and_402_rmff = and_dcpl_48 & (~ operator_28_true_unequal_tmp) & (~ (input_vector_length_sva[0]))
      & bias_sva & (fsm_output[2]);
  assign main_run_loop_and_22_cse = run_wen & (and_dcpl_163 | and_dcpl_165 | main_run_loop_stage_0_6)
      & (fsm_output[2]);
  assign main_run_loop_and_23_cse = run_wen & (and_dcpl_168 | and_dcpl_169 | and_dcpl_170
      | and_dcpl_171) & (fsm_output[2]);
  assign main_run_loop_or_21_cse = and_dcpl_168 | ((~ reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse)
      & and_dcpl_169);
  assign main_run_loop_and_45_cse = reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse
      & and_dcpl_169;
  assign main_run_loop_and_46_cse = (~ main_run_loop_slc_main_run_loop_i_11_0_1_itm_4)
      & and_dcpl_170;
  assign main_run_loop_and_47_cse = main_run_loop_slc_main_run_loop_i_11_0_1_itm_4
      & and_dcpl_170;
  assign main_run_loop_or_22_cse = main_run_loop_and_47_cse | and_dcpl_171;
  assign main_run_loop_and_68_cse = (~ main_run_loop_slc_main_run_loop_i_11_0_1_itm_4)
      & main_run_loop_or_22_cse;
  assign main_run_loop_and_69_cse = main_run_loop_slc_main_run_loop_i_11_0_1_itm_4
      & main_run_loop_or_22_cse;
  assign main_run_loop_or_26_cse = main_run_loop_and_46_cse | and_dcpl_171;
  assign main_run_loop_and_72_cse = (~ main_run_loop_slc_main_run_loop_i_11_0_1_itm_4)
      & main_run_loop_or_26_cse;
  assign main_run_loop_and_73_cse = main_run_loop_slc_main_run_loop_i_11_0_1_itm_4
      & main_run_loop_or_26_cse;
  assign main_run_loop_and_30_ssc = run_wen & (main_run_loop_stage_0 | main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_mx1c0
      | main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_mx1c1) & (fsm_output[2]);
  assign nl_main_run_loop_acc_sdt = ({image_vector_load_loop_index_sva_mx2_31_30
      , image_vector_load_loop_index_sva_mx2_29_27 , image_vector_load_loop_index_sva_mx2_26_0})
      + 32'b00000000000000000000000000000001;
  assign main_run_loop_acc_sdt = nl_main_run_loop_acc_sdt[31:0];
  assign and_429_rgt = or_dcpl_53 & main_run_loop_stage_0_7 & (fsm_output[2]);
  assign main_run_loop_i_and_1_rgt = main_run_loop_stage_0 & (~ and_423_cse);
  assign main_run_loop_i_main_run_loop_i_nor_1_ssc = ~(and_429_rgt | main_run_loop_i_and_1_rgt);
  assign main_run_loop_i_and_4_ssc = and_429_rgt & (~ main_run_loop_i_and_1_rgt);
  assign main_run_loop_i_and_2_ssc = run_wen & ((~(and_423_cse | ((~ main_run_loop_stage_0_7)
      & (fsm_output[2])))) | main_run_loop_i_and_1_rgt);
  assign nl_main_run_loop_if_1_acc_nl = bias_index_10_0_sva + 11'b00000000001;
  assign main_run_loop_if_1_acc_nl = nl_main_run_loop_if_1_acc_nl[10:0];
  assign or_72_nl = (~ main_run_loop_stage_0_3) | (~ operator_28_true_operator_28_true_nor_mdf_sva_1)
      | main_run_loop_asn_40_itm_2;
  assign bias_index_10_0_sva_4 = MUX_v_11_2_2(main_run_loop_if_1_acc_nl, bias_index_10_0_sva,
      or_72_nl);
  assign or_269_tmp = (~ main_run_loop_stage_0_6) | main_run_loop_asn_40_itm_5;
  assign operator_28_true_mux_55_nl = MUX_s_1_2_2(main_run_loop_conc_3_dfmergedata_0_lpi_2,
      main_run_loop_asn_dfmergedata_0_lpi_2_dfm_5, reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse);
  assign main_run_loop_mux_19_nl = MUX_s_1_2_2(operator_28_true_mux_55_nl, main_run_loop_conc_3_dfmergedata_0_lpi_2,
      reg_main_run_loop_asn_40_itm_6_cse);
  assign main_run_loop_conc_3_dfmergedata_0_lpi_2_mx1 = MUX_s_1_2_2(main_run_loop_mux_19_nl,
      main_run_loop_conc_3_dfmergedata_0_lpi_2, or_dcpl_54);
  assign operator_28_true_mux_56_nl = MUX_s_1_2_2(main_run_loop_conc_3_dfmergedata_31_lpi_2,
      main_run_loop_asn_dfmergedata_31_lpi_2_dfm_5, reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse);
  assign main_run_loop_mux_21_nl = MUX_s_1_2_2(operator_28_true_mux_56_nl, main_run_loop_conc_3_dfmergedata_31_lpi_2,
      reg_main_run_loop_asn_40_itm_6_cse);
  assign main_run_loop_conc_3_dfmergedata_31_lpi_2_mx1 = MUX_s_1_2_2(main_run_loop_mux_21_nl,
      main_run_loop_conc_3_dfmergedata_31_lpi_2, or_dcpl_54);
  assign operator_28_true_mux_57_nl = MUX_s_1_2_2(main_run_loop_conc_1_dfmergedata_0_lpi_2,
      main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_5, reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse);
  assign main_run_loop_mux_24_nl = MUX_s_1_2_2(operator_28_true_mux_57_nl, main_run_loop_conc_1_dfmergedata_0_lpi_2,
      reg_main_run_loop_asn_40_itm_6_cse);
  assign main_run_loop_conc_1_dfmergedata_0_lpi_2_mx1 = MUX_s_1_2_2(main_run_loop_mux_24_nl,
      main_run_loop_conc_1_dfmergedata_0_lpi_2, or_dcpl_54);
  assign operator_28_true_mux_58_nl = MUX_s_1_2_2(main_run_loop_conc_1_dfmergedata_31_lpi_2,
      main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_5, reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse);
  assign main_run_loop_mux_26_nl = MUX_s_1_2_2(operator_28_true_mux_58_nl, main_run_loop_conc_1_dfmergedata_31_lpi_2,
      reg_main_run_loop_asn_40_itm_6_cse);
  assign main_run_loop_conc_1_dfmergedata_31_lpi_2_mx1 = MUX_s_1_2_2(main_run_loop_mux_26_nl,
      main_run_loop_conc_1_dfmergedata_31_lpi_2, or_dcpl_54);
  assign catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_mx1 = MUX_v_64_2_2(catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1,
      catapult_dense_16000_2500_compute_dense_accumulator_lpi_2, or_269_tmp);
  assign main_run_loop_i_and_ssc = (~(operator_28_true_unequal_tmp | exit_main_run_loop_lpi_2_dfm_1))
      & main_run_loop_stage_0_2;
  assign image_vector_load_loop_index_sva_mx2_31_30 = MUX_v_2_2_2(image_vector_load_loop_index_sva_31_30,
      main_run_loop_i_sva_1_1_31_30, main_run_loop_i_and_ssc);
  assign image_vector_load_loop_index_sva_mx2_29_27 = MUX_v_3_2_2(image_vector_load_loop_index_sva_29_27,
      main_run_loop_i_sva_1_1_29_27, main_run_loop_i_and_ssc);
  assign image_vector_load_loop_index_sva_mx2_26_0 = MUX_v_27_2_2(image_vector_load_loop_index_sva_26_0,
      main_run_loop_i_sva_1_1_26_0, main_run_loop_i_and_ssc);
  assign main_run_loop_if_1_mux_10_nl = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_1_mx0,
      main_run_loop_if_1_conc_cmx_0_sva_1, reg_main_run_loop_if_1_slc_main_run_loop_i_11_0_7_itm_5_cse);
  assign output_buffer_rsc_0_1_i_d_d_run_0_mx1 = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_1_mx0,
      main_run_loop_if_1_mux_10_nl, bias_sva);
  assign main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_29_27 = MUX1HOT_v_3_3_2(main_run_loop_asn_dfmergedata_1_30_1_lpi_2_29_27,
      main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_29_27, (main_run_loop_if_1_conc_cmx_30_1_sva_1[29:27]),
      {main_run_loop_main_run_loop_nor_3_itm_2 , main_run_loop_and_10_itm_5 , main_run_loop_and_3_tmp_5});
  assign main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_26_0 = MUX1HOT_v_27_3_2(main_run_loop_asn_dfmergedata_1_30_1_lpi_2_26_0,
      main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_26_0, (main_run_loop_if_1_conc_cmx_30_1_sva_1[26:0]),
      {main_run_loop_main_run_loop_nor_3_itm_2 , main_run_loop_and_10_itm_5 , main_run_loop_and_3_tmp_5});
  assign main_run_loop_if_1_mux_11_nl = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_1_mx0,
      main_run_loop_if_1_conc_cmx_31_sva_1, reg_main_run_loop_if_1_slc_main_run_loop_i_11_0_7_itm_5_cse);
  assign output_buffer_rsc_0_1_i_d_d_run_31_mx1 = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_1_mx0,
      main_run_loop_if_1_mux_11_nl, bias_sva);
  assign main_run_loop_if_1_mux_13_nl = MUX_s_1_2_2(main_run_loop_if_1_conc_cmx_0_sva_1,
      main_run_loop_asn_dfmergedata_0_lpi_2_dfm_1_mx0, main_run_loop_if_1_slc_main_run_loop_i_11_0_13_itm_5);
  assign output_buffer_rsc_0_0_i_d_d_run_0_mx1 = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_0_lpi_2_dfm_1_mx0,
      main_run_loop_if_1_mux_13_nl, bias_sva);
  assign main_run_loop_main_run_loop_nor_2_ssc = ~(reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1
      | main_run_loop_and_2_tmp_5);
  assign main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_29_27 = MUX1HOT_v_3_3_2(main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_29_27,
      main_run_loop_asn_dfmergedata_30_1_lpi_2_29_27, (main_run_loop_if_1_conc_cmx_30_1_sva_1[29:27]),
      {main_run_loop_main_run_loop_nor_2_ssc , reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1
      , main_run_loop_and_2_tmp_5});
  assign main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_26_0 = MUX1HOT_v_27_3_2(main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_26_0,
      main_run_loop_asn_dfmergedata_30_1_lpi_2_26_0, (main_run_loop_if_1_conc_cmx_30_1_sva_1[26:0]),
      {main_run_loop_main_run_loop_nor_2_ssc , reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1
      , main_run_loop_and_2_tmp_5});
  assign main_run_loop_if_1_mux_15_nl = MUX_s_1_2_2(main_run_loop_if_1_conc_cmx_31_sva_1,
      main_run_loop_asn_dfmergedata_31_lpi_2_dfm_1_mx0, main_run_loop_if_1_slc_main_run_loop_i_11_0_13_itm_5);
  assign output_buffer_rsc_0_0_i_d_d_run_31_mx1 = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_31_lpi_2_dfm_1_mx0,
      main_run_loop_if_1_mux_15_nl, bias_sva);
  assign catapult_dense_16000_2500_run_catapult_dense_architected_h_ln176_assert_output_image_elements_le_OP_MAX_OUTPUT_LENGTH_CP_ctrl_prb_2
      = MUX1HOT_s_1_1_2(run_wen, fsm_output[0]);
  assign nl_operator_28_true_1_acc_nl = conv_s2s_28_29(~ input_vector_length_rsci_idat)
      + 29'b00000000000000011111010000001;
  assign operator_28_true_1_acc_nl = nl_operator_28_true_1_acc_nl[28:0];
  assign catapult_dense_16000_2500_run_catapult_dense_architected_h_ln178_assert_input_vector_length_le_OP_MAX_VECTOR_LENGTH_CP_sig_1
      = MUX1HOT_s_1_1_2(~ (readslicef_29_1_28(operator_28_true_1_acc_nl)), fsm_output[0]);
  assign catapult_dense_16000_2500_compute_dense_nor_2_nl = ~(MUX_v_30_2_2((catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1[30:1]),
      30'b111111111111111111111111111111, catapult_dense_16000_2500_compute_dense_nor_ovfl_sva_1));
  assign main_run_loop_conc_cse_30_1_sva_mx1w2 = ~(MUX_v_30_2_2(catapult_dense_16000_2500_compute_dense_nor_2_nl,
      30'b111111111111111111111111111111, catapult_dense_16000_2500_compute_dense_and_unfl_sva_1));
  assign operator_28_true_unequal_tmp = (outer_multiply_loop_n_31_1_lpi_2_dfm_1_1)
      != ({(main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_26_0[26]) , (main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_26_0[26])
      , (main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_26_0[26]) , (main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_26_0[26])
      , main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_26_0});
  assign operator_28_true_operator_28_true_nor_mdf_sva_1_1 = ~(operator_28_true_unequal_tmp
      | (input_vector_length_sva[0]));
  assign bias_sva_st_1_mx0w1 = bias_sva_st_1 & ((~ bias_sva_st_2) | (z_out_2[32]));
  assign nl_operator_28_true_2_acc_nl =  -conv_s2s_27_28(outer_multiply_loop_x_sva_1[27:1]);
  assign operator_28_true_2_acc_nl = nl_operator_28_true_2_acc_nl[27:0];
  assign operator_28_true_2_acc_itm_27_1 = readslicef_28_1_27(operator_28_true_2_acc_nl);
  assign nl_inner_multiply_loop_1_operator_28_true_2_acc_nl =  -conv_s2s_28_29(outer_multiply_loop_x_sva_1);
  assign inner_multiply_loop_1_operator_28_true_2_acc_nl = nl_inner_multiply_loop_1_operator_28_true_2_acc_nl[28:0];
  assign inner_multiply_loop_1_operator_28_true_2_acc_itm_28_1 = readslicef_29_1_28(inner_multiply_loop_1_operator_28_true_2_acc_nl);
  assign nl_operator_28_true_1_acc_nl_1 = conv_s2u_20_21(catapult_dense_16000_2500_load_image_end_sva[27:8])
      + 21'b111101100111011010011;
  assign operator_28_true_1_acc_nl_1 = nl_operator_28_true_1_acc_nl_1[20:0];
  assign operator_28_true_1_acc_itm_20_1 = readslicef_21_1_20(operator_28_true_1_acc_nl_1);
  assign main_run_loop_and_3_tmp_1 = (image_vector_load_loop_index_sva_26_0[0]) &
      bias_sva;
  assign main_run_loop_and_1_tmp_1 = (image_vector_load_loop_index_sva_26_0[0]) &
      relu_sva;
  assign nl_operator_28_true_acc_nl_1 = conv_s2u_32_33({image_vector_load_loop_index_sva_mx2_31_30
      , image_vector_load_loop_index_sva_mx2_29_27 , image_vector_load_loop_index_sva_mx2_26_0})
      - conv_s2u_28_33(output_image_elements_sva);
  assign operator_28_true_acc_nl_1 = nl_operator_28_true_acc_nl_1[32:0];
  assign operator_28_true_acc_itm_32_1 = readslicef_33_1_32(operator_28_true_acc_nl_1);
  assign main_run_loop_if_1_conc_cmx_31_sva_1 = ~((~((main_run_loop_if_1_acc_sat_sva_1[31])
      | main_run_loop_if_1_and_unfl_sva_1)) | main_run_loop_if_1_nor_ovfl_sva_1);
  assign main_run_loop_if_1_conc_cmx_0_sva_1 = ~((~((main_run_loop_if_1_acc_sat_sva_1[0])
      | main_run_loop_if_1_nor_ovfl_sva_1)) | main_run_loop_if_1_and_unfl_sva_1);
  assign main_run_loop_if_1_nor_1_nl = ~(MUX_v_30_2_2((main_run_loop_if_1_acc_sat_sva_1[30:1]),
      30'b111111111111111111111111111111, main_run_loop_if_1_nor_ovfl_sva_1));
  assign main_run_loop_if_1_conc_cmx_30_1_sva_1 = ~(MUX_v_30_2_2(main_run_loop_if_1_nor_1_nl,
      30'b111111111111111111111111111111, main_run_loop_if_1_and_unfl_sva_1));
  assign main_run_loop_if_1_mux_18_nl = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_5,
      main_run_loop_asn_dfmergedata_31_lpi_2_dfm_5, reg_main_run_loop_if_1_slc_main_run_loop_i_11_0_7_itm_5_cse);
  assign main_run_loop_if_1_mux_1_nl = MUX_v_3_2_2(main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_29_27,
      main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_29_27, reg_main_run_loop_if_1_slc_main_run_loop_i_11_0_7_itm_5_cse);
  assign main_run_loop_if_1_mux_19_nl = MUX_v_27_2_2(main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_26_0,
      main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_26_0, reg_main_run_loop_if_1_slc_main_run_loop_i_11_0_7_itm_5_cse);
  assign main_run_loop_if_1_mux_2_nl = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_5,
      main_run_loop_asn_dfmergedata_0_lpi_2_dfm_5, reg_main_run_loop_if_1_slc_main_run_loop_i_11_0_7_itm_5_cse);
  assign nl_main_run_loop_if_1_acc_sat_sva_1 = conv_s2s_32_33({main_run_loop_if_1_mux_18_nl
      , main_run_loop_if_1_mux_1_nl , main_run_loop_if_1_mux_19_nl , main_run_loop_if_1_mux_2_nl})
      + conv_s2s_32_33(main_run_loop_if_1_asn_10_itm_4);
  assign main_run_loop_if_1_acc_sat_sva_1 = nl_main_run_loop_if_1_acc_sat_sva_1[32:0];
  assign main_run_loop_if_1_and_unfl_sva_1 = (main_run_loop_if_1_acc_sat_sva_1[32:31]==2'b10);
  assign main_run_loop_if_1_nor_ovfl_sva_1 = ~((main_run_loop_if_1_acc_sat_sva_1[32:31]!=2'b01));
  assign main_run_loop_main_run_loop_nor_ssc = ~(reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1
      | main_run_loop_and_tmp_5);
  assign main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_29_27 = MUX1HOT_v_3_3_2(main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_29_27,
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_29_27, main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_29_27,
      {main_run_loop_main_run_loop_nor_ssc , reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1
      , main_run_loop_and_tmp_5});
  assign main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_26_0 = MUX1HOT_v_27_3_2(main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_26_0,
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_26_0, main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_26_0,
      {main_run_loop_main_run_loop_nor_ssc , reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1
      , main_run_loop_and_tmp_5});
  assign main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_29_27 = MUX1HOT_v_3_3_2(main_run_loop_conc_3_dfmergedata_30_1_lpi_2_29_27,
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_29_27, main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_29_27,
      {main_run_loop_main_run_loop_nor_1_itm_3 , main_run_loop_and_7_itm_5 , main_run_loop_and_1_tmp_5});
  assign main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_26_0 = MUX1HOT_v_27_3_2(main_run_loop_conc_3_dfmergedata_30_1_lpi_2_26_0,
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_26_0, main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_26_0,
      {main_run_loop_main_run_loop_nor_1_itm_3 , main_run_loop_and_7_itm_5 , main_run_loop_and_1_tmp_5});
  assign main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_1_mx0 = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_0_lpi_2,
      main_run_loop_asn_dfmergedata_0_lpi_2_dfm_5, reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1);
  assign main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_1_mx0 = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_31_lpi_2,
      main_run_loop_asn_dfmergedata_31_lpi_2_dfm_5, reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1);
  assign main_run_loop_asn_dfmergedata_0_lpi_2_dfm_1_mx0 = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_5,
      main_run_loop_asn_dfmergedata_0_lpi_2, reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1);
  assign main_run_loop_asn_dfmergedata_31_lpi_2_dfm_1_mx0 = MUX_s_1_2_2(main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_5,
      main_run_loop_asn_dfmergedata_31_lpi_2, reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1);
  assign nl_outer_multiply_loop_x_sva_1 = input_vector_length_sva + ({(~ (outer_multiply_loop_n_31_1_lpi_2_dfm_1_1[26:0]))
      , 1'b1}) + 28'b0000000000000000000000000001;
  assign outer_multiply_loop_x_sva_1 = nl_outer_multiply_loop_x_sva_1[27:0];
  assign main_run_loop_if_qr_0_lpi_2_dfm_1 = main_run_loop_conc_cse_0_sva_1 & main_run_loop_if_acc_itm_32_1;
  assign main_run_loop_if_qr_31_lpi_2_dfm_1 = main_run_loop_conc_cse_31_sva_1 & main_run_loop_if_acc_itm_32_1;
  assign main_run_loop_conc_cse_0_sva_1 = ~((~((catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1[0])
      | catapult_dense_16000_2500_compute_dense_nor_ovfl_sva_1)) | catapult_dense_16000_2500_compute_dense_and_unfl_sva_1);
  assign nl_main_run_loop_if_acc_nl = conv_s2u_32_33({(~ main_run_loop_conc_cse_31_sva_1)
      , (~ main_run_loop_conc_cse_30_1_sva_mx1w2) , (~ main_run_loop_conc_cse_0_sva_1)})
      + 33'b000000000000000000000000000000001;
  assign main_run_loop_if_acc_nl = nl_main_run_loop_if_acc_nl[32:0];
  assign main_run_loop_if_acc_itm_32_1 = readslicef_33_1_32(main_run_loop_if_acc_nl);
  assign main_run_loop_conc_cse_31_sva_1 = ~((~((catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1[31])
      | catapult_dense_16000_2500_compute_dense_and_unfl_sva_1)) | catapult_dense_16000_2500_compute_dense_nor_ovfl_sva_1);
  assign nl_catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1 = conv_s2u_48_49(catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1[63:16])
      + conv_u2u_1_49(catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1[15]);
  assign catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1 = nl_catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1[48:0];
  assign catapult_dense_16000_2500_compute_dense_and_unfl_sva_1 = (catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1[48])
      & (~((catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1[47:31]==17'b11111111111111111)));
  assign catapult_dense_16000_2500_compute_dense_nor_ovfl_sva_1 = ~((catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1[48])
      | (~((catapult_dense_16000_2500_compute_dense_slc_64_16_sat_sva_1[47:31]!=17'b00000000000000000))));
  assign catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_3 = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_mx1, exit_main_run_loop_sva_1_4);
  assign and_dcpl = ~(bias_sva_st_2 | bias_sva_st_1);
  assign and_6_cse = main_run_loop_stage_0_7 & (~ reg_main_run_loop_asn_40_itm_6_cse);
  assign and_dcpl_48 = (~ exit_main_run_loop_lpi_2_dfm_1) & main_run_loop_stage_0_2;
  assign or_dcpl_27 = exit_main_run_loop_lpi_2_dfm_1 | (~ main_run_loop_stage_0_2);
  assign and_dcpl_138 = ~(bias_sva_st_1 | bias_sva);
  assign or_dcpl_51 = (fsm_output[5]) | (fsm_output[3]);
  assign or_dcpl_53 = reg_main_run_loop_asn_40_itm_6_cse | (~ reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse);
  assign or_dcpl_54 = (~ main_run_loop_stage_0_7) | reg_main_run_loop_asn_40_itm_6_cse;
  assign or_dcpl_56 = (fsm_output[5]) | (fsm_output[1]);
  assign and_dcpl_163 = and_6_cse & reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse
      & (~ main_run_loop_stage_0_6);
  assign and_dcpl_165 = or_dcpl_53 & main_run_loop_stage_0_7 & (~ main_run_loop_stage_0_6);
  assign and_dcpl_167 = main_run_loop_stage_0_7 & reg_main_run_loop_asn_40_itm_6_cse;
  assign and_dcpl_168 = and_dcpl_167 & (~ main_run_loop_stage_0_6);
  assign and_dcpl_169 = and_6_cse & (~ main_run_loop_stage_0_6);
  assign and_dcpl_170 = main_run_loop_stage_0_6 & relu_sva;
  assign and_dcpl_171 = main_run_loop_stage_0_6 & (~ relu_sva);
  assign and_dcpl_174 = (~ main_run_loop_stage_0_4) & main_run_loop_stage_0_7;
  assign and_dcpl_185 = (~ main_run_loop_stage_0_2) & main_run_loop_stage_0_7;
  assign and_dcpl_189 = (~ main_run_loop_stage_0_3) & main_run_loop_stage_0_7;
  assign nor_27_nl = ~(exitL_exit_outer_multiply_loop_sva | and_dcpl_48);
  assign nand_34_nl = ~(exitL_exit_outer_multiply_loop_sva & or_dcpl_27);
  assign or_125_nl = (input_vector_length_sva[0]) | operator_28_true_unequal_tmp;
  assign not_tmp_97 = MUX_s_1_2_2(nor_27_nl, nand_34_nl, or_125_nl);
  assign or_tmp_57 = (fsm_output[2:1]!=2'b00);
  assign or_tmp_67 = ~((fsm_output[2]) | (fsm_output[0]));
  assign and_423_cse = or_dcpl_51 | (fsm_output[4]);
  assign output_image_channel_rsci_idat_mx0c1 = bias_sva_st_1 & (~ main_run_loop_asn_dfmergedata_1_0_lpi_2)
      & (fsm_output[4]);
  assign main_run_loop_asn_dfmergedata_1_31_lpi_2_mx0c2 = (fsm_output[4:3]!=2'b00);
  assign image_vector_load_loop_index_sva_mx0c0 = or_dcpl_51 | (fsm_output[0]);
  assign main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5_mx2c0 = and_dcpl_174 & reg_main_run_loop_asn_40_itm_6_cse;
  assign main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5_mx2c1 = and_dcpl_174 & (~
      reg_main_run_loop_asn_40_itm_6_cse);
  assign main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5_mx2c0 = and_dcpl_167 & (~
      main_run_loop_stage_0_5);
  assign main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5_mx2c1 = and_6_cse & (~ main_run_loop_stage_0_5);
  assign main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_mx1c0 = and_6_cse & reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse
      & (~ main_run_loop_stage_0);
  assign main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_mx1c1 = or_dcpl_53 & main_run_loop_stage_0_7
      & (~ main_run_loop_stage_0);
  assign main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5_mx2c0 = and_dcpl_185 & reg_main_run_loop_asn_40_itm_6_cse;
  assign main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5_mx2c1 = and_dcpl_185 & (~
      reg_main_run_loop_asn_40_itm_6_cse);
  assign main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5_mx2c0 = and_dcpl_189 & reg_main_run_loop_asn_40_itm_6_cse;
  assign main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5_mx2c1 = and_dcpl_189 & (~
      reg_main_run_loop_asn_40_itm_6_cse);
  assign catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c0
      = main_run_loop_stage_0_5 & outer_multiply_loop_asn_3_itm_3;
  assign catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c1
      = main_run_loop_stage_0_5 & (~ outer_multiply_loop_asn_3_itm_3);
  assign catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c2
      = (~(main_run_loop_asn_40_itm_5 | main_run_loop_stage_0_5)) & main_run_loop_stage_0_6;
  assign bias_memory_rsci_radr_d = bias_index_10_0_sva_4;
  assign bias_memory_rsci_re_d_pff = bias_memory_rsci_re_d_iff;
  assign input_image_rsc_0_0_i_radr_d_pff = outer_multiply_loop_n_31_1_lpi_2_dfm_1_1[12:0];
  assign input_image_rsc_0_0_i_wadr_d_pff = image_vector_load_loop_index_sva_26_0[13:1];
  assign input_image_rsc_0_0_i_d_d_pff = image_channel_rsci_idat_mxwt;
  assign input_image_rsc_0_0_i_we_d_pff = input_image_rsc_0_0_i_we_d_iff;
  assign input_image_rsc_0_0_i_re_d_pff = input_image_rsc_0_0_i_re_d_iff;
  assign input_image_rsc_0_1_i_we_d_pff = input_image_rsc_0_1_i_we_d_iff;
  assign input_image_rsc_0_1_i_re_d_pff = input_image_rsc_0_1_i_re_d_iff;
  assign output_buffer_rsc_0_0_i_radr_d_pff = image_vector_load_loop_index_sva_26_0[11:1];
  assign output_buffer_rsc_0_0_i_wadr_d = reg_main_run_loop_i_slc_main_run_loop_i_11_1_2_itm_5_cse;
  assign output_buffer_rsc_0_0_i_d_d = {output_buffer_rsc_0_0_i_d_d_run_31_mx1 ,
      main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_29_27 , main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_26_0
      , output_buffer_rsc_0_0_i_d_d_run_0_mx1};
  assign output_buffer_rsc_0_0_i_we_d_pff = output_buffer_rsc_0_0_i_we_d_iff;
  assign output_buffer_rsc_0_0_i_re_d_pff = output_buffer_rsc_0_0_i_re_d_iff;
  assign output_buffer_rsc_0_1_i_d_d = {output_buffer_rsc_0_1_i_d_d_run_31_mx1 ,
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_29_27 , main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_26_0
      , output_buffer_rsc_0_1_i_d_d_run_0_mx1};
  assign output_buffer_rsc_0_1_i_we_d_pff = output_buffer_rsc_0_1_i_we_d_iff;
  assign output_buffer_rsc_0_1_i_re_d_pff = output_buffer_rsc_0_1_i_re_d_iff;
  assign weight_memory_data_rsc_0_0_i_radr_d_pff = catapult_dense_16000_2500_load_image_end_sva[21:0];
  assign weight_memory_data_rsc_0_0_i_re_d_pff = weight_memory_data_rsc_0_0_i_re_d_iff;
  assign weight_memory_data_rsc_0_1_i_re_d_pff = weight_memory_data_rsc_0_1_i_re_d_iff;
  assign or_260_cse = (fsm_output[4]) | (fsm_output[1]);
  assign or_265_cse = ((~(image_vector_load_loop_index_sva_mx0c0 | and_dcpl)) & (fsm_output[1]))
      | (fsm_output[4]);
  assign or_266_cse = image_vector_load_loop_index_sva_mx0c0 | (and_dcpl & (fsm_output[1]));
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      reg_image_channel_rsci_oswt_cse <= 1'b0;
      reg_output_image_elements_triosy_obj_iswt0_cse <= 1'b0;
      reg_weight_memory_data_rsc_0_1_i_iswt0_cse <= 1'b0;
      reg_output_buffer_rsc_0_1_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse
          <= 1'b0;
      reg_output_buffer_rsc_0_0_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse
          <= 1'b0;
      reg_input_image_rsc_0_1_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <=
          1'b0;
      reg_input_image_rsc_0_0_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <=
          1'b0;
      reg_output_image_channel_rsci_iswt0_cse <= 1'b0;
      reg_bias_memory_rsci_iswt0_cse <= 1'b0;
      inner_multiply_loop_1_qif_mul_cmp_1_b <= 32'b00000000000000000000000000000000;
      inner_multiply_loop_1_qif_mul_cmp_1_a <= 32'b00000000000000000000000000000000;
      inner_multiply_loop_1_qif_mul_cmp_b <= 32'b00000000000000000000000000000000;
      inner_multiply_loop_1_qif_mul_cmp_a <= 32'b00000000000000000000000000000000;
      bias_sva_st_1 <= 1'b0;
      bias_sva_st_2 <= 1'b0;
      main_run_loop_i_sva_1_1_31_30 <= 2'b00;
      main_run_loop_stage_0_2 <= 1'b0;
      main_run_loop_stage_0_3 <= 1'b0;
      main_run_loop_stage_0_4 <= 1'b0;
      main_run_loop_stage_0_5 <= 1'b0;
      main_run_loop_stage_0_6 <= 1'b0;
      main_run_loop_stage_0_7 <= 1'b0;
      bias_index_10_0_sva <= 11'b00000000000;
      reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1 <= 1'b0;
      reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse <= 1'b0;
      reg_main_run_loop_asn_40_itm_6_cse <= 1'b0;
      operator_28_true_operator_28_true_nor_mdf_sva_st_4 <= 1'b0;
      main_run_loop_asn_40_itm_5 <= 1'b0;
      operator_28_true_operator_28_true_nor_mdf_sva_st_3 <= 1'b0;
      exit_main_run_loop_lpi_2_dfm_st_4 <= 1'b0;
      operator_28_true_operator_28_true_nor_mdf_sva_st_2 <= 1'b0;
      exit_main_run_loop_lpi_2_dfm_st_3 <= 1'b0;
      operator_28_true_2_slc_operator_28_true_2_acc_27_svs_st_1 <= 1'b0;
      inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_st_1
          <= 1'b0;
      main_run_loop_asn_40_itm_2 <= 1'b0;
      exit_main_run_loop_lpi_2_dfm_1 <= 1'b0;
      outer_multiply_loop_n_31_1_lpi_2_dfm_1_1 <= 31'b0000000000000000000000000000000;
      reg_main_run_loop_if_1_slc_main_run_loop_i_11_0_7_itm_5_cse <= 1'b0;
      main_run_loop_if_1_asn_10_itm_4 <= 32'b00000000000000000000000000000000;
      reg_main_run_loop_i_slc_main_run_loop_i_11_1_2_itm_5_cse <= 11'b00000000000;
      main_run_loop_main_run_loop_nor_3_itm_2 <= 1'b0;
      main_run_loop_and_10_itm_5 <= 1'b0;
      main_run_loop_and_3_tmp_5 <= 1'b0;
      main_run_loop_if_1_slc_main_run_loop_i_11_0_13_itm_5 <= 1'b0;
      main_run_loop_and_2_tmp_5 <= 1'b0;
      main_run_loop_main_run_loop_nor_1_itm_3 <= 1'b0;
      main_run_loop_and_7_itm_5 <= 1'b0;
      main_run_loop_and_1_tmp_5 <= 1'b0;
      main_run_loop_and_tmp_5 <= 1'b0;
      operator_28_true_1_slc_operator_28_true_1_acc_20_svs_1 <= 1'b0;
      operator_28_true_operator_28_true_nor_mdf_sva_1 <= 1'b0;
      main_run_loop_slc_main_run_loop_i_11_0_1_itm_4 <= 1'b0;
      outer_multiply_loop_asn_itm_3 <= 1'b0;
      catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      main_run_loop_slc_main_run_loop_i_11_0_1_itm_3 <= 1'b0;
      outer_multiply_loop_asn_itm_2 <= 1'b0;
      main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_4 <= 11'b00000000000;
      main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_4 <= 1'b0;
      main_run_loop_and_10_itm_4 <= 1'b0;
      main_run_loop_main_run_loop_nor_3_itm_1 <= 1'b0;
      main_run_loop_if_1_asn_10_itm_3 <= 32'b00000000000000000000000000000000;
      main_run_loop_and_7_itm_4 <= 1'b0;
      main_run_loop_main_run_loop_nor_1_itm_2 <= 1'b0;
      main_run_loop_and_3_tmp_4 <= 1'b0;
      main_run_loop_and_2_tmp_4 <= 1'b0;
      main_run_loop_and_1_tmp_4 <= 1'b0;
      main_run_loop_and_tmp_4 <= 1'b0;
      exit_main_run_loop_sva_1_4 <= 1'b0;
      main_run_loop_and_3_tmp_3 <= 1'b0;
      inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_3
          <= 1'b0;
      operator_28_true_2_slc_operator_28_true_2_acc_27_svs_3 <= 1'b0;
      outer_multiply_loop_asn_3_itm_3 <= 1'b0;
      main_run_loop_slc_main_run_loop_i_11_0_1_itm_2 <= 1'b0;
      main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_3 <= 11'b00000000000;
      main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_3 <= 1'b0;
      main_run_loop_and_10_itm_3 <= 1'b0;
      main_run_loop_if_1_asn_10_itm_2 <= 32'b00000000000000000000000000000000;
      main_run_loop_and_7_itm_3 <= 1'b0;
      main_run_loop_main_run_loop_nor_1_itm_1 <= 1'b0;
      main_run_loop_and_2_tmp_3 <= 1'b0;
      main_run_loop_and_1_tmp_3 <= 1'b0;
      main_run_loop_and_tmp_3 <= 1'b0;
      main_run_loop_and_1_tmp_2 <= 1'b0;
      main_run_loop_slc_main_run_loop_i_11_0_1_itm_1 <= 1'b0;
      main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_2 <= 11'b00000000000;
      main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_2 <= 1'b0;
      main_run_loop_and_10_itm_2 <= 1'b0;
      main_run_loop_if_1_asn_10_itm_1 <= 32'b00000000000000000000000000000000;
      main_run_loop_and_7_itm_2 <= 1'b0;
      main_run_loop_and_3_tmp_2 <= 1'b0;
      main_run_loop_and_2_tmp_2 <= 1'b0;
      main_run_loop_and_tmp_2 <= 1'b0;
      operator_28_true_2_slc_operator_28_true_2_acc_27_svs_2 <= 1'b0;
      inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_2
          <= 1'b0;
      exit_main_run_loop_sva_1_3 <= 1'b0;
      main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_1 <= 11'b00000000000;
      main_run_loop_and_10_itm_1 <= 1'b0;
      main_run_loop_and_7_itm_1 <= 1'b0;
      main_run_loop_and_3_tmp_1_1 <= 1'b0;
      main_run_loop_and_2_tmp_1 <= 1'b0;
      main_run_loop_and_1_tmp_1_1 <= 1'b0;
      main_run_loop_and_tmp_1 <= 1'b0;
      exit_main_run_loop_sva_1_2 <= 1'b0;
      exit_main_run_loop_sva_1_1 <= 1'b0;
    end
    else if ( run_wen ) begin
      reg_image_channel_rsci_oswt_cse <= ~((~((fsm_output[1:0]!=2'b00))) | ((~((~(bias_sva_st_2
          & (~ (z_out_2[32])))) & bias_sva_st_1)) & (fsm_output[1])));
      reg_output_image_elements_triosy_obj_iswt0_cse <= and_dcpl_138 & (fsm_output[4]);
      reg_weight_memory_data_rsc_0_1_i_iswt0_cse <= and_370_rmff;
      reg_output_buffer_rsc_0_1_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse
          <= and_376_rmff;
      reg_output_buffer_rsc_0_0_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse
          <= and_382_rmff;
      reg_input_image_rsc_0_1_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <=
          and_388_rmff;
      reg_input_image_rsc_0_0_i_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <=
          and_394_rmff;
      reg_output_image_channel_rsci_iswt0_cse <= (fsm_output[4]) & bias_sva_st_1;
      reg_bias_memory_rsci_iswt0_cse <= and_402_rmff;
      inner_multiply_loop_1_qif_mul_cmp_1_b <= MUX_v_32_2_2(32'b00000000000000000000000000000000,
          weight_memory_data_rsc_0_1_i_q_d_mxwt, operator_28_true_1_slc_operator_28_true_1_acc_20_svs_1);
      inner_multiply_loop_1_qif_mul_cmp_1_a <= input_image_rsc_0_1_i_q_d_mxwt;
      inner_multiply_loop_1_qif_mul_cmp_b <= MUX_v_32_2_2(32'b00000000000000000000000000000000,
          weight_memory_data_rsc_0_0_i_q_d_mxwt, operator_28_true_1_slc_operator_28_true_1_acc_20_svs_1);
      inner_multiply_loop_1_qif_mul_cmp_a <= input_image_rsc_0_0_i_q_d_mxwt;
      bias_sva_st_1 <= (bias_mux_nl & (~ or_dcpl_51)) | (fsm_output[0]);
      bias_sva_st_2 <= bias_mux_2_nl | (~((fsm_output[2:1]!=2'b00)));
      main_run_loop_i_sva_1_1_31_30 <= main_run_loop_acc_sdt[31:30];
      main_run_loop_stage_0_2 <= main_run_loop_stage_0 & (fsm_output[2]);
      main_run_loop_stage_0_3 <= main_run_loop_stage_0_2 & (fsm_output[2]);
      main_run_loop_stage_0_4 <= main_run_loop_stage_0_3 & (fsm_output[2]);
      main_run_loop_stage_0_5 <= main_run_loop_stage_0_4 & (fsm_output[2]);
      main_run_loop_stage_0_6 <= main_run_loop_stage_0_5 & (fsm_output[2]);
      main_run_loop_stage_0_7 <= main_run_loop_stage_0_6 & (fsm_output[2]);
      bias_index_10_0_sva <= MUX_v_11_2_2(11'b00000000000, bias_index_10_0_sva_4,
          (fsm_output[2]));
      reg_main_run_loop_slc_main_run_loop_i_11_0_1_itm_5_cse_1 <= main_run_loop_slc_main_run_loop_i_11_0_1_itm_4;
      reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse <= operator_28_true_operator_28_true_nor_mdf_sva_st_4;
      reg_main_run_loop_asn_40_itm_6_cse <= main_run_loop_asn_40_itm_5;
      operator_28_true_operator_28_true_nor_mdf_sva_st_4 <= operator_28_true_operator_28_true_nor_mdf_sva_st_3;
      main_run_loop_asn_40_itm_5 <= exit_main_run_loop_lpi_2_dfm_st_4;
      operator_28_true_operator_28_true_nor_mdf_sva_st_3 <= operator_28_true_operator_28_true_nor_mdf_sva_st_2;
      exit_main_run_loop_lpi_2_dfm_st_4 <= exit_main_run_loop_lpi_2_dfm_st_3;
      operator_28_true_operator_28_true_nor_mdf_sva_st_2 <= operator_28_true_operator_28_true_nor_mdf_sva_1;
      exit_main_run_loop_lpi_2_dfm_st_3 <= main_run_loop_asn_40_itm_2;
      operator_28_true_2_slc_operator_28_true_2_acc_27_svs_st_1 <= operator_28_true_2_acc_itm_27_1;
      inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_st_1
          <= inner_multiply_loop_1_operator_28_true_2_acc_itm_28_1;
      main_run_loop_asn_40_itm_2 <= exit_main_run_loop_lpi_2_dfm_1;
      exit_main_run_loop_lpi_2_dfm_1 <= (~ operator_28_true_acc_itm_32_1) & outer_multiply_loop_mux_6_nl;
      outer_multiply_loop_n_31_1_lpi_2_dfm_1_1 <= MUX_v_31_2_2((signext_31_1(~ operator_28_true_acc_itm_32_1)),
          outer_multiply_loop_acc_2_nl, not_tmp_97);
      reg_main_run_loop_if_1_slc_main_run_loop_i_11_0_7_itm_5_cse <= main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_4;
      main_run_loop_if_1_asn_10_itm_4 <= main_run_loop_if_1_asn_10_itm_3;
      reg_main_run_loop_i_slc_main_run_loop_i_11_1_2_itm_5_cse <= main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_4;
      main_run_loop_main_run_loop_nor_3_itm_2 <= main_run_loop_main_run_loop_nor_3_itm_1;
      main_run_loop_and_10_itm_5 <= main_run_loop_and_10_itm_4;
      main_run_loop_and_3_tmp_5 <= main_run_loop_and_3_tmp_4;
      main_run_loop_if_1_slc_main_run_loop_i_11_0_13_itm_5 <= main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5;
      main_run_loop_and_2_tmp_5 <= main_run_loop_and_2_tmp_4;
      main_run_loop_main_run_loop_nor_1_itm_3 <= main_run_loop_main_run_loop_nor_1_itm_2;
      main_run_loop_and_7_itm_5 <= main_run_loop_and_7_itm_4;
      main_run_loop_and_1_tmp_5 <= main_run_loop_and_1_tmp_4;
      main_run_loop_and_tmp_5 <= main_run_loop_and_tmp_4;
      operator_28_true_1_slc_operator_28_true_1_acc_20_svs_1 <= operator_28_true_1_acc_itm_20_1;
      operator_28_true_operator_28_true_nor_mdf_sva_1 <= MUX_s_1_2_2(operator_28_true_operator_28_true_nor_mdf_sva_1_1,
          operator_28_true_operator_28_true_nor_mdf_sva, exit_main_run_loop_lpi_2_dfm_1);
      main_run_loop_slc_main_run_loop_i_11_0_1_itm_4 <= main_run_loop_slc_main_run_loop_i_11_0_1_itm_3;
      outer_multiply_loop_asn_itm_3 <= outer_multiply_loop_asn_itm_2;
      catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1 <= nl_catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1[63:0];
      main_run_loop_slc_main_run_loop_i_11_0_1_itm_3 <= main_run_loop_slc_main_run_loop_i_11_0_1_itm_2;
      outer_multiply_loop_asn_itm_2 <= exitL_exit_outer_multiply_loop_sva;
      main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_4 <= main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_3;
      main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_4 <= main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_3;
      main_run_loop_and_10_itm_4 <= main_run_loop_and_10_itm_3;
      main_run_loop_main_run_loop_nor_3_itm_1 <= ~(main_run_loop_slc_main_run_loop_i_11_0_1_itm_3
          | main_run_loop_and_3_tmp_3);
      main_run_loop_if_1_asn_10_itm_3 <= main_run_loop_if_1_asn_10_itm_2;
      main_run_loop_and_7_itm_4 <= main_run_loop_and_7_itm_3;
      main_run_loop_main_run_loop_nor_1_itm_2 <= main_run_loop_main_run_loop_nor_1_itm_1;
      main_run_loop_and_3_tmp_4 <= main_run_loop_and_3_tmp_3;
      main_run_loop_and_2_tmp_4 <= main_run_loop_and_2_tmp_3;
      main_run_loop_and_1_tmp_4 <= main_run_loop_and_1_tmp_3;
      main_run_loop_and_tmp_4 <= main_run_loop_and_tmp_3;
      exit_main_run_loop_sva_1_4 <= exit_main_run_loop_sva_1_3;
      main_run_loop_and_3_tmp_3 <= main_run_loop_and_3_tmp_2;
      inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_3
          <= inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_2;
      operator_28_true_2_slc_operator_28_true_2_acc_27_svs_3 <= operator_28_true_2_slc_operator_28_true_2_acc_27_svs_2;
      outer_multiply_loop_asn_3_itm_3 <= outer_multiply_loop_asn_itm_3;
      main_run_loop_slc_main_run_loop_i_11_0_1_itm_2 <= main_run_loop_slc_main_run_loop_i_11_0_1_itm_1;
      main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_3 <= main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_2;
      main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_3 <= main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_2;
      main_run_loop_and_10_itm_3 <= main_run_loop_and_10_itm_2;
      main_run_loop_if_1_asn_10_itm_2 <= main_run_loop_if_1_asn_10_itm_1;
      main_run_loop_and_7_itm_3 <= main_run_loop_and_7_itm_2;
      main_run_loop_main_run_loop_nor_1_itm_1 <= ~(main_run_loop_slc_main_run_loop_i_11_0_1_itm_2
          | main_run_loop_and_1_tmp_2);
      main_run_loop_and_2_tmp_3 <= main_run_loop_and_2_tmp_2;
      main_run_loop_and_1_tmp_3 <= main_run_loop_and_1_tmp_2;
      main_run_loop_and_tmp_3 <= main_run_loop_and_tmp_2;
      main_run_loop_and_1_tmp_2 <= main_run_loop_and_1_tmp_1_1;
      main_run_loop_slc_main_run_loop_i_11_0_1_itm_1 <= image_vector_load_loop_index_sva_26_0[0];
      main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_2 <= main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_1;
      main_run_loop_if_1_slc_main_run_loop_i_11_0_3_itm_2 <= main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5;
      main_run_loop_and_10_itm_2 <= main_run_loop_and_10_itm_1;
      main_run_loop_if_1_asn_10_itm_1 <= bias_memory_rsci_q_d_mxwt;
      main_run_loop_and_7_itm_2 <= main_run_loop_and_7_itm_1;
      main_run_loop_and_3_tmp_2 <= main_run_loop_and_3_tmp_1_1;
      main_run_loop_and_2_tmp_2 <= main_run_loop_and_2_tmp_1;
      main_run_loop_and_tmp_2 <= main_run_loop_and_tmp_1;
      operator_28_true_2_slc_operator_28_true_2_acc_27_svs_2 <= operator_28_true_2_slc_operator_28_true_2_acc_27_svs_st_1;
      inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_2
          <= inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_st_1;
      exit_main_run_loop_sva_1_3 <= exit_main_run_loop_sva_1_2;
      main_run_loop_i_slc_main_run_loop_i_11_1_3_itm_1 <= image_vector_load_loop_index_sva_26_0[11:1];
      main_run_loop_and_10_itm_1 <= (image_vector_load_loop_index_sva_26_0[0]) &
          (~ main_run_loop_and_3_tmp_1);
      main_run_loop_and_7_itm_1 <= (image_vector_load_loop_index_sva_26_0[0]) & (~
          main_run_loop_and_1_tmp_1);
      main_run_loop_and_3_tmp_1_1 <= main_run_loop_and_3_tmp_1;
      main_run_loop_and_2_tmp_1 <= (~ (image_vector_load_loop_index_sva_26_0[0]))
          & bias_sva;
      main_run_loop_and_1_tmp_1_1 <= main_run_loop_and_1_tmp_1;
      main_run_loop_and_tmp_1 <= (~ (image_vector_load_loop_index_sva_26_0[0])) &
          relu_sva;
      exit_main_run_loop_sva_1_2 <= exit_main_run_loop_sva_1_1;
      exit_main_run_loop_sva_1_1 <= ~ operator_28_true_acc_itm_32_1;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      bias_sva <= 1'b0;
    end
    else if ( run_wen & ((~((~ bias_sva) | (z_out_2[32]))) | (fsm_output[0]) | or_tmp_57
        | or_dcpl_51) ) begin
      bias_sva <= (bias_mux_3_nl & (~ (fsm_output[4]))) | or_dcpl_51;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_image_elements_sva <= 28'b0000000000000000000000000000;
      input_vector_length_sva <= 28'b0000000000000000000000000000;
      relu_sva <= 1'b0;
    end
    else if ( output_image_elements_and_cse ) begin
      output_image_elements_sva <= output_image_elements_rsci_idat;
      input_vector_length_sva <= input_vector_length_rsci_idat;
      relu_sva <= relu_rsci_idat;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_image_channel_rsci_idat <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((bias_sva_st_1 & main_run_loop_asn_dfmergedata_1_0_lpi_2
        & (fsm_output[4])) | output_image_channel_rsci_idat_mx0c1) ) begin
      output_image_channel_rsci_idat <= MUX_v_32_2_2(output_buffer_rsc_0_1_i_q_d_mxwt,
          output_buffer_rsc_0_0_i_q_d_mxwt, output_image_channel_rsci_idat_mx0c1);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_29_27 <= 3'b000;
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_26_0 <= 27'b000000000000000000000000000;
      main_run_loop_asn_dfmergedata_30_1_lpi_2_29_27 <= 3'b000;
      main_run_loop_asn_dfmergedata_30_1_lpi_2_26_0 <= 27'b000000000000000000000000000;
      main_run_loop_conc_3_dfmergedata_30_1_lpi_2_29_27 <= 3'b000;
      main_run_loop_conc_3_dfmergedata_30_1_lpi_2_26_0 <= 27'b000000000000000000000000000;
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_29_27 <= 3'b000;
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_26_0 <= 27'b000000000000000000000000000;
    end
    else if ( main_run_loop_and_cse ) begin
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_29_27 <= MUX_v_3_2_2(main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_29_27,
          main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_29_27, fsm_output[2]);
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_26_0 <= MUX_v_27_2_2(main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_26_0,
          main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_26_0, fsm_output[2]);
      main_run_loop_asn_dfmergedata_30_1_lpi_2_29_27 <= MUX_v_3_2_2(main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_29_27,
          main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_29_27, fsm_output[2]);
      main_run_loop_asn_dfmergedata_30_1_lpi_2_26_0 <= MUX_v_27_2_2(main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_26_0,
          main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_26_0, fsm_output[2]);
      main_run_loop_conc_3_dfmergedata_30_1_lpi_2_29_27 <= MUX_v_3_2_2(main_run_loop_i_sva_1_1_29_27,
          main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_29_27, fsm_output[2]);
      main_run_loop_conc_3_dfmergedata_30_1_lpi_2_26_0 <= MUX_v_27_2_2(main_run_loop_i_sva_1_1_26_0,
          main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_26_0, fsm_output[2]);
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_29_27 <= MUX_v_3_2_2(main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_29_27,
          main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_29_27, fsm_output[2]);
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_26_0 <= MUX_v_27_2_2(main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_26_0,
          main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_26_0, fsm_output[2]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_asn_dfmergedata_0_lpi_2 <= 1'b0;
      main_run_loop_asn_dfmergedata_1_0_lpi_2 <= 1'b0;
    end
    else if ( main_run_loop_and_42_cse ) begin
      main_run_loop_asn_dfmergedata_0_lpi_2 <= MUX1HOT_s_1_3_2(main_run_loop_asn_dfmergedata_0_lpi_2_dfm_5,
          output_buffer_rsc_0_0_i_d_d_run_0_mx1, main_run_loop_asn_dfmergedata_1_31_lpi_2,
          {(fsm_output[0]) , (fsm_output[2]) , (fsm_output[4])});
      main_run_loop_asn_dfmergedata_1_0_lpi_2 <= MUX1HOT_s_1_3_2(main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_5,
          output_buffer_rsc_0_1_i_d_d_run_0_mx1, (image_vector_load_loop_index_sva_26_0[0]),
          {(fsm_output[0]) , (fsm_output[2]) , (fsm_output[4])});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_asn_dfmergedata_31_lpi_2 <= 1'b0;
    end
    else if ( run_wen & ((~(or_dcpl_54 | or_tmp_67)) | (fsm_output[0])) & (reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse
        | (~ (fsm_output[2]))) ) begin
      main_run_loop_asn_dfmergedata_31_lpi_2 <= MUX_s_1_2_2(main_run_loop_asn_dfmergedata_31_lpi_2_dfm_5,
          output_buffer_rsc_0_0_i_d_d_run_31_mx1, fsm_output[2]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_asn_dfmergedata_1_31_lpi_2 <= 1'b0;
    end
    else if ( run_wen & ((~(or_dcpl_54 | or_dcpl_56)) | (fsm_output[0]) | main_run_loop_asn_dfmergedata_1_31_lpi_2_mx0c2)
        & (reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse | (fsm_output[0])
        | main_run_loop_asn_dfmergedata_1_31_lpi_2_mx0c2) ) begin
      main_run_loop_asn_dfmergedata_1_31_lpi_2 <= MUX1HOT_s_1_3_2(main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_5,
          output_buffer_rsc_0_1_i_d_d_run_31_mx1, image_vector_load_loop_index_image_vector_load_loop_index_and_nl,
          {(fsm_output[0]) , (fsm_output[2]) , main_run_loop_asn_dfmergedata_1_31_lpi_2_mx0c2});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_conc_3_dfmergedata_0_lpi_2 <= 1'b0;
      main_run_loop_conc_3_dfmergedata_31_lpi_2 <= 1'b0;
      main_run_loop_conc_1_dfmergedata_0_lpi_2 <= 1'b0;
      main_run_loop_conc_1_dfmergedata_31_lpi_2 <= 1'b0;
      catapult_dense_16000_2500_compute_dense_accumulator_lpi_2 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
    end
    else if ( main_run_loop_and_18_cse ) begin
      main_run_loop_conc_3_dfmergedata_0_lpi_2 <= MUX_s_1_2_2(main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5,
          main_run_loop_conc_3_dfmergedata_0_lpi_2_mx1, fsm_output[2]);
      main_run_loop_conc_3_dfmergedata_31_lpi_2 <= MUX_s_1_2_2(main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5,
          main_run_loop_conc_3_dfmergedata_31_lpi_2_mx1, fsm_output[2]);
      main_run_loop_conc_1_dfmergedata_0_lpi_2 <= MUX_s_1_2_2(main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5,
          main_run_loop_conc_1_dfmergedata_0_lpi_2_mx1, fsm_output[2]);
      main_run_loop_conc_1_dfmergedata_31_lpi_2 <= MUX_s_1_2_2(main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5,
          main_run_loop_conc_1_dfmergedata_31_lpi_2_mx1, fsm_output[2]);
      catapult_dense_16000_2500_compute_dense_accumulator_lpi_2 <= MUX_v_64_2_2(catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1,
          catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_mx1, fsm_output[2]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      image_vector_load_loop_index_sva_31_30 <= 2'b00;
      image_vector_load_loop_index_sva_29_27 <= 3'b000;
      image_vector_load_loop_index_sva_26_0 <= 27'b000000000000000000000000000;
    end
    else if ( image_vector_load_loop_index_and_ssc ) begin
      image_vector_load_loop_index_sva_31_30 <= MUX_v_2_2_2(2'b00, mux_nl, not_285_nl);
      image_vector_load_loop_index_sva_29_27 <= MUX_v_3_2_2(3'b000, mux_62_nl, not_289_nl);
      image_vector_load_loop_index_sva_26_0 <= MUX_v_27_2_2(27'b000000000000000000000000000,
          mux_63_nl, not_286_nl);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_dense_16000_2500_load_image_end_sva <= 28'b0000000000000000000000000000;
    end
    else if ( run_wen & ((~(catapult_dense_16000_2500_load_image_end_mux_nl | (fsm_output[4])))
        | (fsm_output[0]) | (fsm_output[3])) ) begin
      catapult_dense_16000_2500_load_image_end_sva <= MUX_v_28_2_2(28'b0000000000000000000000000000,
          catapult_dense_16000_2500_load_image_end_catapult_dense_16000_2500_load_image_end_mux_nl,
          not_262_nl);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_29_27 <= 3'b000;
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_26_0 <= 27'b000000000000000000000000000;
      main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_29_27 <= 3'b000;
      main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_26_0 <= 27'b000000000000000000000000000;
    end
    else if ( main_run_loop_and_22_cse ) begin
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_29_27 <= MUX1HOT_v_3_3_2(main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_29_27,
          main_run_loop_asn_dfmergedata_1_30_1_lpi_2_29_27, (main_run_loop_conc_cse_30_1_sva_mx1w2[29:27]),
          {and_dcpl_163 , and_dcpl_165 , main_run_loop_stage_0_6});
      main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_5_26_0 <= MUX1HOT_v_27_3_2(main_run_loop_asn_dfmergedata_1_30_1_lpi_2_dfm_6_26_0,
          main_run_loop_asn_dfmergedata_1_30_1_lpi_2_26_0, (main_run_loop_conc_cse_30_1_sva_mx1w2[26:0]),
          {and_dcpl_163 , and_dcpl_165 , main_run_loop_stage_0_6});
      main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_29_27 <= MUX1HOT_v_3_3_2(main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_29_27,
          main_run_loop_asn_dfmergedata_30_1_lpi_2_29_27, main_run_loop_if_main_run_loop_if_and_1_nl,
          {and_dcpl_163 , and_dcpl_165 , main_run_loop_stage_0_6});
      main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_5_26_0 <= MUX1HOT_v_27_3_2(main_run_loop_asn_dfmergedata_30_1_lpi_2_dfm_6_26_0,
          main_run_loop_asn_dfmergedata_30_1_lpi_2_26_0, main_run_loop_if_main_run_loop_if_and_3_nl,
          {and_dcpl_163 , and_dcpl_165 , main_run_loop_stage_0_6});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_5 <= 1'b0;
      main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_5 <= 1'b0;
      main_run_loop_asn_dfmergedata_0_lpi_2_dfm_5 <= 1'b0;
      main_run_loop_asn_dfmergedata_31_lpi_2_dfm_5 <= 1'b0;
    end
    else if ( main_run_loop_and_23_cse ) begin
      main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_5 <= MUX1HOT_s_1_5_2(main_run_loop_asn_dfmergedata_1_0_lpi_2,
          output_buffer_rsc_0_1_i_d_d_run_0_mx1, main_run_loop_if_qr_0_lpi_2_dfm_1,
          main_run_loop_conc_cse_0_sva_1, main_run_loop_conc_1_dfmergedata_0_lpi_2_mx1,
          {main_run_loop_or_21_cse , main_run_loop_and_45_cse , main_run_loop_and_46_cse
          , main_run_loop_and_68_cse , main_run_loop_and_69_cse});
      main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_5 <= MUX1HOT_s_1_5_2(main_run_loop_asn_dfmergedata_1_31_lpi_2,
          output_buffer_rsc_0_1_i_d_d_run_31_mx1, main_run_loop_if_qr_31_lpi_2_dfm_1,
          main_run_loop_conc_cse_31_sva_1, main_run_loop_conc_1_dfmergedata_31_lpi_2_mx1,
          {main_run_loop_or_21_cse , main_run_loop_and_45_cse , main_run_loop_and_46_cse
          , main_run_loop_and_68_cse , main_run_loop_and_69_cse});
      main_run_loop_asn_dfmergedata_0_lpi_2_dfm_5 <= MUX1HOT_s_1_5_2(main_run_loop_asn_dfmergedata_0_lpi_2,
          output_buffer_rsc_0_0_i_d_d_run_0_mx1, main_run_loop_conc_3_dfmergedata_0_lpi_2_mx1,
          main_run_loop_conc_cse_0_sva_1, main_run_loop_if_qr_0_lpi_2_dfm_1, {main_run_loop_or_21_cse
          , main_run_loop_and_45_cse , main_run_loop_and_72_cse , main_run_loop_and_73_cse
          , main_run_loop_and_47_cse});
      main_run_loop_asn_dfmergedata_31_lpi_2_dfm_5 <= MUX1HOT_s_1_5_2(main_run_loop_asn_dfmergedata_31_lpi_2,
          output_buffer_rsc_0_0_i_d_d_run_31_mx1, main_run_loop_conc_3_dfmergedata_31_lpi_2_mx1,
          main_run_loop_conc_cse_31_sva_1, main_run_loop_if_qr_31_lpi_2_dfm_1, {main_run_loop_or_21_cse
          , main_run_loop_and_45_cse , main_run_loop_and_72_cse , main_run_loop_and_73_cse
          , main_run_loop_and_47_cse});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5 <= 1'b0;
    end
    else if ( run_wen & (main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5_mx2c0 | main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5_mx2c1
        | main_run_loop_stage_0_4) & (fsm_output[2]) ) begin
      main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5 <= MUX1HOT_s_1_3_2(main_run_loop_conc_3_dfmergedata_0_lpi_2,
          main_run_loop_asn_dfmergedata_0_lpi_2_dfm_5, main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5,
          {main_run_loop_or_29_nl , main_run_loop_and_61_nl , main_run_loop_stage_0_4});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5 <= 1'b0;
    end
    else if ( run_wen & (main_run_loop_stage_0_5 | main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5_mx2c0
        | main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5_mx2c1) & (fsm_output[2])
        ) begin
      main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5 <= MUX1HOT_s_1_3_2(main_run_loop_conc_3_dfmergedata_31_lpi_2,
          main_run_loop_asn_dfmergedata_31_lpi_2_dfm_5, main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5,
          {main_run_loop_or_30_nl , main_run_loop_and_63_nl , main_run_loop_stage_0_5});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_29_27 <= 3'b000;
    end
    else if ( main_run_loop_and_30_ssc ) begin
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_29_27 <= MUX_v_3_2_2(main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_29_27,
          main_run_loop_conc_1_dfmergedata_30_1_lpi_2_29_27, main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_mx1c1);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_26_0 <= 27'b000000000000000000000000000;
    end
    else if ( main_run_loop_and_30_ssc & (~(not_tmp_97 & main_run_loop_stage_0))
        ) begin
      main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_26_0 <= MUX1HOT_v_27_3_2(main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_6_26_0,
          main_run_loop_conc_1_dfmergedata_30_1_lpi_2_26_0, (z_out_2[26:0]), {main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_mx1c0
          , main_run_loop_conc_1_dfmergedata_30_1_lpi_2_dfm_5_mx1c1 , outer_multiply_loop_and_7_nl});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5 <= 1'b0;
    end
    else if ( run_wen & (main_run_loop_stage_0_2 | main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5_mx2c0
        | main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5_mx2c1) & (fsm_output[2])
        ) begin
      main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5 <= MUX1HOT_s_1_3_2(main_run_loop_conc_1_dfmergedata_0_lpi_2,
          main_run_loop_asn_dfmergedata_1_0_lpi_2_dfm_5, (image_vector_load_loop_index_sva_26_0[0]),
          {main_run_loop_or_33_nl , main_run_loop_and_65_nl , main_run_loop_stage_0_2});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5 <= 1'b0;
    end
    else if ( run_wen & (main_run_loop_stage_0_3 | main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5_mx2c0
        | main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5_mx2c1) & (fsm_output[2])
        ) begin
      main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5 <= MUX1HOT_s_1_3_2(main_run_loop_conc_1_dfmergedata_31_lpi_2,
          main_run_loop_asn_dfmergedata_1_31_lpi_2_dfm_5, main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5,
          {main_run_loop_or_35_nl , main_run_loop_and_67_nl , main_run_loop_stage_0_3});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      operator_28_true_operator_28_true_nor_mdf_sva <= 1'b0;
    end
    else if ( run_wen & (~(or_dcpl_27 | (~ (fsm_output[2])))) ) begin
      operator_28_true_operator_28_true_nor_mdf_sva <= operator_28_true_operator_28_true_nor_mdf_sva_1_1;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_i_sva_1_1_29_27 <= 3'b000;
      main_run_loop_i_sva_1_1_26_0 <= 27'b000000000000000000000000000;
    end
    else if ( main_run_loop_i_and_2_ssc ) begin
      main_run_loop_i_sva_1_1_29_27 <= MUX1HOT_v_3_3_2(main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_29_27,
          main_run_loop_conc_3_dfmergedata_30_1_lpi_2_29_27, (main_run_loop_acc_sdt[29:27]),
          {main_run_loop_i_main_run_loop_i_nor_1_ssc , main_run_loop_i_and_4_ssc
          , main_run_loop_i_and_1_rgt});
      main_run_loop_i_sva_1_1_26_0 <= MUX1HOT_v_27_3_2(main_run_loop_conc_3_dfmergedata_30_1_lpi_2_dfm_6_26_0,
          main_run_loop_conc_3_dfmergedata_30_1_lpi_2_26_0, (main_run_loop_acc_sdt[26:0]),
          {main_run_loop_i_main_run_loop_i_nor_1_ssc , main_run_loop_i_and_4_ssc
          , main_run_loop_i_and_1_rgt});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_run_loop_stage_0 <= 1'b0;
    end
    else if ( run_wen & (~((not_tmp_97 | operator_28_true_acc_itm_32_1 | (~ main_run_loop_stage_0))
        & (fsm_output[2]))) ) begin
      main_run_loop_stage_0 <= ~ (fsm_output[2]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      exitL_exit_outer_multiply_loop_sva <= 1'b0;
    end
    else if ( run_wen & (~(or_dcpl_27 & (fsm_output[2]))) ) begin
      exitL_exit_outer_multiply_loop_sva <= operator_28_true_operator_28_true_nor_mdf_sva_1_1
          | (~ (fsm_output[2]));
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
    end
    else if ( run_wen & (catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c0
        | catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c1
        | catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c2)
        & (fsm_output[2]) ) begin
      catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1 <= MUX1HOT_v_64_3_2(catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_3,
          catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1, catapult_dense_16000_2500_compute_dense_accumulator_lpi_2,
          {catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c0
          , outer_multiply_loop_or_nl , outer_multiply_loop_and_10_nl});
    end
  end
  assign or_237_nl = (fsm_output[2]) | (fsm_output[4]);
  assign bias_mux_nl = MUX_s_1_2_2(bias_sva_st_1_mx0w1, bias_sva, or_237_nl);
  assign bias_mux_2_nl = MUX_s_1_2_2(bias_sva_st_1_mx0w1, bias_sva_st_1, fsm_output[2]);
  assign outer_multiply_loop_mux_6_nl = MUX_s_1_2_2(operator_28_true_operator_28_true_nor_mdf_sva_1_1,
      exitL_exit_outer_multiply_loop_sva, or_dcpl_27);
  assign nl_outer_multiply_loop_acc_2_nl = outer_multiply_loop_n_31_1_lpi_2_dfm_1_1
      + 31'b0000000000000000000000000000001;
  assign outer_multiply_loop_acc_2_nl = nl_outer_multiply_loop_acc_2_nl[30:0];
  assign outer_multiply_loop_mux_9_nl = MUX_v_64_2_2(catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_mx1,
      catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_3, outer_multiply_loop_asn_3_itm_3);
  assign inner_multiply_loop_inner_multiply_loop_and_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      inner_multiply_loop_1_qif_mul_cmp_z_oreg, inner_multiply_loop_1_operator_28_true_2_slc_operator_28_true_2_acc_28_svs_3);
  assign inner_multiply_loop_inner_multiply_loop_and_1_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      inner_multiply_loop_1_qif_mul_cmp_1_z_oreg, operator_28_true_2_slc_operator_28_true_2_acc_27_svs_3);
  assign nl_catapult_dense_16000_2500_compute_dense_accumulator_sva_1_1  = outer_multiply_loop_mux_9_nl
      + inner_multiply_loop_inner_multiply_loop_and_nl + inner_multiply_loop_inner_multiply_loop_and_1_nl;
  assign bias_mux_3_nl = MUX_s_1_2_2(bias_rsci_idat, bias_sva, or_tmp_57);
  assign image_vector_load_loop_index_image_vector_load_loop_index_and_nl = (z_out_1[0])
      & (fsm_output[4]);
  assign image_vector_load_loop_index_image_vector_load_loop_index_or_nl = MUX_v_2_2_2(image_vector_load_loop_index_sva_mx2_31_30,
      2'b11, (fsm_output[1]));
  assign mux_nl = MUX_v_2_2_2(image_vector_load_loop_index_image_vector_load_loop_index_or_nl,
      (z_out_1[31:30]), or_265_cse);
  assign not_285_nl = ~ or_266_cse;
  assign image_vector_load_loop_index_image_vector_load_loop_index_or_1_nl = MUX_v_3_2_2(image_vector_load_loop_index_sva_mx2_29_27,
      3'b111, (fsm_output[1]));
  assign mux_62_nl = MUX_v_3_2_2(image_vector_load_loop_index_image_vector_load_loop_index_or_1_nl,
      (z_out_1[29:27]), or_265_cse);
  assign not_289_nl = ~ or_266_cse;
  assign image_vector_load_loop_index_image_vector_load_loop_index_or_2_nl = MUX_v_27_2_2(image_vector_load_loop_index_sva_mx2_26_0,
      27'b111111111111111111111111111, (fsm_output[1]));
  assign mux_63_nl = MUX_v_27_2_2(image_vector_load_loop_index_image_vector_load_loop_index_or_2_nl,
      (z_out_1[26:0]), or_265_cse);
  assign not_286_nl = ~ or_266_cse;
  assign catapult_dense_16000_2500_load_image_end_mux_2_nl = MUX_v_28_2_2(input_vector_length_rsci_idat,
      output_image_elements_sva, fsm_output[3]);
  assign nl_catapult_dense_16000_2500_load_image_end_acc_nl = catapult_dense_16000_2500_load_image_end_mux_2_nl
      + 28'b1111111111111111111111111111;
  assign catapult_dense_16000_2500_load_image_end_acc_nl = nl_catapult_dense_16000_2500_load_image_end_acc_nl[27:0];
  assign catapult_dense_16000_2500_load_image_end_catapult_dense_16000_2500_load_image_end_mux_nl
      = MUX_v_28_2_2(catapult_dense_16000_2500_load_image_end_acc_nl, (z_out_1[27:0]),
      fsm_output[2]);
  assign not_262_nl = ~ (fsm_output[1]);
  assign catapult_dense_16000_2500_load_image_end_or_1_nl = or_dcpl_27 | (~ operator_28_true_1_acc_itm_20_1);
  assign catapult_dense_16000_2500_load_image_end_mux_nl = MUX_s_1_2_2(bias_sva_st_1,
      catapult_dense_16000_2500_load_image_end_or_1_nl, fsm_output[2]);
  assign main_run_loop_if_main_run_loop_if_and_1_nl = MUX_v_3_2_2(3'b000, (main_run_loop_conc_cse_30_1_sva_mx1w2[29:27]),
      main_run_loop_if_acc_itm_32_1);
  assign main_run_loop_if_main_run_loop_if_and_3_nl = MUX_v_27_2_2(27'b000000000000000000000000000,
      (main_run_loop_conc_cse_30_1_sva_mx1w2[26:0]), main_run_loop_if_acc_itm_32_1);
  assign main_run_loop_or_29_nl = main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5_mx2c0
      | ((~ reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse) & main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5_mx2c1);
  assign main_run_loop_and_61_nl = reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse
      & main_run_loop_conc_3_dfmergedata_0_lpi_2_dfm_5_mx2c1;
  assign main_run_loop_or_30_nl = main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5_mx2c0
      | ((~ reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse) & main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5_mx2c1);
  assign main_run_loop_and_63_nl = reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse
      & main_run_loop_conc_3_dfmergedata_31_lpi_2_dfm_5_mx2c1;
  assign outer_multiply_loop_and_7_nl = (~ not_tmp_97) & main_run_loop_stage_0;
  assign main_run_loop_or_33_nl = main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5_mx2c0
      | ((~ reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse) & main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5_mx2c1);
  assign main_run_loop_and_65_nl = reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse
      & main_run_loop_conc_1_dfmergedata_0_lpi_2_dfm_5_mx2c1;
  assign main_run_loop_or_35_nl = main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5_mx2c0
      | ((~ reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse) & main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5_mx2c1);
  assign main_run_loop_and_67_nl = reg_operator_28_true_operator_28_true_nor_mdf_sva_st_5_cse
      & main_run_loop_conc_1_dfmergedata_31_lpi_2_dfm_5_mx2c1;
  assign outer_multiply_loop_or_nl = ((~ or_269_tmp) & catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c1)
      | catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c2;
  assign outer_multiply_loop_and_10_nl = or_269_tmp & catapult_dense_16000_2500_compute_dense_accumulator_lpi_2_dfm_1_1_mx1c1;
  assign outer_multiply_loop_if_mux_3_nl = MUX_v_2_2_2((signext_2_1(catapult_dense_16000_2500_load_image_end_sva[27])),
      image_vector_load_loop_index_sva_31_30, or_260_cse);
  assign outer_multiply_loop_if_mux_4_nl = MUX_v_30_2_2(({{2{catapult_dense_16000_2500_load_image_end_sva[27]}},
      catapult_dense_16000_2500_load_image_end_sva}), ({image_vector_load_loop_index_sva_29_27
      , image_vector_load_loop_index_sva_26_0}), or_260_cse);
  assign nl_z_out_1 = ({outer_multiply_loop_if_mux_3_nl , outer_multiply_loop_if_mux_4_nl})
      + 32'b00000000000000000000000000000001;
  assign z_out_1 = nl_z_out_1[31:0];
  assign catapult_dense_16000_2500_compute_dense_end_mux_4_nl = MUX_v_2_2_2((signext_2_1(input_vector_length_sva[27])),
      image_vector_load_loop_index_sva_31_30, or_260_cse);
  assign catapult_dense_16000_2500_compute_dense_end_mux_5_nl = MUX_v_30_2_2((signext_30_27(input_vector_length_sva[27:1])),
      ({image_vector_load_loop_index_sva_29_27 , image_vector_load_loop_index_sva_26_0}),
      or_260_cse);
  assign catapult_dense_16000_2500_compute_dense_end_or_1_nl = (~ (fsm_output[2]))
      | or_260_cse;
  assign catapult_dense_16000_2500_compute_dense_end_catapult_dense_16000_2500_compute_dense_end_nand_1_nl
      = ~(MUX_v_28_2_2(28'b0000000000000000000000000000, catapult_dense_16000_2500_load_image_end_sva,
      or_260_cse));
  assign nl_acc_2_nl = conv_s2u_33_34({catapult_dense_16000_2500_compute_dense_end_mux_4_nl
      , catapult_dense_16000_2500_compute_dense_end_mux_5_nl , catapult_dense_16000_2500_compute_dense_end_or_1_nl})
      + conv_s2u_29_34({catapult_dense_16000_2500_compute_dense_end_catapult_dense_16000_2500_compute_dense_end_nand_1_nl
      , 1'b1});
  assign acc_2_nl = nl_acc_2_nl[33:0];
  assign z_out_2 = readslicef_34_33_1(acc_2_nl);

  function automatic  MUX1HOT_s_1_1_2;
    input  input_0;
    input  sel;
    reg  result;
  begin
    result = input_0 & sel;
    MUX1HOT_s_1_1_2 = result;
  end
  endfunction


  function automatic  MUX1HOT_s_1_3_2;
    input  input_2;
    input  input_1;
    input  input_0;
    input [2:0] sel;
    reg  result;
  begin
    result = input_0 & sel[0];
    result = result | (input_1 & sel[1]);
    result = result | (input_2 & sel[2]);
    MUX1HOT_s_1_3_2 = result;
  end
  endfunction


  function automatic  MUX1HOT_s_1_5_2;
    input  input_4;
    input  input_3;
    input  input_2;
    input  input_1;
    input  input_0;
    input [4:0] sel;
    reg  result;
  begin
    result = input_0 & sel[0];
    result = result | (input_1 & sel[1]);
    result = result | (input_2 & sel[2]);
    result = result | (input_3 & sel[3]);
    result = result | (input_4 & sel[4]);
    MUX1HOT_s_1_5_2 = result;
  end
  endfunction


  function automatic [26:0] MUX1HOT_v_27_3_2;
    input [26:0] input_2;
    input [26:0] input_1;
    input [26:0] input_0;
    input [2:0] sel;
    reg [26:0] result;
  begin
    result = input_0 & {27{sel[0]}};
    result = result | (input_1 & {27{sel[1]}});
    result = result | (input_2 & {27{sel[2]}});
    MUX1HOT_v_27_3_2 = result;
  end
  endfunction


  function automatic [2:0] MUX1HOT_v_3_3_2;
    input [2:0] input_2;
    input [2:0] input_1;
    input [2:0] input_0;
    input [2:0] sel;
    reg [2:0] result;
  begin
    result = input_0 & {3{sel[0]}};
    result = result | (input_1 & {3{sel[1]}});
    result = result | (input_2 & {3{sel[2]}});
    MUX1HOT_v_3_3_2 = result;
  end
  endfunction


  function automatic [63:0] MUX1HOT_v_64_3_2;
    input [63:0] input_2;
    input [63:0] input_1;
    input [63:0] input_0;
    input [2:0] sel;
    reg [63:0] result;
  begin
    result = input_0 & {64{sel[0]}};
    result = result | (input_1 & {64{sel[1]}});
    result = result | (input_2 & {64{sel[2]}});
    MUX1HOT_v_64_3_2 = result;
  end
  endfunction


  function automatic  MUX_s_1_2_2;
    input  input_0;
    input  input_1;
    input  sel;
    reg  result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_s_1_2_2 = result;
  end
  endfunction


  function automatic [10:0] MUX_v_11_2_2;
    input [10:0] input_0;
    input [10:0] input_1;
    input  sel;
    reg [10:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_11_2_2 = result;
  end
  endfunction


  function automatic [26:0] MUX_v_27_2_2;
    input [26:0] input_0;
    input [26:0] input_1;
    input  sel;
    reg [26:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_27_2_2 = result;
  end
  endfunction


  function automatic [27:0] MUX_v_28_2_2;
    input [27:0] input_0;
    input [27:0] input_1;
    input  sel;
    reg [27:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_28_2_2 = result;
  end
  endfunction


  function automatic [1:0] MUX_v_2_2_2;
    input [1:0] input_0;
    input [1:0] input_1;
    input  sel;
    reg [1:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_2_2_2 = result;
  end
  endfunction


  function automatic [29:0] MUX_v_30_2_2;
    input [29:0] input_0;
    input [29:0] input_1;
    input  sel;
    reg [29:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_30_2_2 = result;
  end
  endfunction


  function automatic [30:0] MUX_v_31_2_2;
    input [30:0] input_0;
    input [30:0] input_1;
    input  sel;
    reg [30:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_31_2_2 = result;
  end
  endfunction


  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction


  function automatic [2:0] MUX_v_3_2_2;
    input [2:0] input_0;
    input [2:0] input_1;
    input  sel;
    reg [2:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_3_2_2 = result;
  end
  endfunction


  function automatic [63:0] MUX_v_64_2_2;
    input [63:0] input_0;
    input [63:0] input_1;
    input  sel;
    reg [63:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_64_2_2 = result;
  end
  endfunction


  function automatic [0:0] readslicef_21_1_20;
    input [20:0] vector;
    reg [20:0] tmp;
  begin
    tmp = vector >> 20;
    readslicef_21_1_20 = tmp[0:0];
  end
  endfunction


  function automatic [0:0] readslicef_28_1_27;
    input [27:0] vector;
    reg [27:0] tmp;
  begin
    tmp = vector >> 27;
    readslicef_28_1_27 = tmp[0:0];
  end
  endfunction


  function automatic [0:0] readslicef_29_1_28;
    input [28:0] vector;
    reg [28:0] tmp;
  begin
    tmp = vector >> 28;
    readslicef_29_1_28 = tmp[0:0];
  end
  endfunction


  function automatic [0:0] readslicef_33_1_32;
    input [32:0] vector;
    reg [32:0] tmp;
  begin
    tmp = vector >> 32;
    readslicef_33_1_32 = tmp[0:0];
  end
  endfunction


  function automatic [32:0] readslicef_34_33_1;
    input [33:0] vector;
    reg [33:0] tmp;
  begin
    tmp = vector >> 1;
    readslicef_34_33_1 = tmp[32:0];
  end
  endfunction


  function automatic [1:0] signext_2_1;
    input  vector;
  begin
    signext_2_1= {{1{vector}}, vector};
  end
  endfunction


  function automatic [29:0] signext_30_27;
    input [26:0] vector;
  begin
    signext_30_27= {{3{vector[26]}}, vector};
  end
  endfunction


  function automatic [30:0] signext_31_1;
    input  vector;
  begin
    signext_31_1= {{30{vector}}, vector};
  end
  endfunction


  function automatic [27:0] conv_s2s_27_28 ;
    input [26:0]  vector ;
  begin
    conv_s2s_27_28 = {vector[26], vector};
  end
  endfunction


  function automatic [28:0] conv_s2s_28_29 ;
    input [27:0]  vector ;
  begin
    conv_s2s_28_29 = {vector[27], vector};
  end
  endfunction


  function automatic [32:0] conv_s2s_32_33 ;
    input [31:0]  vector ;
  begin
    conv_s2s_32_33 = {vector[31], vector};
  end
  endfunction


  function automatic [20:0] conv_s2u_20_21 ;
    input [19:0]  vector ;
  begin
    conv_s2u_20_21 = {vector[19], vector};
  end
  endfunction


  function automatic [32:0] conv_s2u_28_33 ;
    input [27:0]  vector ;
  begin
    conv_s2u_28_33 = {{5{vector[27]}}, vector};
  end
  endfunction


  function automatic [33:0] conv_s2u_29_34 ;
    input [28:0]  vector ;
  begin
    conv_s2u_29_34 = {{5{vector[28]}}, vector};
  end
  endfunction


  function automatic [32:0] conv_s2u_32_33 ;
    input [31:0]  vector ;
  begin
    conv_s2u_32_33 = {vector[31], vector};
  end
  endfunction


  function automatic [33:0] conv_s2u_33_34 ;
    input [32:0]  vector ;
  begin
    conv_s2u_33_34 = {vector[32], vector};
  end
  endfunction


  function automatic [48:0] conv_s2u_48_49 ;
    input [47:0]  vector ;
  begin
    conv_s2u_48_49 = {vector[47], vector};
  end
  endfunction


  function automatic [48:0] conv_u2u_1_49 ;
    input [0:0]  vector ;
  begin
    conv_u2u_1_49 = {{48{1'b0}}, vector};
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_dense_16000_2500
// ------------------------------------------------------------------


module catapult_dense_16000_2500 (
  clk, rstn, bias_rsc_dat, bias_triosy_lz, relu_rsc_dat, relu_triosy_lz, image_channel_rsc_dat,
      image_channel_rsc_vld, image_channel_rsc_rdy, weight_memory_data_rsc_0_0_radr,
      weight_memory_data_rsc_0_0_re, weight_memory_data_rsc_0_0_q, weight_memory_data_tri_0_0_osy_lz,
      weight_memory_data_rsc_0_1_radr, weight_memory_data_rsc_0_1_re, weight_memory_data_rsc_0_1_q,
      weight_memory_data_tri_0_1_osy_lz, bias_memory_rsc_radr, bias_memory_rsc_re,
      bias_memory_rsc_q, bias_memory_triosy_lz, output_image_channel_rsc_dat, output_image_channel_rsc_vld,
      output_image_channel_rsc_rdy, input_vector_length_rsc_dat, input_vector_length_triosy_lz,
      output_image_elements_rsc_dat, output_image_elements_triosy_lz
);
  input clk;
  input rstn;
  input bias_rsc_dat;
  output bias_triosy_lz;
  input relu_rsc_dat;
  output relu_triosy_lz;
  input [31:0] image_channel_rsc_dat;
  input image_channel_rsc_vld;
  output image_channel_rsc_rdy;
  output [21:0] weight_memory_data_rsc_0_0_radr;
  output weight_memory_data_rsc_0_0_re;
  input [31:0] weight_memory_data_rsc_0_0_q;
  output weight_memory_data_tri_0_0_osy_lz;
  output [21:0] weight_memory_data_rsc_0_1_radr;
  output weight_memory_data_rsc_0_1_re;
  input [31:0] weight_memory_data_rsc_0_1_q;
  output weight_memory_data_tri_0_1_osy_lz;
  output [10:0] bias_memory_rsc_radr;
  output bias_memory_rsc_re;
  input [31:0] bias_memory_rsc_q;
  output bias_memory_triosy_lz;
  output [31:0] output_image_channel_rsc_dat;
  output output_image_channel_rsc_vld;
  input output_image_channel_rsc_rdy;
  input [27:0] input_vector_length_rsc_dat;
  output input_vector_length_triosy_lz;
  input [27:0] output_image_elements_rsc_dat;
  output output_image_elements_triosy_lz;


  // Interconnect Declarations
  wire [10:0] bias_memory_rsci_radr_d;
  wire [31:0] bias_memory_rsci_q_d;
  wire [31:0] input_image_rsc_0_0_i_q_d;
  wire [31:0] input_image_rsc_0_1_i_q_d;
  wire [10:0] output_buffer_rsc_0_0_i_wadr_d;
  wire [31:0] output_buffer_rsc_0_0_i_d_d;
  wire [31:0] output_buffer_rsc_0_0_i_q_d;
  wire [31:0] output_buffer_rsc_0_1_i_d_d;
  wire [31:0] output_buffer_rsc_0_1_i_q_d;
  wire [31:0] weight_memory_data_rsc_0_0_i_q_d;
  wire [31:0] weight_memory_data_rsc_0_1_i_q_d;
  wire [31:0] inner_multiply_loop_1_qif_mul_cmp_a;
  wire [31:0] inner_multiply_loop_1_qif_mul_cmp_b;
  wire [31:0] inner_multiply_loop_1_qif_mul_cmp_1_a;
  wire [31:0] inner_multiply_loop_1_qif_mul_cmp_1_b;
  wire catapult_dense_16000_2500_stall;
  wire input_image_rsc_0_0_we;
  wire [31:0] input_image_rsc_0_0_d;
  wire [12:0] input_image_rsc_0_0_wadr;
  wire [31:0] input_image_rsc_0_0_q;
  wire input_image_rsc_0_0_re;
  wire [12:0] input_image_rsc_0_0_radr;
  wire input_image_rsc_0_1_we;
  wire [31:0] input_image_rsc_0_1_d;
  wire [12:0] input_image_rsc_0_1_wadr;
  wire [31:0] input_image_rsc_0_1_q;
  wire input_image_rsc_0_1_re;
  wire [12:0] input_image_rsc_0_1_radr;
  wire output_buffer_rsc_0_0_we;
  wire [31:0] output_buffer_rsc_0_0_d;
  wire [10:0] output_buffer_rsc_0_0_wadr;
  wire [31:0] output_buffer_rsc_0_0_q;
  wire output_buffer_rsc_0_0_re;
  wire [10:0] output_buffer_rsc_0_0_radr;
  wire output_buffer_rsc_0_1_we;
  wire [31:0] output_buffer_rsc_0_1_d;
  wire [10:0] output_buffer_rsc_0_1_wadr;
  wire [31:0] output_buffer_rsc_0_1_q;
  wire output_buffer_rsc_0_1_re;
  wire [10:0] output_buffer_rsc_0_1_radr;
  wire bias_memory_rsci_re_d_iff;
  wire [12:0] input_image_rsc_0_0_i_radr_d_iff;
  wire [12:0] input_image_rsc_0_0_i_wadr_d_iff;
  wire [31:0] input_image_rsc_0_0_i_d_d_iff;
  wire input_image_rsc_0_0_i_we_d_iff;
  wire input_image_rsc_0_0_i_re_d_iff;
  wire input_image_rsc_0_1_i_we_d_iff;
  wire input_image_rsc_0_1_i_re_d_iff;
  wire [10:0] output_buffer_rsc_0_0_i_radr_d_iff;
  wire output_buffer_rsc_0_0_i_we_d_iff;
  wire output_buffer_rsc_0_0_i_re_d_iff;
  wire output_buffer_rsc_0_1_i_we_d_iff;
  wire output_buffer_rsc_0_1_i_re_d_iff;
  wire [21:0] weight_memory_data_rsc_0_0_i_radr_d_iff;
  wire weight_memory_data_rsc_0_0_i_re_d_iff;
  wire weight_memory_data_rsc_0_1_i_re_d_iff;


  // Interconnect Declarations for Component Instantiations 
  wire signed [63:0] nl_mul_sgnd;
  wire [63:0] nl_catapult_dense_16000_2500_run_inst_inner_multiply_loop_1_qif_mul_cmp_z;
  assign nl_mul_sgnd = $signed(inner_multiply_loop_1_qif_mul_cmp_a) * $signed(inner_multiply_loop_1_qif_mul_cmp_b);
  assign nl_catapult_dense_16000_2500_run_inst_inner_multiply_loop_1_qif_mul_cmp_z
      = $unsigned(nl_mul_sgnd);
  wire signed [63:0] nl_mul_1_sgnd;
  wire [63:0] nl_catapult_dense_16000_2500_run_inst_inner_multiply_loop_1_qif_mul_cmp_1_z;
  assign nl_mul_1_sgnd = $signed(inner_multiply_loop_1_qif_mul_cmp_1_a) * $signed(inner_multiply_loop_1_qif_mul_cmp_1_b);
  assign nl_catapult_dense_16000_2500_run_inst_inner_multiply_loop_1_qif_mul_cmp_1_z
      = $unsigned(nl_mul_1_sgnd);
  ccs_stallbuf_v1 #(.rscid(32'sd0),
  .width(32'sd1)) catapult_dense_16000_2500_stalldrv (
      .stalldrv(catapult_dense_16000_2500_stall)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd13),
  .depth(32'sd8002)) input_image_rsc_0_0_comp (
      .radr(input_image_rsc_0_0_radr),
      .wadr(input_image_rsc_0_0_wadr),
      .d(input_image_rsc_0_0_d),
      .we(input_image_rsc_0_0_we),
      .re(input_image_rsc_0_0_re),
      .clk(clk),
      .q(input_image_rsc_0_0_q)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd13),
  .depth(32'sd8002)) input_image_rsc_0_1_comp (
      .radr(input_image_rsc_0_1_radr),
      .wadr(input_image_rsc_0_1_wadr),
      .d(input_image_rsc_0_1_d),
      .we(input_image_rsc_0_1_we),
      .re(input_image_rsc_0_1_re),
      .clk(clk),
      .q(input_image_rsc_0_1_q)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd11),
  .depth(32'sd1252)) output_buffer_rsc_0_0_comp (
      .radr(output_buffer_rsc_0_0_radr),
      .wadr(output_buffer_rsc_0_0_wadr),
      .d(output_buffer_rsc_0_0_d),
      .we(output_buffer_rsc_0_0_we),
      .re(output_buffer_rsc_0_0_re),
      .clk(clk),
      .q(output_buffer_rsc_0_0_q)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd11),
  .depth(32'sd1252)) output_buffer_rsc_0_1_comp (
      .radr(output_buffer_rsc_0_1_radr),
      .wadr(output_buffer_rsc_0_1_wadr),
      .d(output_buffer_rsc_0_1_d),
      .we(output_buffer_rsc_0_1_we),
      .re(output_buffer_rsc_0_1_re),
      .clk(clk),
      .q(output_buffer_rsc_0_1_q)
    );
  catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rport_5_32_11_1027_1027_32_5_gen
      bias_memory_rsci (
      .q(bias_memory_rsc_q),
      .re(bias_memory_rsc_re),
      .radr(bias_memory_rsc_radr),
      .radr_d(bias_memory_rsci_radr_d),
      .re_d(bias_memory_rsci_re_d_iff),
      .q_d(bias_memory_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(bias_memory_rsci_re_d_iff)
    );
  catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_13_32_13_8002_8002_32_5_gen
      input_image_rsc_0_0_i (
      .we(input_image_rsc_0_0_we),
      .d(input_image_rsc_0_0_d),
      .wadr(input_image_rsc_0_0_wadr),
      .q(input_image_rsc_0_0_q),
      .re(input_image_rsc_0_0_re),
      .radr(input_image_rsc_0_0_radr),
      .radr_d(input_image_rsc_0_0_i_radr_d_iff),
      .wadr_d(input_image_rsc_0_0_i_wadr_d_iff),
      .d_d(input_image_rsc_0_0_i_d_d_iff),
      .we_d(input_image_rsc_0_0_i_we_d_iff),
      .re_d(input_image_rsc_0_0_i_re_d_iff),
      .q_d(input_image_rsc_0_0_i_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(input_image_rsc_0_0_i_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(input_image_rsc_0_0_i_we_d_iff)
    );
  catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_14_32_13_8002_8002_32_5_gen
      input_image_rsc_0_1_i (
      .we(input_image_rsc_0_1_we),
      .d(input_image_rsc_0_1_d),
      .wadr(input_image_rsc_0_1_wadr),
      .q(input_image_rsc_0_1_q),
      .re(input_image_rsc_0_1_re),
      .radr(input_image_rsc_0_1_radr),
      .radr_d(input_image_rsc_0_0_i_radr_d_iff),
      .wadr_d(input_image_rsc_0_0_i_wadr_d_iff),
      .d_d(input_image_rsc_0_0_i_d_d_iff),
      .we_d(input_image_rsc_0_1_i_we_d_iff),
      .re_d(input_image_rsc_0_1_i_re_d_iff),
      .q_d(input_image_rsc_0_1_i_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(input_image_rsc_0_1_i_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(input_image_rsc_0_1_i_we_d_iff)
    );
  catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_15_32_11_1252_1252_32_5_gen
      output_buffer_rsc_0_0_i (
      .we(output_buffer_rsc_0_0_we),
      .d(output_buffer_rsc_0_0_d),
      .wadr(output_buffer_rsc_0_0_wadr),
      .q(output_buffer_rsc_0_0_q),
      .re(output_buffer_rsc_0_0_re),
      .radr(output_buffer_rsc_0_0_radr),
      .radr_d(output_buffer_rsc_0_0_i_radr_d_iff),
      .wadr_d(output_buffer_rsc_0_0_i_wadr_d),
      .d_d(output_buffer_rsc_0_0_i_d_d),
      .we_d(output_buffer_rsc_0_0_i_we_d_iff),
      .re_d(output_buffer_rsc_0_0_i_re_d_iff),
      .q_d(output_buffer_rsc_0_0_i_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(output_buffer_rsc_0_0_i_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(output_buffer_rsc_0_0_i_we_d_iff)
    );
  catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_16_32_11_1252_1252_32_5_gen
      output_buffer_rsc_0_1_i (
      .we(output_buffer_rsc_0_1_we),
      .d(output_buffer_rsc_0_1_d),
      .wadr(output_buffer_rsc_0_1_wadr),
      .q(output_buffer_rsc_0_1_q),
      .re(output_buffer_rsc_0_1_re),
      .radr(output_buffer_rsc_0_1_radr),
      .radr_d(output_buffer_rsc_0_0_i_radr_d_iff),
      .wadr_d(output_buffer_rsc_0_0_i_wadr_d),
      .d_d(output_buffer_rsc_0_1_i_d_d),
      .we_d(output_buffer_rsc_0_1_i_we_d_iff),
      .re_d(output_buffer_rsc_0_1_i_re_d_iff),
      .q_d(output_buffer_rsc_0_1_i_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(output_buffer_rsc_0_1_i_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(output_buffer_rsc_0_1_i_we_d_iff)
    );
  catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rport_17_32_22_2097155_2097155_32_5_gen
      weight_memory_data_rsc_0_0_i (
      .q(weight_memory_data_rsc_0_0_q),
      .re(weight_memory_data_rsc_0_0_re),
      .radr(weight_memory_data_rsc_0_0_radr),
      .radr_d(weight_memory_data_rsc_0_0_i_radr_d_iff),
      .re_d(weight_memory_data_rsc_0_0_i_re_d_iff),
      .q_d(weight_memory_data_rsc_0_0_i_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(weight_memory_data_rsc_0_0_i_re_d_iff)
    );
  catapult_dense_16000_2500_ccs_sample_mem_ccs_ram_sync_1R1W_rport_18_32_22_2097155_2097155_32_5_gen
      weight_memory_data_rsc_0_1_i (
      .q(weight_memory_data_rsc_0_1_q),
      .re(weight_memory_data_rsc_0_1_re),
      .radr(weight_memory_data_rsc_0_1_radr),
      .radr_d(weight_memory_data_rsc_0_0_i_radr_d_iff),
      .re_d(weight_memory_data_rsc_0_1_i_re_d_iff),
      .q_d(weight_memory_data_rsc_0_1_i_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(weight_memory_data_rsc_0_1_i_re_d_iff)
    );
  catapult_dense_16000_2500_run catapult_dense_16000_2500_run_inst (
      .clk(clk),
      .rstn(rstn),
      .bias_rsc_dat(bias_rsc_dat),
      .bias_triosy_lz(bias_triosy_lz),
      .relu_rsc_dat(relu_rsc_dat),
      .relu_triosy_lz(relu_triosy_lz),
      .image_channel_rsc_dat(image_channel_rsc_dat),
      .image_channel_rsc_vld(image_channel_rsc_vld),
      .image_channel_rsc_rdy(image_channel_rsc_rdy),
      .weight_memory_data_tri_0_0_osy_lz(weight_memory_data_tri_0_0_osy_lz),
      .weight_memory_data_tri_0_1_osy_lz(weight_memory_data_tri_0_1_osy_lz),
      .bias_memory_triosy_lz(bias_memory_triosy_lz),
      .output_image_channel_rsc_dat(output_image_channel_rsc_dat),
      .output_image_channel_rsc_vld(output_image_channel_rsc_vld),
      .output_image_channel_rsc_rdy(output_image_channel_rsc_rdy),
      .input_vector_length_rsc_dat(input_vector_length_rsc_dat),
      .input_vector_length_triosy_lz(input_vector_length_triosy_lz),
      .output_image_elements_rsc_dat(output_image_elements_rsc_dat),
      .output_image_elements_triosy_lz(output_image_elements_triosy_lz),
      .bias_memory_rsci_radr_d(bias_memory_rsci_radr_d),
      .bias_memory_rsci_q_d(bias_memory_rsci_q_d),
      .input_image_rsc_0_0_i_q_d(input_image_rsc_0_0_i_q_d),
      .input_image_rsc_0_1_i_q_d(input_image_rsc_0_1_i_q_d),
      .output_buffer_rsc_0_0_i_wadr_d(output_buffer_rsc_0_0_i_wadr_d),
      .output_buffer_rsc_0_0_i_d_d(output_buffer_rsc_0_0_i_d_d),
      .output_buffer_rsc_0_0_i_q_d(output_buffer_rsc_0_0_i_q_d),
      .output_buffer_rsc_0_1_i_d_d(output_buffer_rsc_0_1_i_d_d),
      .output_buffer_rsc_0_1_i_q_d(output_buffer_rsc_0_1_i_q_d),
      .weight_memory_data_rsc_0_0_i_q_d(weight_memory_data_rsc_0_0_i_q_d),
      .weight_memory_data_rsc_0_1_i_q_d(weight_memory_data_rsc_0_1_i_q_d),
      .inner_multiply_loop_1_qif_mul_cmp_a(inner_multiply_loop_1_qif_mul_cmp_a),
      .inner_multiply_loop_1_qif_mul_cmp_b(inner_multiply_loop_1_qif_mul_cmp_b),
      .inner_multiply_loop_1_qif_mul_cmp_z(nl_catapult_dense_16000_2500_run_inst_inner_multiply_loop_1_qif_mul_cmp_z[63:0]),
      .inner_multiply_loop_1_qif_mul_cmp_1_a(inner_multiply_loop_1_qif_mul_cmp_1_a),
      .inner_multiply_loop_1_qif_mul_cmp_1_b(inner_multiply_loop_1_qif_mul_cmp_1_b),
      .inner_multiply_loop_1_qif_mul_cmp_1_z(nl_catapult_dense_16000_2500_run_inst_inner_multiply_loop_1_qif_mul_cmp_1_z[63:0]),
      .catapult_dense_16000_2500_stall(catapult_dense_16000_2500_stall),
      .bias_memory_rsci_re_d_pff(bias_memory_rsci_re_d_iff),
      .input_image_rsc_0_0_i_radr_d_pff(input_image_rsc_0_0_i_radr_d_iff),
      .input_image_rsc_0_0_i_wadr_d_pff(input_image_rsc_0_0_i_wadr_d_iff),
      .input_image_rsc_0_0_i_d_d_pff(input_image_rsc_0_0_i_d_d_iff),
      .input_image_rsc_0_0_i_we_d_pff(input_image_rsc_0_0_i_we_d_iff),
      .input_image_rsc_0_0_i_re_d_pff(input_image_rsc_0_0_i_re_d_iff),
      .input_image_rsc_0_1_i_we_d_pff(input_image_rsc_0_1_i_we_d_iff),
      .input_image_rsc_0_1_i_re_d_pff(input_image_rsc_0_1_i_re_d_iff),
      .output_buffer_rsc_0_0_i_radr_d_pff(output_buffer_rsc_0_0_i_radr_d_iff),
      .output_buffer_rsc_0_0_i_we_d_pff(output_buffer_rsc_0_0_i_we_d_iff),
      .output_buffer_rsc_0_0_i_re_d_pff(output_buffer_rsc_0_0_i_re_d_iff),
      .output_buffer_rsc_0_1_i_we_d_pff(output_buffer_rsc_0_1_i_we_d_iff),
      .output_buffer_rsc_0_1_i_re_d_pff(output_buffer_rsc_0_1_i_re_d_iff),
      .weight_memory_data_rsc_0_0_i_radr_d_pff(weight_memory_data_rsc_0_0_i_radr_d_iff),
      .weight_memory_data_rsc_0_0_i_re_d_pff(weight_memory_data_rsc_0_0_i_re_d_iff),
      .weight_memory_data_rsc_0_1_i_re_d_pff(weight_memory_data_rsc_0_1_i_re_d_iff)
    );
endmodule



