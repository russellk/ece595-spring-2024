
//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_in_wait_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module ccs_in_wait_v1 (idat, rdy, ivld, dat, irdy, vld);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] idat;
  output             rdy;
  output             ivld;
  input  [width-1:0] dat;
  input              irdy;
  input              vld;

  wire   [width-1:0] idat;
  wire               rdy;
  wire               ivld;

  localparam stallOff = 0; 
  wire                  stall_ctrl;
  assign stall_ctrl = stallOff;

  assign idat = dat;
  assign rdy = irdy && !stall_ctrl;
  assign ivld = vld && !stall_ctrl;

endmodule


//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_out_wait_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module ccs_out_wait_v1 (dat, irdy, vld, idat, rdy, ivld);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] dat;
  output             irdy;
  output             vld;
  input  [width-1:0] idat;
  input              rdy;
  input              ivld;

  wire   [width-1:0] dat;
  wire               irdy;
  wire               vld;

  localparam stallOff = 0; 
  wire stall_ctrl;
  assign stall_ctrl = stallOff;

  assign dat = idat;
  assign irdy = rdy && !stall_ctrl;
  assign vld = ivld && !stall_ctrl;

endmodule



//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/mgc_io_sync_v2.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module mgc_io_sync_v2 (ld, lz);
    parameter valid = 0;

    input  ld;
    output lz;

    wire   lz;

    assign lz = ld;

endmodule


//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_in_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2017 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------


module ccs_in_v1 (idat, dat);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] idat;
  input  [width-1:0] dat;

  wire   [width-1:0] idat;

  assign idat = dat;

endmodule


//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_stallbuf_v1.v 
//------------------------------------------------------------------------------
// Catapult Synthesis - Sample I/O Port Library
//
// Copyright (c) 2003-2015 Mentor Graphics Corp.
//       All Rights Reserved
//
// This document may be used and distributed without restriction provided that
// this copyright statement is not removed from the file and that any derivative
// work contains this copyright notice.
//
// The design information contained in this file is intended to be an example
// of the functionality which the end user may study in preparation for creating
// their own custom interfaces. This design does not necessarily present a 
// complete implementation of the named protocol or standard.
//
//------------------------------------------------------------------------------

module ccs_stallbuf_v1 (stalldrv);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output   stalldrv;
  localparam stall_off = 0; 
   
  assign stalldrv = stall_off;

endmodule




//------> /wv/hlsb/CATAPULT/2024.1/2024-02-14/aol/Mgc_home/pkgs/siflibs/ccs_ram_sync_1R1W.v 
module ccs_ram_sync_1R1W
#(
parameter data_width = 8,
parameter addr_width = 7,
parameter depth = 128
)(
	radr, wadr, d, we, re, clk, q
);

	input [addr_width-1:0] radr;
	input [addr_width-1:0] wadr;
	input [data_width-1:0] d;
	input we;
	input re;
	input clk;
	output[data_width-1:0] q;

   // synopsys translate_off
	reg [data_width-1:0] q;

	reg [data_width-1:0] mem [depth-1:0];
		
	always @(posedge clk) begin
		if (we) begin
			mem[wadr] <= d; // Write port
		end
		if (re) begin
			q <= mem[radr] ; // read port
		end
	end
   // synopsys translate_on

endmodule

//------> ./rtl.v 
// ----------------------------------------------------------------------
//  HLS HDL:        Verilog Netlister
//  HLS Version:    2024.1/1091966 Production Release
//  HLS Date:       Wed Feb 14 09:07:18 PST 2024
// 
//  Generated by:   russk@orw-russk-vm
//  Generated date: Wed Apr 17 12:37:28 2024
// ----------------------------------------------------------------------

// 
// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_17_32_8_198_198_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_17_32_8_198_198_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [7:0] wadr;
  input [31:0] q;
  output re;
  output [7:0] radr;
  input [7:0] radr_d;
  input [7:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_16_32_8_198_198_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_16_32_8_198_198_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [7:0] wadr;
  input [31:0] q;
  output re;
  output [7:0] radr;
  input [7:0] radr_d;
  input [7:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_14_32_8_198_198_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_14_32_8_198_198_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [7:0] wadr;
  input [31:0] q;
  output re;
  output [7:0] radr;
  input [7:0] radr_d;
  input [7:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_13_32_12_3960_3960_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_13_32_12_3960_3960_32_5_gen
    (
  we, d, wadr, q, re, radr, radr_d, wadr_d, d_d, we_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d,
      port_1_w_ram_ir_internal_WMASK_B_d
);
  output we;
  output [31:0] d;
  output [11:0] wadr;
  input [31:0] q;
  output re;
  output [11:0] radr;
  input [11:0] radr_d;
  input [11:0] wadr_d;
  input [31:0] d_d;
  input we_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;
  input port_1_w_ram_ir_internal_WMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign we = (port_1_w_ram_ir_internal_WMASK_B_d);
  assign d = (d_d);
  assign wadr = (wadr_d);
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_3_32_10_1024_1024_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_3_32_10_1024_1024_32_5_gen
    (
  q, re, radr, radr_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d
);
  input [31:0] q;
  output re;
  output [9:0] radr;
  input [9:0] radr_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_2_32_14_16384_16384_32_5_gen
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_2_32_14_16384_16384_32_5_gen
    (
  q, re, radr, radr_d, re_d, q_d, port_0_r_ram_ir_internal_RMASK_B_d
);
  input [31:0] q;
  output re;
  output [13:0] radr;
  input [13:0] radr_d;
  input re_d;
  output [31:0] q_d;
  input port_0_r_ram_ir_internal_RMASK_B_d;



  // Interconnect Declarations for Component Instantiations 
  assign q_d = q;
  assign re = (port_0_r_ram_ir_internal_RMASK_B_d);
  assign radr = (radr_d);
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_run_fsm
//  FSM Module
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_run_fsm (
  clk, rstn, run_wen, fsm_output, main_C_0_tr0, main_loop_C_0_tr0, load_images_loop_C_1_tr0,
      main_C_1_tr0, main_loop_1_C_0_tr0, main_loop_2_C_0_tr0, catapult_conv2d_14_14_3_3_1_convolve_do_C_0_tr0,
      inner_loop_C_1_tr0, main_loop_3_C_0_tr0, main_loop_3_C_0_tr1, catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3_tr0,
      stride_loop_C_2_tr0, copy_loop_C_0_tr0, main_loop_5_C_0_tr0, main_loop_1_C_4_tr0
);
  input clk;
  input rstn;
  input run_wen;
  output [24:0] fsm_output;
  reg [24:0] fsm_output;
  input main_C_0_tr0;
  input main_loop_C_0_tr0;
  input load_images_loop_C_1_tr0;
  input main_C_1_tr0;
  input main_loop_1_C_0_tr0;
  input main_loop_2_C_0_tr0;
  input catapult_conv2d_14_14_3_3_1_convolve_do_C_0_tr0;
  input inner_loop_C_1_tr0;
  input main_loop_3_C_0_tr0;
  input main_loop_3_C_0_tr1;
  input catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3_tr0;
  input stride_loop_C_2_tr0;
  input copy_loop_C_0_tr0;
  input main_loop_5_C_0_tr0;
  input main_loop_1_C_4_tr0;


  // FSM State Type Declaration for catapult_conv2d_14_14_3_3_1_run_run_fsm_1
  parameter
    main_C_0 = 5'd0,
    load_images_loop_C_0 = 5'd1,
    main_loop_C_0 = 5'd2,
    load_images_loop_C_1 = 5'd3,
    main_C_1 = 5'd4,
    main_loop_1_C_0 = 5'd5,
    main_loop_2_C_0 = 5'd6,
    inner_loop_C_0 = 5'd7,
    catapult_conv2d_14_14_3_3_1_convolve_do_C_0 = 5'd8,
    inner_loop_C_1 = 5'd9,
    main_loop_1_C_1 = 5'd10,
    main_loop_1_C_2 = 5'd11,
    main_loop_3_C_0 = 5'd12,
    catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_0 = 5'd13,
    catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_1 = 5'd14,
    catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_2 = 5'd15,
    catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3 = 5'd16,
    stride_loop_C_0 = 5'd17,
    stride_loop_C_1 = 5'd18,
    stride_loop_C_2 = 5'd19,
    copy_loop_C_0 = 5'd20,
    main_loop_1_C_3 = 5'd21,
    main_loop_5_C_0 = 5'd22,
    main_loop_1_C_4 = 5'd23,
    main_C_2 = 5'd24;

  reg [4:0] state_var;
  reg [4:0] state_var_NS;


  // Interconnect Declarations for Component Instantiations 
  always @(*)
  begin : catapult_conv2d_14_14_3_3_1_run_run_fsm_1
    case (state_var)
      load_images_loop_C_0 : begin
        fsm_output = 25'b0000000000000000000000010;
        state_var_NS = main_loop_C_0;
      end
      main_loop_C_0 : begin
        fsm_output = 25'b0000000000000000000000100;
        if ( main_loop_C_0_tr0 ) begin
          state_var_NS = load_images_loop_C_1;
        end
        else begin
          state_var_NS = main_loop_C_0;
        end
      end
      load_images_loop_C_1 : begin
        fsm_output = 25'b0000000000000000000001000;
        if ( load_images_loop_C_1_tr0 ) begin
          state_var_NS = main_C_1;
        end
        else begin
          state_var_NS = load_images_loop_C_0;
        end
      end
      main_C_1 : begin
        fsm_output = 25'b0000000000000000000010000;
        if ( main_C_1_tr0 ) begin
          state_var_NS = main_C_2;
        end
        else begin
          state_var_NS = main_loop_1_C_0;
        end
      end
      main_loop_1_C_0 : begin
        fsm_output = 25'b0000000000000000000100000;
        if ( main_loop_1_C_0_tr0 ) begin
          state_var_NS = main_loop_1_C_1;
        end
        else begin
          state_var_NS = main_loop_2_C_0;
        end
      end
      main_loop_2_C_0 : begin
        fsm_output = 25'b0000000000000000001000000;
        if ( main_loop_2_C_0_tr0 ) begin
          state_var_NS = inner_loop_C_0;
        end
        else begin
          state_var_NS = main_loop_2_C_0;
        end
      end
      inner_loop_C_0 : begin
        fsm_output = 25'b0000000000000000010000000;
        state_var_NS = catapult_conv2d_14_14_3_3_1_convolve_do_C_0;
      end
      catapult_conv2d_14_14_3_3_1_convolve_do_C_0 : begin
        fsm_output = 25'b0000000000000000100000000;
        if ( catapult_conv2d_14_14_3_3_1_convolve_do_C_0_tr0 ) begin
          state_var_NS = inner_loop_C_1;
        end
        else begin
          state_var_NS = catapult_conv2d_14_14_3_3_1_convolve_do_C_0;
        end
      end
      inner_loop_C_1 : begin
        fsm_output = 25'b0000000000000001000000000;
        if ( inner_loop_C_1_tr0 ) begin
          state_var_NS = main_loop_1_C_1;
        end
        else begin
          state_var_NS = main_loop_2_C_0;
        end
      end
      main_loop_1_C_1 : begin
        fsm_output = 25'b0000000000000010000000000;
        state_var_NS = main_loop_1_C_2;
      end
      main_loop_1_C_2 : begin
        fsm_output = 25'b0000000000000100000000000;
        state_var_NS = main_loop_3_C_0;
      end
      main_loop_3_C_0 : begin
        fsm_output = 25'b0000000000001000000000000;
        if ( main_loop_3_C_0_tr0 ) begin
          state_var_NS = catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_0;
        end
        else if ( main_loop_3_C_0_tr1 ) begin
          state_var_NS = stride_loop_C_0;
        end
        else begin
          state_var_NS = main_loop_3_C_0;
        end
      end
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_0 : begin
        fsm_output = 25'b0000000000010000000000000;
        state_var_NS = catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_1;
      end
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_1 : begin
        fsm_output = 25'b0000000000100000000000000;
        state_var_NS = catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_2;
      end
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_2 : begin
        fsm_output = 25'b0000000001000000000000000;
        state_var_NS = catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3;
      end
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3 : begin
        fsm_output = 25'b0000000010000000000000000;
        if ( catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3_tr0 ) begin
          state_var_NS = main_loop_1_C_3;
        end
        else begin
          state_var_NS = catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_0;
        end
      end
      stride_loop_C_0 : begin
        fsm_output = 25'b0000000100000000000000000;
        state_var_NS = stride_loop_C_1;
      end
      stride_loop_C_1 : begin
        fsm_output = 25'b0000001000000000000000000;
        state_var_NS = stride_loop_C_2;
      end
      stride_loop_C_2 : begin
        fsm_output = 25'b0000010000000000000000000;
        if ( stride_loop_C_2_tr0 ) begin
          state_var_NS = copy_loop_C_0;
        end
        else begin
          state_var_NS = stride_loop_C_0;
        end
      end
      copy_loop_C_0 : begin
        fsm_output = 25'b0000100000000000000000000;
        if ( copy_loop_C_0_tr0 ) begin
          state_var_NS = main_loop_1_C_3;
        end
        else begin
          state_var_NS = stride_loop_C_0;
        end
      end
      main_loop_1_C_3 : begin
        fsm_output = 25'b0001000000000000000000000;
        state_var_NS = main_loop_5_C_0;
      end
      main_loop_5_C_0 : begin
        fsm_output = 25'b0010000000000000000000000;
        if ( main_loop_5_C_0_tr0 ) begin
          state_var_NS = main_loop_1_C_4;
        end
        else begin
          state_var_NS = main_loop_5_C_0;
        end
      end
      main_loop_1_C_4 : begin
        fsm_output = 25'b0100000000000000000000000;
        if ( main_loop_1_C_4_tr0 ) begin
          state_var_NS = main_C_2;
        end
        else begin
          state_var_NS = main_loop_1_C_0;
        end
      end
      main_C_2 : begin
        fsm_output = 25'b1000000000000000000000000;
        state_var_NS = main_C_0;
      end
      // main_C_0
      default : begin
        fsm_output = 25'b0000000000000000000000001;
        if ( main_C_0_tr0 ) begin
          state_var_NS = main_C_1;
        end
        else begin
          state_var_NS = load_images_loop_C_0;
        end
      end
    endcase
  end

  always @(posedge clk) begin
    if ( ~ rstn ) begin
      state_var <= main_C_0;
    end
    else if ( run_wen ) begin
      state_var <= state_var_NS;
    end
  end

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_staller
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_staller (
  clk, rstn, catapult_conv2d_14_14_3_3_1_stall, run_wen, run_wten, image_channel_rsci_wen_comp,
      output_image_channel_rsci_wen_comp
);
  input clk;
  input rstn;
  input catapult_conv2d_14_14_3_3_1_stall;
  output run_wen;
  output run_wten;
  input image_channel_rsci_wen_comp;
  input output_image_channel_rsci_wen_comp;


  // Interconnect Declarations
  reg run_wten_reg;


  // Interconnect Declarations for Component Instantiations 
  assign run_wen = image_channel_rsci_wen_comp & output_image_channel_rsci_wen_comp
      & (~ catapult_conv2d_14_14_3_3_1_stall);
  assign run_wten = run_wten_reg;
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      run_wten_reg <= 1'b0;
    end
    else begin
      run_wten_reg <= ~ run_wen;
    end
  end
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_num_output_images_triosy_obj_num_output_images_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_num_output_images_triosy_obj_num_output_images_triosy_wait_ctrl
    (
  run_wten, num_output_images_triosy_obj_iswt0, num_output_images_triosy_obj_biwt
);
  input run_wten;
  input num_output_images_triosy_obj_iswt0;
  output num_output_images_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign num_output_images_triosy_obj_biwt = (~ run_wten) & num_output_images_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_num_input_images_triosy_obj_num_input_images_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_num_input_images_triosy_obj_num_input_images_triosy_wait_ctrl
    (
  run_wten, num_input_images_triosy_obj_iswt0, num_input_images_triosy_obj_biwt
);
  input run_wten;
  input num_input_images_triosy_obj_iswt0;
  output num_input_images_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign num_input_images_triosy_obj_biwt = (~ run_wten) & num_input_images_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_max_pool_triosy_obj_max_pool_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_max_pool_triosy_obj_max_pool_triosy_wait_ctrl
    (
  run_wten, max_pool_triosy_obj_iswt0, max_pool_triosy_obj_biwt
);
  input run_wten;
  input max_pool_triosy_obj_iswt0;
  output max_pool_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign max_pool_triosy_obj_biwt = (~ run_wten) & max_pool_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_relu_triosy_obj_relu_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_relu_triosy_obj_relu_triosy_wait_ctrl (
  run_wten, relu_triosy_obj_iswt0, relu_triosy_obj_biwt
);
  input run_wten;
  input relu_triosy_obj_iswt0;
  output relu_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign relu_triosy_obj_biwt = (~ run_wten) & relu_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_bias_triosy_obj_bias_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_bias_triosy_obj_bias_triosy_wait_ctrl (
  run_wten, bias_triosy_obj_iswt0, bias_triosy_obj_biwt
);
  input run_wten;
  input bias_triosy_obj_iswt0;
  output bias_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign bias_triosy_obj_biwt = (~ run_wten) & bias_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl
    (
  run_wten, bias_memory_triosy_obj_iswt0, bias_memory_triosy_obj_biwt
);
  input run_wten;
  input bias_memory_triosy_obj_iswt0;
  output bias_memory_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign bias_memory_triosy_obj_biwt = (~ run_wten) & bias_memory_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_weight_memory_triosy_obj_weight_memory_triosy_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_weight_memory_triosy_obj_weight_memory_triosy_wait_ctrl
    (
  run_wten, weight_memory_triosy_obj_iswt0, weight_memory_triosy_obj_biwt
);
  input run_wten;
  input weight_memory_triosy_obj_iswt0;
  output weight_memory_triosy_obj_biwt;



  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_triosy_obj_biwt = (~ run_wten) & weight_memory_triosy_obj_iswt0;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_dp
    (
  clk, rstn, output_buffer_rsci_q_d, output_buffer_rsci_q_d_mxwt, output_buffer_rsci_biwt_1,
      output_buffer_rsci_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] output_buffer_rsci_q_d;
  output [31:0] output_buffer_rsci_q_d_mxwt;
  input output_buffer_rsci_biwt_1;
  input output_buffer_rsci_bdwt_2;


  // Interconnect Declarations
  reg output_buffer_rsci_bcwt_1;
  reg [31:0] output_buffer_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign output_buffer_rsci_q_d_mxwt = MUX_v_32_2_2(output_buffer_rsci_q_d, output_buffer_rsci_q_d_bfwt,
      output_buffer_rsci_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_buffer_rsci_bcwt_1 <= 1'b0;
    end
    else begin
      output_buffer_rsci_bcwt_1 <= ~((~(output_buffer_rsci_bcwt_1 | output_buffer_rsci_biwt_1))
          | output_buffer_rsci_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_buffer_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( output_buffer_rsci_biwt_1 ) begin
      output_buffer_rsci_q_d_bfwt <= output_buffer_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_ctrl
    (
  run_wen, run_wten, output_buffer_rsci_oswt_1, output_buffer_rsci_biwt_1, output_buffer_rsci_bdwt_2,
      output_buffer_rsci_we_d_run_sct_pff, output_buffer_rsci_iswt0_pff, output_buffer_rsci_re_d_run_sct_pff,
      output_buffer_rsci_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input output_buffer_rsci_oswt_1;
  output output_buffer_rsci_biwt_1;
  output output_buffer_rsci_bdwt_2;
  output output_buffer_rsci_we_d_run_sct_pff;
  input output_buffer_rsci_iswt0_pff;
  output output_buffer_rsci_re_d_run_sct_pff;
  input output_buffer_rsci_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign output_buffer_rsci_bdwt_2 = output_buffer_rsci_oswt_1 & run_wen;
  assign output_buffer_rsci_biwt_1 = (~ run_wten) & output_buffer_rsci_oswt_1;
  assign output_buffer_rsci_we_d_run_sct_pff = output_buffer_rsci_iswt0_pff & run_wen;
  assign output_buffer_rsci_re_d_run_sct_pff = output_buffer_rsci_oswt_1_pff & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_relu_output_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_relu_output_rsc_wait_dp
    (
  clk, rstn, relu_output_rsci_q_d, relu_output_rsci_q_d_mxwt, relu_output_rsci_biwt_1,
      relu_output_rsci_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] relu_output_rsci_q_d;
  output [31:0] relu_output_rsci_q_d_mxwt;
  input relu_output_rsci_biwt_1;
  input relu_output_rsci_bdwt_2;


  // Interconnect Declarations
  reg relu_output_rsci_bcwt_1;
  reg [31:0] relu_output_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign relu_output_rsci_q_d_mxwt = MUX_v_32_2_2(relu_output_rsci_q_d, relu_output_rsci_q_d_bfwt,
      relu_output_rsci_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      relu_output_rsci_bcwt_1 <= 1'b0;
    end
    else begin
      relu_output_rsci_bcwt_1 <= ~((~(relu_output_rsci_bcwt_1 | relu_output_rsci_biwt_1))
          | relu_output_rsci_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      relu_output_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( relu_output_rsci_biwt_1 ) begin
      relu_output_rsci_q_d_bfwt <= relu_output_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_relu_output_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_relu_output_rsc_wait_ctrl
    (
  run_wen, run_wten, relu_output_rsci_oswt_1, relu_output_rsci_biwt_1, relu_output_rsci_bdwt_2,
      relu_output_rsci_we_d_run_sct_pff, relu_output_rsci_iswt0_pff, relu_output_rsci_re_d_run_sct_pff,
      relu_output_rsci_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input relu_output_rsci_oswt_1;
  output relu_output_rsci_biwt_1;
  output relu_output_rsci_bdwt_2;
  output relu_output_rsci_we_d_run_sct_pff;
  input relu_output_rsci_iswt0_pff;
  output relu_output_rsci_re_d_run_sct_pff;
  input relu_output_rsci_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign relu_output_rsci_bdwt_2 = relu_output_rsci_oswt_1 & run_wen;
  assign relu_output_rsci_biwt_1 = (~ run_wten) & relu_output_rsci_oswt_1;
  assign relu_output_rsci_we_d_run_sct_pff = relu_output_rsci_iswt0_pff & run_wen;
  assign relu_output_rsci_re_d_run_sct_pff = relu_output_rsci_oswt_1_pff & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_dp
    (
  clk, rstn, convolution_output_rsci_q_d, convolution_output_rsci_q_d_mxwt, convolution_output_rsci_biwt,
      convolution_output_rsci_bdwt
);
  input clk;
  input rstn;
  input [31:0] convolution_output_rsci_q_d;
  output [31:0] convolution_output_rsci_q_d_mxwt;
  input convolution_output_rsci_biwt;
  input convolution_output_rsci_bdwt;


  // Interconnect Declarations
  reg convolution_output_rsci_bcwt;
  reg [31:0] convolution_output_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign convolution_output_rsci_q_d_mxwt = MUX_v_32_2_2(convolution_output_rsci_q_d,
      convolution_output_rsci_q_d_bfwt, convolution_output_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      convolution_output_rsci_bcwt <= 1'b0;
    end
    else begin
      convolution_output_rsci_bcwt <= ~((~(convolution_output_rsci_bcwt | convolution_output_rsci_biwt))
          | convolution_output_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      convolution_output_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( convolution_output_rsci_biwt ) begin
      convolution_output_rsci_q_d_bfwt <= convolution_output_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_ctrl
    (
  run_wen, run_wten, convolution_output_rsci_oswt, convolution_output_rsci_biwt,
      convolution_output_rsci_bdwt, convolution_output_rsci_we_d_run_sct_pff, convolution_output_rsci_iswt0_1_pff,
      convolution_output_rsci_re_d_run_sct_pff, convolution_output_rsci_oswt_pff
);
  input run_wen;
  input run_wten;
  input convolution_output_rsci_oswt;
  output convolution_output_rsci_biwt;
  output convolution_output_rsci_bdwt;
  output convolution_output_rsci_we_d_run_sct_pff;
  input convolution_output_rsci_iswt0_1_pff;
  output convolution_output_rsci_re_d_run_sct_pff;
  input convolution_output_rsci_oswt_pff;



  // Interconnect Declarations for Component Instantiations 
  assign convolution_output_rsci_bdwt = convolution_output_rsci_oswt & run_wen;
  assign convolution_output_rsci_biwt = (~ run_wten) & convolution_output_rsci_oswt;
  assign convolution_output_rsci_we_d_run_sct_pff = convolution_output_rsci_iswt0_1_pff
      & run_wen;
  assign convolution_output_rsci_re_d_run_sct_pff = convolution_output_rsci_oswt_pff
      & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_current_image_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_current_image_rsc_wait_dp
    (
  clk, rstn, current_image_rsci_q_d, current_image_rsci_q_d_mxwt, current_image_rsci_biwt_1,
      current_image_rsci_bdwt_2
);
  input clk;
  input rstn;
  input [31:0] current_image_rsci_q_d;
  output [31:0] current_image_rsci_q_d_mxwt;
  input current_image_rsci_biwt_1;
  input current_image_rsci_bdwt_2;


  // Interconnect Declarations
  reg current_image_rsci_bcwt_1;
  reg [31:0] current_image_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign current_image_rsci_q_d_mxwt = MUX_v_32_2_2(current_image_rsci_q_d, current_image_rsci_q_d_bfwt,
      current_image_rsci_bcwt_1);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_image_rsci_bcwt_1 <= 1'b0;
    end
    else begin
      current_image_rsci_bcwt_1 <= ~((~(current_image_rsci_bcwt_1 | current_image_rsci_biwt_1))
          | current_image_rsci_bdwt_2);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_image_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( current_image_rsci_biwt_1 ) begin
      current_image_rsci_q_d_bfwt <= current_image_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_current_image_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_current_image_rsc_wait_ctrl
    (
  run_wen, run_wten, current_image_rsci_oswt_1, current_image_rsci_biwt_1, current_image_rsci_bdwt_2,
      current_image_rsci_we_d_run_sct_pff, current_image_rsci_iswt0_pff, current_image_rsci_re_d_run_sct_pff,
      current_image_rsci_oswt_1_pff
);
  input run_wen;
  input run_wten;
  input current_image_rsci_oswt_1;
  output current_image_rsci_biwt_1;
  output current_image_rsci_bdwt_2;
  output current_image_rsci_we_d_run_sct_pff;
  input current_image_rsci_iswt0_pff;
  output current_image_rsci_re_d_run_sct_pff;
  input current_image_rsci_oswt_1_pff;



  // Interconnect Declarations for Component Instantiations 
  assign current_image_rsci_bdwt_2 = current_image_rsci_oswt_1 & run_wen;
  assign current_image_rsci_biwt_1 = (~ run_wten) & current_image_rsci_oswt_1;
  assign current_image_rsci_we_d_run_sct_pff = current_image_rsci_iswt0_pff & run_wen;
  assign current_image_rsci_re_d_run_sct_pff = current_image_rsci_oswt_1_pff & run_wen;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci_output_image_channel_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci_output_image_channel_wait_dp
    (
  clk, rstn, output_image_channel_rsci_oswt, output_image_channel_rsci_wen_comp,
      output_image_channel_rsci_biwt, output_image_channel_rsci_bdwt, output_image_channel_rsci_bcwt
);
  input clk;
  input rstn;
  input output_image_channel_rsci_oswt;
  output output_image_channel_rsci_wen_comp;
  input output_image_channel_rsci_biwt;
  input output_image_channel_rsci_bdwt;
  output output_image_channel_rsci_bcwt;
  reg output_image_channel_rsci_bcwt;



  // Interconnect Declarations for Component Instantiations 
  assign output_image_channel_rsci_wen_comp = (~ output_image_channel_rsci_oswt)
      | output_image_channel_rsci_biwt | output_image_channel_rsci_bcwt;
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_image_channel_rsci_bcwt <= 1'b0;
    end
    else begin
      output_image_channel_rsci_bcwt <= ~((~(output_image_channel_rsci_bcwt | output_image_channel_rsci_biwt))
          | output_image_channel_rsci_bdwt);
    end
  end
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci_output_image_channel_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci_output_image_channel_wait_ctrl
    (
  run_wen, output_image_channel_rsci_oswt, output_image_channel_rsci_biwt, output_image_channel_rsci_bdwt,
      output_image_channel_rsci_bcwt, output_image_channel_rsci_irdy, output_image_channel_rsci_ivld_run_sct
);
  input run_wen;
  input output_image_channel_rsci_oswt;
  output output_image_channel_rsci_biwt;
  output output_image_channel_rsci_bdwt;
  input output_image_channel_rsci_bcwt;
  input output_image_channel_rsci_irdy;
  output output_image_channel_rsci_ivld_run_sct;


  // Interconnect Declarations
  wire output_image_channel_rsci_ogwt;


  // Interconnect Declarations for Component Instantiations 
  assign output_image_channel_rsci_bdwt = output_image_channel_rsci_oswt & run_wen;
  assign output_image_channel_rsci_biwt = output_image_channel_rsci_ogwt & output_image_channel_rsci_irdy;
  assign output_image_channel_rsci_ogwt = output_image_channel_rsci_oswt & (~ output_image_channel_rsci_bcwt);
  assign output_image_channel_rsci_ivld_run_sct = output_image_channel_rsci_ogwt;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp
    (
  clk, rstn, bias_memory_rsci_q_d, bias_memory_rsci_q_d_mxwt, bias_memory_rsci_biwt,
      bias_memory_rsci_bdwt
);
  input clk;
  input rstn;
  input [31:0] bias_memory_rsci_q_d;
  output [31:0] bias_memory_rsci_q_d_mxwt;
  input bias_memory_rsci_biwt;
  input bias_memory_rsci_bdwt;


  // Interconnect Declarations
  reg bias_memory_rsci_bcwt;
  reg [31:0] bias_memory_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign bias_memory_rsci_q_d_mxwt = MUX_v_32_2_2(bias_memory_rsci_q_d, bias_memory_rsci_q_d_bfwt,
      bias_memory_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      bias_memory_rsci_bcwt <= 1'b0;
    end
    else begin
      bias_memory_rsci_bcwt <= ~((~(bias_memory_rsci_bcwt | bias_memory_rsci_biwt))
          | bias_memory_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      bias_memory_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( bias_memory_rsci_biwt ) begin
      bias_memory_rsci_q_d_bfwt <= bias_memory_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl
    (
  run_wen, run_wten, bias_memory_rsci_oswt, bias_memory_rsci_biwt, bias_memory_rsci_bdwt,
      bias_memory_rsci_biwt_pff, bias_memory_rsci_oswt_pff
);
  input run_wen;
  input run_wten;
  input bias_memory_rsci_oswt;
  output bias_memory_rsci_biwt;
  output bias_memory_rsci_bdwt;
  output bias_memory_rsci_biwt_pff;
  input bias_memory_rsci_oswt_pff;



  // Interconnect Declarations for Component Instantiations 
  assign bias_memory_rsci_bdwt = bias_memory_rsci_oswt & run_wen;
  assign bias_memory_rsci_biwt = (~ run_wten) & bias_memory_rsci_oswt;
  assign bias_memory_rsci_biwt_pff = run_wen & bias_memory_rsci_oswt_pff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_dp
    (
  clk, rstn, weight_memory_rsci_q_d, weight_memory_rsci_q_d_mxwt, weight_memory_rsci_biwt,
      weight_memory_rsci_bdwt
);
  input clk;
  input rstn;
  input [31:0] weight_memory_rsci_q_d;
  output [31:0] weight_memory_rsci_q_d_mxwt;
  input weight_memory_rsci_biwt;
  input weight_memory_rsci_bdwt;


  // Interconnect Declarations
  reg weight_memory_rsci_bcwt;
  reg [31:0] weight_memory_rsci_q_d_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_rsci_q_d_mxwt = MUX_v_32_2_2(weight_memory_rsci_q_d, weight_memory_rsci_q_d_bfwt,
      weight_memory_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_memory_rsci_bcwt <= 1'b0;
    end
    else begin
      weight_memory_rsci_bcwt <= ~((~(weight_memory_rsci_bcwt | weight_memory_rsci_biwt))
          | weight_memory_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_memory_rsci_q_d_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( weight_memory_rsci_biwt ) begin
      weight_memory_rsci_q_d_bfwt <= weight_memory_rsci_q_d;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_ctrl
    (
  run_wen, run_wten, weight_memory_rsci_oswt, weight_memory_rsci_biwt, weight_memory_rsci_bdwt,
      weight_memory_rsci_biwt_pff, weight_memory_rsci_oswt_pff
);
  input run_wen;
  input run_wten;
  input weight_memory_rsci_oswt;
  output weight_memory_rsci_biwt;
  output weight_memory_rsci_bdwt;
  output weight_memory_rsci_biwt_pff;
  input weight_memory_rsci_oswt_pff;



  // Interconnect Declarations for Component Instantiations 
  assign weight_memory_rsci_bdwt = weight_memory_rsci_oswt & run_wen;
  assign weight_memory_rsci_biwt = (~ run_wten) & weight_memory_rsci_oswt;
  assign weight_memory_rsci_biwt_pff = run_wen & weight_memory_rsci_oswt_pff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_image_channel_rsci_image_channel_wait_dp
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_image_channel_rsci_image_channel_wait_dp (
  clk, rstn, image_channel_rsci_oswt, image_channel_rsci_wen_comp, image_channel_rsci_idat_mxwt,
      image_channel_rsci_biwt, image_channel_rsci_bdwt, image_channel_rsci_bcwt,
      image_channel_rsci_idat
);
  input clk;
  input rstn;
  input image_channel_rsci_oswt;
  output image_channel_rsci_wen_comp;
  output [31:0] image_channel_rsci_idat_mxwt;
  input image_channel_rsci_biwt;
  input image_channel_rsci_bdwt;
  output image_channel_rsci_bcwt;
  reg image_channel_rsci_bcwt;
  input [31:0] image_channel_rsci_idat;


  // Interconnect Declarations
  reg [31:0] image_channel_rsci_idat_bfwt;


  // Interconnect Declarations for Component Instantiations 
  assign image_channel_rsci_wen_comp = (~ image_channel_rsci_oswt) | image_channel_rsci_biwt
      | image_channel_rsci_bcwt;
  assign image_channel_rsci_idat_mxwt = MUX_v_32_2_2(image_channel_rsci_idat, image_channel_rsci_idat_bfwt,
      image_channel_rsci_bcwt);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      image_channel_rsci_bcwt <= 1'b0;
    end
    else begin
      image_channel_rsci_bcwt <= ~((~(image_channel_rsci_bcwt | image_channel_rsci_biwt))
          | image_channel_rsci_bdwt);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      image_channel_rsci_idat_bfwt <= 32'b00000000000000000000000000000000;
    end
    else if ( image_channel_rsci_biwt ) begin
      image_channel_rsci_idat_bfwt <= image_channel_rsci_idat;
    end
  end

  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_image_channel_rsci_image_channel_wait_ctrl
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_image_channel_rsci_image_channel_wait_ctrl
    (
  run_wen, image_channel_rsci_oswt, image_channel_rsci_biwt, image_channel_rsci_bdwt,
      image_channel_rsci_bcwt, image_channel_rsci_irdy_run_sct, image_channel_rsci_ivld
);
  input run_wen;
  input image_channel_rsci_oswt;
  output image_channel_rsci_biwt;
  output image_channel_rsci_bdwt;
  input image_channel_rsci_bcwt;
  output image_channel_rsci_irdy_run_sct;
  input image_channel_rsci_ivld;


  // Interconnect Declarations
  wire image_channel_rsci_ogwt;


  // Interconnect Declarations for Component Instantiations 
  assign image_channel_rsci_bdwt = image_channel_rsci_oswt & run_wen;
  assign image_channel_rsci_biwt = image_channel_rsci_ogwt & image_channel_rsci_ivld;
  assign image_channel_rsci_ogwt = image_channel_rsci_oswt & (~ image_channel_rsci_bcwt);
  assign image_channel_rsci_irdy_run_sct = image_channel_rsci_ogwt;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_num_output_images_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_num_output_images_triosy_obj (
  num_output_images_triosy_lz, run_wten, num_output_images_triosy_obj_iswt0
);
  output num_output_images_triosy_lz;
  input run_wten;
  input num_output_images_triosy_obj_iswt0;


  // Interconnect Declarations
  wire num_output_images_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) num_output_images_triosy_obj (
      .ld(num_output_images_triosy_obj_biwt),
      .lz(num_output_images_triosy_lz)
    );
  catapult_conv2d_14_14_3_3_1_run_num_output_images_triosy_obj_num_output_images_triosy_wait_ctrl
      catapult_conv2d_14_14_3_3_1_run_num_output_images_triosy_obj_num_output_images_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .num_output_images_triosy_obj_iswt0(num_output_images_triosy_obj_iswt0),
      .num_output_images_triosy_obj_biwt(num_output_images_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_num_input_images_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_num_input_images_triosy_obj (
  num_input_images_triosy_lz, run_wten, num_input_images_triosy_obj_iswt0
);
  output num_input_images_triosy_lz;
  input run_wten;
  input num_input_images_triosy_obj_iswt0;


  // Interconnect Declarations
  wire num_input_images_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) num_input_images_triosy_obj (
      .ld(num_input_images_triosy_obj_biwt),
      .lz(num_input_images_triosy_lz)
    );
  catapult_conv2d_14_14_3_3_1_run_num_input_images_triosy_obj_num_input_images_triosy_wait_ctrl
      catapult_conv2d_14_14_3_3_1_run_num_input_images_triosy_obj_num_input_images_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .num_input_images_triosy_obj_iswt0(num_input_images_triosy_obj_iswt0),
      .num_input_images_triosy_obj_biwt(num_input_images_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_max_pool_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_max_pool_triosy_obj (
  max_pool_triosy_lz, run_wten, max_pool_triosy_obj_iswt0
);
  output max_pool_triosy_lz;
  input run_wten;
  input max_pool_triosy_obj_iswt0;


  // Interconnect Declarations
  wire max_pool_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) max_pool_triosy_obj (
      .ld(max_pool_triosy_obj_biwt),
      .lz(max_pool_triosy_lz)
    );
  catapult_conv2d_14_14_3_3_1_run_max_pool_triosy_obj_max_pool_triosy_wait_ctrl catapult_conv2d_14_14_3_3_1_run_max_pool_triosy_obj_max_pool_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .max_pool_triosy_obj_iswt0(max_pool_triosy_obj_iswt0),
      .max_pool_triosy_obj_biwt(max_pool_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_relu_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_relu_triosy_obj (
  relu_triosy_lz, run_wten, relu_triosy_obj_iswt0
);
  output relu_triosy_lz;
  input run_wten;
  input relu_triosy_obj_iswt0;


  // Interconnect Declarations
  wire relu_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) relu_triosy_obj (
      .ld(relu_triosy_obj_biwt),
      .lz(relu_triosy_lz)
    );
  catapult_conv2d_14_14_3_3_1_run_relu_triosy_obj_relu_triosy_wait_ctrl catapult_conv2d_14_14_3_3_1_run_relu_triosy_obj_relu_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .relu_triosy_obj_iswt0(relu_triosy_obj_iswt0),
      .relu_triosy_obj_biwt(relu_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_bias_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_bias_triosy_obj (
  bias_triosy_lz, run_wten, bias_triosy_obj_iswt0
);
  output bias_triosy_lz;
  input run_wten;
  input bias_triosy_obj_iswt0;


  // Interconnect Declarations
  wire bias_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) bias_triosy_obj (
      .ld(bias_triosy_obj_biwt),
      .lz(bias_triosy_lz)
    );
  catapult_conv2d_14_14_3_3_1_run_bias_triosy_obj_bias_triosy_wait_ctrl catapult_conv2d_14_14_3_3_1_run_bias_triosy_obj_bias_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .bias_triosy_obj_iswt0(bias_triosy_obj_iswt0),
      .bias_triosy_obj_biwt(bias_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_bias_memory_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_bias_memory_triosy_obj (
  bias_memory_triosy_lz, run_wten, bias_memory_triosy_obj_iswt0
);
  output bias_memory_triosy_lz;
  input run_wten;
  input bias_memory_triosy_obj_iswt0;


  // Interconnect Declarations
  wire bias_memory_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) bias_memory_triosy_obj (
      .ld(bias_memory_triosy_obj_biwt),
      .lz(bias_memory_triosy_lz)
    );
  catapult_conv2d_14_14_3_3_1_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl
      catapult_conv2d_14_14_3_3_1_run_bias_memory_triosy_obj_bias_memory_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .bias_memory_triosy_obj_iswt0(bias_memory_triosy_obj_iswt0),
      .bias_memory_triosy_obj_biwt(bias_memory_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_weight_memory_triosy_obj
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_weight_memory_triosy_obj (
  weight_memory_triosy_lz, run_wten, weight_memory_triosy_obj_iswt0
);
  output weight_memory_triosy_lz;
  input run_wten;
  input weight_memory_triosy_obj_iswt0;


  // Interconnect Declarations
  wire weight_memory_triosy_obj_biwt;


  // Interconnect Declarations for Component Instantiations 
  mgc_io_sync_v2 #(.valid(32'sd0)) weight_memory_triosy_obj (
      .ld(weight_memory_triosy_obj_biwt),
      .lz(weight_memory_triosy_lz)
    );
  catapult_conv2d_14_14_3_3_1_run_weight_memory_triosy_obj_weight_memory_triosy_wait_ctrl
      catapult_conv2d_14_14_3_3_1_run_weight_memory_triosy_obj_weight_memory_triosy_wait_ctrl_inst
      (
      .run_wten(run_wten),
      .weight_memory_triosy_obj_iswt0(weight_memory_triosy_obj_iswt0),
      .weight_memory_triosy_obj_biwt(weight_memory_triosy_obj_biwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1 (
  clk, rstn, output_buffer_rsci_q_d, run_wen, run_wten, output_buffer_rsci_oswt_1,
      output_buffer_rsci_q_d_mxwt, output_buffer_rsci_we_d_pff, output_buffer_rsci_iswt0_pff,
      output_buffer_rsci_re_d_pff, output_buffer_rsci_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] output_buffer_rsci_q_d;
  input run_wen;
  input run_wten;
  input output_buffer_rsci_oswt_1;
  output [31:0] output_buffer_rsci_q_d_mxwt;
  output output_buffer_rsci_we_d_pff;
  input output_buffer_rsci_iswt0_pff;
  output output_buffer_rsci_re_d_pff;
  input output_buffer_rsci_oswt_1_pff;


  // Interconnect Declarations
  wire output_buffer_rsci_biwt_1;
  wire output_buffer_rsci_bdwt_2;
  wire output_buffer_rsci_we_d_run_sct_iff;
  wire output_buffer_rsci_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_ctrl
      catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .output_buffer_rsci_oswt_1(output_buffer_rsci_oswt_1),
      .output_buffer_rsci_biwt_1(output_buffer_rsci_biwt_1),
      .output_buffer_rsci_bdwt_2(output_buffer_rsci_bdwt_2),
      .output_buffer_rsci_we_d_run_sct_pff(output_buffer_rsci_we_d_run_sct_iff),
      .output_buffer_rsci_iswt0_pff(output_buffer_rsci_iswt0_pff),
      .output_buffer_rsci_re_d_run_sct_pff(output_buffer_rsci_re_d_run_sct_iff),
      .output_buffer_rsci_oswt_1_pff(output_buffer_rsci_oswt_1_pff)
    );
  catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_dp
      catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_output_buffer_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_buffer_rsci_q_d(output_buffer_rsci_q_d),
      .output_buffer_rsci_q_d_mxwt(output_buffer_rsci_q_d_mxwt),
      .output_buffer_rsci_biwt_1(output_buffer_rsci_biwt_1),
      .output_buffer_rsci_bdwt_2(output_buffer_rsci_bdwt_2)
    );
  assign output_buffer_rsci_we_d_pff = output_buffer_rsci_we_d_run_sct_iff;
  assign output_buffer_rsci_re_d_pff = output_buffer_rsci_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1 (
  clk, rstn, relu_output_rsci_q_d, run_wen, run_wten, relu_output_rsci_oswt_1, relu_output_rsci_q_d_mxwt,
      relu_output_rsci_we_d_pff, relu_output_rsci_iswt0_pff, relu_output_rsci_re_d_pff,
      relu_output_rsci_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] relu_output_rsci_q_d;
  input run_wen;
  input run_wten;
  input relu_output_rsci_oswt_1;
  output [31:0] relu_output_rsci_q_d_mxwt;
  output relu_output_rsci_we_d_pff;
  input relu_output_rsci_iswt0_pff;
  output relu_output_rsci_re_d_pff;
  input relu_output_rsci_oswt_1_pff;


  // Interconnect Declarations
  wire relu_output_rsci_biwt_1;
  wire relu_output_rsci_bdwt_2;
  wire relu_output_rsci_we_d_run_sct_iff;
  wire relu_output_rsci_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_relu_output_rsc_wait_ctrl catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_relu_output_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .relu_output_rsci_oswt_1(relu_output_rsci_oswt_1),
      .relu_output_rsci_biwt_1(relu_output_rsci_biwt_1),
      .relu_output_rsci_bdwt_2(relu_output_rsci_bdwt_2),
      .relu_output_rsci_we_d_run_sct_pff(relu_output_rsci_we_d_run_sct_iff),
      .relu_output_rsci_iswt0_pff(relu_output_rsci_iswt0_pff),
      .relu_output_rsci_re_d_run_sct_pff(relu_output_rsci_re_d_run_sct_iff),
      .relu_output_rsci_oswt_1_pff(relu_output_rsci_oswt_1_pff)
    );
  catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_relu_output_rsc_wait_dp catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_relu_output_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .relu_output_rsci_q_d(relu_output_rsci_q_d),
      .relu_output_rsci_q_d_mxwt(relu_output_rsci_q_d_mxwt),
      .relu_output_rsci_biwt_1(relu_output_rsci_biwt_1),
      .relu_output_rsci_bdwt_2(relu_output_rsci_bdwt_2)
    );
  assign relu_output_rsci_we_d_pff = relu_output_rsci_we_d_run_sct_iff;
  assign relu_output_rsci_re_d_pff = relu_output_rsci_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1 (
  clk, rstn, convolution_output_rsci_q_d, run_wen, run_wten, convolution_output_rsci_oswt,
      convolution_output_rsci_q_d_mxwt, convolution_output_rsci_we_d_pff, convolution_output_rsci_iswt0_1_pff,
      convolution_output_rsci_re_d_pff, convolution_output_rsci_oswt_pff
);
  input clk;
  input rstn;
  input [31:0] convolution_output_rsci_q_d;
  input run_wen;
  input run_wten;
  input convolution_output_rsci_oswt;
  output [31:0] convolution_output_rsci_q_d_mxwt;
  output convolution_output_rsci_we_d_pff;
  input convolution_output_rsci_iswt0_1_pff;
  output convolution_output_rsci_re_d_pff;
  input convolution_output_rsci_oswt_pff;


  // Interconnect Declarations
  wire convolution_output_rsci_biwt;
  wire convolution_output_rsci_bdwt;
  wire convolution_output_rsci_we_d_run_sct_iff;
  wire convolution_output_rsci_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_ctrl
      catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .convolution_output_rsci_oswt(convolution_output_rsci_oswt),
      .convolution_output_rsci_biwt(convolution_output_rsci_biwt),
      .convolution_output_rsci_bdwt(convolution_output_rsci_bdwt),
      .convolution_output_rsci_we_d_run_sct_pff(convolution_output_rsci_we_d_run_sct_iff),
      .convolution_output_rsci_iswt0_1_pff(convolution_output_rsci_iswt0_1_pff),
      .convolution_output_rsci_re_d_run_sct_pff(convolution_output_rsci_re_d_run_sct_iff),
      .convolution_output_rsci_oswt_pff(convolution_output_rsci_oswt_pff)
    );
  catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_dp
      catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_convolution_output_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .convolution_output_rsci_q_d(convolution_output_rsci_q_d),
      .convolution_output_rsci_q_d_mxwt(convolution_output_rsci_q_d_mxwt),
      .convolution_output_rsci_biwt(convolution_output_rsci_biwt),
      .convolution_output_rsci_bdwt(convolution_output_rsci_bdwt)
    );
  assign convolution_output_rsci_we_d_pff = convolution_output_rsci_we_d_run_sct_iff;
  assign convolution_output_rsci_re_d_pff = convolution_output_rsci_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1 (
  clk, rstn, current_image_rsci_q_d, run_wen, run_wten, current_image_rsci_oswt_1,
      current_image_rsci_q_d_mxwt, current_image_rsci_we_d_pff, current_image_rsci_iswt0_pff,
      current_image_rsci_re_d_pff, current_image_rsci_oswt_1_pff
);
  input clk;
  input rstn;
  input [31:0] current_image_rsci_q_d;
  input run_wen;
  input run_wten;
  input current_image_rsci_oswt_1;
  output [31:0] current_image_rsci_q_d_mxwt;
  output current_image_rsci_we_d_pff;
  input current_image_rsci_iswt0_pff;
  output current_image_rsci_re_d_pff;
  input current_image_rsci_oswt_1_pff;


  // Interconnect Declarations
  wire current_image_rsci_biwt_1;
  wire current_image_rsci_bdwt_2;
  wire current_image_rsci_we_d_run_sct_iff;
  wire current_image_rsci_re_d_run_sct_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_current_image_rsc_wait_ctrl
      catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_current_image_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .current_image_rsci_oswt_1(current_image_rsci_oswt_1),
      .current_image_rsci_biwt_1(current_image_rsci_biwt_1),
      .current_image_rsci_bdwt_2(current_image_rsci_bdwt_2),
      .current_image_rsci_we_d_run_sct_pff(current_image_rsci_we_d_run_sct_iff),
      .current_image_rsci_iswt0_pff(current_image_rsci_iswt0_pff),
      .current_image_rsci_re_d_run_sct_pff(current_image_rsci_re_d_run_sct_iff),
      .current_image_rsci_oswt_1_pff(current_image_rsci_oswt_1_pff)
    );
  catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_current_image_rsc_wait_dp
      catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_current_image_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .current_image_rsci_q_d(current_image_rsci_q_d),
      .current_image_rsci_q_d_mxwt(current_image_rsci_q_d_mxwt),
      .current_image_rsci_biwt_1(current_image_rsci_biwt_1),
      .current_image_rsci_bdwt_2(current_image_rsci_bdwt_2)
    );
  assign current_image_rsci_we_d_pff = current_image_rsci_we_d_run_sct_iff;
  assign current_image_rsci_re_d_pff = current_image_rsci_re_d_run_sct_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci (
  clk, rstn, output_image_channel_rsc_dat, output_image_channel_rsc_vld, output_image_channel_rsc_rdy,
      run_wen, output_image_channel_rsci_oswt, output_image_channel_rsci_wen_comp,
      output_image_channel_rsci_idat
);
  input clk;
  input rstn;
  output [31:0] output_image_channel_rsc_dat;
  output output_image_channel_rsc_vld;
  input output_image_channel_rsc_rdy;
  input run_wen;
  input output_image_channel_rsci_oswt;
  output output_image_channel_rsci_wen_comp;
  input [31:0] output_image_channel_rsci_idat;


  // Interconnect Declarations
  wire output_image_channel_rsci_biwt;
  wire output_image_channel_rsci_bdwt;
  wire output_image_channel_rsci_bcwt;
  wire output_image_channel_rsci_irdy;
  wire output_image_channel_rsci_ivld_run_sct;


  // Interconnect Declarations for Component Instantiations 
  ccs_out_wait_v1 #(.rscid(32'sd4),
  .width(32'sd32)) output_image_channel_rsci (
      .irdy(output_image_channel_rsci_irdy),
      .ivld(output_image_channel_rsci_ivld_run_sct),
      .idat(output_image_channel_rsci_idat),
      .rdy(output_image_channel_rsc_rdy),
      .vld(output_image_channel_rsc_vld),
      .dat(output_image_channel_rsc_dat)
    );
  catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci_output_image_channel_wait_ctrl
      catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci_output_image_channel_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .output_image_channel_rsci_oswt(output_image_channel_rsci_oswt),
      .output_image_channel_rsci_biwt(output_image_channel_rsci_biwt),
      .output_image_channel_rsci_bdwt(output_image_channel_rsci_bdwt),
      .output_image_channel_rsci_bcwt(output_image_channel_rsci_bcwt),
      .output_image_channel_rsci_irdy(output_image_channel_rsci_irdy),
      .output_image_channel_rsci_ivld_run_sct(output_image_channel_rsci_ivld_run_sct)
    );
  catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci_output_image_channel_wait_dp
      catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci_output_image_channel_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_image_channel_rsci_oswt(output_image_channel_rsci_oswt),
      .output_image_channel_rsci_wen_comp(output_image_channel_rsci_wen_comp),
      .output_image_channel_rsci_biwt(output_image_channel_rsci_biwt),
      .output_image_channel_rsci_bdwt(output_image_channel_rsci_bdwt),
      .output_image_channel_rsci_bcwt(output_image_channel_rsci_bcwt)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1 (
  clk, rstn, bias_memory_rsci_q_d, run_wen, run_wten, bias_memory_rsci_oswt, bias_memory_rsci_q_d_mxwt,
      bias_memory_rsci_re_d_pff, bias_memory_rsci_oswt_pff
);
  input clk;
  input rstn;
  input [31:0] bias_memory_rsci_q_d;
  input run_wen;
  input run_wten;
  input bias_memory_rsci_oswt;
  output [31:0] bias_memory_rsci_q_d_mxwt;
  output bias_memory_rsci_re_d_pff;
  input bias_memory_rsci_oswt_pff;


  // Interconnect Declarations
  wire bias_memory_rsci_biwt;
  wire bias_memory_rsci_bdwt;
  wire bias_memory_rsci_biwt_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .bias_memory_rsci_oswt(bias_memory_rsci_oswt),
      .bias_memory_rsci_biwt(bias_memory_rsci_biwt),
      .bias_memory_rsci_bdwt(bias_memory_rsci_bdwt),
      .bias_memory_rsci_biwt_pff(bias_memory_rsci_biwt_iff),
      .bias_memory_rsci_oswt_pff(bias_memory_rsci_oswt_pff)
    );
  catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1_bias_memory_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .bias_memory_rsci_q_d(bias_memory_rsci_q_d),
      .bias_memory_rsci_q_d_mxwt(bias_memory_rsci_q_d_mxwt),
      .bias_memory_rsci_biwt(bias_memory_rsci_biwt),
      .bias_memory_rsci_bdwt(bias_memory_rsci_bdwt)
    );
  assign bias_memory_rsci_re_d_pff = bias_memory_rsci_biwt_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1 (
  clk, rstn, weight_memory_rsci_q_d, run_wen, run_wten, weight_memory_rsci_oswt,
      weight_memory_rsci_q_d_mxwt, weight_memory_rsci_re_d_pff, weight_memory_rsci_oswt_pff
);
  input clk;
  input rstn;
  input [31:0] weight_memory_rsci_q_d;
  input run_wen;
  input run_wten;
  input weight_memory_rsci_oswt;
  output [31:0] weight_memory_rsci_q_d_mxwt;
  output weight_memory_rsci_re_d_pff;
  input weight_memory_rsci_oswt_pff;


  // Interconnect Declarations
  wire weight_memory_rsci_biwt;
  wire weight_memory_rsci_bdwt;
  wire weight_memory_rsci_biwt_iff;


  // Interconnect Declarations for Component Instantiations 
  catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_ctrl
      catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .run_wten(run_wten),
      .weight_memory_rsci_oswt(weight_memory_rsci_oswt),
      .weight_memory_rsci_biwt(weight_memory_rsci_biwt),
      .weight_memory_rsci_bdwt(weight_memory_rsci_bdwt),
      .weight_memory_rsci_biwt_pff(weight_memory_rsci_biwt_iff),
      .weight_memory_rsci_oswt_pff(weight_memory_rsci_oswt_pff)
    );
  catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_dp
      catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1_weight_memory_rsc_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .weight_memory_rsci_q_d(weight_memory_rsci_q_d),
      .weight_memory_rsci_q_d_mxwt(weight_memory_rsci_q_d_mxwt),
      .weight_memory_rsci_biwt(weight_memory_rsci_biwt),
      .weight_memory_rsci_bdwt(weight_memory_rsci_bdwt)
    );
  assign weight_memory_rsci_re_d_pff = weight_memory_rsci_biwt_iff;
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run_image_channel_rsci
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run_image_channel_rsci (
  clk, rstn, image_channel_rsc_dat, image_channel_rsc_vld, image_channel_rsc_rdy,
      run_wen, image_channel_rsci_oswt, image_channel_rsci_wen_comp, image_channel_rsci_idat_mxwt
);
  input clk;
  input rstn;
  input [31:0] image_channel_rsc_dat;
  input image_channel_rsc_vld;
  output image_channel_rsc_rdy;
  input run_wen;
  input image_channel_rsci_oswt;
  output image_channel_rsci_wen_comp;
  output [31:0] image_channel_rsci_idat_mxwt;


  // Interconnect Declarations
  wire image_channel_rsci_biwt;
  wire image_channel_rsci_bdwt;
  wire image_channel_rsci_bcwt;
  wire image_channel_rsci_irdy_run_sct;
  wire image_channel_rsci_ivld;
  wire [31:0] image_channel_rsci_idat;


  // Interconnect Declarations for Component Instantiations 
  ccs_in_wait_v1 #(.rscid(32'sd1),
  .width(32'sd32)) image_channel_rsci (
      .rdy(image_channel_rsc_rdy),
      .vld(image_channel_rsc_vld),
      .dat(image_channel_rsc_dat),
      .irdy(image_channel_rsci_irdy_run_sct),
      .ivld(image_channel_rsci_ivld),
      .idat(image_channel_rsci_idat)
    );
  catapult_conv2d_14_14_3_3_1_run_image_channel_rsci_image_channel_wait_ctrl catapult_conv2d_14_14_3_3_1_run_image_channel_rsci_image_channel_wait_ctrl_inst
      (
      .run_wen(run_wen),
      .image_channel_rsci_oswt(image_channel_rsci_oswt),
      .image_channel_rsci_biwt(image_channel_rsci_biwt),
      .image_channel_rsci_bdwt(image_channel_rsci_bdwt),
      .image_channel_rsci_bcwt(image_channel_rsci_bcwt),
      .image_channel_rsci_irdy_run_sct(image_channel_rsci_irdy_run_sct),
      .image_channel_rsci_ivld(image_channel_rsci_ivld)
    );
  catapult_conv2d_14_14_3_3_1_run_image_channel_rsci_image_channel_wait_dp catapult_conv2d_14_14_3_3_1_run_image_channel_rsci_image_channel_wait_dp_inst
      (
      .clk(clk),
      .rstn(rstn),
      .image_channel_rsci_oswt(image_channel_rsci_oswt),
      .image_channel_rsci_wen_comp(image_channel_rsci_wen_comp),
      .image_channel_rsci_idat_mxwt(image_channel_rsci_idat_mxwt),
      .image_channel_rsci_biwt(image_channel_rsci_biwt),
      .image_channel_rsci_bdwt(image_channel_rsci_bdwt),
      .image_channel_rsci_bcwt(image_channel_rsci_bcwt),
      .image_channel_rsci_idat(image_channel_rsci_idat)
    );
endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1_run
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1_run (
  clk, rstn, image_channel_rsc_dat, image_channel_rsc_vld, image_channel_rsc_rdy,
      weight_memory_triosy_lz, bias_memory_triosy_lz, output_image_channel_rsc_dat,
      output_image_channel_rsc_vld, output_image_channel_rsc_rdy, bias_rsc_dat, bias_triosy_lz,
      relu_rsc_dat, relu_triosy_lz, max_pool_rsc_dat, max_pool_triosy_lz, num_input_images_rsc_dat,
      num_input_images_triosy_lz, num_output_images_rsc_dat, num_output_images_triosy_lz,
      weight_memory_rsci_radr_d, weight_memory_rsci_q_d, bias_memory_rsci_radr_d,
      bias_memory_rsci_q_d, current_image_rsci_radr_d, current_image_rsci_wadr_d,
      current_image_rsci_d_d, current_image_rsci_q_d, convolution_output_rsci_radr_d,
      convolution_output_rsci_wadr_d, convolution_output_rsci_d_d, convolution_output_rsci_q_d,
      relu_output_rsci_radr_d, relu_output_rsci_wadr_d, relu_output_rsci_d_d, relu_output_rsci_q_d,
      output_buffer_rsci_radr_d, output_buffer_rsci_wadr_d, output_buffer_rsci_d_d,
      output_buffer_rsci_q_d, catapult_conv2d_14_14_3_3_1_stall, weight_memory_rsci_re_d_pff,
      bias_memory_rsci_re_d_pff, current_image_rsci_we_d_pff, current_image_rsci_re_d_pff,
      convolution_output_rsci_we_d_pff, convolution_output_rsci_re_d_pff, relu_output_rsci_we_d_pff,
      relu_output_rsci_re_d_pff, output_buffer_rsci_we_d_pff, output_buffer_rsci_re_d_pff
);
  input clk;
  input rstn;
  input [31:0] image_channel_rsc_dat;
  input image_channel_rsc_vld;
  output image_channel_rsc_rdy;
  output weight_memory_triosy_lz;
  output bias_memory_triosy_lz;
  output [31:0] output_image_channel_rsc_dat;
  output output_image_channel_rsc_vld;
  input output_image_channel_rsc_rdy;
  input bias_rsc_dat;
  output bias_triosy_lz;
  input relu_rsc_dat;
  output relu_triosy_lz;
  input max_pool_rsc_dat;
  output max_pool_triosy_lz;
  input [27:0] num_input_images_rsc_dat;
  output num_input_images_triosy_lz;
  input [27:0] num_output_images_rsc_dat;
  output num_output_images_triosy_lz;
  output [13:0] weight_memory_rsci_radr_d;
  input [31:0] weight_memory_rsci_q_d;
  output [9:0] bias_memory_rsci_radr_d;
  input [31:0] bias_memory_rsci_q_d;
  output [11:0] current_image_rsci_radr_d;
  output [11:0] current_image_rsci_wadr_d;
  output [31:0] current_image_rsci_d_d;
  input [31:0] current_image_rsci_q_d;
  output [7:0] convolution_output_rsci_radr_d;
  output [7:0] convolution_output_rsci_wadr_d;
  output [31:0] convolution_output_rsci_d_d;
  input [31:0] convolution_output_rsci_q_d;
  output [7:0] relu_output_rsci_radr_d;
  output [7:0] relu_output_rsci_wadr_d;
  output [31:0] relu_output_rsci_d_d;
  input [31:0] relu_output_rsci_q_d;
  output [7:0] output_buffer_rsci_radr_d;
  output [7:0] output_buffer_rsci_wadr_d;
  output [31:0] output_buffer_rsci_d_d;
  input [31:0] output_buffer_rsci_q_d;
  input catapult_conv2d_14_14_3_3_1_stall;
  output weight_memory_rsci_re_d_pff;
  output bias_memory_rsci_re_d_pff;
  output current_image_rsci_we_d_pff;
  output current_image_rsci_re_d_pff;
  output convolution_output_rsci_we_d_pff;
  output convolution_output_rsci_re_d_pff;
  output relu_output_rsci_we_d_pff;
  output relu_output_rsci_re_d_pff;
  output output_buffer_rsci_we_d_pff;
  output output_buffer_rsci_re_d_pff;


  // Interconnect Declarations
  wire run_wen;
  wire run_wten;
  wire image_channel_rsci_wen_comp;
  wire [31:0] image_channel_rsci_idat_mxwt;
  wire [31:0] weight_memory_rsci_q_d_mxwt;
  wire [31:0] bias_memory_rsci_q_d_mxwt;
  wire output_image_channel_rsci_wen_comp;
  reg [31:0] output_image_channel_rsci_idat;
  wire bias_rsci_idat;
  wire relu_rsci_idat;
  wire max_pool_rsci_idat;
  wire [27:0] num_input_images_rsci_idat;
  wire [27:0] num_output_images_rsci_idat;
  wire [31:0] current_image_rsci_q_d_mxwt;
  wire [31:0] convolution_output_rsci_q_d_mxwt;
  wire [31:0] relu_output_rsci_q_d_mxwt;
  wire [31:0] output_buffer_rsci_q_d_mxwt;
  wire [24:0] fsm_output;
  wire [3:0] outer_conv_loop_2_operator_28_true_acc_tmp;
  wire [4:0] nl_outer_conv_loop_2_operator_28_true_acc_tmp;
  wire [3:0] catapult_conv2d_14_14_3_3_1_convolve_do_acc_11_tmp;
  wire [4:0] nl_catapult_conv2d_14_14_3_3_1_convolve_do_acc_11_tmp;
  wire or_dcpl_7;
  wire and_dcpl_1;
  wire and_dcpl_30;
  wire and_dcpl_50;
  wire or_dcpl_53;
  wire or_dcpl_54;
  wire or_dcpl_55;
  wire or_dcpl_57;
  wire or_dcpl_59;
  wire or_dcpl_61;
  wire or_dcpl_62;
  wire or_dcpl_65;
  wire or_dcpl_80;
  wire or_dcpl_83;
  wire and_dcpl_77;
  wire and_dcpl_80;
  wire and_dcpl_94;
  wire or_dcpl_107;
  wire or_dcpl_110;
  wire or_dcpl_111;
  wire or_dcpl_123;
  wire or_dcpl_126;
  wire and_dcpl_108;
  wire or_tmp_152;
  wire or_tmp_191;
  wire and_237_cse;
  wire and_510_cse;
  reg catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1;
  wire exit_main_loop_1_sva_mx0;
  reg operator_28_true_2_slc_operator_28_true_2_acc_6_itm;
  reg catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_5_itm;
  reg [3:0] catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva;
  wire main_loop_3_and_unfl_sva_1;
  wire main_loop_3_nor_ovfl_sva_1;
  reg [31:0] catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm;
  reg [31:0] catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_and_unfl_sva_1;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_nor_ovfl_sva_1;
  reg catapult_conv2d_14_14_3_3_1_convolve_do_pcnxt_sva;
  wire [8:0] catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_sva_3;
  wire [9:0] nl_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_sva_3;
  reg [27:0] inner_loop_i_sva;
  reg load_images_loop_acc_2_cse_sva_28;
  reg catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2;
  reg catapult_conv2d_14_14_3_3_1_convolve_do_stage_0;
  reg main_loop_2_asn_71_itm_1;
  reg [3:0] main_loop_2_index_3_0_sva;
  reg catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_1;
  reg max_pool_sva;
  reg catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_3;
  reg catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_2;
  reg relu_sva;
  reg catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_2;
  reg catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_itm_1;
  reg bias_sva;
  wire [32:0] catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1;
  wire [33:0] nl_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1;
  wire [48:0] catapult_conv2d_14_14_3_3_1_convolve_do_slc_64_16_sat_sva_1;
  reg [4:0] reg_inner_loop_acc_4_sdt_ftd_6;
  wire or_392_tmp;
  wire num_output_images_and_cse;
  wire shift_register_and_1_cse;
  reg reg_num_output_images_triosy_obj_iswt0_cse;
  reg reg_output_buffer_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_relu_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_convolution_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_current_image_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse;
  reg reg_output_image_channel_rsci_iswt0_cse;
  reg reg_bias_memory_rsci_iswt0_cse;
  reg reg_weight_memory_rsci_iswt0_cse;
  reg reg_image_channel_rsci_iswt0_cse;
  wire catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_nor_cse;
  wire or_72_cse;
  wire catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_and_ssc;
  wire catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_nor_cse;
  wire and_375_m1c;
  wire [5:0] load_images_loop_acc_6_sdt;
  wire [6:0] nl_load_images_loop_acc_6_sdt;
  wire or_320_ssc;
  wire and_463_ssc;
  wire and_465_ssc;
  wire or_139_cse;
  wire nor_47_cse;
  wire weight_memory_rsci_re_d_iff;
  wire bias_memory_rsci_re_d_iff;
  wire and_397_rmff;
  wire [10:0] main_loop_mux1h_1_rmff;
  wire [11:0] nl_main_loop_mux1h_1_rmff;
  wire current_image_rsci_we_d_iff;
  wire current_image_rsci_re_d_iff;
  wire and_393_rmff;
  wire convolution_output_rsci_we_d_iff;
  wire convolution_output_rsci_re_d_iff;
  wire or_285_rmff;
  wire relu_output_rsci_we_d_iff;
  wire relu_output_rsci_re_d_iff;
  wire or_283_rmff;
  wire output_buffer_rsci_we_d_iff;
  wire output_buffer_rsci_re_d_iff;
  wire and_372_rmff;
  reg [13:0] weight_index_13_0_lpi_3;
  reg [9:0] bias_index_9_0_sva;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_if_1_and_unfl_sva_1;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_ovfl_sva_1;
  wire main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1;
  wire main_loop_4_qif_slc_bias_output_32_31_0_ncse_0_sva_1;
  reg [4:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0;
  reg catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_5;
  wire catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva_mx0c1;
  wire and_608_cse;
  wire [9:0] z_out;
  wire [10:0] nl_z_out;
  wire [32:0] z_out_1;
  wire [27:0] z_out_2;
  wire [28:0] nl_z_out_2;
  wire [8:0] z_out_3;
  wire [9:0] nl_z_out_3;
  wire [8:0] z_out_5;
  wire [9:0] nl_z_out_5;
  wire [3:0] z_out_7;
  wire [4:0] nl_z_out_7;
  wire or_tmp_271;
  wire [6:0] z_out_9;
  wire [7:0] nl_z_out_9;
  wire [6:0] z_out_10;
  wire [7:0] nl_z_out_10;
  reg [31:0] current_filter_5_lpi_2;
  reg [31:0] current_filter_4_lpi_2;
  reg [31:0] current_filter_6_lpi_2;
  reg [31:0] current_filter_3_lpi_2;
  reg [31:0] current_filter_7_lpi_2;
  reg [31:0] current_filter_2_lpi_2;
  reg [31:0] current_filter_8_lpi_2;
  reg [31:0] current_filter_1_lpi_2;
  reg [31:0] shift_register_2_lpi_2;
  reg [31:0] shift_register_3_lpi_2;
  reg [31:0] shift_register_4_lpi_2;
  reg [31:0] shift_register_5_lpi_2;
  reg [31:0] shift_register_6_lpi_2;
  reg [31:0] shift_register_7_lpi_2;
  reg [31:0] shift_register_8_lpi_2;
  reg [31:0] shift_register_9_lpi_2;
  reg [31:0] shift_register_10_lpi_2;
  reg [31:0] shift_register_11_lpi_2;
  reg [31:0] shift_register_12_lpi_2;
  reg [31:0] shift_register_13_lpi_2;
  reg [31:0] shift_register_14_lpi_2;
  reg [31:0] shift_register_15_lpi_2;
  reg [31:0] shift_register_16_lpi_2;
  reg [31:0] shift_register_17_lpi_2;
  reg [31:0] shift_register_18_lpi_2;
  reg [31:0] shift_register_19_lpi_2;
  reg [31:0] shift_register_20_lpi_2;
  reg [31:0] shift_register_21_lpi_2;
  reg [31:0] shift_register_22_lpi_2;
  reg [31:0] shift_register_23_lpi_2;
  reg [31:0] shift_register_24_lpi_2;
  reg [31:0] shift_register_25_lpi_2;
  reg [31:0] shift_register_26_lpi_2;
  reg [31:0] shift_register_27_lpi_2;
  reg [31:0] shift_register_28_lpi_2;
  reg [31:0] shift_register_29_lpi_2;
  reg [31:0] catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_2;
  reg [31:0] max_pool_shift_register_4_lpi_2;
  reg [31:0] max_pool_shift_register_5_lpi_2;
  reg [31:0] max_pool_shift_register_6_lpi_2;
  reg [31:0] max_pool_shift_register_7_lpi_2;
  reg [31:0] max_pool_shift_register_8_lpi_2;
  reg [31:0] max_pool_shift_register_9_lpi_2;
  reg [31:0] max_pool_shift_register_10_lpi_2;
  reg [31:0] max_pool_shift_register_11_lpi_2;
  reg [31:0] max_pool_shift_register_12_lpi_2;
  reg [31:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_asn_1_ncse_lpi_2;
  reg [31:0] max_pool_shift_register_13_lpi_2;
  reg [31:0] max_pool_shift_register_15_lpi_2;
  reg [27:0] num_input_images_sva;
  reg [27:0] num_output_images_sva;
  reg [27:0] main_loop_1_o_sva;
  reg [31:0] current_filter_0_lpi_4;
  reg [31:0] shift_register_1_lpi_4;
  reg [63:0] inner_conv_loop_qr_8_sva_1;
  reg catapult_conv2d_14_14_3_3_1_in_bounds_return_3_lpi_4_dfm_1;
  reg catapult_conv2d_14_14_3_3_1_in_bounds_return_4_lpi_4_dfm_1;
  reg catapult_conv2d_14_14_3_3_1_in_bounds_return_1_lpi_4_dfm_1;
  reg catapult_conv2d_14_14_3_3_1_in_bounds_return_2_lpi_4_dfm_1;
  reg catapult_conv2d_14_14_3_3_1_in_bounds_return_lpi_4_dfm_1_1;
  reg [31:0] inner_conv_loop_qif_asn_17_itm_1;
  reg [31:0] inner_conv_loop_qif_asn_20_itm_1;
  reg [31:0] inner_conv_loop_qif_asn_21_itm_1;
  reg [63:0] inner_conv_loop_acc_6_itm_1;
  wire [64:0] nl_inner_conv_loop_acc_6_itm_1;
  reg [63:0] inner_conv_loop_inner_conv_loop_and_itm_1;
  reg [63:0] inner_conv_loop_inner_conv_loop_and_itm_2;
  reg [63:0] inner_conv_loop_inner_conv_loop_and_2_itm_1;
  reg [63:0] inner_conv_loop_acc_5_itm_1;
  wire [64:0] nl_inner_conv_loop_acc_5_itm_1;
  reg [63:0] inner_conv_loop_inner_conv_loop_and_4_itm_1;
  reg [63:0] inner_conv_loop_acc_4_itm_1;
  wire [64:0] nl_inner_conv_loop_acc_4_itm_1;
  reg [63:0] inner_conv_loop_acc_3_itm_1;
  wire [64:0] nl_inner_conv_loop_acc_3_itm_1;
  reg catapult_conv2d_14_14_3_3_1_convolve_do_slc_catapult_conv2d_14_14_3_3_1_convolve_do_acc_6_svs_st_1;
  wire shift_register_29_lpi_2_mx0c1;
  wire [31:0] catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_4_dfm_mx1w0;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_2_mx1c1;
  wire inner_loop_i_sva_mx0c1;
  wire [10:0] load_images_loop_acc_4_sdt_1;
  wire [11:0] nl_load_images_loop_acc_4_sdt_1;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_mx0w0;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_mx0c1;
  wire catapult_conv2d_14_14_3_3_1_convolve_count_7_0_sva_mx0c0;
  wire catapult_conv2d_14_14_3_3_1_convolve_count_7_0_sva_mx0c4;
  wire [7:0] main_loop_3_i_7_0_sva_1_mx1w0;
  wire [8:0] nl_main_loop_3_i_7_0_sva_1_mx1w0;
  wire catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm_mx0c0;
  wire catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1_mx0c1;
  wire [4:0] catapult_conv2d_14_14_3_3_1_convolve_col_4_0_sva_3;
  wire [5:0] nl_catapult_conv2d_14_14_3_3_1_convolve_col_4_0_sva_3;
  reg catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_7;
  reg [6:0] catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_6_0;
  reg catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_5;
  reg [4:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_4_0;
  wire operator_28_true_2_or_cse;
  wire or_184_cse;
  reg catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_7;
  reg [6:0] catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_6_0;
  wire max_pool_shift_register_and_11_cse;
  wire catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_and_3_cse;
  wire nand_43_itm;
  wire outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_3_1;
  wire [48:0] outer_conv_loop_3_inner_conv_loop_3_acc_1_itm_63_15_1;
  wire outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_itm_3_1;
  wire outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_itm_3_1;
  wire load_images_loop_acc_7_itm_28_1;
  wire catapult_conv2d_14_14_3_3_1_max_max_val_acc_itm_32_1;
  wire catapult_conv2d_14_14_3_3_1_convolve_count_and_ssc;
  reg reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd;
  reg [6:0] reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1;
  reg [1:0] reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd;
  reg [6:0] reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1;
  wire z_out_4_6;
  wire z_out_6_28;
  wire z_out_8_4;
  wire catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux_cse;

  wire[13:0] main_loop_2_acc_nl;
  wire[14:0] nl_main_loop_2_acc_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_mux_nl;
  wire reg_catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_rgt_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_or_4_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_or_5_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_or_6_nl;
  wire[9:0] operator_28_true_4_acc_nl;
  wire[10:0] nl_operator_28_true_4_acc_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_aelse_mux_19_nl;
  wire or_314_nl;
  wire[6:0] main_loop_index_main_loop_index_mux_nl;
  wire[6:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_and_nl;
  wire[6:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_mux1h_nl;
  wire or_385_nl;
  wire nand_46_nl;
  wire main_loop_index_or_1_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_count_not_1_nl;
  wire[5:0] load_images_loop_acc_9_nl;
  wire[6:0] nl_load_images_loop_acc_9_nl;
  wire[3:0] catapult_conv2d_14_14_3_3_1_convolve_row_mux_nl;
  wire operator_28_true_operator_28_true_nor_nl;
  wire[31:0] catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_and_nl;
  wire catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qelse_mux_1_nl;
  wire[3:0] mux_nl;
  wire nor_71_nl;
  wire[1:0] catapult_conv2d_14_14_3_3_1_convolve_center_pixel_or_2_nl;
  wire[1:0] catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux_1_nl;
  wire[6:0] catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux1h_14_nl;
  wire[6:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_and_nl;
  wire[6:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_mux_nl;
  wire nand_42_nl;
  wire or_349_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_aelse_mux_nl;
  wire or_362_nl;
  wire main_loop_4_mux_1_nl;
  wire[6:0] main_loop_4_mux_2_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_center_pixel_not_4_nl;
  wire[31:0] catapult_conv2d_14_14_3_3_1_convolve_do_if_catapult_conv2d_14_14_3_3_1_convolve_do_if_and_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_first_not_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_and_2_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_and_3_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_6_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_or_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_8_nl;
  wire[63:0] outer_conv_loop_3_inner_conv_loop_2_qif_mul_nl;
  wire signed [63:0] nl_outer_conv_loop_3_inner_conv_loop_2_qif_mul_sgnd;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_9_nl;
  wire[63:0] outer_conv_loop_3_inner_conv_loop_3_qif_mul_nl;
  wire signed [63:0] nl_outer_conv_loop_3_inner_conv_loop_3_qif_mul_sgnd;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_3_nl;
  wire[63:0] outer_conv_loop_1_inner_conv_loop_3_qif_mul_nl;
  wire signed [63:0] nl_outer_conv_loop_1_inner_conv_loop_3_qif_mul_sgnd;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_5_nl;
  wire operator_28_true_not_nl;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_6_nl;
  wire[63:0] outer_conv_loop_2_inner_conv_loop_3_qif_mul_nl;
  wire signed [63:0] nl_outer_conv_loop_2_inner_conv_loop_3_qif_mul_sgnd;
  wire[63:0] inner_conv_loop_inner_conv_loop_and_7_nl;
  wire[63:0] outer_conv_loop_3_inner_conv_loop_1_qif_mul_nl;
  wire signed [63:0] nl_outer_conv_loop_3_inner_conv_loop_1_qif_mul_sgnd;
  wire[63:0] outer_conv_loop_2_inner_conv_loop_1_qif_mul_nl;
  wire signed [63:0] nl_outer_conv_loop_2_inner_conv_loop_1_qif_mul_sgnd;
  wire operator_28_true_not_2_nl;
  wire signed [63:0] nl_outer_conv_loop_2_inner_conv_loop_2_qif_mul_sgnd;
  wire[63:0] outer_conv_loop_1_inner_conv_loop_2_qif_mul_nl;
  wire signed [63:0] nl_outer_conv_loop_1_inner_conv_loop_2_qif_mul_sgnd;
  wire inner_conv_loop_not_14_nl;
  wire[3:0] outer_conv_loop_1_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl;
  wire[4:0] nl_outer_conv_loop_1_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl;
  wire[2:0] outer_conv_loop_1_operator_28_true_acc_nl;
  wire[3:0] nl_outer_conv_loop_1_operator_28_true_acc_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_nor_nl;
  wire[63:0] outer_conv_loop_1_inner_conv_loop_1_qif_mul_nl;
  wire signed [63:0] nl_outer_conv_loop_1_inner_conv_loop_1_qif_mul_sgnd;
  wire catapult_conv2d_14_14_3_3_1_in_bounds_catapult_conv2d_14_14_3_3_1_in_bounds_catapult_conv2d_14_14_3_3_1_in_bounds_nor_nl;
  wire catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux1h_5_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_or_1_nl;
  wire or_379_nl;
  wire[4:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_mux1h_nl;
  wire catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_nor_nl;
  wire nor_72_nl;
  wire[28:0] main_loop_1_acc_nl;
  wire[29:0] nl_main_loop_1_acc_nl;
  wire[3:0] outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl;
  wire[4:0] nl_outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_nor_2_nl;
  wire[29:0] catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_nor_nl;
  wire[29:0] catapult_conv2d_14_14_3_3_1_convolve_do_nor_2_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_nor_1_nl;
  wire[49:0] catapult_conv2d_14_14_3_3_1_convolve_do_acc_12_nl;
  wire[50:0] nl_catapult_conv2d_14_14_3_3_1_convolve_do_acc_12_nl;
  wire[63:0] outer_conv_loop_3_inner_conv_loop_3_acc_1_nl;
  wire[66:0] nl_outer_conv_loop_3_inner_conv_loop_3_acc_1_nl;
  wire[3:0] outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl;
  wire[4:0] nl_outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl;
  wire[3:0] outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_nl;
  wire[4:0] nl_outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_nl;
  wire[2:0] operator_28_true_1_acc_nl;
  wire[3:0] nl_operator_28_true_1_acc_nl;
  wire[28:0] load_images_loop_acc_7_nl;
  wire[29:0] nl_load_images_loop_acc_7_nl;
  wire[32:0] catapult_conv2d_14_14_3_3_1_max_max_val_acc_nl;
  wire[33:0] nl_catapult_conv2d_14_14_3_3_1_max_max_val_acc_nl;
  wire[31:0] catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_5_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_1_nl;
  wire[6:0] catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_8_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_if_1_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_2_nl;
  wire[29:0] catapult_conv2d_14_14_3_3_1_convolve_do_if_1_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_nl;
  wire[29:0] catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_1_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_if_1_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_1_nl;
  wire catapult_conv2d_14_14_3_3_1_convolve_do_aelse_catapult_conv2d_14_14_3_3_1_convolve_do_aelse_or_nl;
  wire main_loop_4_main_loop_4_and_nl;
  wire[29:0] mux_16_nl;
  wire[29:0] main_loop_3_main_loop_3_nor_nl;
  wire[29:0] main_loop_3_nor_1_nl;
  wire nand_47_nl;
  wire catapult_conv2d_14_14_3_3_1_relu_fn_mux_nl;
  wire catapult_conv2d_14_14_3_3_1_relu_fn_catapult_conv2d_14_14_3_3_1_relu_fn_and_2_nl;
  wire[1:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_and_nl;
  wire catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_mux_2_nl;
  wire[3:0] catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_mux_3_nl;
  wire catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_mux_1_nl;
  wire and_373_nl;
  wire catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qelse_or_1_nl;
  wire catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qelse_and_3_nl;
  wire[9:0] catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qif_mux_1_nl;
  wire[33:0] acc_1_nl;
  wire[34:0] nl_acc_1_nl;
  wire[31:0] catapult_conv2d_14_14_3_3_1_max_cd_mux1h_2_nl;
  wire catapult_conv2d_14_14_3_3_1_max_cd_or_1_nl;
  wire[31:0] catapult_conv2d_14_14_3_3_1_max_cd_mux1h_3_nl;
  wire[27:0] load_images_loop_mux_3_nl;
  wire[6:0] main_loop_5_mux_3_nl;
  wire[6:0] operator_28_true_2_acc_nl;
  wire[7:0] nl_operator_28_true_2_acc_nl;
  wire[5:0] operator_28_true_2_mux_1_nl;
  wire[7:0] catapult_conv2d_14_14_3_3_1_send_results_end_mux_2_nl;
  wire[28:0] load_images_loop_acc_nl;
  wire[29:0] nl_load_images_loop_acc_nl;
  wire[27:0] load_images_loop_mux_4_nl;
  wire[3:0] main_loop_2_mux_9_nl;
  wire main_loop_2_and_8_nl;
  wire[4:0] operator_28_true_1_acc_nl_1;
  wire[5:0] nl_operator_28_true_1_acc_nl_1;
  wire[3:0] operator_28_true_1_mux_1_nl;
  wire[2:0] operator_28_true_operator_28_true_and_1_nl;
  wire[2:0] operator_28_true_mux_1_nl;
  wire not_303_nl;
  wire[3:0] operator_28_true_mux1h_3_nl;
  wire operator_28_true_operator_28_true_or_3_nl;
  wire[1:0] operator_28_true_operator_28_true_or_4_nl;
  wire not_305_nl;
  wire[6:0] catapult_conv2d_14_14_3_3_1_convolve_do_mux_5_nl;

  // Interconnect Declarations for Component Instantiations 
  wire  nl_catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_inst_current_image_rsci_iswt0_pff;
  assign nl_catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_inst_current_image_rsci_iswt0_pff
      = catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2 & (fsm_output[2]);
  wire  nl_catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_inst_convolution_output_rsci_iswt0_1_pff;
  assign nl_catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_inst_convolution_output_rsci_iswt0_1_pff
      = catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_3 & (~ catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_2)
      & (fsm_output[8]);
  wire  nl_catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_inst_relu_output_rsci_iswt0_pff;
  assign nl_catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_inst_relu_output_rsci_iswt0_pff
      = catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2 & (fsm_output[12]);
  wire  nl_catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_inst_output_buffer_rsci_iswt0_pff;
  assign nl_catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_inst_output_buffer_rsci_iswt0_pff
      = (catapult_conv2d_14_14_3_3_1_convolve_do_stage_0 & (fsm_output[15])) | (fsm_output[18]);
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_C_0_tr0;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_C_0_tr0 = ~ z_out_6_28;
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_load_images_loop_C_1_tr0;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_load_images_loop_C_1_tr0
      = ~ load_images_loop_acc_7_itm_28_1;
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_1_C_0_tr0;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_1_C_0_tr0 = ~
      load_images_loop_acc_2_cse_sva_28;
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_2_C_0_tr0;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_2_C_0_tr0 = ~
      catapult_conv2d_14_14_3_3_1_convolve_do_stage_0;
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_catapult_conv2d_14_14_3_3_1_convolve_do_C_0_tr0;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_catapult_conv2d_14_14_3_3_1_convolve_do_C_0_tr0
      = ~(catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_3 | catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2
      | catapult_conv2d_14_14_3_3_1_convolve_do_stage_0);
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_inner_loop_C_1_tr0;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_inner_loop_C_1_tr0 = ~ load_images_loop_acc_7_itm_28_1;
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_3_C_0_tr0;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_3_C_0_tr0 = and_dcpl_30
      & max_pool_sva;
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_3_C_0_tr1;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_3_C_0_tr1 = and_dcpl_30
      & (~ max_pool_sva);
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3_tr0;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3_tr0
      = ~ operator_28_true_2_slc_operator_28_true_2_acc_6_itm;
  wire  nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_copy_loop_C_0_tr0;
  assign nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_copy_loop_C_0_tr0 = (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1==7'b1100001);
  ccs_in_v1 #(.rscid(32'sd5),
  .width(32'sd1)) bias_rsci (
      .dat(bias_rsc_dat),
      .idat(bias_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd6),
  .width(32'sd1)) relu_rsci (
      .dat(relu_rsc_dat),
      .idat(relu_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd7),
  .width(32'sd1)) max_pool_rsci (
      .dat(max_pool_rsc_dat),
      .idat(max_pool_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd8),
  .width(32'sd28)) num_input_images_rsci (
      .dat(num_input_images_rsc_dat),
      .idat(num_input_images_rsci_idat)
    );
  ccs_in_v1 #(.rscid(32'sd9),
  .width(32'sd28)) num_output_images_rsci (
      .dat(num_output_images_rsc_dat),
      .idat(num_output_images_rsci_idat)
    );
  catapult_conv2d_14_14_3_3_1_run_image_channel_rsci catapult_conv2d_14_14_3_3_1_run_image_channel_rsci_inst
      (
      .clk(clk),
      .rstn(rstn),
      .image_channel_rsc_dat(image_channel_rsc_dat),
      .image_channel_rsc_vld(image_channel_rsc_vld),
      .image_channel_rsc_rdy(image_channel_rsc_rdy),
      .run_wen(run_wen),
      .image_channel_rsci_oswt(reg_image_channel_rsci_iswt0_cse),
      .image_channel_rsci_wen_comp(image_channel_rsci_wen_comp),
      .image_channel_rsci_idat_mxwt(image_channel_rsci_idat_mxwt)
    );
  catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1 catapult_conv2d_14_14_3_3_1_run_weight_memory_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .weight_memory_rsci_q_d(weight_memory_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .weight_memory_rsci_oswt(reg_weight_memory_rsci_iswt0_cse),
      .weight_memory_rsci_q_d_mxwt(weight_memory_rsci_q_d_mxwt),
      .weight_memory_rsci_re_d_pff(weight_memory_rsci_re_d_iff),
      .weight_memory_rsci_oswt_pff(or_tmp_152)
    );
  catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1 catapult_conv2d_14_14_3_3_1_run_bias_memory_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .bias_memory_rsci_q_d(bias_memory_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .bias_memory_rsci_oswt(reg_bias_memory_rsci_iswt0_cse),
      .bias_memory_rsci_q_d_mxwt(bias_memory_rsci_q_d_mxwt),
      .bias_memory_rsci_re_d_pff(bias_memory_rsci_re_d_iff),
      .bias_memory_rsci_oswt_pff(and_397_rmff)
    );
  catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci catapult_conv2d_14_14_3_3_1_run_output_image_channel_rsci_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_image_channel_rsc_dat(output_image_channel_rsc_dat),
      .output_image_channel_rsc_vld(output_image_channel_rsc_vld),
      .output_image_channel_rsc_rdy(output_image_channel_rsc_rdy),
      .run_wen(run_wen),
      .output_image_channel_rsci_oswt(reg_output_image_channel_rsci_iswt0_cse),
      .output_image_channel_rsci_wen_comp(output_image_channel_rsci_wen_comp),
      .output_image_channel_rsci_idat(output_image_channel_rsci_idat)
    );
  catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1 catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .current_image_rsci_q_d(current_image_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .current_image_rsci_oswt_1(reg_current_image_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .current_image_rsci_q_d_mxwt(current_image_rsci_q_d_mxwt),
      .current_image_rsci_we_d_pff(current_image_rsci_we_d_iff),
      .current_image_rsci_iswt0_pff(nl_catapult_conv2d_14_14_3_3_1_run_current_image_rsci_1_inst_current_image_rsci_iswt0_pff),
      .current_image_rsci_re_d_pff(current_image_rsci_re_d_iff),
      .current_image_rsci_oswt_1_pff(and_393_rmff)
    );
  catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1 catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .convolution_output_rsci_q_d(convolution_output_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .convolution_output_rsci_oswt(reg_convolution_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .convolution_output_rsci_q_d_mxwt(convolution_output_rsci_q_d_mxwt),
      .convolution_output_rsci_we_d_pff(convolution_output_rsci_we_d_iff),
      .convolution_output_rsci_iswt0_1_pff(nl_catapult_conv2d_14_14_3_3_1_run_convolution_output_rsci_1_inst_convolution_output_rsci_iswt0_1_pff),
      .convolution_output_rsci_re_d_pff(convolution_output_rsci_re_d_iff),
      .convolution_output_rsci_oswt_pff(or_285_rmff)
    );
  catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1 catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .relu_output_rsci_q_d(relu_output_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .relu_output_rsci_oswt_1(reg_relu_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .relu_output_rsci_q_d_mxwt(relu_output_rsci_q_d_mxwt),
      .relu_output_rsci_we_d_pff(relu_output_rsci_we_d_iff),
      .relu_output_rsci_iswt0_pff(nl_catapult_conv2d_14_14_3_3_1_run_relu_output_rsci_1_inst_relu_output_rsci_iswt0_pff),
      .relu_output_rsci_re_d_pff(relu_output_rsci_re_d_iff),
      .relu_output_rsci_oswt_1_pff(or_283_rmff)
    );
  catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1 catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_inst
      (
      .clk(clk),
      .rstn(rstn),
      .output_buffer_rsci_q_d(output_buffer_rsci_q_d),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .output_buffer_rsci_oswt_1(reg_output_buffer_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse),
      .output_buffer_rsci_q_d_mxwt(output_buffer_rsci_q_d_mxwt),
      .output_buffer_rsci_we_d_pff(output_buffer_rsci_we_d_iff),
      .output_buffer_rsci_iswt0_pff(nl_catapult_conv2d_14_14_3_3_1_run_output_buffer_rsci_1_inst_output_buffer_rsci_iswt0_pff),
      .output_buffer_rsci_re_d_pff(output_buffer_rsci_re_d_iff),
      .output_buffer_rsci_oswt_1_pff(and_372_rmff)
    );
  catapult_conv2d_14_14_3_3_1_run_weight_memory_triosy_obj catapult_conv2d_14_14_3_3_1_run_weight_memory_triosy_obj_inst
      (
      .weight_memory_triosy_lz(weight_memory_triosy_lz),
      .run_wten(run_wten),
      .weight_memory_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_14_14_3_3_1_run_bias_memory_triosy_obj catapult_conv2d_14_14_3_3_1_run_bias_memory_triosy_obj_inst
      (
      .bias_memory_triosy_lz(bias_memory_triosy_lz),
      .run_wten(run_wten),
      .bias_memory_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_14_14_3_3_1_run_bias_triosy_obj catapult_conv2d_14_14_3_3_1_run_bias_triosy_obj_inst
      (
      .bias_triosy_lz(bias_triosy_lz),
      .run_wten(run_wten),
      .bias_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_14_14_3_3_1_run_relu_triosy_obj catapult_conv2d_14_14_3_3_1_run_relu_triosy_obj_inst
      (
      .relu_triosy_lz(relu_triosy_lz),
      .run_wten(run_wten),
      .relu_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_14_14_3_3_1_run_max_pool_triosy_obj catapult_conv2d_14_14_3_3_1_run_max_pool_triosy_obj_inst
      (
      .max_pool_triosy_lz(max_pool_triosy_lz),
      .run_wten(run_wten),
      .max_pool_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_14_14_3_3_1_run_num_input_images_triosy_obj catapult_conv2d_14_14_3_3_1_run_num_input_images_triosy_obj_inst
      (
      .num_input_images_triosy_lz(num_input_images_triosy_lz),
      .run_wten(run_wten),
      .num_input_images_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_14_14_3_3_1_run_num_output_images_triosy_obj catapult_conv2d_14_14_3_3_1_run_num_output_images_triosy_obj_inst
      (
      .num_output_images_triosy_lz(num_output_images_triosy_lz),
      .run_wten(run_wten),
      .num_output_images_triosy_obj_iswt0(reg_num_output_images_triosy_obj_iswt0_cse)
    );
  catapult_conv2d_14_14_3_3_1_run_staller catapult_conv2d_14_14_3_3_1_run_staller_inst
      (
      .clk(clk),
      .rstn(rstn),
      .catapult_conv2d_14_14_3_3_1_stall(catapult_conv2d_14_14_3_3_1_stall),
      .run_wen(run_wen),
      .run_wten(run_wten),
      .image_channel_rsci_wen_comp(image_channel_rsci_wen_comp),
      .output_image_channel_rsci_wen_comp(output_image_channel_rsci_wen_comp)
    );
  catapult_conv2d_14_14_3_3_1_run_run_fsm catapult_conv2d_14_14_3_3_1_run_run_fsm_inst
      (
      .clk(clk),
      .rstn(rstn),
      .run_wen(run_wen),
      .fsm_output(fsm_output),
      .main_C_0_tr0(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_C_0_tr0),
      .main_loop_C_0_tr0(and_dcpl_30),
      .load_images_loop_C_1_tr0(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_load_images_loop_C_1_tr0),
      .main_C_1_tr0(exit_main_loop_1_sva_mx0),
      .main_loop_1_C_0_tr0(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_1_C_0_tr0),
      .main_loop_2_C_0_tr0(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_2_C_0_tr0),
      .catapult_conv2d_14_14_3_3_1_convolve_do_C_0_tr0(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_catapult_conv2d_14_14_3_3_1_convolve_do_C_0_tr0),
      .inner_loop_C_1_tr0(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_inner_loop_C_1_tr0),
      .main_loop_3_C_0_tr0(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_3_C_0_tr0),
      .main_loop_3_C_0_tr1(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_main_loop_3_C_0_tr1),
      .catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3_tr0(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_C_3_tr0),
      .stride_loop_C_2_tr0(catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1),
      .copy_loop_C_0_tr0(nl_catapult_conv2d_14_14_3_3_1_run_run_fsm_inst_copy_loop_C_0_tr0),
      .main_loop_5_C_0_tr0(and_dcpl_30),
      .main_loop_1_C_4_tr0(exit_main_loop_1_sva_mx0)
    );
  assign num_output_images_and_cse = run_wen & (~ and_dcpl_50);
  assign shift_register_and_1_cse = run_wen & (~(and_237_cse | ((~(catapult_conv2d_14_14_3_3_1_convolve_do_stage_0
      & (z_out_10[6]))) & (fsm_output[8]))));
  assign or_72_cse = (~ catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2) | catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_1;
  assign max_pool_shift_register_and_11_cse = run_wen & (operator_28_true_2_slc_operator_28_true_2_acc_6_itm
      | max_pool_sva) & (fsm_output[16]);
  assign nl_main_loop_mux1h_1_rmff = ({catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_5
      , catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_4_0 , reg_inner_loop_acc_4_sdt_ftd_6})
      + conv_u2u_7_11({reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd , (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1[6:1])});
  assign main_loop_mux1h_1_rmff = nl_main_loop_mux1h_1_rmff[10:0];
  assign or_139_cse = (fsm_output[4]) | (fsm_output[23]);
  assign and_372_rmff = catapult_conv2d_14_14_3_3_1_convolve_do_stage_0 & (fsm_output[22]);
  assign and_375_m1c = (~ catapult_conv2d_14_14_3_3_1_max_max_val_acc_itm_32_1) &
      (fsm_output[15]);
  assign or_283_rmff = (fsm_output[17]) | (fsm_output[13]) | (fsm_output[14]);
  assign or_285_rmff = (catapult_conv2d_14_14_3_3_1_convolve_do_stage_0 & (fsm_output[12]))
      | (catapult_conv2d_14_14_3_3_1_convolve_do_stage_0 & (~ main_loop_2_asn_71_itm_1)
      & (~ (reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd[1])) &
      (fsm_output[8]));
  assign and_393_rmff = catapult_conv2d_14_14_3_3_1_convolve_do_stage_0 & z_out_4_6
      & (fsm_output[8]);
  assign and_397_rmff = bias_sva & (fsm_output[10]);
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_nor_cse
      = ~(catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_5_itm | (catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva[3]));
  assign nor_47_cse = ~((fsm_output[12]) | (fsm_output[8]));
  assign catapult_conv2d_14_14_3_3_1_convolve_count_and_ssc = run_wen & (catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2
      | catapult_conv2d_14_14_3_3_1_convolve_count_7_0_sva_mx0c0 | (fsm_output[8])
      | (fsm_output[12]) | catapult_conv2d_14_14_3_3_1_convolve_count_7_0_sva_mx0c4);
  assign nl_load_images_loop_acc_9_nl = (~ (inner_loop_i_sva[5:0])) + ({(inner_loop_i_sva[3:0])
      , 2'b01});
  assign load_images_loop_acc_9_nl = nl_load_images_loop_acc_9_nl[5:0];
  assign nl_load_images_loop_acc_6_sdt = (load_images_loop_acc_4_sdt_1[10:5]) + load_images_loop_acc_9_nl;
  assign load_images_loop_acc_6_sdt = nl_load_images_loop_acc_6_sdt[5:0];
  assign or_320_ssc = (fsm_output[7]) | (fsm_output[1]);
  assign and_463_ssc = catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_nor_cse
      & (fsm_output[13]);
  assign and_465_ssc = (catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_5_itm
      | (catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva[3])) & (fsm_output[13]);
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_and_3_cse = run_wen &
      (~((fsm_output[2]) | (fsm_output[15]) | (fsm_output[14]) | (fsm_output[8])));
  assign or_184_cse = (~ catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2) | main_loop_2_asn_71_itm_1;
  assign and_608_cse = (~ z_out_8_4) & (fsm_output[13]);
  assign nand_43_itm = ~((~((fsm_output[22]) | (fsm_output[7]))) & and_dcpl_108 &
      catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_nor_cse & nor_47_cse);
  assign or_392_tmp = (fsm_output[15:14]!=2'b00);
  assign catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux_cse
      = MUX_s_1_2_2((reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd[0]),
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_7,
      fsm_output[12]);
  assign operator_28_true_2_or_cse = (fsm_output[13]) | (fsm_output[8]);
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_nor_cse = ~((fsm_output[14:13]!=2'b00));
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_and_ssc = run_wen
      & catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_nor_cse;
  assign catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_4_dfm_mx1w0 = MUX_v_32_2_2(32'b00000000000000000000000000000000,
      current_image_rsci_q_d_mxwt, catapult_conv2d_14_14_3_3_1_convolve_do_slc_catapult_conv2d_14_14_3_3_1_convolve_do_acc_6_svs_st_1);
  assign nl_load_images_loop_acc_4_sdt_1 = (~ (inner_loop_i_sva[10:0])) + ({(inner_loop_i_sva[8:0])
      , 2'b01});
  assign load_images_loop_acc_4_sdt_1 = nl_load_images_loop_acc_4_sdt_1[10:0];
  assign catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_mx0w0 = or_dcpl_7 & catapult_conv2d_14_14_3_3_1_convolve_do_stage_0;
  assign nl_main_loop_3_i_7_0_sva_1_mx1w0 = ({reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd
      , reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1}) + 8'b00000001;
  assign main_loop_3_i_7_0_sva_1_mx1w0 = nl_main_loop_3_i_7_0_sva_1_mx1w0[7:0];
  assign nl_main_loop_1_acc_nl = conv_s2u_28_29(z_out_2) - conv_s2u_28_29(num_output_images_sva);
  assign main_loop_1_acc_nl = nl_main_loop_1_acc_nl[28:0];
  assign exit_main_loop_1_sva_mx0 = MUX_s_1_2_2((~ z_out_6_28), (~ (readslicef_29_1_28(main_loop_1_acc_nl))),
      fsm_output[23]);
  assign nl_outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl
      = (z_out_9[4:1]) + conv_u2s_1_4(z_out_9[0]);
  assign outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl
      = nl_outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl[3:0];
  assign outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_3_1
      = readslicef_4_1_3(outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_nl);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_nor_2_nl
      = ~((~((catapult_conv2d_14_14_3_3_1_convolve_do_slc_64_16_sat_sva_1[31]) |
      catapult_conv2d_14_14_3_3_1_convolve_do_and_unfl_sva_1)) | catapult_conv2d_14_14_3_3_1_convolve_do_nor_ovfl_sva_1);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_nor_2_nl = ~(MUX_v_30_2_2((catapult_conv2d_14_14_3_3_1_convolve_do_slc_64_16_sat_sva_1[30:1]),
      30'b111111111111111111111111111111, catapult_conv2d_14_14_3_3_1_convolve_do_nor_ovfl_sva_1));
  assign catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_nor_nl
      = ~(MUX_v_30_2_2(catapult_conv2d_14_14_3_3_1_convolve_do_nor_2_nl, 30'b111111111111111111111111111111,
      catapult_conv2d_14_14_3_3_1_convolve_do_and_unfl_sva_1));
  assign catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_nor_1_nl
      = ~((~((catapult_conv2d_14_14_3_3_1_convolve_do_slc_64_16_sat_sva_1[0]) | catapult_conv2d_14_14_3_3_1_convolve_do_nor_ovfl_sva_1))
      | catapult_conv2d_14_14_3_3_1_convolve_do_and_unfl_sva_1);
  assign nl_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1 = conv_s2s_32_33(catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1)
      + conv_s2s_32_33({catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_nor_2_nl
      , catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_nor_nl
      , catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_nor_1_nl});
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1 = nl_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1[32:0];
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_1_and_unfl_sva_1 = (catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1[32:31]==2'b10);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_ovfl_sva_1 = ~((catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1[32:31]!=2'b01));
  assign nl_catapult_conv2d_14_14_3_3_1_convolve_do_acc_12_nl = conv_s2s_49_50(outer_conv_loop_3_inner_conv_loop_3_acc_1_itm_63_15_1)
      + 50'b00000000000000000000000000000000000000000000000001;
  assign catapult_conv2d_14_14_3_3_1_convolve_do_acc_12_nl = nl_catapult_conv2d_14_14_3_3_1_convolve_do_acc_12_nl[49:0];
  assign catapult_conv2d_14_14_3_3_1_convolve_do_slc_64_16_sat_sva_1 = readslicef_50_49_1(catapult_conv2d_14_14_3_3_1_convolve_do_acc_12_nl);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_and_unfl_sva_1 = (catapult_conv2d_14_14_3_3_1_convolve_do_slc_64_16_sat_sva_1[48])
      & (~((catapult_conv2d_14_14_3_3_1_convolve_do_slc_64_16_sat_sva_1[47:31]==17'b11111111111111111)));
  assign catapult_conv2d_14_14_3_3_1_convolve_do_nor_ovfl_sva_1 = ~((catapult_conv2d_14_14_3_3_1_convolve_do_slc_64_16_sat_sva_1[48])
      | (~((catapult_conv2d_14_14_3_3_1_convolve_do_slc_64_16_sat_sva_1[47:31]!=17'b00000000000000000))));
  assign nl_outer_conv_loop_3_inner_conv_loop_3_acc_1_nl = inner_conv_loop_acc_5_itm_1
      + inner_conv_loop_acc_4_itm_1 + inner_conv_loop_acc_3_itm_1 + inner_conv_loop_acc_6_itm_1
      + inner_conv_loop_inner_conv_loop_and_itm_2;
  assign outer_conv_loop_3_inner_conv_loop_3_acc_1_nl = nl_outer_conv_loop_3_inner_conv_loop_3_acc_1_nl[63:0];
  assign outer_conv_loop_3_inner_conv_loop_3_acc_1_itm_63_15_1 = readslicef_64_49_15(outer_conv_loop_3_inner_conv_loop_3_acc_1_nl);
  assign nl_outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl
      = ({1'b1 , (outer_conv_loop_2_operator_28_true_acc_tmp[2:0])}) + 4'b0001;
  assign outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl
      = nl_outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl[3:0];
  assign outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_itm_3_1
      = readslicef_4_1_3(outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl);
  assign nl_outer_conv_loop_2_operator_28_true_acc_tmp = (z_out_9[4:1]) + 4'b0001;
  assign outer_conv_loop_2_operator_28_true_acc_tmp = nl_outer_conv_loop_2_operator_28_true_acc_tmp[3:0];
  assign nl_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_sva_3 = ({reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd
      , reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1}) + 9'b000000001;
  assign catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_sva_3 = nl_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_sva_3[8:0];
  assign nl_catapult_conv2d_14_14_3_3_1_convolve_col_4_0_sva_3 = catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0
      + 5'b00001;
  assign catapult_conv2d_14_14_3_3_1_convolve_col_4_0_sva_3 = nl_catapult_conv2d_14_14_3_3_1_convolve_col_4_0_sva_3[4:0];
  assign nl_catapult_conv2d_14_14_3_3_1_convolve_do_acc_11_tmp = conv_s2s_1_4(~ (catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva[0]))
      + conv_u2s_3_4(catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva[3:1]);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_acc_11_tmp = nl_catapult_conv2d_14_14_3_3_1_convolve_do_acc_11_tmp[3:0];
  assign nl_operator_28_true_1_acc_nl = (catapult_conv2d_14_14_3_3_1_convolve_do_acc_11_tmp[2:0])
      + 3'b001;
  assign operator_28_true_1_acc_nl = nl_operator_28_true_1_acc_nl[2:0];
  assign nl_outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_nl
      = ({1'b1 , operator_28_true_1_acc_nl}) + 4'b0001;
  assign outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_nl
      = nl_outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_nl[3:0];
  assign outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_itm_3_1
      = readslicef_4_1_3(outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_nl);
  assign main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1 = ~((~((z_out_1[31])
      | main_loop_3_and_unfl_sva_1)) | main_loop_3_nor_ovfl_sva_1);
  assign main_loop_4_qif_slc_bias_output_32_31_0_ncse_0_sva_1 = ~((~((z_out_1[0])
      | main_loop_3_nor_ovfl_sva_1)) | main_loop_3_and_unfl_sva_1);
  assign main_loop_3_and_unfl_sva_1 = (z_out_1[32:31]==2'b10);
  assign main_loop_3_nor_ovfl_sva_1 = ~((z_out_1[32:31]!=2'b01));
  assign nl_load_images_loop_acc_7_nl = conv_s2u_28_29(z_out_2) - conv_s2u_28_29(num_input_images_sva);
  assign load_images_loop_acc_7_nl = nl_load_images_loop_acc_7_nl[28:0];
  assign load_images_loop_acc_7_itm_28_1 = readslicef_29_1_28(load_images_loop_acc_7_nl);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_5_nl = MUX_v_32_2_2(relu_output_rsci_q_d_mxwt,
      catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1, z_out_1[32]);
  assign nl_catapult_conv2d_14_14_3_3_1_max_max_val_acc_nl = conv_s2u_32_33(catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_5_nl)
      - conv_s2u_32_33(catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm);
  assign catapult_conv2d_14_14_3_3_1_max_max_val_acc_nl = nl_catapult_conv2d_14_14_3_3_1_max_max_val_acc_nl[32:0];
  assign catapult_conv2d_14_14_3_3_1_max_max_val_acc_itm_32_1 = readslicef_33_1_32(catapult_conv2d_14_14_3_3_1_max_max_val_acc_nl);
  assign or_dcpl_7 = (~ catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2) | (z_out_5[8]);
  assign and_dcpl_1 = catapult_conv2d_14_14_3_3_1_convolve_do_stage_0 & (z_out_10[6]);
  assign and_dcpl_30 = ~(catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2 | catapult_conv2d_14_14_3_3_1_convolve_do_stage_0);
  assign and_dcpl_50 = ~((fsm_output[24]) | (fsm_output[0]));
  assign or_dcpl_53 = (main_loop_2_index_3_0_sva[1]) | (~ catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2);
  assign or_dcpl_54 = (main_loop_2_index_3_0_sva[3:2]!=2'b01);
  assign or_dcpl_55 = or_dcpl_54 | (~ (main_loop_2_index_3_0_sva[0]));
  assign or_dcpl_57 = or_dcpl_54 | (main_loop_2_index_3_0_sva[0]);
  assign or_dcpl_59 = ~((main_loop_2_index_3_0_sva[1]) & catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2);
  assign or_dcpl_61 = (main_loop_2_index_3_0_sva[3:2]!=2'b00);
  assign or_dcpl_62 = or_dcpl_61 | (~ (main_loop_2_index_3_0_sva[0]));
  assign or_dcpl_65 = or_dcpl_61 | (main_loop_2_index_3_0_sva[0]);
  assign or_dcpl_80 = (fsm_output[7]) | (fsm_output[5]);
  assign or_dcpl_83 = (fsm_output[21]) | (fsm_output[11]) | (fsm_output[9]);
  assign and_dcpl_77 = ~((fsm_output[11]) | (fsm_output[9]));
  assign and_dcpl_80 = ~((fsm_output[22]) | (fsm_output[21]) | (fsm_output[2]));
  assign and_dcpl_94 = ~((fsm_output[18:17]!=2'b00));
  assign or_dcpl_107 = (fsm_output[21:20]!=2'b00);
  assign or_dcpl_110 = (fsm_output[18:17]!=2'b00);
  assign or_dcpl_111 = or_dcpl_110 | (fsm_output[19]) | (fsm_output[22]);
  assign or_dcpl_123 = (fsm_output[14]) | (fsm_output[12]);
  assign or_dcpl_126 = (fsm_output[16:15]!=2'b00);
  assign and_dcpl_108 = ~((fsm_output[16:15]!=2'b00));
  assign and_237_cse = ~((fsm_output[9:8]!=2'b00));
  assign or_tmp_152 = catapult_conv2d_14_14_3_3_1_convolve_do_stage_0 & (fsm_output[6]);
  assign or_tmp_191 = (fsm_output[8:7]!=2'b00);
  assign and_510_cse = or_dcpl_126 | (fsm_output[14]);
  assign shift_register_29_lpi_2_mx0c1 = (fsm_output[9]) | (or_72_cse & and_dcpl_1
      & (fsm_output[8]));
  assign catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_2_mx1c1 = catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_3
      & (~ catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_2) & catapult_conv2d_14_14_3_3_1_convolve_do_stage_0;
  assign inner_loop_i_sva_mx0c1 = (fsm_output[3]) | (fsm_output[9]);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_mx0c1 = or_dcpl_83 | or_dcpl_80
      | (fsm_output[1]);
  assign catapult_conv2d_14_14_3_3_1_convolve_count_7_0_sva_mx0c0 = and_dcpl_94 &
      (~ (fsm_output[19])) & (~((fsm_output[22:20]!=3'b000))) & (~((fsm_output[2])
      | (fsm_output[16]) | (fsm_output[13]))) & nor_47_cse;
  assign catapult_conv2d_14_14_3_3_1_convolve_count_7_0_sva_mx0c4 = or_dcpl_111 |
      or_dcpl_107 | (fsm_output[16]) | (fsm_output[13]);
  assign catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm_mx0c0 = (fsm_output[9])
      | (fsm_output[5]);
  assign catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva_mx0c1 = (~((fsm_output[6])
      | (fsm_output[16]))) & (~((fsm_output[13]) | (fsm_output[15]))) & (~((fsm_output[14])
      | (fsm_output[12]) | (fsm_output[8])));
  assign catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1_mx0c1
      = (fsm_output[20]) | (fsm_output[12]);
  assign weight_memory_rsci_radr_d = weight_index_13_0_lpi_3;
  assign weight_memory_rsci_re_d_pff = weight_memory_rsci_re_d_iff;
  assign bias_memory_rsci_radr_d = bias_index_9_0_sva;
  assign bias_memory_rsci_re_d_pff = bias_memory_rsci_re_d_iff;
  assign current_image_rsci_radr_d = {main_loop_mux1h_1_rmff , (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1[0])};
  assign current_image_rsci_wadr_d = {main_loop_mux1h_1_rmff , (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1[0])};
  assign current_image_rsci_d_d = image_channel_rsci_idat_mxwt;
  assign current_image_rsci_we_d_pff = current_image_rsci_we_d_iff;
  assign current_image_rsci_re_d_pff = current_image_rsci_re_d_iff;
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_1_nl = MUX_s_1_2_2((reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd[0]),
      reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd, fsm_output[12]);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_8_nl = MUX_v_7_2_2(reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1,
      reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1, fsm_output[12]);
  assign convolution_output_rsci_radr_d = {catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_1_nl
      , catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_8_nl};
  assign convolution_output_rsci_wadr_d = {catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_7
      , catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_6_0};
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_1_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_2_nl
      = ~((~((catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1[31]) | catapult_conv2d_14_14_3_3_1_convolve_do_if_1_and_unfl_sva_1))
      | catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_ovfl_sva_1);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_1_nl = ~(MUX_v_30_2_2((catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1[30:1]),
      30'b111111111111111111111111111111, catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_ovfl_sva_1));
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_1_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_nl
      = ~(MUX_v_30_2_2(catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_1_nl, 30'b111111111111111111111111111111,
      catapult_conv2d_14_14_3_3_1_convolve_do_if_1_and_unfl_sva_1));
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_1_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_1_nl
      = ~((~((catapult_conv2d_14_14_3_3_1_convolve_do_if_1_acc_sat_sva_1[0]) | catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_ovfl_sva_1))
      | catapult_conv2d_14_14_3_3_1_convolve_do_if_1_and_unfl_sva_1);
  assign convolution_output_rsci_d_d = {catapult_conv2d_14_14_3_3_1_convolve_do_if_1_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_2_nl
      , catapult_conv2d_14_14_3_3_1_convolve_do_if_1_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_nl
      , catapult_conv2d_14_14_3_3_1_convolve_do_if_1_catapult_conv2d_14_14_3_3_1_convolve_do_if_1_nor_1_nl};
  assign convolution_output_rsci_we_d_pff = convolution_output_rsci_we_d_iff;
  assign convolution_output_rsci_re_d_pff = convolution_output_rsci_re_d_iff;
  assign catapult_conv2d_14_14_3_3_1_convolve_do_aelse_catapult_conv2d_14_14_3_3_1_convolve_do_aelse_or_nl
      = (catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1
      & (fsm_output[17])) | (fsm_output[14]);
  assign relu_output_rsci_radr_d = {reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1
      , catapult_conv2d_14_14_3_3_1_convolve_do_aelse_catapult_conv2d_14_14_3_3_1_convolve_do_aelse_or_nl};
  assign relu_output_rsci_wadr_d = {catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_7
      , catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_6_0};
  assign main_loop_4_main_loop_4_and_nl = main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1
      & (~ relu_sva);
  assign main_loop_3_nor_1_nl = ~(MUX_v_30_2_2((z_out_1[30:1]), 30'b111111111111111111111111111111,
      main_loop_3_nor_ovfl_sva_1));
  assign main_loop_3_main_loop_3_nor_nl = ~(MUX_v_30_2_2(main_loop_3_nor_1_nl, 30'b111111111111111111111111111111,
      main_loop_3_and_unfl_sva_1));
  assign nand_47_nl = ~(main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1 & relu_sva);
  assign mux_16_nl = MUX_v_30_2_2((signext_30_1(~ main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1)),
      main_loop_3_main_loop_3_nor_nl, nand_47_nl);
  assign catapult_conv2d_14_14_3_3_1_relu_fn_catapult_conv2d_14_14_3_3_1_relu_fn_and_2_nl
      = main_loop_4_qif_slc_bias_output_32_31_0_ncse_0_sva_1 & (~ main_loop_4_qif_slc_bias_output_32_31_0_ncse_31_sva_1);
  assign catapult_conv2d_14_14_3_3_1_relu_fn_mux_nl = MUX_s_1_2_2(main_loop_4_qif_slc_bias_output_32_31_0_ncse_0_sva_1,
      catapult_conv2d_14_14_3_3_1_relu_fn_catapult_conv2d_14_14_3_3_1_relu_fn_and_2_nl,
      relu_sva);
  assign relu_output_rsci_d_d = {main_loop_4_main_loop_4_and_nl , mux_16_nl , catapult_conv2d_14_14_3_3_1_relu_fn_mux_nl};
  assign relu_output_rsci_we_d_pff = relu_output_rsci_we_d_iff;
  assign relu_output_rsci_re_d_pff = relu_output_rsci_re_d_iff;
  assign output_buffer_rsci_radr_d = {(reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd[0])
      , reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1};
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_and_nl
      = MUX_v_2_2_2(2'b00, (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1[6:5]),
      (fsm_output[18]));
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_mux_2_nl = MUX_s_1_2_2(catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_5,
      (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1[4]), fsm_output[18]);
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_mux_3_nl = MUX_v_4_2_2((catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0[4:1]),
      (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1[3:0]), fsm_output[18]);
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_mux_1_nl = MUX_s_1_2_2((catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0[0]),
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1,
      fsm_output[18]);
  assign output_buffer_rsci_wadr_d = {catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_and_nl
      , catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_mux_2_nl , catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_mux_3_nl
      , catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_mux_1_nl};
  assign and_373_nl = catapult_conv2d_14_14_3_3_1_max_max_val_acc_itm_32_1 & (fsm_output[15]);
  assign catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qelse_or_1_nl = ((~ (z_out_1[32]))
      & and_375_m1c) | (fsm_output[18]);
  assign catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qelse_and_3_nl = (z_out_1[32])
      & and_375_m1c;
  assign output_buffer_rsci_d_d = MUX1HOT_v_32_3_2(catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm,
      relu_output_rsci_q_d_mxwt, catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1,
      {and_373_nl , catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qelse_or_1_nl
      , catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qelse_and_3_nl});
  assign output_buffer_rsci_we_d_pff = output_buffer_rsci_we_d_iff;
  assign output_buffer_rsci_re_d_pff = output_buffer_rsci_re_d_iff;
  assign or_tmp_271 = (fsm_output[13]) | (fsm_output[20]);
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      output_image_channel_rsci_idat <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (fsm_output[22]) & catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2
        ) begin
      output_image_channel_rsci_idat <= output_buffer_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      bias_index_9_0_sva <= 10'b0000000000;
    end
    else if ( run_wen & ((~ and_dcpl_50) | (fsm_output[10])) ) begin
      bias_index_9_0_sva <= MUX_v_10_2_2(10'b0000000000, z_out, (fsm_output[10]));
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      num_output_images_sva <= 28'b0000000000000000000000000000;
      max_pool_sva <= 1'b0;
      relu_sva <= 1'b0;
      bias_sva <= 1'b0;
      num_input_images_sva <= 28'b0000000000000000000000000000;
      load_images_loop_acc_2_cse_sva_28 <= 1'b0;
    end
    else if ( num_output_images_and_cse ) begin
      num_output_images_sva <= num_output_images_rsci_idat;
      max_pool_sva <= max_pool_rsci_idat;
      relu_sva <= relu_rsci_idat;
      bias_sva <= bias_rsci_idat;
      num_input_images_sva <= num_input_images_rsci_idat;
      load_images_loop_acc_2_cse_sva_28 <= z_out_6_28;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_5_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(or_dcpl_55 | or_dcpl_53 | (~ (fsm_output[6])))) ) begin
      current_filter_5_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_4_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(or_dcpl_57 | or_dcpl_53 | (~ (fsm_output[6])))) ) begin
      current_filter_4_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_6_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(or_dcpl_57 | or_dcpl_59 | (~ (fsm_output[6])))) ) begin
      current_filter_6_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_3_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(or_dcpl_62 | or_dcpl_59 | (~ (fsm_output[6])))) ) begin
      current_filter_3_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_7_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(or_dcpl_55 | or_dcpl_59 | (~ (fsm_output[6])))) ) begin
      current_filter_7_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_2_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(or_dcpl_65 | or_dcpl_59 | (~ (fsm_output[6])))) ) begin
      current_filter_2_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_8_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~((main_loop_2_index_3_0_sva[2]) | (~ (main_loop_2_index_3_0_sva[3]))
        | (main_loop_2_index_3_0_sva[0]) | or_dcpl_53 | (~ (fsm_output[6])))) ) begin
      current_filter_8_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_1_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(or_dcpl_62 | or_dcpl_53 | (~ (fsm_output[6])))) ) begin
      current_filter_1_lpi_2 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      shift_register_2_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~ and_237_cse) ) begin
      shift_register_2_lpi_2 <= shift_register_3_lpi_2;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      shift_register_3_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_4_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_5_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_6_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_7_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_8_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_9_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_10_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_11_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_12_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_13_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_14_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_15_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_16_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_17_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_18_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_19_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_20_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_21_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_22_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_23_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_24_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_25_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_26_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_27_lpi_2 <= 32'b00000000000000000000000000000000;
      shift_register_28_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( shift_register_and_1_cse ) begin
      shift_register_3_lpi_2 <= shift_register_4_lpi_2;
      shift_register_4_lpi_2 <= shift_register_5_lpi_2;
      shift_register_5_lpi_2 <= shift_register_6_lpi_2;
      shift_register_6_lpi_2 <= shift_register_7_lpi_2;
      shift_register_7_lpi_2 <= shift_register_8_lpi_2;
      shift_register_8_lpi_2 <= shift_register_9_lpi_2;
      shift_register_9_lpi_2 <= shift_register_10_lpi_2;
      shift_register_10_lpi_2 <= shift_register_11_lpi_2;
      shift_register_11_lpi_2 <= shift_register_12_lpi_2;
      shift_register_12_lpi_2 <= shift_register_13_lpi_2;
      shift_register_13_lpi_2 <= shift_register_14_lpi_2;
      shift_register_14_lpi_2 <= shift_register_15_lpi_2;
      shift_register_15_lpi_2 <= shift_register_16_lpi_2;
      shift_register_16_lpi_2 <= shift_register_17_lpi_2;
      shift_register_17_lpi_2 <= shift_register_18_lpi_2;
      shift_register_18_lpi_2 <= shift_register_19_lpi_2;
      shift_register_19_lpi_2 <= shift_register_20_lpi_2;
      shift_register_20_lpi_2 <= shift_register_21_lpi_2;
      shift_register_21_lpi_2 <= shift_register_22_lpi_2;
      shift_register_22_lpi_2 <= shift_register_23_lpi_2;
      shift_register_23_lpi_2 <= shift_register_24_lpi_2;
      shift_register_24_lpi_2 <= shift_register_25_lpi_2;
      shift_register_25_lpi_2 <= shift_register_26_lpi_2;
      shift_register_26_lpi_2 <= shift_register_27_lpi_2;
      shift_register_27_lpi_2 <= shift_register_28_lpi_2;
      shift_register_28_lpi_2 <= shift_register_29_lpi_2;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      shift_register_29_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2 & (~
        catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_1) & and_dcpl_1 & (fsm_output[8]))
        | shift_register_29_lpi_2_mx0c1) ) begin
      shift_register_29_lpi_2 <= MUX_v_32_2_2(catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_4_dfm_mx1w0,
          catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm, shift_register_29_lpi_2_mx0c1);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2 & (~
        catapult_conv2d_14_14_3_3_1_convolve_do_stage_0)) | catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_2_mx1c1)
        & (fsm_output[8]) ) begin
      catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_2 <= MUX_v_32_2_2(catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_4_dfm_mx1w0,
          catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1, catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_2_mx1c1);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      max_pool_shift_register_4_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_5_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_6_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_7_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_8_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_9_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_10_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_11_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_12_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_13_lpi_2 <= 32'b00000000000000000000000000000000;
      max_pool_shift_register_15_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( max_pool_shift_register_and_11_cse ) begin
      max_pool_shift_register_4_lpi_2 <= max_pool_shift_register_6_lpi_2;
      max_pool_shift_register_5_lpi_2 <= max_pool_shift_register_7_lpi_2;
      max_pool_shift_register_6_lpi_2 <= max_pool_shift_register_8_lpi_2;
      max_pool_shift_register_7_lpi_2 <= max_pool_shift_register_9_lpi_2;
      max_pool_shift_register_8_lpi_2 <= max_pool_shift_register_10_lpi_2;
      max_pool_shift_register_9_lpi_2 <= max_pool_shift_register_11_lpi_2;
      max_pool_shift_register_10_lpi_2 <= max_pool_shift_register_12_lpi_2;
      max_pool_shift_register_11_lpi_2 <= max_pool_shift_register_13_lpi_2;
      max_pool_shift_register_12_lpi_2 <= catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_asn_1_ncse_lpi_2;
      max_pool_shift_register_13_lpi_2 <= max_pool_shift_register_15_lpi_2;
      max_pool_shift_register_15_lpi_2 <= catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_asn_1_ncse_lpi_2 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((operator_28_true_2_slc_operator_28_true_2_acc_6_itm & (~((~((fsm_output[23])
        | (fsm_output[16]))) | ((~ max_pool_sva) & (fsm_output[23]))))) | (max_pool_sva
        & (fsm_output[23]))) ) begin
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_asn_1_ncse_lpi_2 <= catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      reg_num_output_images_triosy_obj_iswt0_cse <= 1'b0;
      reg_output_buffer_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= 1'b0;
      reg_relu_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= 1'b0;
      reg_convolution_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse
          <= 1'b0;
      reg_current_image_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= 1'b0;
      reg_output_image_channel_rsci_iswt0_cse <= 1'b0;
      reg_bias_memory_rsci_iswt0_cse <= 1'b0;
      reg_weight_memory_rsci_iswt0_cse <= 1'b0;
      reg_image_channel_rsci_iswt0_cse <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2 <= 1'b0;
      main_loop_2_index_3_0_sva <= 4'b0000;
      reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd <= 2'b00;
      reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1 <= 7'b0000000;
      catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_2 <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_2
          <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_1 <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_do_slc_catapult_conv2d_14_14_3_3_1_convolve_do_acc_6_svs_st_1
          <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_do_pcnxt_sva <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_3 <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_7
          <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_6_0
          <= 7'b0000000;
      inner_conv_loop_acc_6_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_inner_conv_loop_and_itm_2 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_5_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_4_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_acc_3_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_qif_asn_17_itm_1 <= 32'b00000000000000000000000000000000;
      inner_conv_loop_qif_asn_20_itm_1 <= 32'b00000000000000000000000000000000;
      inner_conv_loop_qif_asn_21_itm_1 <= 32'b00000000000000000000000000000000;
      catapult_conv2d_14_14_3_3_1_in_bounds_return_4_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_14_14_3_3_1_in_bounds_return_1_lpi_4_dfm_1 <= 1'b0;
      inner_conv_loop_inner_conv_loop_and_4_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_qr_8_sva_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      inner_conv_loop_inner_conv_loop_and_2_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      catapult_conv2d_14_14_3_3_1_in_bounds_return_3_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_14_14_3_3_1_in_bounds_return_2_lpi_4_dfm_1 <= 1'b0;
      catapult_conv2d_14_14_3_3_1_in_bounds_return_lpi_4_dfm_1_1 <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_itm_1
          <= 1'b0;
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_7
          <= 1'b0;
      inner_conv_loop_inner_conv_loop_and_itm_1 <= 64'b0000000000000000000000000000000000000000000000000000000000000000;
      shift_register_1_lpi_4 <= 32'b00000000000000000000000000000000;
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_5_itm <= 1'b0;
    end
    else if ( run_wen ) begin
      reg_num_output_images_triosy_obj_iswt0_cse <= exit_main_loop_1_sva_mx0 & or_139_cse;
      reg_output_buffer_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= and_372_rmff;
      reg_relu_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= or_283_rmff;
      reg_convolution_output_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse
          <= or_285_rmff;
      reg_current_image_rsci_port_0_r_ram_ir_internal_RMASK_B_d_run_psct_cse <= and_393_rmff;
      reg_output_image_channel_rsci_iswt0_cse <= catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2
          & (fsm_output[22]);
      reg_bias_memory_rsci_iswt0_cse <= and_397_rmff;
      reg_weight_memory_rsci_iswt0_cse <= or_tmp_152;
      reg_image_channel_rsci_iswt0_cse <= (fsm_output[1]) | (or_dcpl_7 & catapult_conv2d_14_14_3_3_1_convolve_do_stage_0
          & (fsm_output[2]));
      catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2 <= (catapult_conv2d_14_14_3_3_1_convolve_do_aelse_mux_19_nl
          & (~(or_dcpl_83 | or_dcpl_80))) | (and_dcpl_80 & and_dcpl_77 & (~((fsm_output[7])
          | (fsm_output[5]))) & nor_47_cse);
      main_loop_2_index_3_0_sva <= MUX_v_4_2_2(4'b0000, catapult_conv2d_14_14_3_3_1_convolve_row_mux_nl,
          (fsm_output[6]));
      reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd <= MUX_v_2_2_2(2'b00,
          catapult_conv2d_14_14_3_3_1_convolve_center_pixel_or_2_nl, nand_43_itm);
      reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1 <= MUX_v_7_2_2(7'b0000000,
          catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux1h_14_nl, nand_43_itm);
      catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_2 <= catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_1;
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_2
          <= catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1;
      catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_1 <= MUX_s_1_2_2((~ (z_out_10[6])),
          catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_3, fsm_output[12]);
      catapult_conv2d_14_14_3_3_1_convolve_do_slc_catapult_conv2d_14_14_3_3_1_convolve_do_acc_6_svs_st_1
          <= z_out_4_6;
      catapult_conv2d_14_14_3_3_1_convolve_do_pcnxt_sva <= (~ catapult_conv2d_14_14_3_3_1_convolve_do_pcnxt_sva)
          & (fsm_output[8]);
      catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_3 <= catapult_conv2d_14_14_3_3_1_convolve_do_aelse_mux_nl
          & ((fsm_output[11]) | (fsm_output[12]) | (fsm_output[8]));
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_7
          <= main_loop_4_mux_1_nl & (~ nor_47_cse);
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_6_0
          <= MUX_v_7_2_2(7'b0000000, main_loop_4_mux_2_nl, catapult_conv2d_14_14_3_3_1_convolve_center_pixel_not_4_nl);
      inner_conv_loop_acc_6_itm_1 <= nl_inner_conv_loop_acc_6_itm_1[63:0];
      inner_conv_loop_inner_conv_loop_and_itm_2 <= inner_conv_loop_inner_conv_loop_and_itm_1;
      inner_conv_loop_acc_5_itm_1 <= nl_inner_conv_loop_acc_5_itm_1[63:0];
      inner_conv_loop_acc_4_itm_1 <= nl_inner_conv_loop_acc_4_itm_1[63:0];
      inner_conv_loop_acc_3_itm_1 <= nl_inner_conv_loop_acc_3_itm_1[63:0];
      inner_conv_loop_qif_asn_17_itm_1 <= shift_register_3_lpi_2;
      inner_conv_loop_qif_asn_20_itm_1 <= shift_register_17_lpi_2;
      inner_conv_loop_qif_asn_21_itm_1 <= shift_register_29_lpi_2;
      catapult_conv2d_14_14_3_3_1_in_bounds_return_4_lpi_4_dfm_1 <= outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_itm_3_1
          & (~ (outer_conv_loop_2_operator_28_true_acc_tmp[3]));
      catapult_conv2d_14_14_3_3_1_in_bounds_return_1_lpi_4_dfm_1 <= outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ (z_out_9[4]));
      inner_conv_loop_inner_conv_loop_and_4_itm_1 <= MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
          outer_conv_loop_2_inner_conv_loop_1_qif_mul_nl, operator_28_true_not_2_nl);
      inner_conv_loop_qr_8_sva_1 <= $unsigned(nl_outer_conv_loop_2_inner_conv_loop_2_qif_mul_sgnd);
      inner_conv_loop_inner_conv_loop_and_2_itm_1 <= MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
          outer_conv_loop_1_inner_conv_loop_2_qif_mul_nl, inner_conv_loop_not_14_nl);
      catapult_conv2d_14_14_3_3_1_in_bounds_return_3_lpi_4_dfm_1 <= (readslicef_4_1_3(outer_conv_loop_1_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl))
          & (~ (catapult_conv2d_14_14_3_3_1_convolve_do_acc_11_tmp[3]));
      catapult_conv2d_14_14_3_3_1_in_bounds_return_2_lpi_4_dfm_1 <= outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_3_1);
      catapult_conv2d_14_14_3_3_1_in_bounds_return_lpi_4_dfm_1_1 <= outer_conv_loop_2_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_itm_3_1
          & outer_conv_loop_3_inner_conv_loop_2_catapult_conv2d_14_14_3_3_1_in_bounds_if_2_acc_1_itm_3_1
          & (~ (outer_conv_loop_2_operator_28_true_acc_tmp[3]));
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_itm_1
          <= reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd[1];
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_7
          <= catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux_cse;
      inner_conv_loop_inner_conv_loop_and_itm_1 <= MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
          outer_conv_loop_1_inner_conv_loop_1_qif_mul_nl, catapult_conv2d_14_14_3_3_1_in_bounds_catapult_conv2d_14_14_3_3_1_in_bounds_catapult_conv2d_14_14_3_3_1_in_bounds_nor_nl);
      shift_register_1_lpi_4 <= shift_register_2_lpi_2;
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_5_itm <= catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux1h_5_nl
          & (~ (fsm_output[12]));
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      inner_loop_i_sva <= 28'b0000000000000000000000000000;
    end
    else if ( run_wen & ((fsm_output[0]) | (fsm_output[5]) | inner_loop_i_sva_mx0c1)
        ) begin
      inner_loop_i_sva <= MUX_v_28_2_2(28'b0000000000000000000000000000, z_out_2,
          inner_loop_i_sva_mx0c1);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      weight_index_13_0_lpi_3 <= 14'b00000000000000;
    end
    else if ( run_wen & (~((and_dcpl_50 & (~ (fsm_output[6]))) | ((~ catapult_conv2d_14_14_3_3_1_convolve_do_stage_0)
        & (fsm_output[6])))) ) begin
      weight_index_13_0_lpi_3 <= MUX_v_14_2_2(14'b00000000000000, main_loop_2_acc_nl,
          or_tmp_152);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_convolve_do_stage_0 <= 1'b0;
    end
    else if ( run_wen & ((~(reg_catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_rgt_nl
        | (and_dcpl_80 & and_dcpl_77 & (~ (fsm_output[7])) & (~((fsm_output[6]) |
        (fsm_output[5]) | (fsm_output[1]))) & nor_47_cse))) | (fsm_output[2]) | catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_mx0c1
        | (fsm_output[13])) ) begin
      catapult_conv2d_14_14_3_3_1_convolve_do_stage_0 <= (catapult_conv2d_14_14_3_3_1_convolve_do_mux_nl
          & (~((fsm_output[12]) | (fsm_output[22]))) & (~((fsm_output[6]) | (fsm_output[8]))))
          | catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_mx0c1;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd <= 1'b0;
      reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1 <= 7'b0000000;
    end
    else if ( catapult_conv2d_14_14_3_3_1_convolve_count_and_ssc ) begin
      reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd <= (main_loop_3_i_7_0_sva_1_mx1w0[7])
          & (~ catapult_conv2d_14_14_3_3_1_convolve_count_7_0_sva_mx0c0);
      reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1 <= MUX_v_7_2_2(7'b0000000,
          main_loop_index_main_loop_index_mux_nl, catapult_conv2d_14_14_3_3_1_convolve_count_not_1_nl);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_5 <= 1'b0;
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_4_0 <= 5'b00000;
    end
    else if ( catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_and_3_cse ) begin
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_5 <= MUX1HOT_s_1_3_2((load_images_loop_acc_6_sdt[5]),
          (z_out[5]), catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_5,
          {or_320_ssc , and_463_ssc , and_465_ssc});
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_4_0 <= MUX1HOT_v_5_3_2((load_images_loop_acc_6_sdt[4:0]),
          (z_out[4:0]), catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0,
          {or_320_ssc , and_463_ssc , and_465_ssc});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_loop_1_o_sva <= 28'b0000000000000000000000000000;
    end
    else if ( run_wen & or_139_cse ) begin
      main_loop_1_o_sva <= MUX_v_28_2_2(28'b0000000000000000000000000000, z_out_2,
          (fsm_output[23]));
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      main_loop_2_asn_71_itm_1 <= 1'b0;
    end
    else if ( run_wen & (~ (fsm_output[8])) ) begin
      main_loop_2_asn_71_itm_1 <= MUX_s_1_2_2((~ (z_out_9[4])), operator_28_true_operator_28_true_nor_nl,
          fsm_output[7]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      current_filter_0_lpi_4 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & (~(or_dcpl_65 | or_dcpl_53 | or_tmp_191)) ) begin
      current_filter_0_lpi_4 <= weight_memory_rsci_q_d_mxwt;
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((~(catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qelse_mux_1_nl
        | (fsm_output[7:6]!=2'b00) | or_dcpl_123)) | catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm_mx0c0
        | (fsm_output[11]) | (fsm_output[15]) | (fsm_output[16])) ) begin
      catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm <= MUX1HOT_v_32_6_2(catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_2,
          catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_4_dfm_mx1w0, catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_and_nl,
          catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1, relu_output_rsci_q_d_mxwt,
          max_pool_shift_register_4_lpi_2, {catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm_mx0c0
          , (fsm_output[8]) , (fsm_output[11]) , (fsm_output[13]) , (fsm_output[15])
          , (fsm_output[16])});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva <= 4'b0000;
    end
    else if ( run_wen & ((~(z_out_8_4 | catapult_conv2d_14_14_3_3_1_convolve_do_pcnxt_sva
        | and_510_cse)) | (fsm_output[6]) | catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva_mx0c1
        | (fsm_output[13:12]!=2'b00)) ) begin
      catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva <= MUX_v_4_2_2(4'b0000,
          mux_nl, nor_71_nl);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1
          <= 1'b0;
    end
    else if ( run_wen & ((fsm_output[8]) | catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1_mx0c1
        | (fsm_output[19])) ) begin
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1
          <= (catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_5_itm & (~ catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_1_itm_1_mx0c1))
          | (fsm_output[19]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1 <= 32'b00000000000000000000000000000000;
    end
    else if ( run_wen & ((catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_6_nl
        & (~(or_dcpl_111 | or_dcpl_107 | (fsm_output[15])))) | (fsm_output[14]))
        ) begin
      catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1 <= MUX1HOT_v_32_4_2(catapult_conv2d_14_14_3_3_1_convolve_do_if_catapult_conv2d_14_14_3_3_1_convolve_do_if_and_nl,
          catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_2, relu_output_rsci_q_d_mxwt,
          max_pool_shift_register_5_lpi_2, {catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_and_2_nl
          , catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_and_3_nl , (fsm_output[14])
          , (fsm_output[16])});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_6_0
          <= 7'b0000000;
    end
    else if ( run_wen & (~ or_392_tmp) ) begin
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_6_0
          <= MUX1HOT_v_7_3_2(reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1,
          catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_6_0,
          z_out_9, {(fsm_output[8]) , (fsm_output[12]) , catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_nor_nl});
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      operator_28_true_2_slc_operator_28_true_2_acc_6_itm <= 1'b0;
    end
    else if ( run_wen & operator_28_true_2_or_cse ) begin
      operator_28_true_2_slc_operator_28_true_2_acc_6_itm <= MUX_s_1_2_2(outer_conv_loop_2_inner_conv_loop_2_operator_28_true_slc_operator_28_true_operator_28_true_acc_itm_3_1,
          z_out_4_6, fsm_output[13]);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_5 <= 1'b0;
      catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0 <= 5'b00000;
    end
    else if ( catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_and_ssc ) begin
      catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_5 <= catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_5
          & (~ (fsm_output[12]));
      catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0 <= MUX_v_5_2_2(5'b00000,
          catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_mux1h_nl,
          nor_72_nl);
    end
  end
  always @(posedge clk) begin
    if ( ~ rstn ) begin
      reg_inner_loop_acc_4_sdt_ftd_6 <= 5'b00000;
    end
    else if ( run_wen & (~((fsm_output[2]) | (fsm_output[8]))) ) begin
      reg_inner_loop_acc_4_sdt_ftd_6 <= load_images_loop_acc_4_sdt_1[4:0];
    end
  end
  assign or_314_nl = (fsm_output[22]) | (fsm_output[12]) | (fsm_output[8]);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_aelse_mux_19_nl = MUX_s_1_2_2(catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_mx0w0,
      catapult_conv2d_14_14_3_3_1_convolve_do_stage_0, or_314_nl);
  assign catapult_conv2d_14_14_3_3_1_convolve_row_mux_nl = MUX_v_4_2_2(catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva,
      main_loop_2_index_3_0_sva, or_184_cse);
  assign catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux_1_nl
      = MUX_v_2_2_2((catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_sva_3[8:7]),
      (z_out_3[8:7]), fsm_output[22]);
  assign catapult_conv2d_14_14_3_3_1_convolve_center_pixel_or_2_nl = MUX_v_2_2_2(catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux_1_nl,
      2'b11, (fsm_output[7]));
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_mux_nl = MUX_v_7_2_2(z_out_10,
      reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1, and_510_cse);
  assign nand_42_nl = ~(and_dcpl_108 & catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_nor_cse);
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_and_nl
      = MUX_v_7_2_2(7'b0000000, catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_mux_nl,
      nand_42_nl);
  assign or_349_nl = or_dcpl_126 | (fsm_output[13]) | or_dcpl_123;
  assign catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux1h_14_nl = MUX1HOT_v_7_4_2(7'b1110001,
      (catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_sva_3[6:0]), catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_and_nl,
      (z_out_3[6:0]), {(fsm_output[7]) , (fsm_output[8]) , or_349_nl , (fsm_output[22])});
  assign or_362_nl = (fsm_output[12:11]!=2'b00);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_aelse_mux_nl = MUX_s_1_2_2(catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_2,
      relu_sva, or_362_nl);
  assign main_loop_4_mux_1_nl = MUX_s_1_2_2(catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_7,
      (z_out_3[7]), fsm_output[12]);
  assign main_loop_4_mux_2_nl = MUX_v_7_2_2(catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_6_0,
      (z_out_3[6:0]), fsm_output[12]);
  assign catapult_conv2d_14_14_3_3_1_convolve_center_pixel_not_4_nl = ~ nor_47_cse;
  assign nl_outer_conv_loop_3_inner_conv_loop_2_qif_mul_sgnd = $signed(current_filter_7_lpi_2)
      * $signed(catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm);
  assign outer_conv_loop_3_inner_conv_loop_2_qif_mul_nl = $unsigned(nl_outer_conv_loop_3_inner_conv_loop_2_qif_mul_sgnd);
  assign inner_conv_loop_inner_conv_loop_and_8_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_3_inner_conv_loop_2_qif_mul_nl, catapult_conv2d_14_14_3_3_1_in_bounds_return_2_lpi_4_dfm_1);
  assign nl_outer_conv_loop_3_inner_conv_loop_3_qif_mul_sgnd = $signed(current_filter_8_lpi_2)
      * $signed(catapult_conv2d_14_14_3_3_1_convolve_do_qr_lpi_4_dfm_mx1w0);
  assign outer_conv_loop_3_inner_conv_loop_3_qif_mul_nl = $unsigned(nl_outer_conv_loop_3_inner_conv_loop_3_qif_mul_sgnd);
  assign inner_conv_loop_inner_conv_loop_and_9_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_3_inner_conv_loop_3_qif_mul_nl, catapult_conv2d_14_14_3_3_1_in_bounds_return_lpi_4_dfm_1_1);
  assign nl_inner_conv_loop_acc_6_itm_1  = inner_conv_loop_inner_conv_loop_and_8_nl
      + inner_conv_loop_inner_conv_loop_and_9_nl;
  assign nl_outer_conv_loop_1_inner_conv_loop_3_qif_mul_sgnd = $signed(current_filter_2_lpi_2)
      * $signed(inner_conv_loop_qif_asn_17_itm_1);
  assign outer_conv_loop_1_inner_conv_loop_3_qif_mul_nl = $unsigned(nl_outer_conv_loop_1_inner_conv_loop_3_qif_mul_sgnd);
  assign inner_conv_loop_inner_conv_loop_and_3_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_1_inner_conv_loop_3_qif_mul_nl, catapult_conv2d_14_14_3_3_1_in_bounds_return_3_lpi_4_dfm_1);
  assign nl_inner_conv_loop_acc_5_itm_1  = inner_conv_loop_inner_conv_loop_and_2_itm_1
      + inner_conv_loop_inner_conv_loop_and_3_nl;
  assign operator_28_true_not_nl = ~ operator_28_true_2_slc_operator_28_true_2_acc_6_itm;
  assign inner_conv_loop_inner_conv_loop_and_5_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      inner_conv_loop_qr_8_sva_1, operator_28_true_not_nl);
  assign nl_inner_conv_loop_acc_4_itm_1  = inner_conv_loop_inner_conv_loop_and_4_itm_1
      + inner_conv_loop_inner_conv_loop_and_5_nl;
  assign nl_outer_conv_loop_2_inner_conv_loop_3_qif_mul_sgnd = $signed(current_filter_5_lpi_2)
      * $signed(inner_conv_loop_qif_asn_20_itm_1);
  assign outer_conv_loop_2_inner_conv_loop_3_qif_mul_nl = $unsigned(nl_outer_conv_loop_2_inner_conv_loop_3_qif_mul_sgnd);
  assign inner_conv_loop_inner_conv_loop_and_6_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_2_inner_conv_loop_3_qif_mul_nl, catapult_conv2d_14_14_3_3_1_in_bounds_return_4_lpi_4_dfm_1);
  assign nl_outer_conv_loop_3_inner_conv_loop_1_qif_mul_sgnd = $signed(current_filter_6_lpi_2)
      * $signed(inner_conv_loop_qif_asn_21_itm_1);
  assign outer_conv_loop_3_inner_conv_loop_1_qif_mul_nl = $unsigned(nl_outer_conv_loop_3_inner_conv_loop_1_qif_mul_sgnd);
  assign inner_conv_loop_inner_conv_loop_and_7_nl = MUX_v_64_2_2(64'b0000000000000000000000000000000000000000000000000000000000000000,
      outer_conv_loop_3_inner_conv_loop_1_qif_mul_nl, catapult_conv2d_14_14_3_3_1_in_bounds_return_1_lpi_4_dfm_1);
  assign nl_inner_conv_loop_acc_3_itm_1  = inner_conv_loop_inner_conv_loop_and_6_nl
      + inner_conv_loop_inner_conv_loop_and_7_nl;
  assign nl_outer_conv_loop_2_inner_conv_loop_1_qif_mul_sgnd = $signed(current_filter_3_lpi_2)
      * $signed(shift_register_15_lpi_2);
  assign outer_conv_loop_2_inner_conv_loop_1_qif_mul_nl = $unsigned(nl_outer_conv_loop_2_inner_conv_loop_1_qif_mul_sgnd);
  assign operator_28_true_not_2_nl = ~ (z_out_9[4]);
  assign nl_outer_conv_loop_2_inner_conv_loop_2_qif_mul_sgnd = $signed(current_filter_4_lpi_2)
      * $signed(shift_register_16_lpi_2);
  assign nl_outer_conv_loop_1_inner_conv_loop_2_qif_mul_sgnd = $signed(current_filter_1_lpi_2)
      * $signed(shift_register_2_lpi_2);
  assign outer_conv_loop_1_inner_conv_loop_2_qif_mul_nl = $unsigned(nl_outer_conv_loop_1_inner_conv_loop_2_qif_mul_sgnd);
  assign inner_conv_loop_not_14_nl = ~ (catapult_conv2d_14_14_3_3_1_convolve_do_acc_11_tmp[3]);
  assign nl_outer_conv_loop_1_operator_28_true_acc_nl = (z_out_9[3:1]) + 3'b001;
  assign outer_conv_loop_1_operator_28_true_acc_nl = nl_outer_conv_loop_1_operator_28_true_acc_nl[2:0];
  assign nl_outer_conv_loop_1_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl
      = ({1'b1 , outer_conv_loop_1_operator_28_true_acc_nl}) + 4'b0001;
  assign outer_conv_loop_1_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl
      = nl_outer_conv_loop_1_inner_conv_loop_3_catapult_conv2d_14_14_3_3_1_in_bounds_if_3_acc_nl[3:0];
  assign nl_outer_conv_loop_1_inner_conv_loop_1_qif_mul_sgnd = $signed(current_filter_0_lpi_4)
      * $signed(shift_register_1_lpi_4);
  assign outer_conv_loop_1_inner_conv_loop_1_qif_mul_nl = $unsigned(nl_outer_conv_loop_1_inner_conv_loop_1_qif_mul_sgnd);
  assign catapult_conv2d_14_14_3_3_1_in_bounds_catapult_conv2d_14_14_3_3_1_in_bounds_catapult_conv2d_14_14_3_3_1_in_bounds_nor_nl
      = ~((z_out_9[4]) | (catapult_conv2d_14_14_3_3_1_convolve_do_acc_11_tmp[3]));
  assign catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_or_1_nl
      = (catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_sva_3[8]) | (~ (fsm_output[8]));
  assign or_379_nl = and_510_cse | (z_out_8_4 & (fsm_output[13]));
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux1h_5_nl = MUX1HOT_s_1_3_2(catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_or_1_nl,
      (~ catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_5_itm), catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_5_itm,
      {or_tmp_191 , and_608_cse , or_379_nl});
  assign nl_main_loop_2_acc_nl = weight_index_13_0_lpi_3 + 14'b00000000000001;
  assign main_loop_2_acc_nl = nl_main_loop_2_acc_nl[13:0];
  assign catapult_conv2d_14_14_3_3_1_convolve_do_mux_nl = MUX_s_1_2_2(catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_mx0w0,
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_if_nor_cse,
      fsm_output[13]);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_or_4_nl = (~ catapult_conv2d_14_14_3_3_1_convolve_do_stage_0)
      | (z_out_10[6]);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_or_5_nl = (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1!=7'b1000011)
      | (~ reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd) | (catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_6_0!=7'b1000011)
      | (~(catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_7
      & catapult_conv2d_14_14_3_3_1_convolve_do_stage_0));
  assign nl_operator_28_true_4_acc_nl = conv_u2s_9_10({reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd
      , reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1}) + conv_s2s_9_10({1'b1
      , (signext_8_7(~ reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1))})
      + 10'b0000000001;
  assign operator_28_true_4_acc_nl = nl_operator_28_true_4_acc_nl[9:0];
  assign catapult_conv2d_14_14_3_3_1_convolve_do_or_6_nl = (~ catapult_conv2d_14_14_3_3_1_convolve_do_stage_0)
      | (readslicef_10_1_9(operator_28_true_4_acc_nl));
  assign reg_catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_rgt_nl = MUX1HOT_s_1_4_2((z_out_9[4]),
      catapult_conv2d_14_14_3_3_1_convolve_do_or_4_nl, catapult_conv2d_14_14_3_3_1_convolve_do_or_5_nl,
      catapult_conv2d_14_14_3_3_1_convolve_do_or_6_nl, {(fsm_output[6]) , (fsm_output[8])
      , (fsm_output[12]) , (fsm_output[22])});
  assign or_385_nl = or_dcpl_110 | (fsm_output[19]) | (fsm_output[22]) | (fsm_output[13]);
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_mux1h_nl = MUX1HOT_v_7_4_2(reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1,
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_1_6_0,
      z_out_9, (z_out_5[6:0]), {or_385_nl , (fsm_output[16]) , (fsm_output[20]) ,
      (fsm_output[21])});
  assign nand_46_nl = ~(and_dcpl_94 & (~((fsm_output[19]) | (fsm_output[22]))) &
      (~((fsm_output[21:20]!=2'b00))) & (~((fsm_output[16]) | (fsm_output[13]))));
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_and_nl = MUX_v_7_2_2(7'b0000000,
      catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_mux1h_nl, nand_46_nl);
  assign main_loop_index_or_1_nl = ((~ catapult_conv2d_14_14_3_3_1_convolve_do_stage_0)
      & (fsm_output[12])) | catapult_conv2d_14_14_3_3_1_convolve_count_7_0_sva_mx0c4;
  assign main_loop_index_main_loop_index_mux_nl = MUX_v_7_2_2((main_loop_3_i_7_0_sva_1_mx1w0[6:0]),
      catapult_conv2d_14_14_3_3_1_perform_max_pool_tail_pixel_and_nl, main_loop_index_or_1_nl);
  assign catapult_conv2d_14_14_3_3_1_convolve_count_not_1_nl = ~ catapult_conv2d_14_14_3_3_1_convolve_count_7_0_sva_mx0c0;
  assign operator_28_true_operator_28_true_nor_nl = ~((inner_loop_i_sva!=28'b0000000000000000000000000000));
  assign catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_and_nl
      = MUX_v_32_2_2(32'b00000000000000000000000000000000, bias_memory_rsci_q_d_mxwt,
      bias_sva);
  assign catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qelse_mux_1_nl = MUX_s_1_2_2(or_72_cse,
      (z_out_1[32]), fsm_output[13]);
  assign mux_nl = MUX_v_4_2_2(z_out_7, 4'b1001, fsm_output[12]);
  assign nor_71_nl = ~(catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva_mx0c1
      | and_608_cse);
  assign catapult_conv2d_14_14_3_3_1_convolve_first_not_nl = ~ main_loop_2_asn_71_itm_1;
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_catapult_conv2d_14_14_3_3_1_convolve_do_if_and_nl
      = MUX_v_32_2_2(32'b00000000000000000000000000000000, convolution_output_rsci_q_d_mxwt,
      catapult_conv2d_14_14_3_3_1_convolve_first_not_nl);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_and_2_nl = (~ catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_itm_1)
      & (fsm_output[8]);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_and_3_nl = catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_itm_1
      & (fsm_output[8]);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_or_nl
      = catapult_conv2d_14_14_3_3_1_convolve_do_asn_13_itm_2 | (~ catapult_conv2d_14_14_3_3_1_convolve_do_stage_0_3)
      | (~ catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_8_itm_1);
  assign catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_mux_6_nl = MUX_s_1_2_2(catapult_conv2d_14_14_3_3_1_convolve_do_catapult_conv2d_14_14_3_3_1_convolve_do_if_qelse_or_nl,
      operator_28_true_2_slc_operator_28_true_2_acc_6_itm, fsm_output[16]);
  assign catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_nor_nl
      = ~(or_392_tmp | (~((fsm_output[15:13]!=3'b000))));
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_nor_nl
      = ~((fsm_output[8]) | (fsm_output[16]));
  assign catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_mux1h_nl
      = MUX1HOT_v_5_3_2(5'b10001, catapult_conv2d_14_14_3_3_1_convolve_col_4_0_sva_3,
      catapult_conv2d_14_14_3_3_1_perform_max_pool_if_do_mux_3_itm_4_0, {catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_nor_nl
      , (fsm_output[8]) , (fsm_output[16])});
  assign nor_72_nl = ~((fsm_output[12]) | ((~ z_out_8_4) & (fsm_output[8])));
  assign catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qif_mux_1_nl = MUX_v_10_2_2(bias_index_9_0_sva,
      (signext_10_6({catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_5
      , catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0})), fsm_output[13]);
  assign nl_z_out = catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qif_mux_1_nl
      + 10'b0000000001;
  assign z_out = nl_z_out[9:0];
  assign catapult_conv2d_14_14_3_3_1_max_cd_mux1h_2_nl = MUX1HOT_v_32_3_2(relu_output_rsci_q_d_mxwt,
      catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1, convolution_output_rsci_q_d_mxwt,
      {(fsm_output[15]) , (fsm_output[13]) , (fsm_output[12])});
  assign catapult_conv2d_14_14_3_3_1_max_cd_or_1_nl = (~ (fsm_output[12])) | (fsm_output[15])
      | (fsm_output[13]);
  assign catapult_conv2d_14_14_3_3_1_max_cd_mux1h_3_nl = MUX1HOT_v_32_3_2((~ catapult_conv2d_14_14_3_3_1_convolve_do_if_qr_lpi_4_dfm_1_1),
      (~ catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm), catapult_conv2d_14_14_3_3_1_apply_bias_bias_value_qr_lpi_2_dfm,
      {(fsm_output[15]) , (fsm_output[13]) , (fsm_output[12])});
  assign nl_acc_1_nl = conv_s2u_33_34({catapult_conv2d_14_14_3_3_1_max_cd_mux1h_2_nl
      , catapult_conv2d_14_14_3_3_1_max_cd_or_1_nl}) + conv_s2u_33_34({catapult_conv2d_14_14_3_3_1_max_cd_mux1h_3_nl
      , 1'b1});
  assign acc_1_nl = nl_acc_1_nl[33:0];
  assign z_out_1 = readslicef_34_33_1(acc_1_nl);
  assign load_images_loop_mux_3_nl = MUX_v_28_2_2(inner_loop_i_sva, main_loop_1_o_sva,
      fsm_output[23]);
  assign nl_z_out_2 = load_images_loop_mux_3_nl + 28'b0000000000000000000000000001;
  assign z_out_2 = nl_z_out_2[27:0];
  assign main_loop_5_mux_3_nl = MUX_v_7_2_2(reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1,
      catapult_conv2d_14_14_3_3_1_convolve_center_pixel_slc_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_7_0_1_itm_2_6_0,
      fsm_output[12]);
  assign nl_z_out_3 = conv_u2u_8_9({catapult_conv2d_14_14_3_3_1_convolve_center_pixel_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_mux_cse
      , main_loop_5_mux_3_nl}) + 9'b000000001;
  assign z_out_3 = nl_z_out_3[8:0];
  assign operator_28_true_2_mux_1_nl = MUX_v_6_2_2((z_out_10[6:1]), ({reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd
      , (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1[6:2])}), fsm_output[8]);
  assign nl_operator_28_true_2_acc_nl = conv_u2u_6_7(operator_28_true_2_mux_1_nl)
      + 7'b1001111;
  assign operator_28_true_2_acc_nl = nl_operator_28_true_2_acc_nl[6:0];
  assign z_out_4_6 = readslicef_7_1_6(operator_28_true_2_acc_nl);
  assign catapult_conv2d_14_14_3_3_1_send_results_end_mux_2_nl = MUX_v_8_2_2((signext_8_7({(~
      max_pool_sva) , max_pool_sva , max_pool_sva , 1'b0 , (~ max_pool_sva) , 1'b0
      , max_pool_sva})), ({reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd
      , reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1}), fsm_output[2]);
  assign nl_z_out_5 = ({1'b1 , (signext_2_1(~ (fsm_output[2]))) , 4'b1111 , (~ (fsm_output[2]))
      , 1'b1}) + conv_u2u_8_9(catapult_conv2d_14_14_3_3_1_send_results_end_mux_2_nl);
  assign z_out_5 = nl_z_out_5[8:0];
  assign load_images_loop_mux_4_nl = MUX_v_28_2_2((~ num_input_images_rsci_idat),
      (~ num_output_images_sva), fsm_output[4]);
  assign nl_load_images_loop_acc_nl = conv_s2u_28_29(load_images_loop_mux_4_nl) +
      29'b00000000000000000000000000001;
  assign load_images_loop_acc_nl = nl_load_images_loop_acc_nl[28:0];
  assign z_out_6_28 = readslicef_29_1_28(load_images_loop_acc_nl);
  assign main_loop_2_and_8_nl = or_184_cse & (~ operator_28_true_2_or_cse);
  assign main_loop_2_mux_9_nl = MUX_v_4_2_2(catapult_conv2d_14_14_3_3_1_convolve_row_3_0_pc0_sva,
      main_loop_2_index_3_0_sva, main_loop_2_and_8_nl);
  assign nl_z_out_7 = main_loop_2_mux_9_nl + 4'b0001;
  assign z_out_7 = nl_z_out_7[3:0];
  assign operator_28_true_1_mux_1_nl = MUX_v_4_2_2((catapult_conv2d_14_14_3_3_1_convolve_col_4_0_sva_3[4:1]),
      z_out_7, fsm_output[13]);
  assign nl_operator_28_true_1_acc_nl_1 = conv_s2u_4_5(operator_28_true_1_mux_1_nl)
      + 5'b11001;
  assign operator_28_true_1_acc_nl_1 = nl_operator_28_true_1_acc_nl_1[4:0];
  assign z_out_8_4 = readslicef_5_1_4(operator_28_true_1_acc_nl_1);
  assign operator_28_true_mux_1_nl = MUX_v_3_2_2((signext_3_1(catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0[4])),
      (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1[6:4]), or_tmp_271);
  assign not_303_nl = ~ (fsm_output[6]);
  assign operator_28_true_operator_28_true_and_1_nl = MUX_v_3_2_2(3'b000, operator_28_true_mux_1_nl,
      not_303_nl);
  assign operator_28_true_mux1h_3_nl = MUX1HOT_v_4_3_2(z_out_7, (catapult_conv2d_14_14_3_3_1_perform_max_pool_out_pixel_5_0_sva_4_0[3:0]),
      (reg_catapult_conv2d_14_14_3_3_1_convolve_count_7_0_ftd_1[3:0]), {(fsm_output[6])
      , (fsm_output[8]) , or_tmp_271});
  assign operator_28_true_operator_28_true_or_3_nl = (~ or_tmp_271) | (fsm_output[8]);
  assign not_305_nl = ~ or_tmp_271;
  assign operator_28_true_operator_28_true_or_4_nl = MUX_v_2_2_2((signext_2_1(fsm_output[8])),
      2'b11, not_305_nl);
  assign nl_z_out_9 = ({operator_28_true_operator_28_true_and_1_nl , operator_28_true_mux1h_3_nl})
      + conv_s2u_5_7({operator_28_true_operator_28_true_or_3_nl , (fsm_output[8])
      , operator_28_true_operator_28_true_or_4_nl , 1'b1});
  assign z_out_9 = nl_z_out_9[6:0];
  assign catapult_conv2d_14_14_3_3_1_convolve_do_mux_5_nl = MUX_v_7_2_2((catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_sva_3[8:2]),
      reg_catapult_conv2d_14_14_3_3_1_convolve_center_pixel_8_0_ftd_1, fsm_output[13]);
  assign nl_z_out_10 = catapult_conv2d_14_14_3_3_1_convolve_do_mux_5_nl + ({(~ (fsm_output[13]))
      , 2'b00 , (signext_3_1(~ (fsm_output[13]))) , 1'b1});
  assign z_out_10 = nl_z_out_10[6:0];

  function automatic  MUX1HOT_s_1_3_2;
    input  input_2;
    input  input_1;
    input  input_0;
    input [2:0] sel;
    reg  result;
  begin
    result = input_0 & sel[0];
    result = result | (input_1 & sel[1]);
    result = result | (input_2 & sel[2]);
    MUX1HOT_s_1_3_2 = result;
  end
  endfunction


  function automatic  MUX1HOT_s_1_4_2;
    input  input_3;
    input  input_2;
    input  input_1;
    input  input_0;
    input [3:0] sel;
    reg  result;
  begin
    result = input_0 & sel[0];
    result = result | (input_1 & sel[1]);
    result = result | (input_2 & sel[2]);
    result = result | (input_3 & sel[3]);
    MUX1HOT_s_1_4_2 = result;
  end
  endfunction


  function automatic [31:0] MUX1HOT_v_32_3_2;
    input [31:0] input_2;
    input [31:0] input_1;
    input [31:0] input_0;
    input [2:0] sel;
    reg [31:0] result;
  begin
    result = input_0 & {32{sel[0]}};
    result = result | (input_1 & {32{sel[1]}});
    result = result | (input_2 & {32{sel[2]}});
    MUX1HOT_v_32_3_2 = result;
  end
  endfunction


  function automatic [31:0] MUX1HOT_v_32_4_2;
    input [31:0] input_3;
    input [31:0] input_2;
    input [31:0] input_1;
    input [31:0] input_0;
    input [3:0] sel;
    reg [31:0] result;
  begin
    result = input_0 & {32{sel[0]}};
    result = result | (input_1 & {32{sel[1]}});
    result = result | (input_2 & {32{sel[2]}});
    result = result | (input_3 & {32{sel[3]}});
    MUX1HOT_v_32_4_2 = result;
  end
  endfunction


  function automatic [31:0] MUX1HOT_v_32_6_2;
    input [31:0] input_5;
    input [31:0] input_4;
    input [31:0] input_3;
    input [31:0] input_2;
    input [31:0] input_1;
    input [31:0] input_0;
    input [5:0] sel;
    reg [31:0] result;
  begin
    result = input_0 & {32{sel[0]}};
    result = result | (input_1 & {32{sel[1]}});
    result = result | (input_2 & {32{sel[2]}});
    result = result | (input_3 & {32{sel[3]}});
    result = result | (input_4 & {32{sel[4]}});
    result = result | (input_5 & {32{sel[5]}});
    MUX1HOT_v_32_6_2 = result;
  end
  endfunction


  function automatic [3:0] MUX1HOT_v_4_3_2;
    input [3:0] input_2;
    input [3:0] input_1;
    input [3:0] input_0;
    input [2:0] sel;
    reg [3:0] result;
  begin
    result = input_0 & {4{sel[0]}};
    result = result | (input_1 & {4{sel[1]}});
    result = result | (input_2 & {4{sel[2]}});
    MUX1HOT_v_4_3_2 = result;
  end
  endfunction


  function automatic [4:0] MUX1HOT_v_5_3_2;
    input [4:0] input_2;
    input [4:0] input_1;
    input [4:0] input_0;
    input [2:0] sel;
    reg [4:0] result;
  begin
    result = input_0 & {5{sel[0]}};
    result = result | (input_1 & {5{sel[1]}});
    result = result | (input_2 & {5{sel[2]}});
    MUX1HOT_v_5_3_2 = result;
  end
  endfunction


  function automatic [6:0] MUX1HOT_v_7_3_2;
    input [6:0] input_2;
    input [6:0] input_1;
    input [6:0] input_0;
    input [2:0] sel;
    reg [6:0] result;
  begin
    result = input_0 & {7{sel[0]}};
    result = result | (input_1 & {7{sel[1]}});
    result = result | (input_2 & {7{sel[2]}});
    MUX1HOT_v_7_3_2 = result;
  end
  endfunction


  function automatic [6:0] MUX1HOT_v_7_4_2;
    input [6:0] input_3;
    input [6:0] input_2;
    input [6:0] input_1;
    input [6:0] input_0;
    input [3:0] sel;
    reg [6:0] result;
  begin
    result = input_0 & {7{sel[0]}};
    result = result | (input_1 & {7{sel[1]}});
    result = result | (input_2 & {7{sel[2]}});
    result = result | (input_3 & {7{sel[3]}});
    MUX1HOT_v_7_4_2 = result;
  end
  endfunction


  function automatic  MUX_s_1_2_2;
    input  input_0;
    input  input_1;
    input  sel;
    reg  result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_s_1_2_2 = result;
  end
  endfunction


  function automatic [9:0] MUX_v_10_2_2;
    input [9:0] input_0;
    input [9:0] input_1;
    input  sel;
    reg [9:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_10_2_2 = result;
  end
  endfunction


  function automatic [13:0] MUX_v_14_2_2;
    input [13:0] input_0;
    input [13:0] input_1;
    input  sel;
    reg [13:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_14_2_2 = result;
  end
  endfunction


  function automatic [27:0] MUX_v_28_2_2;
    input [27:0] input_0;
    input [27:0] input_1;
    input  sel;
    reg [27:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_28_2_2 = result;
  end
  endfunction


  function automatic [1:0] MUX_v_2_2_2;
    input [1:0] input_0;
    input [1:0] input_1;
    input  sel;
    reg [1:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_2_2_2 = result;
  end
  endfunction


  function automatic [29:0] MUX_v_30_2_2;
    input [29:0] input_0;
    input [29:0] input_1;
    input  sel;
    reg [29:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_30_2_2 = result;
  end
  endfunction


  function automatic [31:0] MUX_v_32_2_2;
    input [31:0] input_0;
    input [31:0] input_1;
    input  sel;
    reg [31:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_32_2_2 = result;
  end
  endfunction


  function automatic [2:0] MUX_v_3_2_2;
    input [2:0] input_0;
    input [2:0] input_1;
    input  sel;
    reg [2:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_3_2_2 = result;
  end
  endfunction


  function automatic [3:0] MUX_v_4_2_2;
    input [3:0] input_0;
    input [3:0] input_1;
    input  sel;
    reg [3:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_4_2_2 = result;
  end
  endfunction


  function automatic [4:0] MUX_v_5_2_2;
    input [4:0] input_0;
    input [4:0] input_1;
    input  sel;
    reg [4:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_5_2_2 = result;
  end
  endfunction


  function automatic [63:0] MUX_v_64_2_2;
    input [63:0] input_0;
    input [63:0] input_1;
    input  sel;
    reg [63:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_64_2_2 = result;
  end
  endfunction


  function automatic [5:0] MUX_v_6_2_2;
    input [5:0] input_0;
    input [5:0] input_1;
    input  sel;
    reg [5:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_6_2_2 = result;
  end
  endfunction


  function automatic [6:0] MUX_v_7_2_2;
    input [6:0] input_0;
    input [6:0] input_1;
    input  sel;
    reg [6:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_7_2_2 = result;
  end
  endfunction


  function automatic [7:0] MUX_v_8_2_2;
    input [7:0] input_0;
    input [7:0] input_1;
    input  sel;
    reg [7:0] result;
  begin
    case (sel)
      1'b0 : begin
        result = input_0;
      end
      default : begin
        result = input_1;
      end
    endcase
    MUX_v_8_2_2 = result;
  end
  endfunction


  function automatic [0:0] readslicef_10_1_9;
    input [9:0] vector;
    reg [9:0] tmp;
  begin
    tmp = vector >> 9;
    readslicef_10_1_9 = tmp[0:0];
  end
  endfunction


  function automatic [0:0] readslicef_29_1_28;
    input [28:0] vector;
    reg [28:0] tmp;
  begin
    tmp = vector >> 28;
    readslicef_29_1_28 = tmp[0:0];
  end
  endfunction


  function automatic [0:0] readslicef_33_1_32;
    input [32:0] vector;
    reg [32:0] tmp;
  begin
    tmp = vector >> 32;
    readslicef_33_1_32 = tmp[0:0];
  end
  endfunction


  function automatic [32:0] readslicef_34_33_1;
    input [33:0] vector;
    reg [33:0] tmp;
  begin
    tmp = vector >> 1;
    readslicef_34_33_1 = tmp[32:0];
  end
  endfunction


  function automatic [0:0] readslicef_4_1_3;
    input [3:0] vector;
    reg [3:0] tmp;
  begin
    tmp = vector >> 3;
    readslicef_4_1_3 = tmp[0:0];
  end
  endfunction


  function automatic [48:0] readslicef_50_49_1;
    input [49:0] vector;
    reg [49:0] tmp;
  begin
    tmp = vector >> 1;
    readslicef_50_49_1 = tmp[48:0];
  end
  endfunction


  function automatic [0:0] readslicef_5_1_4;
    input [4:0] vector;
    reg [4:0] tmp;
  begin
    tmp = vector >> 4;
    readslicef_5_1_4 = tmp[0:0];
  end
  endfunction


  function automatic [48:0] readslicef_64_49_15;
    input [63:0] vector;
    reg [63:0] tmp;
  begin
    tmp = vector >> 15;
    readslicef_64_49_15 = tmp[48:0];
  end
  endfunction


  function automatic [0:0] readslicef_7_1_6;
    input [6:0] vector;
    reg [6:0] tmp;
  begin
    tmp = vector >> 6;
    readslicef_7_1_6 = tmp[0:0];
  end
  endfunction


  function automatic [9:0] signext_10_6;
    input [5:0] vector;
  begin
    signext_10_6= {{4{vector[5]}}, vector};
  end
  endfunction


  function automatic [1:0] signext_2_1;
    input  vector;
  begin
    signext_2_1= {{1{vector}}, vector};
  end
  endfunction


  function automatic [29:0] signext_30_1;
    input  vector;
  begin
    signext_30_1= {{29{vector}}, vector};
  end
  endfunction


  function automatic [2:0] signext_3_1;
    input  vector;
  begin
    signext_3_1= {{2{vector}}, vector};
  end
  endfunction


  function automatic [7:0] signext_8_7;
    input [6:0] vector;
  begin
    signext_8_7= {{1{vector[6]}}, vector};
  end
  endfunction


  function automatic [3:0] conv_s2s_1_4 ;
    input [0:0]  vector ;
  begin
    conv_s2s_1_4 = {{3{vector[0]}}, vector};
  end
  endfunction


  function automatic [9:0] conv_s2s_9_10 ;
    input [8:0]  vector ;
  begin
    conv_s2s_9_10 = {vector[8], vector};
  end
  endfunction


  function automatic [32:0] conv_s2s_32_33 ;
    input [31:0]  vector ;
  begin
    conv_s2s_32_33 = {vector[31], vector};
  end
  endfunction


  function automatic [49:0] conv_s2s_49_50 ;
    input [48:0]  vector ;
  begin
    conv_s2s_49_50 = {vector[48], vector};
  end
  endfunction


  function automatic [4:0] conv_s2u_4_5 ;
    input [3:0]  vector ;
  begin
    conv_s2u_4_5 = {vector[3], vector};
  end
  endfunction


  function automatic [6:0] conv_s2u_5_7 ;
    input [4:0]  vector ;
  begin
    conv_s2u_5_7 = {{2{vector[4]}}, vector};
  end
  endfunction


  function automatic [28:0] conv_s2u_28_29 ;
    input [27:0]  vector ;
  begin
    conv_s2u_28_29 = {vector[27], vector};
  end
  endfunction


  function automatic [32:0] conv_s2u_32_33 ;
    input [31:0]  vector ;
  begin
    conv_s2u_32_33 = {vector[31], vector};
  end
  endfunction


  function automatic [33:0] conv_s2u_33_34 ;
    input [32:0]  vector ;
  begin
    conv_s2u_33_34 = {vector[32], vector};
  end
  endfunction


  function automatic [3:0] conv_u2s_1_4 ;
    input [0:0]  vector ;
  begin
    conv_u2s_1_4 = {{3{1'b0}}, vector};
  end
  endfunction


  function automatic [3:0] conv_u2s_3_4 ;
    input [2:0]  vector ;
  begin
    conv_u2s_3_4 =  {1'b0, vector};
  end
  endfunction


  function automatic [9:0] conv_u2s_9_10 ;
    input [8:0]  vector ;
  begin
    conv_u2s_9_10 =  {1'b0, vector};
  end
  endfunction


  function automatic [6:0] conv_u2u_6_7 ;
    input [5:0]  vector ;
  begin
    conv_u2u_6_7 = {1'b0, vector};
  end
  endfunction


  function automatic [10:0] conv_u2u_7_11 ;
    input [6:0]  vector ;
  begin
    conv_u2u_7_11 = {{4{1'b0}}, vector};
  end
  endfunction


  function automatic [8:0] conv_u2u_8_9 ;
    input [7:0]  vector ;
  begin
    conv_u2u_8_9 = {1'b0, vector};
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    catapult_conv2d_14_14_3_3_1
// ------------------------------------------------------------------


module catapult_conv2d_14_14_3_3_1 (
  clk, rstn, image_channel_rsc_dat, image_channel_rsc_vld, image_channel_rsc_rdy,
      weight_memory_rsc_radr, weight_memory_rsc_re, weight_memory_rsc_q, weight_memory_triosy_lz,
      bias_memory_rsc_radr, bias_memory_rsc_re, bias_memory_rsc_q, bias_memory_triosy_lz,
      output_image_channel_rsc_dat, output_image_channel_rsc_vld, output_image_channel_rsc_rdy,
      bias_rsc_dat, bias_triosy_lz, relu_rsc_dat, relu_triosy_lz, max_pool_rsc_dat,
      max_pool_triosy_lz, num_input_images_rsc_dat, num_input_images_triosy_lz, num_output_images_rsc_dat,
      num_output_images_triosy_lz
);
  input clk;
  input rstn;
  input [31:0] image_channel_rsc_dat;
  input image_channel_rsc_vld;
  output image_channel_rsc_rdy;
  output [13:0] weight_memory_rsc_radr;
  output weight_memory_rsc_re;
  input [31:0] weight_memory_rsc_q;
  output weight_memory_triosy_lz;
  output [9:0] bias_memory_rsc_radr;
  output bias_memory_rsc_re;
  input [31:0] bias_memory_rsc_q;
  output bias_memory_triosy_lz;
  output [31:0] output_image_channel_rsc_dat;
  output output_image_channel_rsc_vld;
  input output_image_channel_rsc_rdy;
  input bias_rsc_dat;
  output bias_triosy_lz;
  input relu_rsc_dat;
  output relu_triosy_lz;
  input max_pool_rsc_dat;
  output max_pool_triosy_lz;
  input [27:0] num_input_images_rsc_dat;
  output num_input_images_triosy_lz;
  input [27:0] num_output_images_rsc_dat;
  output num_output_images_triosy_lz;


  // Interconnect Declarations
  wire [13:0] weight_memory_rsci_radr_d;
  wire [31:0] weight_memory_rsci_q_d;
  wire [9:0] bias_memory_rsci_radr_d;
  wire [31:0] bias_memory_rsci_q_d;
  wire [11:0] current_image_rsci_radr_d;
  wire [11:0] current_image_rsci_wadr_d;
  wire [31:0] current_image_rsci_d_d;
  wire [31:0] current_image_rsci_q_d;
  wire [7:0] convolution_output_rsci_radr_d;
  wire [7:0] convolution_output_rsci_wadr_d;
  wire [31:0] convolution_output_rsci_d_d;
  wire [31:0] convolution_output_rsci_q_d;
  wire [7:0] relu_output_rsci_radr_d;
  wire [7:0] relu_output_rsci_wadr_d;
  wire [31:0] relu_output_rsci_d_d;
  wire [31:0] relu_output_rsci_q_d;
  wire [7:0] output_buffer_rsci_radr_d;
  wire [7:0] output_buffer_rsci_wadr_d;
  wire [31:0] output_buffer_rsci_d_d;
  wire [31:0] output_buffer_rsci_q_d;
  wire catapult_conv2d_14_14_3_3_1_stall;
  wire current_image_rsc_we;
  wire [31:0] current_image_rsc_d;
  wire [11:0] current_image_rsc_wadr;
  wire [31:0] current_image_rsc_q;
  wire current_image_rsc_re;
  wire [11:0] current_image_rsc_radr;
  wire convolution_output_rsc_we;
  wire [31:0] convolution_output_rsc_d;
  wire [7:0] convolution_output_rsc_wadr;
  wire [31:0] convolution_output_rsc_q;
  wire convolution_output_rsc_re;
  wire [7:0] convolution_output_rsc_radr;
  wire relu_output_rsc_we;
  wire [31:0] relu_output_rsc_d;
  wire [7:0] relu_output_rsc_wadr;
  wire [31:0] relu_output_rsc_q;
  wire relu_output_rsc_re;
  wire [7:0] relu_output_rsc_radr;
  wire output_buffer_rsc_we;
  wire [31:0] output_buffer_rsc_d;
  wire [7:0] output_buffer_rsc_wadr;
  wire [31:0] output_buffer_rsc_q;
  wire output_buffer_rsc_re;
  wire [7:0] output_buffer_rsc_radr;
  wire weight_memory_rsci_re_d_iff;
  wire bias_memory_rsci_re_d_iff;
  wire current_image_rsci_we_d_iff;
  wire current_image_rsci_re_d_iff;
  wire convolution_output_rsci_we_d_iff;
  wire convolution_output_rsci_re_d_iff;
  wire relu_output_rsci_we_d_iff;
  wire relu_output_rsci_re_d_iff;
  wire output_buffer_rsci_we_d_iff;
  wire output_buffer_rsci_re_d_iff;


  // Interconnect Declarations for Component Instantiations 
  ccs_stallbuf_v1 #(.rscid(32'sd0),
  .width(32'sd1)) catapult_conv2d_14_14_3_3_1_stalldrv (
      .stalldrv(catapult_conv2d_14_14_3_3_1_stall)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd12),
  .depth(32'sd3960)) current_image_rsc_comp (
      .radr(current_image_rsc_radr),
      .wadr(current_image_rsc_wadr),
      .d(current_image_rsc_d),
      .we(current_image_rsc_we),
      .re(current_image_rsc_re),
      .clk(clk),
      .q(current_image_rsc_q)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd8),
  .depth(32'sd198)) convolution_output_rsc_comp (
      .radr(convolution_output_rsc_radr),
      .wadr(convolution_output_rsc_wadr),
      .d(convolution_output_rsc_d),
      .we(convolution_output_rsc_we),
      .re(convolution_output_rsc_re),
      .clk(clk),
      .q(convolution_output_rsc_q)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd8),
  .depth(32'sd198)) relu_output_rsc_comp (
      .radr(relu_output_rsc_radr),
      .wadr(relu_output_rsc_wadr),
      .d(relu_output_rsc_d),
      .we(relu_output_rsc_we),
      .re(relu_output_rsc_re),
      .clk(clk),
      .q(relu_output_rsc_q)
    );
  ccs_ram_sync_1R1W #(.data_width(32'sd32),
  .addr_width(32'sd8),
  .depth(32'sd198)) output_buffer_rsc_comp (
      .radr(output_buffer_rsc_radr),
      .wadr(output_buffer_rsc_wadr),
      .d(output_buffer_rsc_d),
      .we(output_buffer_rsc_we),
      .re(output_buffer_rsc_re),
      .clk(clk),
      .q(output_buffer_rsc_q)
    );
  catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_2_32_14_16384_16384_32_5_gen
      weight_memory_rsci (
      .q(weight_memory_rsc_q),
      .re(weight_memory_rsc_re),
      .radr(weight_memory_rsc_radr),
      .radr_d(weight_memory_rsci_radr_d),
      .re_d(weight_memory_rsci_re_d_iff),
      .q_d(weight_memory_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(weight_memory_rsci_re_d_iff)
    );
  catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rport_3_32_10_1024_1024_32_5_gen
      bias_memory_rsci (
      .q(bias_memory_rsc_q),
      .re(bias_memory_rsc_re),
      .radr(bias_memory_rsc_radr),
      .radr_d(bias_memory_rsci_radr_d),
      .re_d(bias_memory_rsci_re_d_iff),
      .q_d(bias_memory_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(bias_memory_rsci_re_d_iff)
    );
  catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_13_32_12_3960_3960_32_5_gen
      current_image_rsci (
      .we(current_image_rsc_we),
      .d(current_image_rsc_d),
      .wadr(current_image_rsc_wadr),
      .q(current_image_rsc_q),
      .re(current_image_rsc_re),
      .radr(current_image_rsc_radr),
      .radr_d(current_image_rsci_radr_d),
      .wadr_d(current_image_rsci_wadr_d),
      .d_d(current_image_rsci_d_d),
      .we_d(current_image_rsci_we_d_iff),
      .re_d(current_image_rsci_re_d_iff),
      .q_d(current_image_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(current_image_rsci_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(current_image_rsci_we_d_iff)
    );
  catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_14_32_8_198_198_32_5_gen
      convolution_output_rsci (
      .we(convolution_output_rsc_we),
      .d(convolution_output_rsc_d),
      .wadr(convolution_output_rsc_wadr),
      .q(convolution_output_rsc_q),
      .re(convolution_output_rsc_re),
      .radr(convolution_output_rsc_radr),
      .radr_d(convolution_output_rsci_radr_d),
      .wadr_d(convolution_output_rsci_wadr_d),
      .d_d(convolution_output_rsci_d_d),
      .we_d(convolution_output_rsci_we_d_iff),
      .re_d(convolution_output_rsci_re_d_iff),
      .q_d(convolution_output_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(convolution_output_rsci_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(convolution_output_rsci_we_d_iff)
    );
  catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_16_32_8_198_198_32_5_gen
      relu_output_rsci (
      .we(relu_output_rsc_we),
      .d(relu_output_rsc_d),
      .wadr(relu_output_rsc_wadr),
      .q(relu_output_rsc_q),
      .re(relu_output_rsc_re),
      .radr(relu_output_rsc_radr),
      .radr_d(relu_output_rsci_radr_d),
      .wadr_d(relu_output_rsci_wadr_d),
      .d_d(relu_output_rsci_d_d),
      .we_d(relu_output_rsci_we_d_iff),
      .re_d(relu_output_rsci_re_d_iff),
      .q_d(relu_output_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(relu_output_rsci_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(relu_output_rsci_we_d_iff)
    );
  catapult_conv2d_14_14_3_3_1_ccs_sample_mem_ccs_ram_sync_1R1W_rwport_17_32_8_198_198_32_5_gen
      output_buffer_rsci (
      .we(output_buffer_rsc_we),
      .d(output_buffer_rsc_d),
      .wadr(output_buffer_rsc_wadr),
      .q(output_buffer_rsc_q),
      .re(output_buffer_rsc_re),
      .radr(output_buffer_rsc_radr),
      .radr_d(output_buffer_rsci_radr_d),
      .wadr_d(output_buffer_rsci_wadr_d),
      .d_d(output_buffer_rsci_d_d),
      .we_d(output_buffer_rsci_we_d_iff),
      .re_d(output_buffer_rsci_re_d_iff),
      .q_d(output_buffer_rsci_q_d),
      .port_0_r_ram_ir_internal_RMASK_B_d(output_buffer_rsci_re_d_iff),
      .port_1_w_ram_ir_internal_WMASK_B_d(output_buffer_rsci_we_d_iff)
    );
  catapult_conv2d_14_14_3_3_1_run catapult_conv2d_14_14_3_3_1_run_inst (
      .clk(clk),
      .rstn(rstn),
      .image_channel_rsc_dat(image_channel_rsc_dat),
      .image_channel_rsc_vld(image_channel_rsc_vld),
      .image_channel_rsc_rdy(image_channel_rsc_rdy),
      .weight_memory_triosy_lz(weight_memory_triosy_lz),
      .bias_memory_triosy_lz(bias_memory_triosy_lz),
      .output_image_channel_rsc_dat(output_image_channel_rsc_dat),
      .output_image_channel_rsc_vld(output_image_channel_rsc_vld),
      .output_image_channel_rsc_rdy(output_image_channel_rsc_rdy),
      .bias_rsc_dat(bias_rsc_dat),
      .bias_triosy_lz(bias_triosy_lz),
      .relu_rsc_dat(relu_rsc_dat),
      .relu_triosy_lz(relu_triosy_lz),
      .max_pool_rsc_dat(max_pool_rsc_dat),
      .max_pool_triosy_lz(max_pool_triosy_lz),
      .num_input_images_rsc_dat(num_input_images_rsc_dat),
      .num_input_images_triosy_lz(num_input_images_triosy_lz),
      .num_output_images_rsc_dat(num_output_images_rsc_dat),
      .num_output_images_triosy_lz(num_output_images_triosy_lz),
      .weight_memory_rsci_radr_d(weight_memory_rsci_radr_d),
      .weight_memory_rsci_q_d(weight_memory_rsci_q_d),
      .bias_memory_rsci_radr_d(bias_memory_rsci_radr_d),
      .bias_memory_rsci_q_d(bias_memory_rsci_q_d),
      .current_image_rsci_radr_d(current_image_rsci_radr_d),
      .current_image_rsci_wadr_d(current_image_rsci_wadr_d),
      .current_image_rsci_d_d(current_image_rsci_d_d),
      .current_image_rsci_q_d(current_image_rsci_q_d),
      .convolution_output_rsci_radr_d(convolution_output_rsci_radr_d),
      .convolution_output_rsci_wadr_d(convolution_output_rsci_wadr_d),
      .convolution_output_rsci_d_d(convolution_output_rsci_d_d),
      .convolution_output_rsci_q_d(convolution_output_rsci_q_d),
      .relu_output_rsci_radr_d(relu_output_rsci_radr_d),
      .relu_output_rsci_wadr_d(relu_output_rsci_wadr_d),
      .relu_output_rsci_d_d(relu_output_rsci_d_d),
      .relu_output_rsci_q_d(relu_output_rsci_q_d),
      .output_buffer_rsci_radr_d(output_buffer_rsci_radr_d),
      .output_buffer_rsci_wadr_d(output_buffer_rsci_wadr_d),
      .output_buffer_rsci_d_d(output_buffer_rsci_d_d),
      .output_buffer_rsci_q_d(output_buffer_rsci_q_d),
      .catapult_conv2d_14_14_3_3_1_stall(catapult_conv2d_14_14_3_3_1_stall),
      .weight_memory_rsci_re_d_pff(weight_memory_rsci_re_d_iff),
      .bias_memory_rsci_re_d_pff(bias_memory_rsci_re_d_iff),
      .current_image_rsci_we_d_pff(current_image_rsci_we_d_iff),
      .current_image_rsci_re_d_pff(current_image_rsci_re_d_iff),
      .convolution_output_rsci_we_d_pff(convolution_output_rsci_we_d_iff),
      .convolution_output_rsci_re_d_pff(convolution_output_rsci_re_d_iff),
      .relu_output_rsci_we_d_pff(relu_output_rsci_we_d_iff),
      .relu_output_rsci_re_d_pff(relu_output_rsci_re_d_iff),
      .output_buffer_rsci_we_d_pff(output_buffer_rsci_we_d_iff),
      .output_buffer_rsci_re_d_pff(output_buffer_rsci_re_d_iff)
    );
endmodule



