
`timescale 1ns/1ns

module bus_fifo
    (
        CLK,
        RESET_N,
        DATA_STROBE,
        DATA_IN,
        DATA_READY,
        DATA_OUT,
        DATA_ACK,
        NOT_FULL
    );

    parameter width = 8;
    parameter depth = 8;

    input               CLK;
    input               RESET_N;
    input               DATA_STROBE;
    input  [width-1:0]  DATA_IN;
    output              DATA_READY;
    output [width-1:0]  DATA_OUT;
    input               DATA_ACK;
    output              NOT_FULL;

    reg    [width-1:0]  fifo_mem [(1<<depth)-1:0];
    reg    [depth-1:0]  input_pointer;
    reg    [depth-1:0]  output_pointer;
    wire   [depth-1:0]  load;

    wire                empty;
    wire   [depth:0]    input_pointer_plus_one;

    assign empty = (input_pointer == output_pointer) ? 1'b1 : 1'b0;
    assign NOT_FULL  = (input_pointer_plus_one == output_pointer) ? 1'b0 : 1'b1;
    assign DATA_READY = !empty;

    assign input_pointer_plus_one = input_pointer + 1;
    assign DATA_OUT = (empty) ? {width {1'bz}} : fifo_mem[output_pointer];

    assign load = input_pointer - output_pointer;

    always @(posedge CLK) begin
        
        if (!RESET_N) begin 
            input_pointer <= {width{1'b0}};
            output_pointer <= {width{1'b0}};
        end else begin
            if (DATA_STROBE & NOT_FULL) begin
                fifo_mem[input_pointer] <= DATA_IN;
                input_pointer <= input_pointer + 1;
            end
            if (DATA_ACK & !empty) begin
                output_pointer <= output_pointer + 1;
            end
        end
    end

endmodule
