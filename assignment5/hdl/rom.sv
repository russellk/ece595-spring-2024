
module rom
    (
        clk,
        rstn,
        read_address,
        data_out,
        output_enable
    );

    parameter address_width      = 16;
    parameter data_width         = 32; 
    parameter datafile           = "please_define_a_datafile";

    input                        clk;
    input                        rstn;
    input [address_width-1:0]    read_address;
    output [data_width-1:0]      data_out;
    input                        output_enable;
    
    reg [data_width-1:0] mem [(1<<address_width)-1:0];
    reg [data_width-1:0] read_data;

    initial $readmemh(datafile, mem);

    assign data_out = read_data;

    always @(posedge clk) begin
        if (rstn == 1'b0) begin
            read_data <= { data_width {1'b0}};
        end else begin
            if (output_enable) begin
                read_data <= mem[read_address];
            end
        end
    end

endmodule
