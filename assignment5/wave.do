onerror {resume}
quietly WaveActivateNextPane {} 0
# add wave -noupdate /testbench/pixel_lines
add wave -noupdate /testbench/dut/clk
add wave -noupdate /testbench/dut/rstn
add wave -noupdate /testbench/dut/image
add wave -noupdate /testbench/dut/image_valid
add wave -noupdate /testbench/dut/image_ready
add wave -noupdate /testbench/dut/prediction
add wave -noupdate /testbench/dut/prediction_valid
add wave -noupdate /testbench/dut/prediction_ready
add wave -noupdate /testbench/dut/layer1_weight_address
add wave -noupdate /testbench/dut/layer1_weight_output_enable
add wave -noupdate /testbench/dut/layer1_weights
add wave -noupdate /testbench/dut/layer1_bias_address
add wave -noupdate /testbench/dut/layer1_bias_output_enable
add wave -noupdate /testbench/dut/layer1_bias_value
add wave -noupdate /testbench/dut/layer1_out
add wave -noupdate /testbench/dut/layer1_out_valid
add wave -noupdate /testbench/dut/layer1_out_ready
# add wave -noupdate /testbench/dut/image_height
# add wave -noupdate /testbench/dut/image_width
# add wave -noupdate /testbench/dut/tests
add wave -noupdate /testbench/dut/layer2_weight_address
add wave -noupdate /testbench/dut/layer2_weight_output_enable
add wave -noupdate /testbench/dut/layer2_weights
add wave -noupdate /testbench/dut/layer2_bias_address
add wave -noupdate /testbench/dut/layer2_bias_output_enable
add wave -noupdate /testbench/dut/layer2_bias_value
add wave -noupdate /testbench/dut/layer2_out
add wave -noupdate /testbench/dut/layer2_out_valid
add wave -noupdate /testbench/dut/layer2_out_ready
add wave -noupdate /testbench/dut/layer3_weight_even_address
add wave -noupdate /testbench/dut/layer3_weight_even_output_enable
add wave -noupdate /testbench/dut/layer3_even_weights
add wave -noupdate /testbench/dut/layer3_weight_odd_address
add wave -noupdate /testbench/dut/layer3_weight_odd_output_enable
add wave -noupdate /testbench/dut/layer3_odd_weights
add wave -noupdate /testbench/dut/layer3_bias_address
add wave -noupdate /testbench/dut/layer3_bias_output_enable
add wave -noupdate /testbench/dut/layer3_bias_value
add wave -noupdate /testbench/dut/layer3_out
add wave -noupdate /testbench/dut/layer3_out_valid
add wave -noupdate /testbench/dut/layer3_out_ready
# add wave -noupdate /testbench/dut/layer3_out_fifo
# add wave -noupdate /testbench/dut/layer3_out_valid_fifo
# add wave -noupdate /testbench/dut/layer3_out_ready_fifo
add wave -noupdate /testbench/dut/layer4_weight_even_address
add wave -noupdate /testbench/dut/layer4_weight_even_output_enable
add wave -noupdate /testbench/dut/layer4_even_weights
add wave -noupdate /testbench/dut/layer4_weight_odd_address
add wave -noupdate /testbench/dut/layer4_weight_odd_output_enable
add wave -noupdate /testbench/dut/layer4_odd_weights
add wave -noupdate /testbench/dut/layer4_bias_address
add wave -noupdate /testbench/dut/layer4_bias_output_enable
add wave -noupdate /testbench/dut/layer4_bias_value
add wave -noupdate /testbench/dut/layer4_out
add wave -noupdate /testbench/dut/layer4_out_valid
add wave -noupdate /testbench/dut/layer4_out_ready
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1 us}
