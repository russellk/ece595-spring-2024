
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#include "svdpi.h"
#include "tbxbindings.h"

#ifndef NUM_TESTS
#define NUM_TESTS 10
#endif

static const int image_size = 28*28;

static FILE *image_file;
static bool  data_ready = false;
static int   image_index;
static int   image[image_size];
static unsigned char char_image[image_size];  
static char  labels[NUM_TESTS];
static char  label;
static int   label_index_in = 0;
static int   label_index_out = 0;
static int   correct_inferences = 0;

void print_char_image(unsigned char *image)
{
    for (int r=0; r<28; r++) {
      for (int c=0; c<28; c++) {
         printf("%2x ", image[r*28 +c]);
      }
      printf("\n");
   }
} 

void get_new_image()
{
    static bool   init = false;
    static int    image_count = 0;
    static int    size = 0;
    char         *filename = "/u/russellk/mnist_data.bin";
    struct stat   st;
    int           record; 
    int           r;

    if (!init) {
       init = true;

       image_file = fopen(filename, "r");
       if (!image_file) {
          fprintf(stderr, "Unable to open file \"%s\" for reading \n", filename);
          perror("testbench");
          exit(1);
       } 

       if (!stat(filename, &st)) size = st.st_size;
       else {
          fprintf(stderr, "Unable to stat file \"%s\" \n", filename);
          perror("testbench");
          exit(2);
       }
       srand(time(NULL));
    }

    record = rand() % (size/(image_size + 1));

    fseek(image_file, record * (image_size + 1), SEEK_SET);

    r = fread(&label, 1, 1, image_file);
    if (r != 1) {
        printf("Error on reading label from input file \n");
        printf("Read %d, expected %d \n", r, 1);
        printf("Error number is %d \n", ferror(image_file));
        exit(3);
     }

     labels[label_index_in++] = label;

     //if (chatty) prinf("label: %d \n", label);

     r = fread(char_image, image_size, 1, image_file);
     if (r != 1) {
        printf("Error on reading image from input file \n");
        printf("Read %d, expected %d \n", r, 1);
        printf("Error number is %d \n", ferror(image_file));
        exit(4);
     }

    //if (chatty) print_char_image(char_image);

    for (int i=0; i<image_size; i++) {
        float          value           = ((float) char_image[i])/255.0;
        float          shifted_value   = value * 0x10000;
        unsigned long  bit_pattern     = (unsigned long) shifted_value;

        image[i] = bit_pattern;
    }

    data_ready = true;
    image_index = 0;
}

void get_num_tests(int *n)
{
   *n = NUM_TESTS;
   printf("Running %d tests \n", NUM_TESTS);
}

void get_feature(svBitVecVal *feature)
{
    const bool chatty = true;

    if (!data_ready) get_new_image();
    svPutPartselBit(feature, image[image_index++], 0, 32);
    if (image_index >= image_size) {
       if (chatty) {
          printf("label: %d \n", label);
          print_char_image(char_image);
       }
       data_ready = false;
    }
}


static int max_probabilities(int *probabilities)
{
    int max = 0;
    int max_val = probabilities[0];

    for (int i=0; i<10; i++) {
       if (max_val < probabilities[i]) {
          max = i;
          max_val = probabilities[i];
       }
    }

    return max;
}

void send_prediction(const svBitVecVal* buffer, int digit_in)
{
    svBitVecVal b;
    static int digit = 0;
    static int probabilities[10];
    char label;
    const bool chatty = true;

    svGetPartselBit(&b, buffer, 0, 32);

    probabilities[digit++] = b;

    if (digit >= 10) {
       label = labels[label_index_out++];
       int prediction = max_probabilities(probabilities);
       if (chatty) {
          printf("label = %d \n", label);
          for (int i=0; i<10; i++) printf("p[%d] = %5.3f \n", i, ((float)probabilities[i])/65535.0);
       }
       if (label == prediction) {
          correct_inferences++;
          if (chatty) printf("Correct Prediction!!\n");
       } else {
          if (chatty) printf("You were WRONG! \n");
       }
       digit = 0;
    }
}

void finish_up()
{
    fclose(image_file);
    printf("=========================================\n");
    printf("mnist accuracy = %5.2f  (%5d/%5d)        \n", (((float) correct_inferences) * 100.0) / label_index_out, correct_inferences, NUM_TESTS);
    printf("=========================================\n");
}
 
