import scemi_pipes_pkg::*;

`define NUM_TESTS 20
`define IMAGE_SIZE (28*28)
`define BUFFER_SIZE (`IMAGE_SIZE*4)

module testbench;

   integer image_file;
   bit     done = 0;

   // pipes require that data be passed as unsigned bytes

   byte unsigned image_buffer [] = new[`BUFFER_SIZE];
   byte unsigned prediction_buffer [] = new [40];

   byte unsigned image[`IMAGE_SIZE-1:0];
   real predictions[10];

   byte  label;

   task print_image();
      int   r;
      int   c;
      
      begin
        $display("label = %d ", label);
        for (r=0; r<28; r++) begin
           for (c=0; c<28; c++) begin
              $write("%2x ", image[28*r+c]);
           end
           $write("\n");
        end
      end
   endtask

   task copy_predictions_from_buffer();
      real f, r;
      int i;
   
      begin
        for (i=0; i<10; i++) begin
           f = 0.0;
           r = prediction_buffer[i*4+0];
           f += r / 32'h00010000;
           r = prediction_buffer[i*4+1];
           f += r / 32'h00000100;
           r = prediction_buffer[i*4+2];
           f += r * 32'h00000100;
           r = prediction_buffer[i*4+3];
           f += r * 32'h00010000;
           predictions[i] = f;
        end
      end
    endtask
           

   task copy_image_to_buffer();
      int i;
      real f;
      int n;

      begin
        for (i=0; i<`IMAGE_SIZE; i++) begin
           f = (image[i] * 32'h00010000)/255.0;
           n = f;
           image_buffer[i*4+0] = (n >> 0) & 8'hFF;
           image_buffer[i*4+1] = (n >> 8) & 8'hFF;
           image_buffer[i*4+2] = (n >> 16) & 8'hFF;
           image_buffer[i*4+3] = (n >> 24) & 8'hFF;
        end
      end
   endtask


   initial image_file = $fopen("/u/russellk/mnist_data.bin", "rb");

   task get_random_image();
      int i;
      byte n;
      int index;

      begin
        index = $urandom_range(10000) * (`IMAGE_SIZE + 1);
        $fseek(image_file, index, 0);
        $fread(n, image_file);
        label = n;
        for (i=0; i<`IMAGE_SIZE; i++) begin
          $fread(n, image_file);
          image[i] = n;
        end
      end
   endtask

   final $fclose(image_file);


   function int max_prediction(real predictions[]);
      int i;
      automatic int max_index = 0;
      automatic real maxp = predictions[0];

      for (i=0; i<10; i++) begin
         if (maxp < predictions[i]) begin
            maxp = predictions[i];
            max_index = i;
         end
      end
      max_prediction = max_index;
   endfunction

   // add your code to send and receive over pipes here
   // also, compute accuracy

endmodule : testbench




