
module two_roads;

  parameter north = 0;
  parameter east  = 1;
  parameter south = 2;
  parameter west  = 3;
  parameter north_to_west = 4;
  parameter west_to_south = 5;
  parameter south_to_east = 6;
  parameter east_to_north = 7;

  parameter LIGHTS = 8;

  logic clock;
  logic reset;

  // tbx clkgen
  initial begin
    clock = 0;
    forever #1
      clock = ~clock;
  end

  // tbx clkgen
  initial begin
     reset = 1;
     #20 reset = 0;
     #2000000 $finish();
  end

  logic [LIGHTS-1:0] clear;
  logic [LIGHTS-1:0] waiting;
  logic [LIGHTS-1:0] traffic;
  logic [LIGHTS-1:0] red;
  logic [LIGHTS-1:0] yellow;
  logic [LIGHTS-1:0] green;

  genvar n;

  generate for(n=0; n<LIGHTS; n++) begin

    stoplight light(
       .clock         (clock),
       .reset         (reset), 
       .road_clear    (clear[n]),
       .car_detected  (waiting[n]),
       .red           (red[n]),
       .yellow        (yellow[n]),
       .green         (green[n])
    );
  
    traffic_queue north_queue(
      .clock          (clock),
      .reset          (reset),
      .add_car        (traffic[n]),
      .green_light    (green[n]),
      .car_present    (waiting[n])
    );
  
    traffic_generator north_traffic_generator(
      .clock          (clock), 
      .reset          (reset), 
      .car            (traffic[n])
    );
    end
  endgenerate

  traffic_control #(.lights(LIGHTS)) arbiter (
     .clock        (clock),
     .reset        (reset),
     .waiting      (waiting),
     .in_use       (~red),
     .clear        (clear)
  );

endmodule
