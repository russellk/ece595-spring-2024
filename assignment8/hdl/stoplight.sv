module stoplight
 #(
    parameter  PATHS = 4,
               RED = 1,
               YELLOW = 2,
               GREEN = 3,
               green_period = 20,
               yellow_period = 4
  ) (
    input  logic clock,
    input  logic reset,

    input  logic road_clear,
    input  logic car_detected,
    output logic red,
    output logic yellow,
    output logic green
  );

  logic [7:0] counter;
  logic [1:0] state;

  assign red    = state == RED;
  assign yellow = state == YELLOW;
  assign green  = state == GREEN;

  always @(posedge clock) begin
    if (reset) begin
      state <= RED;
      counter <= 0;
    end else begin 
      if (state == RED) begin
        if (car_detected & road_clear) begin
          counter <= green_period;
          state <= GREEN;
        end
      end else if (state == YELLOW) begin
        if (counter == 0) begin
          state <= RED;
        end else begin
          counter <= counter - 1;
        end
      end else if (state == GREEN) begin
        if (counter <= 0) begin
          counter <= yellow_period;
          state <= YELLOW;
        end else begin
          counter <= counter - 1;
        end
      end else begin
        // something really bad happened
        state <= RED;
      end
    end
  end
endmodule
      

