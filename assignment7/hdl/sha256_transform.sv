module sha256_xform(
   input  logic             clock,
   input  logic             reset,

   input  logic             start,
   output logic             done,
   input  logic[255:0]      state, 
   input  logic[511:0]      data, 
   output logic[31:0]       hash  [7:0]);

   function logic[31:0] rotleft(
      input logic [31:0] a, input int b);
     
      rotleft = (b==0) ? a : (a << b) | (a >> (32-b));  
   endfunction
   
   function logic[31:0] rotright(
      input logic[31:0] a, input int b);
   
      rotright = (b==0) ? a : (a >> b) | (a << (32-b));
   endfunction
   
   function logic[31:0] ch(
      input logic [31:0] x,
      input logic [31:0] y,
      input logic [31:0] z);
   
      ch = (x & y) ^ ((~x) & z);
   endfunction
   
   function logic[31:0] maj(
      input logic [31:0] x,
      input logic [31:0] y,
      input logic [31:0] z);
   
      maj = (x & y) ^ (x & z) ^ (y & z);
   endfunction;
   
   function logic[31:0] ep0(
      input logic [31:0] x);
   
      ep0 = rotright(x,2) ^ rotright(x,13) ^ rotright(x,22);
   endfunction
   
   function logic[31:0] ep1(
      input logic [31:0] x);
   
      ep1 = rotright(x,6) ^ rotright(x,11) ^ rotright(x,25);
   endfunction
   
   function logic[31:0] sig0(
      input logic [31:0] x);
      
      sig0 = rotright(x,7) ^ rotright(x,18) ^ (x >> 3);
   endfunction
   
   function logic[31:0] sig1(
      input logic [31:0] x);
   
      sig1 = rotright(x,17) ^ rotright(x,19) ^ (x >> 10);
   endfunction
         
   logic[31:0] k[63:0] = {
      32'h428a2f98,32'h71374491,32'hb5c0fbcf,32'he9b5dba5,32'h3956c25b,32'h59f111f1,32'h923f82a4,32'hab1c5ed5,
      32'hd807aa98,32'h12835b01,32'h243185be,32'h550c7dc3,32'h72be5d74,32'h80deb1fe,32'h9bdc06a7,32'hc19bf174,
      32'he49b69c1,32'hefbe4786,32'h0fc19dc6,32'h240ca1cc,32'h2de92c6f,32'h4a7484aa,32'h5cb0a9dc,32'h76f988da,
      32'h983e5152,32'ha831c66d,32'hb00327c8,32'hbf597fc7,32'hc6e00bf3,32'hd5a79147,32'h06ca6351,32'h14292967,
      32'h27b70a85,32'h2e1b2138,32'h4d2c6dfc,32'h53380d13,32'h650a7354,32'h766a0abb,32'h81c2c92e,32'h92722c85,
      32'ha2bfe8a1,32'ha81a664b,32'hc24b8b70,32'hc76c51a3,32'hd192e819,32'hd6990624,32'hf40e3585,32'h106aa070,
      32'h19a4c116,32'h1e376c08,32'h2748774c,32'h34b0bcb5,32'h391c0cb3,32'h4ed8aa4a,32'h5b9cca4f,32'h682e6ff3,
      32'h748f82ee,32'h78a5636f,32'h84c87814,32'h8cc70208,32'h90befffa,32'ha4506ceb,32'hbef9a3f7,32'hc67178f2 };

   logic [31:0] m [63:0];
   logic [7:0] pass;
   logic enable;
   logic [31:0] a, b, c, d, e, f, g, h;
 
   genvar n;

   generate for (n=0;n<16; n++) begin : low_m_init;
        assign m[n] = data[((n+1)*32)-1:(n*32)];
     end
   endgenerate

   generate for (n=16; n<64; n++) begin: high_m_assign;
         assign m[n] = sig1(m[n-2]) + m[n-7] + sig0(m[n-15]) + m[n-16];
      end
   endgenerate

   always @(posedge clock) begin
      if (reset) begin
         done <= 0;
         pass <= 8'b0;
      end else begin
         if (start) begin
            done <= 0;
            enable <= 1;
         end else begin
            if (enable) begin
               pass <= pass + 1;
            end
            if (pass == 64) begin
               done <= 1;
               enable <= 0;
               pass <= 8'b0;
            end
         end
      end
   end

   always @(posedge clock) begin
      if (reset) begin
         a <= 32'h0;
         b <= 32'h0;
         c <= 32'h0;
         d <= 32'h0;
         e <= 32'h0;
         f <= 32'h0;
         g <= 32'h0;
         h <= 32'h0;
      end else begin
         if (start) begin
            a <= state[31:0];      
            b <= state[63:32];      
            c <= state[95:64];      
            d <= state[127:96];      
            e <= state[159:128];      
            f <= state[191:160];      
            g <= state[223:192];      
            h <= state[255:224];      
         end else begin
            if (enable & (pass < 64)) begin
               a <= h + ep1(e) + ch(e,f,g) + k[pass] + m[pass] + ep0(a) + maj(a,b,c);
               b <= a;
               c <= b;
               d <= c;
               e <= d + h + ep1(e) + ch(e,f,g) + k[pass] + m[pass];
               f <= e;
               g <= f;
               h <= g;
            end
         end
      end
   end

   always @(posedge clock) begin
      if (reset) begin
         hash[0] <= 32'h0;
         hash[1] <= 32'h0;
         hash[2] <= 32'h0;
         hash[3] <= 32'h0;
         hash[4] <= 32'h0;
         hash[5] <= 32'h0;
         hash[6] <= 32'h0;
         hash[7] <= 32'h0;
      end else begin
         if (pass == 64) begin
            hash[0] <= a + state[31:0];
            hash[1] <= b + state[63:32];
            hash[2] <= c + state[95:64];
            hash[3] <= d + state[127:96];
            hash[4] <= e + state[159:128];
            hash[5] <= f + state[191:160];
            hash[6] <= g + state[223:192];
            hash[7] <= h + state[255:224];
         end
      end
   end

endmodule
