`define NUM_HASHES 100000
`define INSTANCES  10

module testbench;

   logic clock;
   logic reset;

   logic [255:0] state_vector;
   logic [511:0] data_vector;
   // logic [31:0] state [7:0] ;
   // logic [31:0] data [15:0];
   logic [31:0] hash [`INSTANCES-1:0][7:0];
   logic start;
   logic done [`INSTANCES-1:0];
   logic chatty = 0;

   initial begin
      clock = 0;
      forever #2 clock = ~clock;
   end

   initial begin 
      reset = 1;
      #20 reset = 0;
   end

   function logic [255:0] get_state();
     logic [255:0] ret;

     ret[127:000] = {$urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom()};
     ret[255:128] = {$urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom()};
     get_state = ret;
   endfunction

   function get_data();
     logic [511:0] ret;
     ret[127:000] = {$urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom()};
     ret[255:128] = {$urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom()};
     ret[383:256] = {$urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom()};
     ret[511:384] = {$urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom(), $urandom()};
     get_data = ret;
   endfunction

   genvar n;
   generate for(n=0; n<`INSTANCES; n++) begin : instance_loop

   sha256_xform hash_inst(
       .clock         (clock),
       .reset         (reset),
       .start         (start),
       .done          (done[n]),
       .state         (state_vector),
       .data          (data_vector + n),
       .hash          (hash[n]));
   end 
   endgenerate

   int i;

   initial begin
     start <= 0;
     @(negedge reset);

     state_vector = get_state();
     data_vector = get_data();

     for (i=0; i<`NUM_HASHES; i++) begin

        @(posedge clock);
        start <= 1;
        @(posedge clock);
        start <= 0;
        repeat(4) @(posedge clock);
    
        while (!done[0]) @(posedge clock);

        if (chatty)     
        for (int j=0; j<`INSTANCES; j++) begin
            $write("hash[%d] = ", j);
            for (int k=0; k<8; k++) $write("%8x ", hash[j][k]);
            $write("\n");
        end
        data_vector += `INSTANCES;
     end
     $finish();
   end
endmodule
