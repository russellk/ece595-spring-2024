view wave
dataset open ./veloce.wave/wave1.stw wave1
wave add -d wave1 {adder.a[7:0]} {adder.b[7:0]} adder.clk adder.enable adder.reset {adder.result[8:0]}
echo "wave1.stw loaded and signals added. Open the Wave window to observe outputs."