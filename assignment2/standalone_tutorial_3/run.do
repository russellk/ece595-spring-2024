configure -emul Greg
reg setvalue adder.reset 1
reg setvalue adder.enable 0
run 10
reg setvalue adder.reset 0
run 10
reg setvalue adder.enable 1
reg setvalue adder.a 8'h5
reg setvalue adder.b 8'h8
run 1
reg setvalue adder.a 8'hFF
reg setvalue adder.b 8'hFF
run 1
reg setvalue adder.a 8'h10
reg setvalue adder.b 8'h01
run 1
reg setvalue adder.a 8'hAA
reg setvalue adder.b 8'h55
run 1
reg setvalue adder.a 8'h09
reg setvalue adder.b 8'h01
run 1
reg setvalue adder.a 8'h11
reg setvalue adder.b 8'h22
run 1
reg setvalue adder.a 8'h00
reg setvalue adder.b 8'h00
run 1
reg setvalue adder.a 8'hFF
reg setvalue adder.b 8'h00
run 1
reg setvalue adder.a 8'hAB
reg setvalue adder.b 8'hCD
run 1
reg setvalue adder.a 8'h04
reg setvalue adder.b 8'h07
run 1
upload -tracedir ./veloce.wave/wave1
exit 
