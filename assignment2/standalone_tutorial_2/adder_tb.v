/*
 * Portland State University
 * Veloce Standalone Emulation Mode Tutorial
 * adder_tb.v - Memory based stanadlone mode testbench 
 * The testbench contains two memory blocks. These are loaded from txt files.
 * The testbench generates memory address and reads test cases from the memory
 * to apply to the DUT. 
 * 
 * Author: Sameer Ghewari, April 2015 
 *         Klein 2023, 2024
 */

module testbench (
    input clk, 
    input reset, 
    input enable, 
    output [8:0] result 
  );
 
  // Declare Memory to hold OperandA and OperandB. 
  // This example ueses 8 bit wide 256 location deep memory (in other worlds 256 operands can be read from text file)
  // You would want to parameterize this in a real project 

  reg [7:0] operandA_mem [255:0];
  reg [7:0] operandB_mem [255:0];
  reg [7:0] result_mem [255:0];
    
  // Read memory from text files inside initial block 
  // Note that $readmemh is synthesizable for Veloce only to load memories 

  initial
  begin
    $readmemh("operandA.txt",operandA_mem);
    $readmemh("operandB.txt",operandB_mem);
  end

  // Connection to the DUT

  reg [7:0] operandA;
  reg [7:0] operandB; 

  // Memory Address Counter 

  reg [7:0] mem_address; 
 
  // The following logic generats memory address for the memory, memory ouputs data
  // (test cases) and provide to the design 

  always @(posedge clk or posedge reset) 
  begin 
    if (reset) begin      
      mem_address <= 0;
      operandA    <= 0;
      operandB    <= 0; 
    end else begin
      if (enable) begin      

        // Increment memory address 

        mem_address <= mem_address + 1;

        // Read data (test case) from specified location and apply to the DUT

        operandA <= operandA_mem[mem_address];
        operandB <= operandB_mem[mem_address];

        // Store result

        result_mem[mem_address] <= result;
      end // else if (enable)
    end // else if (reset) 
  end // always 
 
  // Instantiate the Adder DUT 
  adder adder_inst(
        .clk     (clk), 
        .reset   (reset), 

        .result  (result), 
        .a       (operandA), 
        .b       (operandB), 
        .enable  (enable)
  ); 
 
endmodule 
