/*
 *  Portland State University
 *  Veloce Standalone Emulation Mode Tutorial
 *  adder_tb.v - Synthesizable Adder Testbench 
 *  The synthesizable testbench is achieved by using counters.
 *  They generate numbers 0-255 (8 bit)
 *  
 *  Author: Haera Chung, September 2010
 *          R. Klein, April 2023
 */

 module puresim_testbench (); 
 
  reg clk; 
  reg reset; 
  reg enable; 
  wire [8:0] result; 
 
  initial begin
    reset = 0;
    #1  reset = 1;
    #10 reset = 0; 
  end

  initial begin
    clk = 0;
  end
  always #1 clk = ~clk;

  initial enable = 1;

  testbench testbench(
    .clk      (clk),
    .reset    (reset),
    .enable   (enable),
    .result   (result)
  );

 endmodule 
