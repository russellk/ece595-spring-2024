/*
 *  Portland State University 
 *  Veloce Standalone Emulation Mode Tutorial
 *  adder.v - Adder DUT 
 *  This DUT is a simple 8 bit adder. It is synchronous. 
 *  If enable is asserted at active edge of clock, result is computed. 
 *  Note that result[8] = Carry and result[7:0] = Sum. 
 *  
 *  Author: Haera Chung, September 2010
 *          R. Klein, April 2023, 2024
 */

module adder (
    input         clk, 
    input         reset, 

    input  [7:0]  a, 
    input  [7:0]  b, 
    input         enable,
    output [8:0]  result
  );
 
  reg [8:0] result_reg; 

  assign result = result_reg; 

  always @(posedge clk) 
  begin 
    if (reset) begin
      result_reg <= 0;
    end else begin
      if (enable) result_reg <= a + b; 
    end
  end 
endmodule 
