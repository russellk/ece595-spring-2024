/*
 *  Portland State University
 *  Veloce Standalone Emulation Mode Tutorial
 *  adder_tb.v - Synthesizable Adder Testbench 
 *  The synthesizable testbench is achieved by using counters.
 *  They generate numbers 0-255 (8 bit)
 *  
 *  Author: Haera Chung, September 2010
 *          R. Klein, April 2023, 2024
 */

 module testbench (
    input clk, 
    input reset,
    input enable, 
    output [8:0] result 
  );
 
  reg [7:0] counter1; 
  reg [7:0] counter2; 

  always @(posedge clk or posedge reset) 
  begin 
    if (reset) begin      
      counter1 <= 8'h00;
      counter2 <= 8'h00; 
    end else begin
      if (enable) begin      
        if (counter1 == 255) begin
          counter1 <= 0;  
        end else begin
          counter1 <= counter1 + 1;      
        end
	  
        if (counter1 == 0) counter2 <= counter2 + 1; 
      end // else if (enable)
    end // else if (reset) 
  end // always 
 
  // Instantiate the Adder DUT 
  adder adder_inst(
    .clk    (clk), 
    .reset  (reset), 

    .result (result), 
    .a      (counter1), 
    .b      (counter2), 
    .enable (enable)
  ); 
 
 endmodule 
