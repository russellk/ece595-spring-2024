module traffic_control 
  #( parameter
      lights = 2
  ) (
     input logic clock,
     input logic reset,

     input logic [lights-1:0] waiting,
     input logic [lights-1:0] in_use,
     output logic [lights-1:0] clear
  );

  function logic [lights-1:0] top_bit(input logic [lights-1:0] requests);
  begin
    logic [lights-1:0] ret;
    int i;
    ret = 0;
    for (i=0; i<lights; i++) begin
       if (requests[i]) begin
          ret[i] = 1;
          break;
       end
    end
    top_bit = ret;
  end
  endfunction

  logic grant;

  always @(posedge clock) begin
    if (reset) begin
      grant <= 0;
    end else begin
      if (grant) grant <= 0;
      else begin
        if (!in_use) begin 
           clear <= (waiting) ? top_bit(waiting) : 0;
           grant <= 1;
        end
        else clear <= 0;
      end
    end
  end

endmodule
