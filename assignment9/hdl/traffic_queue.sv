module traffic_queue (
      input  logic clock,
      input  logic reset,

      input  logic add_car,
      input  logic green_light,

      output logic car_present
  );

  logic [9:0] car_count;
  logic [9:0] max_cars = 10'h3FF; 

  assign car_present = (car_count > 0);

  always @(posedge clock) begin
    if (reset) begin 
      car_count <= 0; 
    end else begin
      if (add_car & !green_light) if (car_count < max_cars) car_count <= car_count + 1;
      if (green_light & !add_car) if (car_count > 0) car_count <= car_count - 1;
    end
  end
endmodule
