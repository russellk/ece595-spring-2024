module traffic_generator
  #(  parameter 
      probability =   10,
      of          = 1000
  ) (
      input  logic clock,
      input  logic reset,

      output logic car
  );

/*
  always @(posedge clock) begin
    if (reset) begin
      car <= 0;
    end else begin
      if (probability > $urandom % of) car <= 1;
      else car <= 0;
    end
  end
*/
  import "DPI-C" function bit random_car(input int probability, input int of);

  always @(posedge clock) begin
    if (reset) begin
      car <= 0;
    end else begin
      car <= random_car(probability, of);
    end
  end
endmodule
