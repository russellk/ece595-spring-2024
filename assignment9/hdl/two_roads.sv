
module two_roads(
  input logic clock, 
  input logic reset,
  output logic [7:0] clear,
  output logic [7:0] waiting,
  output logic [7:0] traffic,
  output logic [7:0] red,
  output logic [7:0] yellow,
  output logic [7:0] green);

  parameter LIGHTS = 8;

  parameter north = 0;
  parameter east  = 1;
  parameter south = 2;
  parameter west  = 3;
  parameter north_to_west = 4;
  parameter west_to_south = 5;
  parameter south_to_east = 6;
  parameter east_to_north = 7;

  genvar n;

  generate for(n=0; n<LIGHTS; n++) begin

    stoplight light(
       .clock         (clock),
       .reset         (reset), 
       .road_clear    (clear[n]),
       .car_detected  (waiting[n]),
       .red           (red[n]),
       .yellow        (yellow[n]),
       .green         (green[n])
    );
  
    traffic_queue north_queue(
      .clock          (clock),
      .reset          (reset),
      .add_car        (traffic[n]),
      .green_light    (green[n]),
      .car_present    (waiting[n])
    );
  
    traffic_generator north_traffic_generator(
      .clock          (clock), 
      .reset          (reset), 
      .car            (traffic[n])
    );
    end
  endgenerate

  traffic_control #(.lights(LIGHTS)) arbiter (
     .clock        (clock),
     .reset        (reset),
     .waiting      (waiting),
     .in_use       (~red),
     .clear        (clear)
  );

endmodule
