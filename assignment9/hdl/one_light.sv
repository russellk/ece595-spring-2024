module one_light;

  logic clock;
  logic reset;

  initial begin
    clock = 0;
    forever #1
      clock = ~clock;
  end

  initial begin
     reset = 1;
     #20 reset = 0;
  end

  logic north_clear = 1;
  logic north_waiting;
  logic north_red;
  logic north_yellow;
  logic north_green;

  stoplight north(
       .clock         (clock),
       .reset         (reset), 
       .road_clear    (north_clear),
       .car_detected  (north_waiting),
       .red           (north_red),
       .yellow        (north_yellow),
       .green         (north_green)
  );

  traffic_queue north_queue(
      .clock          (clock),
      .reset          (reset),
      .add_car        (north_traffic),
      .green_light    (north_green),
      .car_present    (north_waiting)
  );
  
  traffic_generator north_traffic_generator(
      .clock          (clock), 
      .reset          (reset), 
      .car            (north_traffic)
  );

endmodule
