
module top;

  parameter LIGHTS = 8;

  logic clock;
  logic reset;

  // tbx clkgen
  initial begin
    clock = 0;
    forever #1
      clock = ~clock;
  end

  // tbx clkgen
  initial begin
     reset = 1;
     #20 reset = 0;
     #2000000 $finish();
  end

  logic  [LIGHTS-1:0] clear;
  logic  [LIGHTS-1:0] waiting;
  logic  [LIGHTS-1:0] traffic;
  logic  [LIGHTS-1:0] red;
  logic  [LIGHTS-1:0] yellow;
  logic  [LIGHTS-1:0] green;

  two_roads tr(
     .clock (clock), 
     .reset (reset),
     .red   (red),
     .yellow (yellow),
     .green  (green),
     .clear  (clear),
     .traffic (traffic),
     .waiting (waiting)
  );

endmodule

