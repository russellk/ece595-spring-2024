
// Null peripheral, used as a placeholder when no accelerator is in the design
// should not be accessed, but will not cause issues if it is

module cat_accel
    (
        clock,
        resetn,
        read_addr,
        read_data,
        oe,
        write_addr,
        write_data,
        be,
        we
    );

    parameter address_width     = 22;
    parameter data_width        = 2; //   in 2^data_width bytes
                                     //   0 = 8 bits  1 = 16 bits  2 = 32 bits  3 = 64 bits
                                     //   memory is always byte addressible

    input                                clock;
    input                                resetn;
    input [address_width-1:0]            read_addr;
    output [((1<<data_width)*8)-1:0]     read_data;
    input                                oe;
    input [address_width-1:0]            write_addr;
    input [((1<<data_width)*8)-1:0]      write_data;
    input [(1<<data_width)-1:0]          be;
    input                                we;
    
    assign read_data = 32'h0000DEAD;

    always @(posedge clock) begin
        if (oe || (we && be)) begin
           $display("Null peripheral accessed, did you mean to do that? ");
        end
    end

endmodule
