
`include "axi_bus_defines.svh"

`define DONE_ADDR 10'h3FC
`define DONE_DATA 32'h06070BED

`define mmasters 1
`define mslaves  1

module tbench();

    logic clk = 1'b0;
    logic rst;

    wire [`id_bits-1:0]         M_AW_ID[`mmasters-1:0];
    wire [`addr_bits-1:0]       M_AW_ADDR[`mmasters-1:0];
    wire [`len_bits-1:0]        M_AW_LEN[`mmasters-1:0];
    wire [`size_bits-1:0]       M_AW_SIZE[`mmasters-1:0];
    wire [`burst_bits-1:0]      M_AW_BURST[`mmasters-1:0];
    wire [`lock_bits-1:0]       M_AW_LOCK[`mmasters-1:0];
    wire [`cache_bits-1:0]      M_AW_CACHE[`mmasters-1:0];
    wire [`prot_bits-1:0]       M_AW_PROT[`mmasters-1:0];
    wire [`qos_bits-1:0]        M_AW_QOS[`mmasters-1:0];
    wire                        M_AW_VALID[`mmasters-1:0];
    wire                        M_AW_READY[`mmasters-1:0];

    wire [`id_bits-1:0]         M_W_ID[`mmasters-1:0];
    wire [`data_bits-1:0]       M_W_DATA[`mmasters-1:0];
    wire [`strb_bits-1:0]       M_W_STRB[`mmasters-1:0];
    wire                        M_W_LAST[`mmasters-1:0];
    wire                        M_W_VALID[`mmasters-1:0];
    wire                        M_W_READY[`mmasters-1:0];

    wire [`id_bits-1:0]         M_B_ID[`mmasters-1:0];
    wire [`resp_bits-1:0]       M_B_RESP[`mmasters-1:0];
    wire                        M_B_VALID[`mmasters-1:0];
    wire                        M_B_READY[`mmasters-1:0];

    wire [`id_bits-1:0]         M_AR_ID[`mmasters-1:0];
    wire [`addr_bits-1:0]       M_AR_ADDR[`mmasters-1:0];
    wire [`len_bits-1:0]        M_AR_LEN[`mmasters-1:0];
    wire [`size_bits-1:0]       M_AR_SIZE[`mmasters-1:0];
    wire [`burst_bits-1:0]      M_AR_BURST[`mmasters-1:0];
    wire [`lock_bits-1:0]       M_AR_LOCK[`mmasters-1:0];
    wire [`cache_bits-1:0]      M_AR_CACHE[`mmasters-1:0];
    wire [`prot_bits-1:0]       M_AR_PROT[`mmasters-1:0];
    wire [`qos_bits-1:0]        M_AR_QOS[`mmasters-1:0];
    wire                        M_AR_VALID[`mmasters-1:0];
    wire                        M_AR_READY[`mmasters-1:0];

    wire [`id_bits-1:0]         M_R_ID[`mmasters-1:0];
    wire [`data_bits-1:0]       M_R_DATA[`mmasters-1:0];
    wire [`resp_bits-1:0]       M_R_RESP[`mmasters-1:0];
    wire                        M_R_LAST[`mmasters-1:0];
    wire                        M_R_VALID[`mmasters-1:0];
    wire                        M_R_READY[`mmasters-1:0];

    wire [`mmasters-1:0]        MS_AW_MASTER[`mslaves-1:0];
    wire [`id_bits-1:0]         MS_AW_ID[`mslaves-1:0];
    wire [`addr_bits-1:0]       MS_AW_ADDR[`mslaves-1:0];
    wire [`len_bits-1:0]        MS_AW_LEN[`mslaves-1:0];
    wire [`size_bits-1:0]       MS_AW_SIZE[`mslaves-1:0];
    wire [`burst_bits-1:0]      MS_AW_BURST[`mslaves-1:0];
    wire [`lock_bits-1:0]       MS_AW_LOCK[`mslaves-1:0];
    wire [`cache_bits-1:0]      MS_AW_CACHE[`mslaves-1:0];
    wire [`prot_bits-1:0]       MS_AW_PROT[`mslaves-1:0];
    wire [`qos_bits-1:0]        MS_AW_QOS[`mslaves-1:0];
    wire                        MS_AW_VALID[`mslaves-1:0];
    wire                        MS_AW_READY[`mslaves-1:0];

    wire [`mmasters-1:0]        MS_W_MASTER[`mslaves-1:0];
    wire [`id_bits-1:0]         MS_W_ID[`mslaves-1:0];
    wire [`data_bits-1:0]       MS_W_DATA[`mslaves-1:0];
    wire [`strb_bits-1:0]       MS_W_STRB[`mslaves-1:0];
    wire                        MS_W_LAST[`mslaves-1:0];
    wire                        MS_W_VALID[`mslaves-1:0];
    wire                        MS_W_READY[`mslaves-1:0];

    wire [`mmasters-1:0]        MS_B_MASTER[`mslaves-1:0];
    wire [`id_bits-1:0]         MS_B_ID[`mslaves-1:0];
    wire [`resp_bits-1:0]       MS_B_RESP[`mslaves-1:0];
    wire                        MS_B_VALID[`mslaves-1:0];
    wire                        MS_B_READY[`mslaves-1:0];

    wire [`mmasters-1:0]        MS_AR_MASTER[`mslaves-1:0];
    wire [`id_bits-1:0]         MS_AR_ID[`mslaves-1:0];
    wire [`addr_bits-1:0]       MS_AR_ADDR[`mslaves-1:0];
    wire [`len_bits-1:0]        MS_AR_LEN[`mslaves-1:0];
    wire [`size_bits-1:0]       MS_AR_SIZE[`mslaves-1:0];
    wire [`burst_bits-1:0]      MS_AR_BURST[`mslaves-1:0];
    wire [`lock_bits-1:0]       MS_AR_LOCK[`mslaves-1:0];
    wire [`cache_bits-1:0]      MS_AR_CACHE[`mslaves-1:0];
    wire [`prot_bits-1:0]       MS_AR_PROT[`mslaves-1:0];
    wire [`qos_bits-1:0]        MS_AR_QOS[`mslaves-1:0];
    wire                        MS_AR_VALID[`mslaves-1:0];
    wire                        MS_AR_READY[`mslaves-1:0];

    wire [`mmasters-1:0]        MS_R_MASTER[`mslaves-1:0];
    wire [`id_bits-1:0]         MS_R_ID[`mslaves-1:0];
    wire [`data_bits-1:0]       MS_R_DATA[`mslaves-1:0];
    wire [`resp_bits-1:0]       MS_R_RESP[`mslaves-1:0];
    wire                        MS_R_LAST[`mslaves-1:0];
    wire                        MS_R_VALID[`mslaves-1:0];
    wire                        MS_R_READY[`mslaves-1:0];

    wire [20:0]                 SRAM_READ_ADDR;
    wire [63:0]                 SRAM_READ_DATA;
    wire                        SRAM_OE;
    wire [20:0]                 SRAM_WRITE_ADDR;
    wire [63:0]                 SRAM_WRITE_DATA;
    wire [7:0]                  SRAM_WRITE_BE;
    wire                        SRAM_WRITE_STROBE;

    always
       #2 clk = ~clk;

    initial begin
       rst = 1'b1;
       #100 rst = 1'b0;
    end


    top t0(
      .clock (clk),
      .reset (rst),

      .M_AW_ID       (M_AW_ID[0]),
      .M_AW_ADDR     (M_AW_ADDR[0]),
      .M_AW_LEN      (M_AW_LEN[0]),
      .M_AW_SIZE     (M_AW_SIZE[0]),
      .M_AW_BURST    (M_AW_BURST[0]),
      .M_AW_LOCK     (M_AW_LOCK[0]),
      .M_AW_CACHE    (M_AW_CACHE[0]),
      .M_AW_PROT     (M_AW_PROT[0]),
      .M_AW_QOS      (M_AW_QOS[0]),
      .M_AW_VALID    (M_AW_VALID[0]),
      .M_AW_READY    (M_AW_READY[0]),

      .M_W_ID        (M_W_ID[0]),
      .M_W_DATA      (M_W_DATA[0]),
      .M_W_STRB      (M_W_STRB[0]),
      .M_W_LAST      (M_W_LAST[0]),
      .M_W_VALID     (M_W_VALID[0]),
      .M_W_READY     (M_W_READY[0]),

      .M_B_ID        (M_B_ID[0]),
      .M_B_RESP      (M_B_RESP[0]),
      .M_B_VALID     (M_B_VALID[0]),
      .M_B_READY     (M_B_READY[0]),

      .M_AR_ID       (M_AR_ID[0]),
      .M_AR_ADDR     (M_AR_ADDR[0]),
      .M_AR_LEN      (M_AR_LEN[0]),
      .M_AR_SIZE     (M_AR_SIZE[0]),
      .M_AR_BURST    (M_AR_BURST[0]),
      .M_AR_LOCK     (M_AR_LOCK[0]),
      .M_AR_CACHE    (M_AR_CACHE[0]),
      .M_AR_PROT     (M_AR_PROT[0]),
      .M_AR_QOS      (M_AR_QOS[0]),
      .M_AR_VALID    (M_AR_VALID[0]),
      .M_AR_READY    (M_AR_READY[0]),

      .M_R_ID        (M_R_ID[0]),
      .M_R_DATA      (M_R_DATA[0]),
      .M_R_RESP      (M_R_RESP[0]),
      .M_R_LAST      (M_R_LAST[0]),
      .M_R_VALID     (M_R_VALID[0]),
      .M_R_READY     (M_R_READY[0])
    );

    axi_matrix #(`mmasters, `mslaves, 1, 1) mem_complex (

        .ACLK        (clk),
        .ARESETn     (!rst),

        .AWID        (M_AW_ID),
        .AWADDR      (M_AW_ADDR),
        .AWLEN       (M_AW_LEN),
        .AWSIZE      (M_AW_SIZE),
        .AWBURST     (M_AW_BURST),
        .AWLOCK      (M_AW_LOCK),
        .AWCACHE     (M_AW_CACHE),
        .AWPROT      (M_AW_PROT),
        .AWQOS       (M_AW_QOS),
        .AWVALID     (M_AW_VALID),
        .AWREADY     (M_AW_READY),

        .WDATA       (M_W_DATA),
        .WSTRB       (M_W_STRB),
        .WLAST       (M_W_LAST),
        .WVALID      (M_W_VALID),
        .WREADY      (M_W_READY),

        .BID         (M_B_ID),
        .BRESP       (M_B_RESP),
        .BVALID      (M_B_VALID),
        .BREADY      (M_B_READY),

        .ARID        (M_AR_ID),
        .ARADDR      (M_AR_ADDR),
        .ARLEN       (M_AR_LEN),
        .ARSIZE      (M_AR_SIZE),
        .ARBURST     (M_AR_BURST),
        .ARLOCK      (M_AR_LOCK),
        .ARCACHE     (M_AR_CACHE),
        .ARPROT      (M_AR_PROT),
        .ARQOS       (M_AR_QOS),
        .ARVALID     (M_AR_VALID),
        .ARREADY     (M_AR_READY),

        .RID         (M_R_ID),
        .RDATA       (M_R_DATA),
        .RRESP       (M_R_RESP),
        .RLAST       (M_R_LAST),
        .RVALID      (M_R_VALID),
        .RREADY      (M_R_READY),

        .S_AWMASTER  (MS_AW_MASTER),
        .S_AWID      (MS_AW_ID),
        .S_AWADDR    (MS_AW_ADDR),
        .S_AWLEN     (MS_AW_LEN),
        .S_AWSIZE    (MS_AW_SIZE),
        .S_AWBURST   (MS_AW_BURST),
        .S_AWLOCK    (MS_AW_LOCK),
        .S_AWCACHE   (MS_AW_CACHE),
        .S_AWPROT    (MS_AW_PROT),
        .S_AWQOS     (MS_AW_QOS),
        .S_AWVALID   (MS_AW_VALID),
        .S_AWREADY   (MS_AW_READY),

        .S_WMASTER   (MS_W_MASTER),
        .S_WID       (MS_W_ID),
        .S_WDATA     (MS_W_DATA),
        .S_WSTRB     (MS_W_STRB),
        .S_WLAST     (MS_W_LAST),
        .S_WVALID    (MS_W_VALID),
        .S_WREADY    (MS_W_READY),

        .S_BMASTER   (MS_B_MASTER),
        .S_BID       (MS_B_ID),
        .S_BRESP     (MS_B_RESP),
        .S_BVALID    (MS_B_VALID),
        .S_BREADY    (MS_B_READY),

        .S_ARMASTER  (MS_AR_MASTER),
        .S_ARID      (MS_AR_ID),
        .S_ARADDR    (MS_AR_ADDR),
        .S_ARLEN     (MS_AR_LEN),
        .S_ARSIZE    (MS_AR_SIZE),
        .S_ARBURST   (MS_AR_BURST),
        .S_ARLOCK    (MS_AR_LOCK),
        .S_ARCACHE   (MS_AR_CACHE),
        .S_ARPROT    (MS_AR_PROT),
        .S_ARQOS     (MS_AR_QOS),
        .S_ARVALID   (MS_AR_VALID),
        .S_ARREADY   (MS_AR_READY),

        .S_RMASTER   (MS_R_MASTER),
        .S_RID       (MS_R_ID),
        .S_RDATA     (MS_R_DATA),
        .S_RRESP     (MS_R_RESP),
        .S_RLAST     (MS_R_LAST),
        .S_RVALID    (MS_R_VALID),
        .S_RREADY    (MS_R_READY)

      );

      axi_slave_interface
        #(
        .masters   (`mmasters),
        .width     (24),
        .id_bits   (`id_bits),
        .p_size    (3),
        .b_size    (3))

        mem_if (

        .ACLK      (clk),
        .ARESETn   (!rst),

        .AWMASTER  (MS_AW_MASTER[0]),
        .AWID      (MS_AW_ID[0]),
        .AWADDR    (MS_AW_ADDR[0][23:0]),
        .AWLEN     (MS_AW_LEN[0]),
        .AWSIZE    (MS_AW_SIZE[0]),
        .AWBURST   (MS_AW_BURST[0]),
        .AWLOCK    (MS_AW_LOCK[0]),
        .AWCACHE   (MS_AW_CACHE[0]),
        .AWPROT    (MS_AW_PROT[0]),
        .AWVALID   (MS_AW_VALID[0]),
        .AWREADY   (MS_AW_READY[0]),

        .WMASTER   (MS_W_MASTER[0]),
        .WID       (MS_W_ID[0]),
        .WDATA     (MS_W_DATA[0]),
        .WSTRB     (MS_W_STRB[0]),
        .WLAST     (MS_W_LAST[0]),
        .WVALID    (MS_W_VALID[0]),
        .WREADY    (MS_W_READY[0]),

        .BMASTER   (MS_B_MASTER[0]),
        .BID       (MS_B_ID[0]),
        .BRESP     (MS_B_RESP[0]),
        .BVALID    (MS_B_VALID[0]),
        .BREADY    (MS_B_READY[0]),

        .ARMASTER  (MS_AR_MASTER[0]),
        .ARID      (MS_AR_ID[0]),
        .ARADDR    (MS_AR_ADDR[0][23:0]),
        .ARLEN     (MS_AR_LEN[0]),
        .ARSIZE    (MS_AR_SIZE[0]),
        .ARBURST   (MS_AR_BURST[0]),
        .ARLOCK    (MS_AR_LOCK[0]),
        .ARCACHE   (MS_AR_CACHE[0]),
        .ARPROT    (MS_AR_PROT[0]),
        .ARVALID   (MS_AR_VALID[0]),
        .ARREADY   (MS_AR_READY[0]),

        .RMASTER   (MS_R_MASTER[0]),
        .RID       (MS_R_ID[0]),
        .RDATA     (MS_R_DATA[0]),
        .RRESP     (MS_R_RESP[0]),
        .RLAST     (MS_R_LAST[0]),
        .RVALID    (MS_R_VALID[0]),
        .RREADY    (MS_R_READY[0]),

        .SRAM_READ_ADDRESS       (SRAM_READ_ADDR),
        .SRAM_READ_DATA          (SRAM_READ_DATA),
        .SRAM_OUTPUT_ENABLE      (SRAM_OE),

        .SRAM_WRITE_ADDRESS      (SRAM_WRITE_ADDR),
        .SRAM_WRITE_DATA         (SRAM_WRITE_DATA),
        .SRAM_WRITE_BYTE_ENABLE  (SRAM_WRITE_BE),
        .SRAM_WRITE_STROBE       (SRAM_WRITE_STROBE)
    );

    sram #(.address_width (24), .data_width (3)) code_mem (
        .CLK                     (clk),
        .RSTn                    (!rst),

        .READ_ADDR               (SRAM_READ_ADDR),
        .DATA_OUT                (SRAM_READ_DATA),
        .OE                      (SRAM_OE),

        .WRITE_ADDR              (SRAM_WRITE_ADDR),
        .DATA_IN                 (SRAM_WRITE_DATA),
        .WE                      (SRAM_WRITE_STROBE),
        .BE                      (SRAM_WRITE_BE)
   );


    always @(posedge clk) begin
      if ((tbench.t0.tty0.WE              == 1'b1) &&
          (tbench.t0.tty0.WRITE_ADDR      == `DONE_ADDR) &&
          (tbench.t0.tty0.DATA_IN         == `DONE_DATA))
           $finish();
    end

endmodule

